#!/usr/bin/python


"""Update the OS ABI number of known death test NaCl modules.

Driver code to update death test nexe files.  The user is expected to
run this on a Linux or OSX system (due to file paths) in the
native_client directory.

"""

import re
import sys

import set_abi_version


NACL_DEATH_TESTS = [
    'service_runtime/testdata/integer_overflow_while_madvising.nexe',
    'service_runtime/testdata/negative_hole.nexe',
    'service_runtime/testdata/text_too_big.nexe',
    ]


def GetCurrentAbiVersion():
  return int(re.search(r'^#define.*EF_NACL_ABIVERSION\s+(\d+)',
                       open('include/nacl_elf.h', 'rt').read(),
                       re.M).group(1))


def main():
  abi = GetCurrentAbiVersion()
  for f in NACL_DEATH_TESTS:
    print 'Updating: ', f
    try:
      set_abi_version.ModifyFileOsAbiVersion(f, abi)
    except IOError, e:
      print 'FAILED: ', str(e)
      return 1
    # endtry
  # endfor
# enddef

if __name__ == '__main__':
  sys.exit(main())
# endif

#!/usr/bin/python
# Copyright 2008, Google Inc.
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#     * Neither the name of Google Inc. nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#
# Note: If you change this script, make sure the wiki page is up to date:
# http://code.google.com/p/nativeclient/wiki/FilesInstalled


"""Firefox installer for the NaCl developer release

A simple script for a NaCl developer to install the Firefox plugin
on a machine after a NaCl build from source. To be executed from the
root of the NaCl source tree.

"""

import os
import platform
import shutil
import stat
import struct
import sys


# Is this script embedded in an installer?
# Set with a command line option (e.g. PKGINSTALL).
EMBEDDED = False

def Usage(progname):
  print("""
USAGE:
  %s PLATFORM_BASE=<platform_base>

You don't usually invoke this script directly.
Instead, you use SCons, with the following syntax:
  ./scons [--prebuilt] firefox_install [DBG=1]
where:
 --prebuilt means don't attempt to rebuild the plugin binaries
 DBG=1 means install the debug version of the plugin
For example:
  %s PLATFORM_BASE=.../native_client/scons-out/
is the same as
  ./scons  --prebuilt firefox_install

Note: by default the scons command will install the release version
of the plugin. In order to install the debug version, use:
  ./scons --prebuilt firefox_install DBG=1
which is the same as
  %s PLATFORM_BASE=.../native_client/scons-out/ MODE=1

In this case scons will take care of the command line args.
""" % (progname, progname, progname))


def FatalError(msg):
  sys.stderr.write(msg)
  sys.stderr.write("\n**** INSTALLATION FAILED.\n\n")
  sys.exit(-1)

def CheckCondition(boolcond, emsg):
  if boolcond: return
  FatalError("Error:" + emsg + "\n")

def CheckFileExists(what, emsg):
  if os.path.isfile(what): return
  FatalError("Error: \"%s\" not found.\n%s\n" % (what, emsg))

def CheckDirExists(what, emsg):
  if os.path.isdir(what): return
  FatalError("Error: directory \"%s\" not found.\n%s\n" % (what, emsg))

def OkayToInstall():
  # Can't be interactive if embedded.
  if EMBEDDED:
    return True
  sys.stdout.write("Okay to continue? [y/n] ")
  answer = sys.stdin.readline()
  if answer[0] == "y":
    print "Okay, you asked for it."
    return True
  else:
    print "Okay, I'm bailing out then."
    return False

def CopyFile(src, dst):
  try:
    print "copying", src, "to", dst, "..."
    if os.path.isfile(dst): os.unlink(dst)
    shutil.copy2(src, dst)
  except OSError, err:
    FatalError("ERROR: Could not copy %s to %s\n" % (src, dst))

def CopyDir(src, dst):
  try:
    print "copying directory", src, "to", dst, "..."
    shutil.copytree(src, dst)
  except OSError, err:
    FatalError("ERROR: Could not copy %s to %s: %s\n" % (src, dst, err))

def CopyFilesOrDirs(src_dir, dst_dir, list):
  for f in list:
    name = os.path.join(src_dir, f)
    if os.path.isfile(name):
      CopyFile(name, dst_dir)
    if os.path.isdir(name):
      CopyDir(name, dst_dir)

def RemoveDir(dir):
  try:
    print "[Removing", dir, "...]"
    shutil.rmtree(dir)
  except OSError, err:
    FatalError("ERROR: Could not remove %s\n" % (dir))

def TryMakedir(dirname):
  if not os.path.isdir(dirname):
    try:
      print "[Creating", dirname, "...]"
      os.makedirs(dirname)
    except OSError, err:
      FatalError("ERROR: Could not create %s\n" % dirname)

def RemoveFile(fname):
  try:
    print "[Removing", fname, "...]"
    os.unlink(fname)
  except OSError, err:
    FatalError("ERROR: Could not remove %s\n" % fname)

def RemoveFilesOrDirs(dir, list):
  for f in list:
    name = os.path.join(dir, f)
    if os.path.isfile(name):
      RemoveFile(name)
    if os.path.isdir(name):
      RemoveDir(name)

def GetFromStaging(PLATFORM_BASE, what):
  # Try local version in case we're an installer and all is "right here!"
  if EMBEDDED:
    localversion = os.path.join(PLATFORM_BASE, what)
    if os.path.exists(localversion):
      return localversion
  return os.path.join(PLATFORM_BASE, "staging", what)

def PrintStars():
  print "*********************************************************************"

def GetPluginDir():
  pl = sys.platform
  if pl in ['linux', 'linux2']:
    return os.path.expandvars("$HOME/.mozilla/plugins")
  if pl in ['darwin', 'mac']:
    return os.path.expanduser(os.path.join("~", "Library", "Internet Plug-Ins"))
  elif pl in ['win32', 'cygwin', 'win', 'windows']:
    return os.path.join(os.getenv('PROGRAMFILES'), "Mozilla Firefox", "plugins")
  else:
    FatalError("Unknown platform %s" % pl)

def GetFilesToCopy():
  pl = sys.platform
  if pl in ['linux', 'linux2']:
    return ["libnpGoogleNaClPlugin.so", "sel_ldr", "sel_ldr_bin", "sel_mon", "sel_ldr_bin.trace"]
  if pl in ['darwin', 'mac']:
    return ["npGoogleNaClPlugin.bundle"]
  elif pl in ['win32', 'cygwin', 'win', 'windows']:
    return ["npGoogleNaClPlugin.dll", "sel_ldr.exe"]
  else:
    FatalError("Unknown platform %s" % pl)

def InstallSuccess(sel_ldr):
  PrintStars()
  print "* You have successfully installed the NaCl Firefox plugin."
  print "* As a self-test, please confirm you can run"
  print "*    ", sel_ldr
  print "* from a shell/command prompt. With no args you should see"
  print "*     No nacl file specified"
  print "* on Linux or Mac and no output on Windows."
  PrintStars()

def RunningOn64BitSystem():
  if (8 == len(struct.pack('P',0))):
    return True
  return False

def Linux_install(PLATFORM_BASE):
  PLUGINDIR = GetPluginDir()
  PLUGIN = os.path.join(PLATFORM_BASE, "staging", "libnpGoogleNaClPlugin.so")
  SEL_LDR = GetFromStaging(PLATFORM_BASE, "sel_ldr")
  SEL_LDR_SANDBOX = GetFromStaging(PLATFORM_BASE, "sel_ldr.trace")
  SEL_LDR_SH = os.path.join(PLATFORM_BASE, "../../tools/sel_ldr.bash")
  LIBSDL = GetFromStaging(PLATFORM_BASE, "libSDL-1.2.so.0")
  SEL_LDR_INSTALLED_AS = os.path.join(PLUGINDIR, "sel_ldr_bin")
  SEL_LDR_SH_INSTALLED_AS = os.path.join(PLUGINDIR, "sel_ldr")
  SEL_LDR_SANDBOX_INSTALLED_AS = os.path.join(PLUGINDIR, "sel_ldr_bin.trace")
  SDL_INSTALLED_AS = os.path.join(PLUGINDIR, "libSDL-1.2.so.0")
  SEL_MON = GetFromStaging(PLATFORM_BASE, "sel_mon")

  # Check that all source and destination files/directories exist
  CheckFileExists(SEL_LDR, "Have you built NaCl yet?")
  CheckFileExists(PLUGIN, "Have you built NaCl yet?")
  CheckFileExists(LIBSDL, "Have you built NaCl yet?")
  CheckFileExists("/bin/bash", "No /bin/bash? NaCl needs this to launch sel_ldr")

  # Tell the user what we're going to do
  print "This script will install"
  print PLUGIN, ",", LIBSDL, "and", SEL_LDR, "in", PLUGINDIR
  if not OkayToInstall():
    sys.exit(0)

  # do the deed...
  TryMakedir(PLUGINDIR)
  CopyFile(PLUGIN, PLUGINDIR)
  if RunningOn64BitSystem():
    # Call the wrapper on 64-bit Linux
    print '64bit system detected running nspluginwrapper'
    cmd = 'nspluginwrapper -i %s' % os.path.join(PLUGINDIR,
                                                 os.path.basename(PLUGIN))
    print cmd
    os.system(cmd)
  CopyFile(SEL_MON, PLUGINDIR)
  # avoid installing libSDL twice
  if not os.path.isfile(SDL_INSTALLED_AS):
    CopyFile(LIBSDL, SDL_INSTALLED_AS)
  CopyFile(SEL_LDR, SEL_LDR_INSTALLED_AS)
  CopyFile(SEL_LDR_SH, SEL_LDR_SH_INSTALLED_AS)
  CopyFile(SEL_LDR_SANDBOX, SEL_LDR_SANDBOX_INSTALLED_AS)
  os.chmod(SEL_LDR_SH_INSTALLED_AS, stat.S_IRUSR | stat.S_IXUSR | stat.S_IWUSR)
  InstallSuccess(os.path.join(PLUGINDIR, "sel_ldr"))

def Windows_install(PLATFORM_BASE):
  PLUGINDIR = GetPluginDir()
  SEL_LDR = GetFromStaging(PLATFORM_BASE, "sel_ldr.exe")
  SDL_DLL = GetFromStaging(PLATFORM_BASE,  "SDL.dll")
  PLUGIN = os.path.join(PLATFORM_BASE, "staging", "npGoogleNaClPlugin.dll")

  # Check that all source and destination files/directories exist
  CheckFileExists(SEL_LDR, "Have you built NaCl yet?")
  CheckFileExists(SDL_DLL, "Have you built NaCl yet?")
  CheckFileExists(PLUGIN, "Have you built NaCl yet?")
  CheckDirExists(PLUGINDIR, "I don't know where to install browser plugins.")

  # Tell the user what we're going to do
  print "This script will install"
  print PLUGIN, ",", SEL_LDR, "and", SDL_DLL
  print "in", PLUGINDIR
  if not OkayToInstall():
    sys.exit(0)

  # do the deed...
  TryMakedir(PLUGINDIR)
  CopyFile(SEL_LDR, PLUGINDIR)
  CopyFile(SDL_DLL, PLUGINDIR)
  CopyFile(PLUGIN, PLUGINDIR)
  InstallSuccess(os.path.join(PLUGINDIR, "sel_ldr.exe"))

def MacNeedsSDL(SDLDEST):
  SDLPath = None
  if os.path.isdir("/Library/Frameworks/SDL.framework"):
    SDLPath = "/Library/Frameworks/SDL.framework"
  elif os.path.isdir(SDLDEST):
    SDLPath = SDLDEST
  else:
    return True

  print "*"
  print "* It looks like SDL is already installed as"
  print "*   ", SDLPath
  print "* For Native Client we recommend SDL Version 1.2.13"
  print "* Please make sure your SDL version is compatible"
  print "* with Native Client."
  print "*"
  return False


def Mac_install(PLATFORM_BASE):
  PLUGINDIR = GetPluginDir()
  BUNDLEDIR = os.path.join(PLUGINDIR, "npGoogleNaClPlugin.bundle")
  SEL_LDR = GetFromStaging(PLATFORM_BASE, "sel_ldr")

  # Local versions in case we're running in an installer (not from the
  # source tree) and everything is in one spot
  BUNDLE = None
  if EMBEDDED:
    localversion = os.path.join(PLATFORM_BASE, "npGoogleNaClPlugin.bundle")
    if os.path.exists(localversion):
      BUNDLE = localversion
  if not BUNDLE:
    BUNDLE = os.path.join(PLATFORM_BASE, "staging", "npGoogleNaClPlugin.bundle")

  SDLFRAMEWORK = None
  if EMBEDDED:
    localversion = os.path.join(PLATFORM_BASE, "SDL.framework")
    if os.path.exists(localversion):
      SDLFRAMEWORK = localversion
  if not SDLFRAMEWORK:
    SDLFRAMEWORK = os.path.join(PLATFORM_BASE, "Frameworks", "SDL.framework")
  FRAMEWORKS = os.path.expanduser(os.path.join("~", "Library", "Frameworks"))
  SDLDEST = os.path.join(FRAMEWORKS, "SDL.framework")
  SEL_LDR_DIR = os.path.join(BUNDLEDIR, "Contents", "Resources")

  # Check that all source and destination files/directories exist
  CheckDirExists(BUNDLE, "Have you built NaCl yet?")
  CheckDirExists(SDLFRAMEWORK, "Have you built NaCl yet?")
  CheckFileExists(os.path.join(BUNDLE,
                               "Contents", "MacOS", "npGoogleNaClPlugin"),
                  "Have you built NaCl yet?")
  CheckFileExists(SEL_LDR, "Have you built NaCl yet?")
  TryMakedir(PLUGINDIR)

  # Tell the user what we're going to do
  print "This script will install:"
  print "  ", BUNDLE, "in", BUNDLEDIR
  print "  ", SEL_LDR, "in", SEL_LDR_DIR
  print "and"
  print SDLFRAMEWORK, "in", SDLDEST
  if not OkayToInstall():
    sys.exit(0)

  # do the deed...
  if os.path.isdir(BUNDLEDIR):
    RemoveDir(BUNDLEDIR)

  CopyDir(BUNDLE, BUNDLEDIR)
  if MacNeedsSDL(SDLDEST):
    if not os.path.isdir(FRAMEWORKS):
      os.makedirs(FRAMEWORKS)
    CopyDir(SDLFRAMEWORK, SDLDEST)
  CopyFile(SEL_LDR, SEL_LDR_DIR)
  InstallSuccess(os.path.join(SEL_LDR_DIR, "sel_ldr"))

def ParseArgv(argv):
  args = {}
  for arg in argv[1:]:
    if (arg.find("=") < 0):
      Usage(argv[0])
    name, value = arg.split("=")
    print("%s=%s" % (name, value))
    args[name] = value
  # require PLATFORM_BASE (or PKGINSTALL) to be set
  if ("PLATFORM_BASE" not in args and "PKGINSTALL" not in args
      and "BACKUP" not in args and "RESTORE" not in args):
    return None
  if "PKGINSTALL" in args:
    global EMBEDDED
    EMBEDDED = True
  if "MODE" not in args: args["MODE"] = '0'
  if "BACKUP" not in args: args["BACKUP"] = '0'
  if "RESTORE" not in args: args["RESTORE"] = '0'
  return args

def GetTmpDirectory():
  pl = sys.platform
  if pl in ['linux', 'linux2', 'darwin', 'mac']:
    tmp_dir = '/tmp/nacl'
  elif pl in ['win32', 'cygwin', 'win', 'windows']:
    os_tmp = os.getenv('TMP')
    tmp_dir = os.path.join(os_tmp, 'nacl')
  else:
    FatalError("Backup/Restore cannot continue. Unknown platform %s" % pl)
  return tmp_dir

def Backup():
  tmp_dir = GetTmpDirectory()
  plugin_dir = GetPluginDir()
  files_to_copy = GetFilesToCopy()

  if os.path.isdir(tmp_dir):
    RemoveFilesOrDirs(tmp_dir, files_to_copy)
  else:
    # If for some reason there is a file with the same name.
    if os.path.isfile(tmp_dir):
      RemoveFile(tmp_dir)
    TryMakedir(tmp_dir)

  if os.path.exists(plugin_dir):
    CopyFilesOrDirs(plugin_dir, tmp_dir, files_to_copy)

def Restore():
  tmp_dir = GetTmpDirectory()
  plugin_dir = GetPluginDir()
  files_to_copy = GetFilesToCopy()

  if os.path.exists(plugin_dir):
    RemoveFilesOrDirs(plugin_dir, files_to_copy)

  if os.path.exists(tmp_dir):
    CopyFilesOrDirs(tmp_dir, plugin_dir, files_to_copy)
    RemoveFilesOrDirs(tmp_dir, files_to_copy)

def main(argv):
  args = ParseArgv(argv)
  if args is None:
    Usage(argv[0])
    return -1

  if args["BACKUP"] == '1':
    Backup()
    return 0

  if args["RESTORE"] == '1':
    Restore()
    return 0

  if args["MODE"] == '1':
    mode = 'dbg-'
  else:
    mode = 'opt-'

  pl = sys.platform
  if pl in ['linux', 'linux2']:
    Linux_install(args["PLATFORM_BASE"] + mode + 'linux')
  elif pl in ['win32', 'cygwin', 'win', 'windows']:
    Windows_install(args["PLATFORM_BASE"] + mode + 'win')
  elif pl in ['darwin', 'mac']:
    if os.uname()[-1] != 'i386':
      FatalError('Cannot install on PPC Macs.')
    if "PKGINSTALL" in args:
      Mac_install(args["PKGINSTALL"])
    else:
      Mac_install(args["PLATFORM_BASE"] + mode + 'mac')
  else:
    print "Install cannot continue. Unknown platform %s" % pl
    Usage(argv[0])
    return -1

  PLUGINDEMO = os.path.join("scons-out", "nacl", "staging", "index.html")
  print "* To test this installation also try the test links on the page"
  print "*    ", PLUGINDEMO
  PrintStars()
  return 0

if '__main__' ==  __name__:
  sys.exit(main(sys.argv))

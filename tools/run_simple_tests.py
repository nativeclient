#!/usr/bin/python
# Copyright 2008, Google Inc.
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#     * Neither the name of Google Inc. nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


"""This script runs the tests listed in the input file.

This script runs the tests listed in the input file
and reports the results in jUnit XML format (written to the
output file). All tests that return 0 are considered to be successful.

  main(): The only function, runs the tests
"""

# python imports
import os
import sys
import xml.dom.minidom

# local imports
import test_lib


def main(argv):
  # make sure WindowsError is defined
  try:
    WindowsError
  except NameError:
    class WindowsError: pass

  input_file = os.path.join(os.getcwd(), argv[1])
  output_file = os.path.join(os.getcwd(),argv[2])
  doc = xml.dom.minidom.Document()

  listfile = open(input_file,'rU')
  try:
    os.remove(output_file)
  except (WindowsError, OSError):
    pass
  xmlfile = open(output_file,'w')

  errorcount = 0
  failcount = 0
  testcount = 0
  timecount = 0

  suite = doc.createElement("testsuite")
  suite.setAttribute("name", "SimpleTests")
  suite.setAttribute("disabled", "0")

  doc.appendChild(suite)

  for line in listfile:
    if len(line) < 3:
      # in case we have empty lines in the end of the file
      break
    cmdline = line.split()
    cmd=cmdline[0]

    test=doc.createElement("testcase")
    test.setAttribute("name", str(cmd))

    path = os.path.join(argv[3], cmd)
    print path

    duration, retcode, failed = test_lib.RunTest([path] + cmdline[1:])

    total=int(duration * 1000)
    test.setAttribute("time", str(total))
    test.setAttribute("status", "run")
    testcount+=1
    timecount+=total

    if retcode:
      failcount+=1
      failure_msg=doc.createElement("failure")
      failure_msg.setAttribute("message", "test "+cmd+" failed")
      test.appendChild(failure_msg)
    elif failed:
      errorcount+=1
      err_msg=doc.createElement("error")
      err_msg.setAttribute("message", "error: " + str(sys.exc_info()[1]))
      test.appendChild(err_msg)

    suite.appendChild(test)

  suite.setAttribute("time", str(timecount))
  suite.setAttribute("tests", str(testcount))
  suite.setAttribute("failures", str(failcount))
  suite.setAttribute("errors", str(errorcount))
  xmlfile.write(doc.toprettyxml())
  xmlfile.close()
  listfile.close()

  return 0

if __name__ == '__main__':
  sys.exit(main(sys.argv))

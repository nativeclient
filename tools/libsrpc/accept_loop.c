/* @@rewrite(insert c-copyright) */

#include <stdlib.h>
#include <unistd.h>
#include "nacl_srpc.h"
#include "nacl_srpc_internal.h"
#include <sys/nacl_syscalls.h>

#define BOUND_SOCKET  3

extern int __srpc_get_fd();

struct worker_state {
  int d;
  int is_privileged;
};

/**
 * Handle a shutdown request from the untrusted command channel.  We should
 * allow user code to override this to do exit processing -- for now,
 * user code will have to use onexit or atexit to do cleanup, which is
 * suboptimal in a multithreaded environment.
 */
static int srpc_shutdown_request(NaClSrpcChannel* channel,
                                 NaClSrpcArg **in_args,
                                 NaClSrpcArg **out_arg) {
  struct worker_state *state =
      (struct worker_state *) channel->server_instance_data;

  if (state->is_privileged) {
    /*
     * do onexit/atexit processing, then exit_group.  really should do
     * something sane wrt threads, but we do not implement thread
     * cancellation (as yet?).
     */
    exit(0);
  }
  return NACL_SRPC_RESULT_BREAK;
}

/**
 * Export the shutdown method on the main rpc service loop.  Use __shutdown
 * to avoid name collisions.
 */
NACL_SRPC_METHOD("__shutdown::", srpc_shutdown_request);

/**
 * Basic SRPC worker thread: run the NaClSrpcServerLoop.
 */
static void *srpc_worker(void *arg) {
  struct worker_state *state = (struct worker_state *) arg;

  NaClSrpcServerLoop(state->d, __kNaClSrpcHandlers, (void*) arg);

  (void) close(state->d);
  if (state->is_privileged) {
    _exit(0);
  }
  free(arg);
  return 0;
}

/**
 * Acceptor loop: accept client connections, and for each, spawn a
 * worker thread that invokes NaClSrpcDefaultServerLoop.
 */
static void *srpc_default_acceptor(void *arg) {
  int       first = (int) arg;
  int       d;

  while (-1 != (d = imc_accept(BOUND_SOCKET))) {
    struct worker_state *state = malloc(sizeof *state);
    pthread_t           worker_tid;

    if (NULL == state) {
      /*
       * shed load; the client can come back later when we have more
       * memory.
       */
      (void) close(d);
      continue;
    }
    state->d = d;
    state->is_privileged = first;
    /* worker thread is responsible for state and d. */
    pthread_create(&worker_tid, NULL, srpc_worker, state);
    first = 0;
  }
  return NULL;
}

static pthread_mutex_t mu = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
static int multimedia_done = 0;

void srpc_multimedia_done() {
  pthread_mutex_lock(&mu);
  multimedia_done = 1;
  pthread_cond_broadcast(&cv);
  pthread_mutex_unlock(&mu);
}

/**
 * Internal SRPC initialization.
 *
 * We check to see if we are running embedded in the browser, and if
 * so, do nacl_multimedia bridge first (which blocks until the plugin
 * provides us with the shared memory handles).  Then, we spawn a
 * thread which is our main accept loop, allowing the main thread to
 * continue to run and perform graphics operations, possibly also
 * getting processing requests from clients via an event queue of some
 * kind.
 *
 * The accept loop spawns worker threads on a per-client-connection
 * basis.  (We could use a thread pool, but do not at this point.)
 * The worker threads just handle RPC requests using
 * NaClSrpcDefaultServerLoop().  The first worker thread is
 * "privileged", in that it is responsible for shutting down the NaCl
 * app, and we expect that this first connection comes from the
 * browser plugin.
 */
void srpc_init() {
  pthread_t       acceptor_tid;
  int             is_embedded;

  is_embedded = (__srpc_get_fd() != -1);
  if (is_embedded) {
    pthread_create(&acceptor_tid, NULL,
                   srpc_default_acceptor, (void *) 1);
    pthread_detach(acceptor_tid);

    pthread_mutex_lock(&mu);
    while (!multimedia_done) {
      pthread_cond_wait(&cv, &mu);
    }

    pthread_mutex_unlock(&mu);
  }
}

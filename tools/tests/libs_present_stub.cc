// Copyright 2009 Google Inc. All Rights Reserved.
// Author: sehr@google.com (David Sehr)

// We want to have calls to various library routines, but do not want
// those calls executed when the test is run.  run_tests is a global boolean
// initialized in this file to defeat optimizations removing the calls.
bool run_tests = false;

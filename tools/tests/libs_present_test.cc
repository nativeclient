// Copyright 2009 Google Inc. All Rights Reserved.
// Author: sehr@google.com (David Sehr)
// This file tests for the presence of libraries and .o files in the SDK
// it does not actually execute any of the library code.


// This list should include all exported header files (directly or indirectly)
// to ensure they were properly included in the SDK.
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <nacl/nacl_av.h>
#include <nacl/nacl_imc.h>
#include <nacl/nacl_npapi.h>
#include <nacl/nacl_srpc.h>


// External boolean (initialized to false) to enable test for linking without
// actually running anything.
extern bool run_tests;

// Dummy variables used to hold return values.
bool bool_value;
double double_value;
pthread_t pthread_t_value;
char* char_ptr_value;
char char_array_value[128];

extern "C" {
  // For npruntime support.
NPClass *GetNPSimpleClass() {
  return NULL;
}
}

static void TestLibsPresent() {
  // This code should invoke one method from each exported library to
  // ensure the library was built correctly.

  // Test that libm is present.
  if (run_tests)
    double_value = sin(0.0);

  // Test that libav is present.
  if (run_tests)
    nacl_multimedia_init(NACL_SUBSYSTEM_VIDEO);

  // Test that libgoogle_nacl_imc is present.
  if (run_tests)
    bool_value = nacl::WouldBlock();

  // Test that libgoogle_nacl_npruntime is present.
  if (run_tests)
    NaClNP_MainLoop(0);

  // Test that libpthread is present.
  if (run_tests)
    pthread_t_value = pthread_self();

  // Test that libsrpc is present.
  if (run_tests)
    char_ptr_value = NaClSrpcErrorString(NACL_SRPC_RESULT_OK);

  // Test that libunimpl is present.
  if (run_tests)
    char_ptr_value = getcwd(char_array_value, sizeof(char_array_value));
}

int main(int argc, char **argv) {
  // EH tests that libsupc++ is present.
  try {
    TestLibsPresent();
  } catch(...) {
    // iotream tests that libstdc++ is present.
    std::cout << "FAIL" << std::endl;
    return 1;
  }
  // printf tests that libc is present.
  printf("PASS\n");
  return 0;
}

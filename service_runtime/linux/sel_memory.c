/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * NaCl Service Runtime memory allocation code
 */
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#define _XOPEN_SOURCE 600 /* get XSI compliant version of strerror_r */
#include <errno.h>
#include <string.h>

#include "native_client/service_runtime/sel_memory.h"
#include "native_client/service_runtime/nacl_config.h"
#include "native_client/service_runtime/nacl_log.h"
#include "native_client/include/nacl_platform.h"

void NaCl_page_free(void     *p,
                    size_t   size)
{
  if (p == 0 || size == 0)
    return;
  (void) munmap(p, size);
}

/*
 * Note that NaCl_page_alloc does not allocate pages that satisify
 * NaClIsAllocPageMultiple.  On linux/osx, the system does not impose
 * any such restrictions, and we only need to enforce the restriction
 * on NaCl app code to ensure that the app code is portable across all
 * host OSes.
 */
int NaCl_page_alloc(void    **p,
                    size_t  size)
{
  void *addr;

  /* Allocating at a fixed, round address aids debugging */
  addr = mmap((void *) 0x10000000,
              size,
              PROT_EXEC | PROT_READ | PROT_WRITE,
              MAP_PRIVATE | MAP_ANONYMOUS,
              -1,
              (off_t) 0);

  if (addr == MAP_FAILED) {
    addr = NULL;
  }

  if (NULL != addr) {
    *p = addr;
  }
  return (addr == NULL) ? -ENOMEM : 0;
}

/*
* This is critical to make the text region non-writable, and the data
* region read/write but no exec.  Of course, some kernels do not
* respect the lack of PROT_EXEC.
*/
int   NaCl_mprotect(void          *addr,
                    size_t        len,
                    int           prot)
{
  int  ret;

  return (ret = mprotect(addr, len, prot)) == -1 ? -errno : ret;
}

int   NaCl_madvise(void           *start,
                   size_t         length,
                   int            advice)
{
  int ret;

  /*
   * MADV_DONTNEED and MADV_NORMAL are needed
   */
  return (ret = madvise(start, length, advice)) == -1 ? -errno : ret;
}

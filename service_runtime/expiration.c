/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * NaCl expiration check
 */

#include "native_client/service_runtime/expiration.h"

int NaClHasExpired() {
#if defined(EXPIRATION_CHECK)
  int year;
  int month;
  int day;
  time_t t;
  struct tm today;
  if (-1 == time(&t)) {
    return 1;
  }
  if (NULL == LOCALTIME_R(&t, &today)) {
    return 1;
  }
  year = today.tm_year + 1900;
  month = today.tm_mon + 1;
  day = today.tm_mday;
  if (year > EXPIRATION_YEAR) {
    return 1;
  }
  if (year < EXPIRATION_YEAR) {
    return 0;
  }
  if (month > EXPIRATION_MONTH) {
    return 1;
  }
  if (month < EXPIRATION_MONTH) {
    return 0;
  }
  if (day > EXPIRATION_DAY) {
    return 1;
  }
#endif  /* if defined(EXPIRATION_CHECK) */
  return 0;
}

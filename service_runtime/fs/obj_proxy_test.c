/*
 * Exercise object proxy mapping.  Don't actually use real objects,
 * but just made-up values.
 *
 * Use a simple array to hold object / name tuples to check that the
 * object proxy can do lookups properly of all entered values as well
 * as negative examples of objects that have never been entered /
 * names that had never been issued.  This array is unsorted, so
 * searching it is inefficient, but should be obviously correct.
 */

#include "native_client/include/portability.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if !NACL_WINDOWS
# include <unistd.h>
#endif

#if defined(HAVE_SDL)
# include <SDL.h>
#endif

#include "native_client/service_runtime/nacl_all_modules.h"
#include "native_client/service_runtime/nacl_log.h"
#include "native_client/service_runtime/nacl_secure_random.h"

#include "native_client/service_runtime/fs/obj_proxy.h"

int                   verbose;
struct NaClSecureRng  gRng;

struct TestObj {
  void        *obj;
  char const  *name;
};

struct TestData {
  struct TestObj  *obj_tbl;
  int             obj_tbl_size;
  int             obj_tbl_used;
  int             namelen;
};

void TestDataCtor(struct TestData *self, int namelen)
{
  self->obj_tbl = NULL;
  self->obj_tbl_size = 0;
  self->obj_tbl_used = 0;
  self->namelen = namelen;
}

void TestDataDtor(struct TestData *self)
{
  free(self->obj_tbl);
}

void TestDataEnterObj(struct TestData *self, void *obj, char const *name)
{
  if (self->obj_tbl_used >= self->obj_tbl_size) {
    if (self->obj_tbl_size) {
      self->obj_tbl_size *= 2;
    } else {
      self->obj_tbl_size = 16;
    }
    self->obj_tbl = realloc(self->obj_tbl,
                            self->obj_tbl_size * sizeof *self->obj_tbl);
    if (NULL == self->obj_tbl) {
      fprintf(stderr, "obj_tbl_enter: out of memory\n");
      exit(1);
    }
  }
  self->obj_tbl[self->obj_tbl_used].obj = obj;
  self->obj_tbl[self->obj_tbl_used].name = name;
  ++self->obj_tbl_used;
}

char const *TestDataFindName(struct TestData *self, void *obj)
{
  int i;

  for (i = 0; i < self->obj_tbl_used; ++i) {
    if (self->obj_tbl[i].obj == obj) {
      return self->obj_tbl[i].name;
    }
  }
  return NULL;
}

void *TestDataFindObj(struct TestData *self, char const *name) {
  int i;

  for (i = 0; i < self->obj_tbl_used; ++i) {
    if (!memcmp(self->obj_tbl[i].name, name, self->namelen)) {
      return self->obj_tbl[i].obj;
    }
  }
  return NULL;
}

void print_name(FILE *ostr, char const *name, int namelen)
{
  int i;
  int v;

  for (i = 0; i < namelen; ++i) {
    v = name[i] & 0xff;
    if (0 == (i & 0x7)) {
      putc(' ', ostr);
    }
    fprintf(ostr, "%02x", v);
  }
}

int test_for_size(int namelen, int num_samples, int num_bogus)
{
  struct TestData     td;
  struct NaClObjProxy nop;
  int                 samp;
  void                *obj;
  char const          *name;
  char const          *true_name;
  void                *found_obj;
  int                 passed = 0;
  char                *bogus_name;

  bogus_name = malloc(namelen);

  printf("Testing for name length %d, %d legit samples, %d bogus lookups\n",
         namelen, num_samples, num_bogus);
  TestDataCtor(&td, namelen);
  NaClObjProxyCtor(&nop, namelen);

  printf("insertion\n");
  for (samp = 0; samp < num_samples; ++samp) {
    obj = (void *) samp;
    name = NaClObjProxyInsert(&nop, obj);

    if (verbose) {
      printf("%4d: ", samp);
      print_name(stdout, name, namelen);
      putchar('\n');
    }

    TestDataEnterObj(&td, obj, name);
  }

  printf("lookup\n");
  /* now look up every one */
  for (samp = 0; samp < num_samples; ++samp) {
    obj = (void *) samp;

    true_name = TestDataFindName(&td, obj);
    if (NULL == true_name) {
      printf("`True name' for object %d not found\n", samp);
      goto cleanup;
    }
    if (verbose) {
      printf("[ %4d: ", samp);
      print_name(stdout, true_name, namelen);
      printf(" ]\n");
    }

    name = NaClObjProxyFindNameByObj(&nop, obj);
    if (NULL == name) {
      printf("Object %d not found!\n", samp);
      goto cleanup;
    }

    if (memcmp(name, true_name, namelen)) {
      printf("Name mismatch!  Expected ");
      print_name(stdout, true_name, namelen);
      printf(" got ");
      print_name(stdout, name, namelen);
      printf("\n");
      goto cleanup;
    }
  }

  /* now look up every one, backwards */
  printf("order reversed lookup\n");
  for (samp = num_samples; --samp >= 0; ) {
    obj = (void *) samp;

    true_name = TestDataFindName(&td, obj);
    if (NULL == true_name) {
      printf("`True name' for object %d not found\n", samp);
      goto cleanup;
    }
    if (verbose) {
      printf("[ %4d: ", samp);
      print_name(stdout, true_name, namelen);
      printf(" ]\n");
    }

    name = NaClObjProxyFindNameByObj(&nop, obj);
    if (NULL == name) {
      printf("Object %d not found!\n", samp);
      goto cleanup;
    }

    if (memcmp(name, true_name, namelen)) {
      printf("Name mismatch!  Expected ");
      print_name(stdout, true_name, namelen);
      printf(" got ");
      print_name(stdout, name, namelen);
      printf("\n");
      goto cleanup;
    }
  }

  printf("lookup by name\n");
  /* now look up every one */
  for (samp = 0; samp < num_samples; ++samp) {
    obj = (void *) samp;

    true_name = TestDataFindName(&td, obj);
    if (NULL == true_name) {
      printf("`True name' for object %d not found\n", samp);
      goto cleanup;
    }
    if (verbose) {
      printf("[ %4d: ", samp);
      print_name(stdout, true_name, namelen);
      printf(" ]\n");
    }

    if (!NaClObjProxyFindObjByName(&nop, true_name, &found_obj)) {
      printf("Object %d not found, name ", samp);
      print_name(stdout, true_name, namelen);
      printf("!\n");
      goto cleanup;
    }

    if (found_obj != obj) {
      printf("Object mismatch!  Expected 0x%08"PRIxPTR" got 0x%08"PRIxPTR"\n",
             (uintptr_t) obj, (uintptr_t) found_obj);
      goto cleanup;
    }
  }

  printf("lookup by name, rev order\n");
  /* now look up every one */
  for (samp = num_samples; --samp >= 0; ) {
    obj = (void *) samp;

    true_name = TestDataFindName(&td, obj);
    if (NULL == true_name) {
      printf("`True name' for object %d not found\n", samp);
      goto cleanup;
    }
    if (verbose) {
      printf("[ %4d: ", samp);
      print_name(stdout, true_name, namelen);
      printf(" ]\n");
    }

    if (!NaClObjProxyFindObjByName(&nop, true_name, &found_obj)) {
      printf("Object %d not found!\n", samp);
      goto cleanup;
    }

    if (found_obj != obj) {
      printf("Object mismatch!  Expected 0x%08"PRIxPTR" got 0x%08"PRIxPTR,
             (uintptr_t) obj, (uintptr_t) found_obj);
      goto cleanup;
    }
  }

  printf("Bogus objects\n");
  for (samp = num_samples; samp < num_samples + num_bogus; ++samp) {
    obj = (void *) samp;
    name = NaClObjProxyFindNameByObj(&nop, obj);
    if (NULL != name) {
      printf("Bogus object %d found!\n", samp);
      goto cleanup;
    }
  }

  printf("Bogus names\n");
  for (samp = 0; samp < num_bogus; ++samp) {
    NaClSecureRngGenBytes(&gRng, bogus_name, namelen);
    if (NaClObjProxyFindObjByName(&nop, bogus_name, &obj)) {
      printf("Bogus name ");
      print_name(stdout, bogus_name, namelen);
      printf(" found!\n");
      goto cleanup;
    }
  }

  printf("OK\n");
  passed = 1;

cleanup:
  NaClObjProxyDtor(&nop);
  TestDataDtor(&td);
  free(bogus_name);
  return passed;
}

int main(int ac, char **av)
{
  int opt;
  int namelen_start = 4;
  int namelen_end = 32;
  int namelen_inc = 1;
  int num_samples = 1 << 10;
  int num_bogus = 1024;
  int namelen;
  int pass = 1;

  while (-1 != (opt = getopt(ac, av, "s:e:i:n:b:v"))) {
    switch (opt) {
      case 's':
        namelen_start = strtol(optarg, (char **) 0, 0);
        break;
      case 'e':
        namelen_end = strtol(optarg, (char **) 0, 0);
        break;
      case 'i':
        namelen_inc = strtol(optarg, (char **) 0, 0);
        break;
      case 'n':
        num_samples = strtol(optarg, (char **) 0, 0);
        break;
      case 'b':
        num_bogus = strtol(optarg, (char **) 0, 0);
        break;
      case 'v':
        ++verbose;
        break;
      default:
        fprintf(stderr,
                "Usage: obj_proxy_test [-s start_namelen] [-e end_namelen]\n"
                "                      [-i incr_namelen] [-n num_samples]\n"
                "                      [-b num_bogus_samples]\n"
                "                      [-v]\n"
                " NB: it is bad if num_samples >= 2**(4*start_namelen)\n"
                " since the birthday paradox will cause name collisions\n"
                );
        exit(1);
    }
  }
  NaClAllModulesInit();
  if (namelen_start > namelen_end || namelen_inc <= 0) {
    fprintf(stderr, "obj_proxy_test: name length parameters are bogus\n");
    exit(1);
  }
  NaClSecureRngCtor(&gRng);
  for (namelen = namelen_start; namelen < namelen_end; namelen += namelen_inc) {
    pass = pass && test_for_size(namelen, num_samples, num_bogus);
  }
  NaClSecureRngDtor(&gRng);
  NaClAllModulesFini();
  printf("%s\n", pass ? "PASS" : "FAIL");

  return !pass;
}

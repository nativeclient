/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * NaCl Service Runtime.  Secure RNG abstraction.
 */

#ifndef NATIVE_CLIENT_SERVICE_RUNTIME_NACL_SECURE_RANDOM_H_
#define NATIVE_CLIENT_SERVICE_RUNTIME_NACL_SECURE_RANDOM_H_

#include "native_client/include/portability.h"

#include "native_client/include/nacl_base.h"

/*
 * Get struct NaClSecureRng, since users will need its size for
 * placement-new style initialization.
 */
#if NACL_LINUX || NACL_OSX
# include "native_client/service_runtime/linux/nacl_secure_random_types.h"
#elif NACL_WINDOWS
# include "native_client/service_runtime/win/nacl_secure_random_types.h"
#endif

EXTERN_C_BEGIN

void NaClSecureRngModuleInit(void);

void NaClSecureRngModuleFini(void);

int NaClSecureRngCtor(struct NaClSecureRng *self);

void NaClSecureRngDtor(struct NaClSecureRng *self);

char NaClSecureRngGenByte(struct NaClSecureRng *self);

uint32_t NaClSecureRngGenUint32(struct NaClSecureRng *self);

void NaClSecureRngGenBytes(struct NaClSecureRng *self,
                           char                 *buf,
                           size_t               nbytes);

/*
 * Generate an uniformly random number in [0, range_max).  May invoke
 * the provided generator multiple times.
 */
uint32_t NaClSecureRngUniform(struct NaClSecureRng  *self,
                              uint32_t              range_max);

EXTERN_C_END

#endif

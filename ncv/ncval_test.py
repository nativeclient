
import os
import shutil
import subprocess
import tempfile
import unittest

import ncval_stubout


def write_file(filename, data):
    fh = open(filename, "w")
    try:
        fh.write(data)
    finally:
        fh.close()


def read_file(filename):
    fh = open(filename, "r")
    try:
        return fh.read()
    finally:
        fh.close()


class TempDirTestCase(unittest.TestCase):

    def setUp(self):
        self._on_teardown = []

    def make_temp_dir(self):
        temp_dir = tempfile.mkdtemp(prefix="tmp-%s-" % self.__class__.__name__)
        def tear_down():
            shutil.rmtree(temp_dir)
        self._on_teardown.append(tear_down)
        return temp_dir

    def tearDown(self):
        for func in reversed(self._on_teardown):
            func()


class NcValTest(TempDirTestCase):

    def assertEquals(self, x, y):
        if x != y:
            if type(x) == str and type(y) == str:
                raise AssertionError('"%s"\n!="%s"' % (x, y))
            raise AssertionError("%r != %r" % (x, y))

    def _get_errors(self, obj_file):
        proc = subprocess.Popen(["ncval", obj_file], stdout=subprocess.PIPE)
        lines = [line for line in proc.stdout
                 if line.startswith("VALIDATOR")]
        self.assertEquals(proc.wait(), 0)
        return lines

    def test_stub_out(self):
        asm_file = os.path.join(self.make_temp_dir(), "example.S")
        obj_file = os.path.join(self.make_temp_dir(), "example")
        write_file(asm_file, """
	.global _start
_start:
	jmp 0x12345678
	jmp *%ecx
	mov %fs,%dx
	ret
	int $0x80
""")
        subprocess.check_call(["nacl-gcc", "-nostartfiles", asm_file,
                               "-o", obj_file])
        self.assertEquals("".join(self._get_errors(obj_file)), """\
VALIDATOR: 00010000 (00001000:?): Jump target out of range
VALIDATOR: 00010005 (00001005:2): Unsafe indirect jump
VALIDATOR: 00010007 (00001007:3): Bad prefix
VALIDATOR: 00010007 (00001007:3): Illegal instruction
VALIDATOR: 0001000a (0000100a:1): Illegal instruction
VALIDATOR: 0001000b (0000100b:2): Illegal instruction
""")
        ncval_stubout.main([obj_file])
        # Can't stub out bad jumps yet
        self.assertEquals("".join(self._get_errors(obj_file)), """\
VALIDATOR: 00010000 (00001000:?): Jump target out of range
""")

    def test_multiple_sections(self):
        # Check that we handle file indexes correctly in the presence
        # of multiple ELF sections.
        asm_file = os.path.join(self.make_temp_dir(), "example.S")
        obj_file = os.path.join(self.make_temp_dir(), "example")
        write_file(asm_file, """
	.global _start
_start:
	nop
	nop
	.section .fini, "x", @progbits
	ret
""")
        subprocess.check_call(["nacl-gcc", "-nostartfiles", asm_file,
                               "-o", obj_file])
        self.assertNotEquals(self._get_errors(obj_file), [])
        ncval_stubout.main([obj_file])
        self.assertEquals(self._get_errors(obj_file), [])


class JumpRewriterTest(TempDirTestCase):

    def _assemble(self, asm_source):
        asm_file = os.path.join(self.make_temp_dir(), "foo.S")
        obj_file = os.path.join(self.make_temp_dir(), "foo.o")
        write_file(asm_file, asm_source)
        subprocess.check_call(["gcc", "-c", asm_file, "-o", obj_file])
        return obj_file

    def _disassemble(self, obj_file):
        proc = subprocess.Popen(["objdump", "-d", obj_file],
                                stdout=subprocess.PIPE)
        return proc.communicate()[0]

    def assert_object_files_equal(self, obj_file, obj_file_expect):
        if read_file(obj_file) != read_file(obj_file_expect):
            raise AssertionError("Unexpected output:\n%s\nExpected:\n%s" %
                                 (self._disassemble(obj_file),
                                  self._disassemble(obj_file_expect)))

    def test_rewriting(self):        
        original = """
	// Instructions to be rewritten
	nop; nop; nop
	jmp *%eax
	nop; nop; nop
	jmp *%ebx
	nop; nop; nop
	jmp *%ecx
	nop; nop; nop
	jmp *%edx

	nop; nop; nop
	call *%eax
	nop; nop; nop
	call *%ebx
	nop; nop; nop
	call *%ecx
	nop; nop; nop
	call *%edx
"""
        rewritten = """
	and $0xffffffe0, %eax
	jmp *%eax
	and $0xffffffe0, %ebx
	jmp *%ebx
	and $0xffffffe0, %ecx
	jmp *%ecx
	and $0xffffffe0, %edx
	jmp *%edx

	and $0xffffffe0, %eax
	call *%eax
	and $0xffffffe0, %ebx
	call *%ebx
	and $0xffffffe0, %ecx
	call *%ecx
	and $0xffffffe0, %edx
	call *%edx
"""
        leave_alone = """
	// These should be left alone
	nop; nop; nop
	jmp 0x12345678
	mov $123, %eax
"""
        obj_file = self._assemble(original + leave_alone)
        obj_file_expect = self._assemble(rewritten + leave_alone)
        subprocess.check_call(["ncrewrite", obj_file])
        self.assert_object_files_equal(obj_file, obj_file_expect)

        obj_file = self._assemble(rewritten + leave_alone)
        obj_file_expect = self._assemble(original + leave_alone)
        subprocess.check_call(["ncrewrite", "--nop", obj_file])
        self.assert_object_files_equal(obj_file, obj_file_expect)

    def test_rewriting_missing_nops(self):
        input_data = """
        // Not enough preceding nops to rewrite
	nop; nop
	jmp *%ecx 
"""
        obj_file = self._assemble(input_data * 2)
        original = read_file(obj_file)
        proc = subprocess.Popen(["ncrewrite", obj_file], stderr=subprocess.PIPE)
        stderr = proc.communicate()[1]
        self.assertEquals(stderr,
                          "00000002: cannot rewrite\n"
                          "00000006: cannot rewrite\n")
        self.assertEquals(proc.wait(), 0)
        # Object file should not have been changed.
        self.assertEquals(original, read_file(obj_file))


if __name__ == "__main__":
    unittest.main()

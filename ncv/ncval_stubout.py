#!/usr/bin/env python

# Force an executable or library to pass the validator by overwriting
# any non-validating instruction with hlt instructions.

import os
import re
import subprocess
import sys


ncval = os.path.join(os.path.dirname(__file__),
                     "../scons-out/dbg-linux/staging/ncval")


def fix_up_file(obj_file):
    proc = subprocess.Popen([ncval, obj_file], stdout=subprocess.PIPE)
    regexp = re.compile(
        "VALIDATOR: [0-9a-f]+ \((?P<file_addr>[0-9a-f]+):(?P<size>[0-9a-f]+)\)")
    rewrites = []
    for line in proc.stdout:
        match = regexp.match(line)
        if match is not None:
            sys.stdout.write("%s: %s" % (obj_file, line))
            offset = int(match.group("file_addr"), 16)
            size = int(match.group("size"), 16)
            rewrites.append((offset, size))
    fh = os.fdopen(os.open(obj_file, os.O_WRONLY), "w")
    for offset, size in rewrites:
        fh.seek(offset)
        # Overwrite with hlt instructions
        fh.write("\xf4" * size)
    fh.close()


def main(args):
    for filename in args:
        fix_up_file(filename)


if __name__ == "__main__":
    main(sys.argv[1:])

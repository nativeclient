#!/usr/bin/env python

# Annotate the output of ncval with line numbers, taken from debugging
# information using binutils' addr2line.

import linecache
import os
import re
import subprocess
import sys


ncval = os.path.join(os.path.dirname(__file__),
                     "../scons-out/dbg-linux/staging/ncval")


objdump_regexp = re.compile(r"\s*([0-9a-f]+):")


def check(obj_file):
    def disassemble_address(addr):
        proc = subprocess.Popen(
            ["objdump", "-d", obj_file, "--start-address", "0x" + addr,
             "--stop-address", "0x%x" % (int(addr, 16) + 16)],
            stdout=subprocess.PIPE)
        for line in proc.stdout:
            match = objdump_regexp.match(line)
            if match is not None:
                sys.stdout.write(line)
                break

    def decode_address(addr):
        proc = subprocess.Popen(
            ["addr2line", "-f", "-i", "-e", obj_file, addr],
            stdout=subprocess.PIPE)
        for info in proc.stdout:
            sys.stdout.write("  %s: %s" % (addr, info))
            if ":" in info:
                filename, lineno = info.split(":", 1)
                src_line = linecache.getline(filename, int(lineno))
                if src_line != "":
                    sys.stdout.write("    " + src_line.lstrip())

    proc = subprocess.Popen([ncval, obj_file], stdout=subprocess.PIPE)
    regexp = re.compile("[0-9a-f]{3,}")
    for line in proc.stdout:
        sys.stdout.write(line)
        if line.startswith("VALIDATOR:"):
            match = regexp.search(line)
            if match is not None:
                addr = match.group()
                disassemble_address(addr)
                decode_address(addr)


def main(args):
    for obj_file in args:
        check(obj_file)


if __name__ == "__main__":
    main(sys.argv[1:])

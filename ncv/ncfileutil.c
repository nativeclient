/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ncfileutil.c - open an executable file. FOR TESTING ONLY.
 */
#include "native_client/include/portability.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
/*
 * TODO: move to portability.h and define an OPEN portability macro?
 * Drawback is that portability.h becomes a black hole of includes
 * that every source file includes but contains includes that are
 * mostly not needed, unnecessarily increasing compile time (even w/
 * pre-compiled headers) and obfuscating what system feature each
 * source file really needs.  Perhaps port/open.h, port/foo.h for
 * declarations associated with needing foo?
 */
#if NACL_WINDOWS
#include <io.h>
#define open _open
#else
#include <unistd.h>
#endif
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>

#include "native_client/ncv/ncfileutil.h"

/* This module is intended for testing use only, not for production use */
/* in sel_ldr. To prevent unintended production usage, define a symbol  */
/* that will cause a load-time error for sel_ldr.                       */
int gNaClValidateImage_foo = 0;
void NaClValidateImage() { gNaClValidateImage_foo += 1; }

static INLINE uint32_t NCPageTrunc(uint32_t v) {
  return (v & ~(kNCFileUtilPageSize - 1));
}

static INLINE uint32_t NCPageRound(uint32_t v) {
  return(NCPageTrunc(v + (kNCFileUtilPageSize - 1)));
}

/***********************************************************************/
/* THIS ROUTINE IS FOR DEBUGGING/TESTING ONLY, NOT FOR SECURE RUNTIME  */
/* ALL PAGES ARE LEFT WRITEABLE BY THIS LOADER.                        */
/***********************************************************************/
/* Loading a NC executable from a host file */
static int readat(const int fd, void *buf, const size_t sz, const size_t at)
{
  size_t sofar = 0;
  int nread;
  char *cbuf = (char *)buf;

  if (0 > lseek(fd, at, SEEK_SET)) {
    fprintf(stderr, "readat: lseek failed\n");
    return -1;
  }
  /* Strangely this loop is needed on Windows. It seems the read()   */
  /* implementation doesn't always return as many bytes as requested */
  /* so you have to keep on trying.                                  */
  do {
    nread = read(fd, &cbuf[sofar], sz - sofar);
    if (nread <= 0) {
      fprintf(stderr, "readat: read failed\n");
      return -1;
    }
    sofar += nread;
  } while (sz != sofar);
  return nread;
}

static int nc_load(ncfile *ncf, int fd)
{
#define NCLOADFAIL(_args)                         \
  do {                                            \
    fprintf(stderr, "nc_load(%s):", ncf->fname);  \
    fprintf _args ; return -1;                    \
  } while (0)

  Elf32_Ehdr h;
  ssize_t nread;
  size_t vmemlo, vmemhi, shsize, phsize;
  int i;

  /* Read and check the ELF header */
  nread = readat(fd, &h, sizeof(h), 0);
  if (nread < 0 || (size_t) nread < sizeof(h)) {
    NCLOADFAIL((stderr, "could not read ELF header\n"));
  }

  /* do a bunch of sanity checks */
  if (strncmp((char *)h.e_ident, ELFMAG, strlen(ELFMAG))) {
    NCLOADFAIL((stderr, "bad magic number\n"));
  }
  if (h.e_ident[EI_OSABI] != ELFOSABI_NACL) {
    fprintf(stderr, "%s: bad OS ABI %x\n", ncf->fname, h.e_ident[EI_OSABI]);
    /* return; */
  }
  if (h.e_ident[EI_ABIVERSION] != EF_NACL_ABIVERSION) {
    fprintf(stderr, "%s: bad ABI version %d\n", ncf->fname,
            h.e_ident[EI_ABIVERSION]);
    /* return; */
  }
  if ((h.e_flags & EF_NACL_ALIGN_MASK) == EF_NACL_ALIGN_16) {
    ncf->ncalign = 16;
  } else if ((h.e_flags & EF_NACL_ALIGN_MASK) == EF_NACL_ALIGN_32) {
    ncf->ncalign = 32;
  } else {
    fprintf(stderr, "%s: bad align mask %x\n", ncf->fname,
            (uint32_t)(h.e_flags & EF_NACL_ALIGN_MASK));
    ncf->ncalign = 16;
    /* return; */
  }

  /* Read the program header table */
  if (h.e_phnum <= 0 || h.e_phnum > kMaxPhnum) {
    NCLOADFAIL((stderr, "h.e_phnum %d > kMaxPhnum %d\n",
                h.e_phnum, kMaxPhnum));
  }
  ncf->phnum = h.e_phnum;
  ncf->pheaders = (Elf32_Phdr *)calloc(h.e_phnum, sizeof(Elf32_Phdr));
  if (NULL == ncf->pheaders) {
    NCLOADFAIL((stderr, "calloc(%d, %"PRIdS") failed\n",
                h.e_phnum,
                sizeof(Elf32_Phdr)));
  }
  phsize = h.e_phnum * sizeof(*ncf->pheaders);
  nread = readat(fd, ncf->pheaders, phsize, h.e_phoff);
  if (nread < 0 || (size_t) nread < phsize) return -1;

  /* Iterate through the program headers to find the virtual */
  /* size of loaded text.                                    */
  vmemlo = 0xffffffff;
  vmemhi = 0;
  for (i = 0; i < h.e_phnum; i++) {
    if (ncf->pheaders[i].p_type != PT_LOAD) continue;
    if (0 == (ncf->pheaders[i].p_flags & PF_X)) continue;
    /* This is executable text. Check low and high addrs */
    if (vmemlo > ncf->pheaders[i].p_vaddr) vmemlo = ncf->pheaders[i].p_vaddr;
    if (vmemhi < ncf->pheaders[i].p_vaddr + ncf->pheaders[i].p_memsz) {
      vmemhi = ncf->pheaders[i].p_vaddr + ncf->pheaders[i].p_memsz;
    }
  }
  vmemhi = NCPageRound(vmemhi);
  ncf->size = vmemhi - vmemlo;
  ncf->vbase = vmemlo;
  if (vmemlo != NCPageTrunc(vmemlo)) {
    NCLOADFAIL((stderr, "vmemlo is not aligned\n"));
  }
  ncf->data = (uint8_t *)calloc(1, ncf->size);
  if (NULL == ncf->data) {
    NCLOADFAIL((stderr, "calloc(1, %d) failed\n", (int)ncf->size));
  }

  /* Load program text segments */
  for (i = 0; i < h.e_phnum; i++) {
    const Elf32_Phdr *p = &ncf->pheaders[i];
    if (p->p_type != PT_LOAD) continue;
    if (0 == (ncf->pheaders[i].p_flags & PF_X)) continue;

    assert(ncf->size >= NCPageRound(p->p_vaddr - ncf->vbase + p->p_memsz));
    nread = readat(fd, &(ncf->data[p->p_vaddr - ncf->vbase]),
                   p->p_filesz, p->p_offset);
    if (nread < 0 || (size_t) nread < p->p_filesz) {
      NCLOADFAIL((stderr, "could not read segment %d (%d < %d)\n",
                  i, (int)nread, p->p_filesz));
    }
  }
  /* load the section headers */
  ncf->shnum = h.e_shnum;
  shsize = ncf->shnum * sizeof(*ncf->sheaders);
  ncf->sheaders = (Elf32_Shdr *)calloc(1, shsize);
  if (NULL == ncf->sheaders) {
    NCLOADFAIL((stderr, "calloc(1, %d) failed\n", (int)shsize));
  }
  nread = readat(fd, ncf->sheaders, shsize, h.e_shoff);
  if (nread < 0 || (size_t) nread < shsize) {
    NCLOADFAIL((stderr, "could not read section headers\n"));
  }

  /* success! */
  return 0;
}

ncfile *nc_loadfile(const char *filename)
{
  ncfile *ncf;
  int fd;
  int rdflags = O_RDONLY;
#if NACL_WINDOWS
  rdflags |= _O_BINARY;
#endif
  fd = open(filename, rdflags);
  if (fd < 0) return NULL;

  /* Allocate the ncfile structure */
  ncf = calloc(1, sizeof(ncfile));
  if (ncf == NULL) return NULL;
  ncf->size = 0;
  ncf->data = NULL;
  ncf->fname = filename;

  if (nc_load(ncf, fd) < 0) {
    close(fd);
    free(ncf);
    return NULL;
  }
  close(fd);
  return ncf;
}


void nc_freefile(ncfile *ncf)
{
  if (ncf->data != NULL) free(ncf->data);
  free(ncf);
}
/***********************************************************************/

void GetVBaseAndLimit(ncfile *ncf, uint32_t *vbase, uint32_t *vlimit) {
  int ii;
  uint32_t base = 0xffffffff;
  uint32_t limit = 0;

  for (ii = 0; ii < ncf->shnum; ii++) {
    if ((ncf->sheaders[ii].sh_flags & SHF_EXECINSTR) == SHF_EXECINSTR) {
      if (ncf->sheaders[ii].sh_addr < base) base = ncf->sheaders[ii].sh_addr;
      if (ncf->sheaders[ii].sh_addr + ncf->sheaders[ii].sh_size > limit)
        limit = ncf->sheaders[ii].sh_addr + ncf->sheaders[ii].sh_size;
    }
  }
  *vbase = base;
  *vlimit = limit;
}

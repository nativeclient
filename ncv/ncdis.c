/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ncdis.c - disassemble using NaCl decoder.
 * Mostly for testing.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include "native_client/include/nacl_elf.h"
#include "native_client/ncv/ncfileutil.h"
#include "native_client/ncv/ncdecode.h"
/* NOTE: Generated! */
#include "native_client/ncv/ncdisasmtab.h"

static const char *progname;

void fatal(const char *fmt, ...)
{
  va_list ap;
  fprintf(stderr, "%s: fatal error: ", progname);
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fputc('\n', stderr);
  exit(2);
}

/* later this will make decoding x87 instructions a bit more concise. */
static const char **kDisasmX87Op[8] = { kDisasm87D8,
                                   kDisasm87D9,
                                   kDisasm87DA,
                                   kDisasm87DB,
                                   kDisasm87DC,
                                   kDisasm87DD,
                                   kDisasm87DE,
                                   kDisasm87DF };

/* disassembler stuff */
static const char *DisFmt(const struct NCDecoderState *mstate) {
  static const uint8_t kWAITOp = 0x9b;
  uint8_t *opbyte = &mstate->inst.maddr[mstate->inst.prefixbytes];
  uint8_t pm = mstate->inst.prefixmask;

  if (mstate->opinfo->insttype == NACLi_X87) {
    if (opbyte[0] != kWAITOp) {
      return kDisasmX87Op[opbyte[0]-0xd8][mstate->inst.mrm];
    }
  }
  if (mstate->opinfo->insttype == NACLi_FCMOV) {
    return kDisasmX87Op[opbyte[0]-0xd8][mstate->inst.mrm];
  }
  if (*opbyte != kTwoByteOpcodeByte1) return kDisasm1ByteOp[opbyte[0]];
  if (opbyte[1] == 0x0f) return kDisasm0F0FOp[opbyte[mstate->inst.length - 1]];
  if (opbyte[1] == 0x38) return kDisasm0F38Op[opbyte[2]];
  if (opbyte[1] == 0x3A) return kDisasm0F3AOp[opbyte[2]];
  if (! (pm & (kPrefixDATA16 | kPrefixREPNE | kPrefixREP))) {
    return kDisasm0FXXOp[opbyte[1]];
  }
  if (pm & kPrefixDATA16) return kDisasm660FXXOp[opbyte[1]];
  if (pm & kPrefixREPNE)  return kDisasmF20FXXOp[opbyte[1]];
  if (pm & kPrefixREP)    return kDisasmF30FXXOp[opbyte[1]];

  /* no update; should be invalid */
  return "internal error";
}

static int ByteImmediate(const uint8_t* byte_array) {
  return (char) byte_array[0];
}

static int WordImmediate(const uint8_t* byte_array) {
  return (short) (byte_array[0] + (byte_array[1] << 8));
}

static int DwordImmediate(const uint8_t* byte_array) {
  return (byte_array[0] +
          (byte_array[1] << 8) +
          (byte_array[2] << 16) +
          (byte_array[3] << 24));
}

static const char* gp_regs[] = {
  "%eax", "%ecx", "%edx", "%ebx", "%esp", "%ebp", "%esi", "%edi"
};

static const char* mmx_regs[] = {
  "%mm0", "%mm1", "%mm2", "%mm3", "%mm4", "%mm5", "%mm6", "%mm7"
};

static const char* xmm_regs[] = {
  "%xmm0", "%xmm1", "%xmm2", "%xmm3", "%xmm4", "%xmm5", "%xmm6", "%xmm7"
};

static const char* seg_regs[] = {
  "%es", "%cs", "%ss", "%ds", "%fs", "%gs"
};

static void SibPrint(const struct NCDecoderState *mstate, uint32_t sib_offset) {
  uint8_t sib = mstate->inst.maddr[sib_offset];

  if (sib_ss(sib) == 0) {
    if (sib_base(sib) == 5) {
      const uint8_t* disp_addr = mstate->inst.maddr + sib_offset + 1;
      fprintf(stdout, "[0x%x]", DwordImmediate(disp_addr));
    } else {
      /* Has a base register */
      if (sib_index(sib) == 4) {
        /* No index */
        fprintf(stdout, "[%s]", gp_regs[sib_base(sib)]);
      } else {
        fprintf(stdout, "[%s + %s]",
                gp_regs[sib_base(sib)],
                gp_regs[sib_index(sib)]);
      }
    }
  } else {
    if (sib_index(sib) == 4) {
      /* No index */
      fprintf(stdout, "[%s]", gp_regs[sib_base(sib)]);
    } else {
      fprintf(stdout, "[%s + %d * %s]",
              gp_regs[sib_base(sib)],
              1 << sib_ss(sib),
              gp_regs[sib_index(sib)]);
    }
  }
}

static void SegPrefixPrint(const struct NCDecoderState *mstate) {
  uint8_t pm = mstate->inst.prefixmask;
  if (pm & kPrefixSEGCS) {
    fprintf(stdout, "cs:");
  } else if (pm & kPrefixSEGSS) {
    fprintf(stdout, "ss:");
  } else if (pm & kPrefixSEGFS) {
    fprintf(stdout, "fs:");
  } else if (pm & kPrefixSEGGS) {
    fprintf(stdout, "gs:");
  }
}

static void RegMemPrint(const struct NCDecoderState *mstate,
                        const char* reg_names[]) {
  uint32_t sib_offset =
      mstate->inst.prefixbytes +
      1 +
      mstate->inst.hasopbyte2 +
      mstate->inst.hasopbyte3 +
      1;
  const uint8_t* disp_addr = mstate->inst.maddr +
                             sib_offset +
                             mstate->inst.hassibbyte;

  switch (modrm_mod(mstate->inst.mrm)) {
    case 0:
      SegPrefixPrint(mstate);
      if (4 == modrm_rm(mstate->inst.mrm)) {
        SibPrint(mstate, sib_offset);
      } else if (5 == modrm_rm(mstate->inst.mrm)) {
        fprintf(stdout, "[0x%x]", DwordImmediate(disp_addr));
      } else {
        fprintf(stdout, "[%s]", gp_regs[modrm_rm(mstate->inst.mrm)]);
      }
      break;
    case 1:
      {
        SegPrefixPrint(mstate);
        fprintf(stdout, "0x%x", ByteImmediate(disp_addr));
        if (4 == modrm_rm(mstate->inst.mrm)) {
          SibPrint(mstate, sib_offset);
        } else {
          fprintf(stdout, "[%s]", gp_regs[modrm_rm(mstate->inst.mrm)]);
        }
      }
      break;
    case 2:
      {
        SegPrefixPrint(mstate);
        fprintf(stdout, "0x%x", DwordImmediate(disp_addr));
        if (4 == modrm_rm(mstate->inst.mrm)) {
          SibPrint(mstate, sib_offset);
        } else {
          fprintf(stdout, "[%s]", gp_regs[modrm_rm(mstate->inst.mrm)]);
        }
      }
      break;
    case 3:
      fprintf(stdout, "%s", reg_names[modrm_rm(mstate->inst.mrm)]);
      break;
  }
}

static void InstFormat(const char* format,
                       const struct NCDecoderState *mstate) {
  char token_buf[128];
  char* fmt = token_buf;
  int pos = 0;

  strncpy(token_buf, format, sizeof(token_buf));

  while (1) {
    char* token = strtok(fmt, " ,\n");
    if (NULL == token) {
      break;
    }
    if (pos > 1) {
      fprintf(stdout, ", ");
    } else if (pos > 0) {
      fprintf(stdout, " ");
    }
    if (('$' == token[0]) && !strncmp(token, "$group", 6)) {
      int mrm = modrm_reg(mstate->inst.mrm);
      if (!strcmp(token, "$group1")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP1][mrm]);
      } else if (!strcmp(token, "$group2")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP2][mrm]);
      } else if (!strcmp(token, "$group3")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP3][mrm]);
      } else if (!strcmp(token, "$group4")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP4][mrm]);
      } else if (!strcmp(token, "$group5")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP5][mrm]);
      } else if (!strcmp(token, "$group6")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP6][mrm]);
      } else if (!strcmp(token, "$group7")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP7][mrm]);
      } else if (!strcmp(token, "$group8")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP8][mrm]);
      } else if (!strcmp(token, "$group9")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP9][mrm]);
      } else if (!strcmp(token, "$group10")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP10][mrm]);
      } else if (!strcmp(token, "$group11")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP11][mrm]);
      } else if (!strcmp(token, "$group12")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP12][mrm]);
      } else if (!strcmp(token, "$group13")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP13][mrm]);
      } else if (!strcmp(token, "$group14")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP14][mrm]);
      } else if (!strcmp(token, "$group15")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP15][mrm]);
      } else if (!strcmp(token, "$group16")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP16][mrm]);
      } else if (!strcmp(token, "$group17")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP17][mrm]);
      } else if (!strcmp(token, "$group1a")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUP1A][mrm]);
      } else if (!strcmp(token, "$groupp")) {
        fprintf(stdout, "%s", kDisasmModRMOp[GROUPP][mrm]);
      } else {
        fprintf(stdout, "%s", token);
      }
    } else if ('$' == token[0]) {
      /* Tokens starting with a $ but not $group need formatting */
      switch (token[1]) {
        case 'A':
          fprintf(stdout, "$A");
          break;
        case 'C':
          fprintf(stdout, "%%cr%d", modrm_reg(mstate->inst.mrm));
          break;
        case 'D':
          fprintf(stdout, "%%dr%d", modrm_reg(mstate->inst.mrm));
          break;
        case 'E':
        case 'M': /* mod should never be 3 for 'M' */
          /* TODO: byte and word accesses */
          RegMemPrint(mstate, gp_regs);
          break;
        case 'F':
          fprintf(stdout, "eflags");
          break;
        case 'G':
          fprintf(stdout, "%s", gp_regs[modrm_reg(mstate->inst.mrm)]);
          break;
        case 'I':
          {
            const uint8_t* imm_addr = mstate->inst.maddr +
                                      mstate->inst.prefixbytes +
                                      1 +
                                      mstate->inst.hasopbyte2 +
                                      mstate->inst.hasopbyte3 +
                                      mstate->opinfo->hasmrmbyte +
                                      mstate->inst.hassibbyte +
                                      mstate->inst.dispbytes;
            if ('b' == token[2]) {
              fprintf(stdout, "0x%x", ByteImmediate(imm_addr));
            } else if ('w' == token[2]) {
              fprintf(stdout, "0x%x", WordImmediate(imm_addr));
            } else {
              fprintf(stdout, "0x%x", DwordImmediate(imm_addr));
            }
          }
          break;
        case 'J':
          {
            const uint8_t* imm_addr = mstate->inst.maddr +
                                      mstate->inst.prefixbytes +
                                      1 +
                                      mstate->inst.hasopbyte2 +
                                      mstate->inst.hasopbyte3 +
                                      mstate->opinfo->hasmrmbyte +
                                      mstate->inst.hassibbyte +
                                      mstate->inst.dispbytes;
            if ('b' == token[2]) {
              fprintf(stdout, "0x%x",
                      mstate->inst.vaddr + mstate->inst.length +
                      ByteImmediate(imm_addr));
            } else {
              fprintf(stdout, "0x%x",
                      mstate->inst.vaddr + mstate->inst.length +
                      DwordImmediate(imm_addr));
            }
          }
          break;
        case 'O':
          {
            const uint8_t* imm_addr = mstate->inst.maddr +
                                      mstate->inst.prefixbytes +
                                      1 +
                                      mstate->inst.hasopbyte2 +
                                      mstate->inst.hasopbyte3;
            fprintf(stdout, "[0x%x]", DwordImmediate(imm_addr));
          }
          break;
        case 'P':
          if ('R' == token[2]) {
            fprintf(stdout, "%%mm%d", modrm_rm(mstate->inst.mrm));
          } else {
            fprintf(stdout, "%%mm%d", modrm_reg(mstate->inst.mrm));
          }
          break;
        case 'Q':
          RegMemPrint(mstate, mmx_regs);
          break;
        case 'R':
          fprintf(stdout, "%s", gp_regs[modrm_rm(mstate->inst.mrm)]);
          break;
        case 'S':
          fprintf(stdout, "%s", seg_regs[modrm_reg(mstate->inst.mrm)]);
          break;
        case 'V':
          if ('R' == token[2]) {
            fprintf(stdout, "%%xmm%d", modrm_rm(mstate->inst.mrm));
          } else {
            fprintf(stdout, "%%xmm%d", modrm_reg(mstate->inst.mrm));
          }
          break;
        case 'W':
          RegMemPrint(mstate, xmm_regs);
          break;
        case 'X':
          fprintf(stdout, "ds:[esi]");
          break;
        case 'Y':
          fprintf(stdout, "es:[edi]");
          break;
        default:
          fprintf(stdout, "token('%s')", token);
          break;
      }
    } else {
      /* Print the token as is */
      fprintf(stdout, "%s", token);
    }
    fmt = NULL;
    ++pos;
  }
}

static void PrintInst(const struct NCDecoderState *mstate) {
  int i;
  fprintf(stdout, " %x:\t%02x", mstate->inst.vaddr,
          mstate->inst.maddr[0]);
  for (i = 1; i < mstate->inst.length; i++) {
    fprintf(stdout, " %02x", mstate->inst.maddr[i]);
  }
  for (i = mstate->inst.length; i < 7; i++) fprintf(stdout, "   ");
  fprintf(stdout, "\t");
  InstFormat(DisFmt(mstate), mstate);
  fprintf(stdout, "\n");
}

int AnalyzeSections(ncfile *ncf) {
  int badsections = 0;
  int ii;
  Elf32_Shdr *shdr = ncf->sheaders;

  for (ii = 0; ii < ncf->shnum; ii++) {
    printf("section %d sh_addr %x offset %x flags %x\n",
           ii, (uint32_t)shdr[ii].sh_addr,
           (uint32_t)shdr[ii].sh_offset, (uint32_t)shdr[ii].sh_flags);
    if ((shdr[ii].sh_flags & SHF_EXECINSTR) != SHF_EXECINSTR)
      continue;
    printf("parsing section %d\n", ii);
    NCDecodeSegment(ncf->data + (shdr[ii].sh_addr - ncf->vbase),
                    shdr[ii].sh_addr, shdr[ii].sh_size, NULL);
  }
  return -badsections;
}


void AnalyzeCodeSegments(ncfile *ncf, const char *fname) {
  if (AnalyzeSections(ncf) < 0) {
    fprintf(stderr, "%s: text validate failed\n", fname);
  }
}

static const char* GrockArgv(int argc, const char *argv[]) {
  progname = argv[0];

  if (argc == 1) {
    fprintf(stderr, "Running %s unit tests\n", progname);
    return NULL;
  }
  return argv[argc-1];
}

/* TODO: try to avoid this */
static void PseudeUseToAvoidCompilerWarnings() {
  if (kDisasm660F38Op[0] == NULL ||
      kDisasmF20F38Op[0] == NULL ||
      kDisasm660F3AOp[0] == NULL) {
    fprintf(stderr, "malformed tables???\n");
    exit(-1);
  }
}

int main(int argc, const char *argv[]) {
  const char *loadname = GrockArgv(argc, argv);
  ncfile *ncf;

  PseudeUseToAvoidCompilerWarnings();
  NCDecodeRegisterCallbacks(PrintInst, NULL, NULL, NULL);
  if (loadname == NULL) {
    extern void ncdecode_unittests();

    ncdecode_unittests();
    return 0;
  }

  ncf = nc_loadfile(loadname);
  if (ncf == NULL)
    fatal("nc_loadfile(%s): %s\n", strerror(errno));

  AnalyzeCodeSegments(ncf, loadname);

  nc_freefile(ncf);

  return 0;
}

#!/usr/bin/python
# Copyright 2008, Google Inc.
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#     * Neither the name of Google Inc. nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#
"""discmp.py - compare disassembler output

This script compares output from the Native Client decoder to output
from GNU objdump to make sure they agree on instruction lengths. It
returns a zero status on success and non-zero on failure.

Usage:
  discmp.py PLATFORM_BASE=<platform_base> <fileordir> <fileordir>
for example:
  discmp.py PLATFORM_BASE=/home/who/proj/nacl/googleclient/native_client/scons-out/dbg-linux ncv/testdata/hanoi

"""

import re
import sys
import os

def Usage():
  print("""
USAGE:
  %s PLATFORM_BASE=<platform_base> <fileordir> <fileordir>

For example:
  %s PLATFORM_BASE=/home/who/proj/nacl/googleclient/native_client/scons-out/dbg-linux ncv/testdata/hanoi

Commonly this installer will be invoked from hammer as:
  ./hammer decoder_test
In this case hammer will take care of the command line args.
""" % (sys.argv[0], sys.argv[0]))
  sys.exit(-1)

def FindBinaries(PlatformBase):
  ncdis = os.path.join(PlatformBase, "staging", "ncdis")
  # Someday if nacl-objdump gets more permissive we can use it
  # objdump = os.path.join(nacl.SDK_BASE, "bin", "nacl-objdump")
  objdump = "/usr/bin/objdump"
  if not os.path.isfile(ncdis):
    print "Error: could not find", ncdis
    sys.exit(-1)
  if not os.path.isfile(objdump):
    print "Error: could not find", objdump
    sys.exit(-1)
  return ncdis, objdump

class DisFile(object):
  """Processes variants of disassembler output from various sources,
   providing a list of instruction lengths, based on addresses."""

  def __init__(self, stream):
    self.fd = stream
    self.lastline = None
    self.lastaddr = None
    self.thisaddr = None
    self.thisline = None
    # note we ignore lines that have only continuation bytes;
    # no opcode
    self.dislinefmt = re.compile("\s*([0-9a-f]+):\t(.*\t+.*)")
    self.data16fmt = re.compile("\s*([0-9a-f]+):\s+66\s+data16")
    self.waitfmt = re.compile("\s*([0-9a-f]+):\s+9b\s+[f]?wait")

  def NextDisInst(self):
    while (True):
      line = self.fd.readline()
      if line == "": return 0, None
      match = self.data16fmt.match(line)
      if (match):
        addr = match.group(1)
        line = self.fd.readline()
        match = self.dislinefmt.match(line)
        return (int(addr, 16),
                " " + addr + ":\t60 " + match.group(2) + "\n")
      match = self.waitfmt.match(line)
      if (match):
        addr = match.group(1)
        line = self.fd.readline()
        match = self.dislinefmt.match(line)
        return (int(addr, 16),
                " " + addr + ":\t9b " + match.group(2) + "\n")
      match = self.dislinefmt.match(line)
      if (match):
        return int(match.group(1), 16), line

  def NextInst(self):
    if self.lastaddr is None:
      self.lastaddr, self.lastline = self.NextDisInst()
    else:
      self.lastaddr = self.thisaddr
      self.lastline = self.thisline
    self.thisaddr, self.thisline = self.NextDisInst()
    if self.thisline is None:
      # don't know how long the last line was, so just return 1
      return (1, self.lastline)
    if self.lastline is None:
      return 0, None
    else:
      return (self.thisaddr - self.lastaddr, self.lastline)

def IsElfBinary(fname):
  fd = open(fname)
  iselfbinary = fd.readline().startswith("\x7fELF")
  fd.close()
  return iselfbinary

def DoOneFile(fname, ncdis, objdump):
  if not IsElfBinary(fname):
    print("Error:", fname, "is not an ELF binary\nContinuing...")
    return
  df1 = DisFile(os.popen(objdump + " -dz " + fname))
  df2 = DisFile(os.popen(ncdis + " " + fname))
  instcount = 0
  while (1):
    instcount += 1
    len1, line1 = df1.NextInst()
    len2, line2 = df2.NextInst()
    if line1 is None: break
    if line2 is None: break
    if (len1 != len2):
      sys.stdout.write("ERROR: inst length mistmatch %d != %d\n" %
                       (len1, len2))
      sys.stdout.write(line1)
      sys.stdout.write(line2)
      sys.exit(-1)
  if line1 or line2:
    sys.stdout.write("ERROR: disasm output is different lengths\n")
    sys.exit(-1)
  sys.stdout.write("%s: %d instructions; 0 errors!\n"
                   % (fname, instcount))

def DoOneFileOrDir(name, ncdis, objdump):
  if os.path.isfile(name): DoOneFile(name, ncdis, objdump)
  elif os.path.isdir(name):
    for dirpath, dirlist, filelist in os.walk(name):
      for fname in filelist:
        DoOneFile(os.path.join(dirpath, fname), ncdis, objdump)
  else: print "invalid argument", name

def ParseArgv():
  files = []
  args = {}
  print "***ARGV:"
  for arg in sys.argv[1:]:
    print "***   ", arg
    if arg.find("=") > 0:
      name, value = arg.split("=")
      print("%s=%s" % (name, value))
      args[name] = value
    else:
      files.append(arg)
  # require MODE and PLATFORM_BASE to be set
  if "PLATFORM_BASE" not in args: Usage()
  return args, files

def main():
  args, files = ParseArgv()
  ncdis, objdump = FindBinaries(args["PLATFORM_BASE"])
  for name in files:
    DoOneFileOrDir(name, ncdis, objdump)
  sys.exit(0)

if '__main__' == __name__:
  main()

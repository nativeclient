/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ncfileutil.h - open an executable file. FOR TESTING ONLY.
 */
#ifndef NATICE_CLIENT_NCV_NCFILEUTIL_H__
#define NATICE_CLIENT_NCV_NCFILEUTIL_H__

#include "native_client/include/portability.h"
#include "native_client/include/nacl_elf.h"

EXTERN_C_BEGIN

/* memory access permissions have a granularity of 4KB pages. */
static const int kNCFileUtilPageShift =      12;
static const int kNCFileUtilPageSize = (1 << 12);
/* this needs to be a #define because it's used as an array size */
#define kMaxPhnum 32

struct ncfile {
  const char *fname; /* name of loaded file */
  uint32_t vbase;    /* base address in virtual memory */
  size_t size;       /* size of program memory         */
  uint8_t *data;     /* the actual loaded bytes        */
  Elf32_Half phnum;  /* number of Elf program headers */
  Elf32_Phdr *pheaders;  /* copy of the Elf program headers */
  Elf32_Half shnum;  /* number of Elf section headers */
  Elf32_Shdr *sheaders;  /* copy of the Elf section headers */
  int ncalign;
};
typedef struct ncfile ncfile;

ncfile *nc_loadfile(const char *filename);

void nc_freefile(ncfile *ncf);

void GetVBaseAndLimit(ncfile *ncf, uint32_t *vbase, uint32_t *vlimit);

EXTERN_C_END

#endif  /* NATICE_CLIENT_NCV_NCFILEUTIL_H__ */

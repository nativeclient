#!/usr/bin/python
# Copyright 2008, Google Inc.
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#     * Neither the name of Google Inc. nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


"""checkncv.py - Crude unit testing infrastructure for ncv, the
native client validator.

Right now this is little more than a glorified cmp that knows where to
find particular test input and output files.
TODO: Make this more flexible with respect to ncv output format.
TODO: Make this work with a library version of the validator.
"""

import os
import sys
import tempfile

NCVBIN = "./ncval"

def CheckSystem(cmd):
  """Execute a command line in a sub-shell, checking return code and
  catching exceptions. Calls sys.exit() if there is a failure.

  Args:
    cmd: the command line
  """
  try:
    rc = os.system(cmd)
    if rc != 0:
      sys.stderr.write("%s returned non zero %d\n" % (cmd, rc))
      sys.exit(rc)
  except:
    sys.stderr.write("Command %s failed\n" % cmd)
    sys.exit(-1);

def RunNCV(bin, outpath):
  CheckSystem("%s %s > %s" % (NCVBIN, bin, outpath))

def CheckNCVOutput(expected, got):
  CheckSystem("cmp %s %s" % (expected, got))

def main():
  if len(sys.argv) != 2:
    sys.stderr.write("Usage: %s <testdir>\n" % sys.argv[0]);
    sys.exit(-1);
  binary = sys.argv[1] + "/bin"
  expectoutput = sys.argv[1] + "/ncv"
  tmpfd, tmppath = tempfile.mkstemp(suffix=".txt", text=True);
  try:
    os.close(tmpfd)
    RunNCV(binary, tmppath)
    CheckNCVOutput(expectoutput, tmppath)
  finally:
    os.unlink(tmppath)

main()

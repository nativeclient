/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ncdecode.c - table driven decoder for Native Client
 *
 * Most x86 decoders I've looked at are big case statements. While
 * this organization is fairly transparent and obvious, it tends to
 * lead to messy control flow (gotos, etc.) that make the decoder
 * more complicated, hence harder to maintain and harder to validate.
 *
 * This decoder is table driven, which will hopefully result in
 * substantially less code. Although the code+tables may be more
 * lines of code than a decoder built around a switch statement,
 * the smaller amount of actual procedural code and the regular
 * structure of the tables should make it easier to understand,
 * debug, and easier to become confident the decoder is correct.
 *
 * As it is specialized to Native Client, this decoder can also
 * benefit from any exclusions or simplifications we decide to
 * make in the dialect of x86 machine code accepted by Native
 * Client. Any such simplifications should ultimately be easily
 * recognized by inspection of the decoder configuration tables.
 * ALSO, the decoder mostly needs to worry about accurate
 * instruction lengths and finding opcodes. It does not need
 * to completely resolve the operands of all instructions.
 */
#include <stdio.h>
#include <assert.h>
#include "native_client/ncv/ncdecode.h"
#include "native_client/ncv/ncdecodetab.h"

#define DEBUGGING 0
#if DEBUGGING
#define dprint(s)   fprintf s
#else
#define dprint(s)
#endif
#define eprint(s)   fprintf s

static const uint32_t kPrefixTable[256] = {
  /* 0x00-0x0f */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0x10-0x1f */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0x20-0x2f */
  0, 0, 0, 0, 0, 0, kPrefixSEGES, 0, 0, 0, 0, 0, 0, 0, kPrefixSEGCS, 0,
  /* 0x30-0x3f */
  0, 0, 0, 0, 0, 0, kPrefixSEGSS, 0, 0, 0, 0, 0, 0, 0, kPrefixSEGDS, 0,
  /* 0x40-0x4f */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0x50-0x5f */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0x60-0x6f */
  0, 0, 0, 0, kPrefixSEGFS, kPrefixSEGGS, kPrefixDATA16, kPrefixADDR16,
  0, 0, 0, 0, 0, 0, 0, 0,
  /* 0x70-0x7f */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0x80-0x8f */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0x90-0x9f */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0xa0-0xaf */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0xb0-0xbf */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0xc0-0xcf */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0xd0-0xdf */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0xe0-0xef */
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0xf0-0xff */
  kPrefixLOCK, 0, kPrefixREPNE, kPrefixREP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

/* later this will make decoding x87 instructions a bit more concise. */
static const struct OpInfo *kDecodeX87Op[8] = { kDecode87D8,
                                                kDecode87D9,
                                                kDecode87DA,
                                                kDecode87DB,
                                                kDecode87DC,
                                                kDecode87DD,
                                                kDecode87DE,
                                                kDecode87DF };

static void NullDecoderAction(const struct NCDecoderState *mstate) {
  UNREFERENCED_PARAMETER(mstate);
}
static void NullDecoderStats(struct NCValidatorState *vstate) {
  UNREFERENCED_PARAMETER(vstate);
}
static void DefaultInternalError(struct NCValidatorState *vstate) {
  UNREFERENCED_PARAMETER(vstate);
}

NCDecoderAction g_DecoderAction = NullDecoderAction;
NCDecoderStats g_NewSegment = NullDecoderStats;
NCDecoderStats g_InternalError = DefaultInternalError;
NCDecoderStats g_SegFault = NullDecoderStats;

/* Error Condition Handling */
static void ErrorSegmentation(struct NCValidatorState *vstate) {
  eprint((stdout, "ErrorSegmentation\n"));
  /* When the decoder is used by the NaCl validator    */
  /* the validator provides an error handler that does */
  /* the necessary bookeeping to track these errors.   */
  g_SegFault(vstate);
}

static void ErrorInternal(struct NCValidatorState *vstate) {
  eprint((stdout, "ErrorInternal\n"));
  /* When the decoder is used by the NaCl validator    */
  /* the validator provides an error handler that does */
  /* the necessary bookeeping to track these errors.   */
  g_InternalError(vstate);
}

void InitDecoder(struct NCDecoderState *mstate) {
  mstate->inst.vaddr = mstate->vpc;
  mstate->inst.maddr = mstate->mpc;
  mstate->inst.prefixbytes = 0;
  mstate->inst.prefixmask = 0;
  mstate->inst.hasopbyte2 = 0;
  mstate->inst.hasopbyte3 = 0;
  mstate->inst.hassibbyte = 0;
  mstate->inst.mrm = 0;
  mstate->inst.immtype = IMM_UNKNOWN;
  mstate->inst.dispbytes = 0;
  mstate->inst.length = 0;
  mstate->opinfo = NULL;
}

/* at most four prefix bytes are allowed */
void ConsumePrefixBytes(struct NCDecoderState *mstate) {
  uint8_t nb;
  int ii;

  for (ii = 0; ii < kMaxPrefixBytes; ++ii) {
    nb = *mstate->nextbyte;
    if (kPrefixTable[nb] == 0) return;
    mstate->inst.prefixmask |= kPrefixTable[nb];
    mstate->inst.prefixbytes += 1;
    mstate->nextbyte += 1;
  }
}

static const struct OpInfo *GetExtendedOpInfo(struct NCDecoderState *mstate,
                                              uint8_t opbyte2) {
  uint32_t pm;
  pm = mstate->inst.prefixmask;
  if ((pm & (kPrefixDATA16 | kPrefixREPNE | kPrefixREP)) == 0) {
    return &kDecode0FXXOp[opbyte2];
  } else if (pm & kPrefixDATA16) {
    return &kDecode660FXXOp[opbyte2];
  } else if (pm & kPrefixREPNE) {
    return &kDecodeF20FXXOp[opbyte2];
  } else if (pm & kPrefixREP) {
    return &kDecodeF30FXXOp[opbyte2];
  }
  ErrorInternal(mstate->vstate);
  return mstate->opinfo;
}

static void GetX87OpInfo(struct NCDecoderState *mstate) {
  /* WAIT is an x87 instruction but not in the coproc opcode space. */
  const uint8_t kWAITOp = 0x9b;
  uint8_t kFirstX87Opcode = 0xd8;
  uint8_t kLastX87Opcode = 0xdf;
  uint8_t op1 = mstate->inst.maddr[mstate->inst.prefixbytes];
  if (op1 < kFirstX87Opcode || op1 > kLastX87Opcode) {
    if (op1 != kWAITOp) ErrorInternal(mstate->vstate);
    return;
  }
  mstate->opinfo = &kDecodeX87Op[op1 - kFirstX87Opcode][mstate->inst.mrm];
}

void ConsumeOpcodeBytes(struct NCDecoderState *mstate) {
  uint8_t opcode = *mstate->nextbyte;
  mstate->opinfo = &kDecode1ByteOp[opcode];
  mstate->nextbyte += 1;
  if (opcode == kTwoByteOpcodeByte1) {
    uint8_t opcode2 = *mstate->nextbyte;
    mstate->opinfo = GetExtendedOpInfo(mstate, opcode2);
    mstate->inst.hasopbyte2 = 1;
    mstate->nextbyte += 1;
    if (mstate->opinfo->insttype == NACLi_3BYTE) {
      uint8_t opcode3 = *mstate->nextbyte;
      uint32_t pm;
      pm = mstate->inst.prefixmask;
      mstate->nextbyte += 1;
      mstate->inst.hasopbyte3 = 1;

      dprint(("NACLi_3BYTE\n"));
      switch (opcode2) {
      case 0x38:        /* SSSE3, SSE4 */
        if (pm & kPrefixDATA16) {
          mstate->opinfo = &kDecode660F38Op[opcode3];
        } else if (pm & kPrefixREPNE) {
          mstate->opinfo = &kDecodeF20F38Op[opcode3];
        } else if (pm == 0) {
          mstate->opinfo = &kDecode0F38Op[opcode3];
        } else {
          /* Other prefixes like F3 cause an undefined instruction error. */
          /* Note from decoder table that NACLi_3BYTE is only used with   */
          /* data16 and repne prefixes.                                   */
          ErrorInternal(mstate->vstate);
        }
        break;
      case 0x3A:        /* SSSE3, SSE4 */
        if (pm & kPrefixDATA16) {
          mstate->opinfo = &kDecode660F3AOp[opcode3];
        } else if (pm == 0) {
          mstate->opinfo = &kDecode0F3AOp[opcode3];
        } else {
          /* Other prefixes like F3 cause an undefined instruction error. */
          /* Note from decoder table that NACLi_3BYTE is only used with   */
          /* data16 and repne prefixes.                                   */
          ErrorInternal(mstate->vstate);
        }
        break;
      default:
        /* if this happens there is a decoding table bug */
        ErrorInternal(mstate->vstate);
        break;
      }
    }
  }
  mstate->inst.immtype = mstate->opinfo->immtype;
}

void ConsumeModRM(struct NCDecoderState *mstate) {
  if (mstate->opinfo->hasmrmbyte != 0) {
    const uint8_t mrm = *mstate->nextbyte;
    mstate->inst.mrm = mrm;
    mstate->nextbyte += 1;
    if (mstate->opinfo->insttype == NACLi_X87) {
      GetX87OpInfo(mstate);
    }
    if (mstate->opinfo->opinmrm) {
      const struct OpInfo *mopinfo =
        &kDecodeModRMOp[mstate->opinfo->opinmrm][modrm_opcode(mrm)];
      mstate->opinfo = mopinfo;
      if (mstate->inst.immtype == IMM_UNKNOWN) {
        assert(0);
        mstate->inst.immtype = mopinfo->immtype;
      }
      /* handle weird case for 0xff TEST Ib/Iv */
      if (modrm_opcode(mrm) == 0) {
        if (mstate->inst.immtype == IMM_GROUP3_F6) {
          mstate->inst.immtype = IMM_FIXED1;
        }
        if (mstate->inst.immtype == IMM_GROUP3_F7) {
          mstate->inst.immtype = IMM_DATAV;
        }
      }
    }
    if (mstate->inst.prefixmask & kPrefixADDR16) {
      switch (modrm_mod(mrm)) {
        case 0:
          if (modrm_rm(mrm) == 0x06) mstate->inst.dispbytes = 2;  /* disp16 */
          else mstate->inst.dispbytes = 0;
          break;
        case 1:
          mstate->inst.dispbytes = 1;           /* disp8 */
          break;
        case 2:
          mstate->inst.dispbytes = 2;           /* disp16 */
          break;
        case 3:
          mstate->inst.dispbytes = 0;           /* no disp */
          break;
        default:
          ErrorInternal(mstate->vstate);
      }
      mstate->inst.hassibbyte = 0;
    } else {
      switch (modrm_mod(mrm)) {
        case 0:
          if (modrm_rm(mrm) == 0x05) mstate->inst.dispbytes = 4;  /* disp32 */
          else mstate->inst.dispbytes = 0;
          break;
        case 1:
          mstate->inst.dispbytes = 1;           /* disp8 */
          break;
        case 2:
          mstate->inst.dispbytes = 4;           /* disp32 */
          break;
        case 3:
          mstate->inst.dispbytes = 0;           /* no disp */
          break;
        default:
          ErrorInternal(mstate->vstate);
      }
      mstate->inst.hassibbyte = ((modrm_rm(mrm) == 0x04) &&
                                 (modrm_mod(mrm) != 3));
    }
  }
}

void ConsumeSIB(struct NCDecoderState *mstate) {
  if (mstate->inst.hassibbyte != 0) {
    const uint8_t sib = *mstate->nextbyte;
    mstate->nextbyte += 1;
    if (sib_base(sib) == 0x05) {
      switch (modrm_mod(mstate->inst.mrm)) {
      case 0: mstate->inst.dispbytes = 4; break;
      case 1: mstate->inst.dispbytes = 1; break;
      case 2: mstate->inst.dispbytes = 4; break;
      case 3:
      default:
        ErrorInternal(mstate->vstate);
      }
    }
  }
}

void ConsumeID(struct NCDecoderState *mstate) {
  if (mstate->inst.immtype == IMM_UNKNOWN) {
    ErrorInternal(mstate->vstate);
  }
  /* NOTE: NaCl allows at most one prefix byte */
  if (mstate->inst.prefixmask & kPrefixDATA16) {
    mstate->nextbyte += kImmTypeToSize66[mstate->inst.immtype];
  } else if (mstate->inst.prefixmask & kPrefixADDR16) {
    mstate->nextbyte += kImmTypeToSize67[mstate->inst.immtype];
  } else {
    mstate->nextbyte += kImmTypeToSize[mstate->inst.immtype];
  }
  mstate->nextbyte += mstate->inst.dispbytes;
  mstate->inst.length = mstate->nextbyte - mstate->mpc;
}

/* Actually this routine is special for 3DNow instructions */
void MaybeGet3ByteOpInfo(struct NCDecoderState *mstate) {
  if (mstate->opinfo->insttype == NACLi_3DNOW) {
    uint8_t opbyte1 = mstate->mpc[mstate->inst.prefixbytes];
    uint8_t opbyte2 = mstate->mpc[mstate->inst.prefixbytes + 1];
    uint8_t immbyte = mstate->mpc[mstate->inst.length - 1];
    if (opbyte1 == kTwoByteOpcodeByte1 &&
        opbyte2 == k3DNowOpcodeByte2) {
      mstate->opinfo = &kDecode0F0FOp[immbyte];
    }
  }
}

void NCDecodeRegisterCallbacks(NCDecoderAction decoderaction,
                               NCDecoderStats newsegment,
                               NCDecoderStats segfault,
                               NCDecoderStats internalerror) {
  if (decoderaction != NULL) g_DecoderAction = decoderaction;
  if (newsegment != NULL) g_NewSegment = newsegment;
  if (segfault != NULL) g_SegFault = segfault;
  if (internalerror != NULL) g_InternalError = internalerror;
}

struct NCDecoderState *PreviousInst(const struct NCDecoderState *mstate,
                                    int nindex) {
  int index = (mstate->dbindex + nindex + kDecodeBufferSize)
      & (kDecodeBufferSize - 1);
  return &mstate->decodebuffer[index];
}

/* The actual decoder */
void NCDecodeSegment(uint8_t *mbase, uint32_t vbase, size_t size,
                     struct NCValidatorState *vstate) {
  const uint32_t vlimit = vbase + size;
  struct NCDecoderState decodebuffer[kDecodeBufferSize];
  struct NCDecoderState *mstate;
  int dbindex;
  for (dbindex = 0; dbindex < kDecodeBufferSize; ++dbindex) {
    decodebuffer[dbindex].vstate = vstate;
    decodebuffer[dbindex].decodebuffer = decodebuffer;
    decodebuffer[dbindex].dbindex = dbindex;
    decodebuffer[dbindex].inst.length = 0;  /* indicates no instruction */
    decodebuffer[dbindex].vpc = 0;
    decodebuffer[dbindex].mpc = 0;
  }
  mstate = &decodebuffer[0];
  mstate->mpc = (uint8_t *)mbase;
  mstate->nextbyte = mbase;
  mstate->vpc = vbase;

  dprint(("DecodeSegment(%x-%x)\n", vbase, vlimit));
  g_NewSegment(mstate->vstate);
  while (mstate->vpc < vlimit) {
    uint32_t newpc;
    InitDecoder(mstate);
    ConsumePrefixBytes(mstate);
    ConsumeOpcodeBytes(mstate);
    ConsumeModRM(mstate);
    ConsumeSIB(mstate);
    ConsumeID(mstate);
    MaybeGet3ByteOpInfo(mstate);
    /* now scrutinize this instruction */
    newpc = mstate->vpc + mstate->inst.length;
    if (newpc > vlimit) {
      eprint((stdout, "%x > %x\n", newpc, vlimit));
      ErrorSegmentation(vstate);
      break;
    }
    g_DecoderAction(mstate);
    /* get read for next round */
    dbindex = (dbindex + 1) & (kDecodeBufferSize - 1);
    decodebuffer[dbindex].vpc = newpc;
    decodebuffer[dbindex].mpc = mstate->mpc + mstate->inst.length;
    decodebuffer[dbindex].nextbyte = mstate->nextbyte;
    mstate = &decodebuffer[dbindex];
  }
}

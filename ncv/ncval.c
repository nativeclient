/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ncval.c - command line validator for NaCl.
 * Mostly for testing.
 */
#include "native_client/include/portability.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include "native_client/include/nacl_elf.h"
#include <sys/timeb.h>
#include <time.h>
#include "ncfileutil.h"
#include "ncvalidate.h"
#include "ncvalidate_internaltypes.h"
#include "ncdecode.h"

static const char *progname;

#define VERBOSE 1
#if VERBOSE
#define vprint(args) printf args
#else
#define vprint(args)
#endif

#if NACL_WINDOWS
#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif

static int gettimeofday(struct timeval *tv, struct timezone *tz)
{
  struct __timeb32 time_info;

  if (NULL == tv) return -EFAULT;
  if (!_ftime32_s(&time_info)) {
    /* got the time */
    tv->tv_sec = time_info.time;
    /* translate from milli to micro seconds */
    tv->tv_usec = time_info.millitm * 1000;
  } else {
    return -EINVAL;
  }
  return 0;
}
#endif


void fatal(const char *fmt, ...)
{
  va_list ap;
  fprintf(stderr, "%s: fatal error: ", progname);
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fputc('\n', stderr);
  exit(2);
}

int AnalyzeSections(ncfile *ncf, struct NCValidatorState *vstate) {
  int badsections = 0;
  int ii;
  Elf32_Phdr *phdr = ncf->pheaders;

  for (ii = 0; ii < ncf->phnum; ii++) {
    vprint(("segment[%d] p_type %d p_offset %x vaddr %x paddr %x align %u\n",
            ii, phdr[ii].p_type, (uint32_t)phdr[ii].p_offset,
            (uint32_t)phdr[ii].p_vaddr, (uint32_t)phdr[ii].p_paddr,
            (uint32_t)phdr[ii].p_align));
    vprint(("    filesz %x memsz %x flags %x\n",
            phdr[ii].p_filesz, (uint32_t)phdr[ii].p_memsz,
            (uint32_t)phdr[ii].p_flags));
    if ((PT_LOAD != phdr[ii].p_type) ||
        (0 == (phdr[ii].p_flags & PF_X)))
      continue;
    vprint(("parsing segment %d\n", ii));
    /* note we use NCDecodeSegment instead of NCValidateSegment */
    /* because we don't want the check for a hlt at the end of  */
    /* the text segment as required by NaCl.                    */
    vstate->file_offset = phdr[ii].p_offset - phdr[ii].p_vaddr;
    NCDecodeSegment(ncf->data + (phdr[ii].p_vaddr - ncf->vbase),
                    phdr[ii].p_vaddr, phdr[ii].p_memsz, vstate);
  }
  return -badsections;
}


static void AnalyzeCodeSegments(ncfile *ncf, const char *fname) {
  uint32_t vbase, vlimit;
  struct NCValidatorState *vstate;

  GetVBaseAndLimit(ncf, &vbase, &vlimit);
  vstate = NCValidateInit(vbase, vlimit, ncf->ncalign);
  if (AnalyzeSections(ncf, vstate) < 0) {
    fprintf(stderr, "%s: text validate failed\n", fname);
  }
  if (NCValidateFinish(vstate) != 0) {
    vprint(("***MODULE %s IS UNSAFE***\n", fname));
  } else {
    vprint(("***module %s is safe***\n", fname));
  }
  Stats_Print(stdout, vstate);
  NCValidateFreeState(&vstate);
  vprint(("Validated %s\n", fname));
}

double timeval2double(const struct timeval *tv) {
  return ((double)tv->tv_sec + ((double)tv->tv_usec)/1000000);
}

void PrintTimes(FILE *f, const char *fname,
                struct timeval *t0,
                struct timeval *t1,
                struct timeval *t2) {
  double dt0, dt1, dt2;
  dt0 = timeval2double(t0);
  dt1 = timeval2double(t1);
  dt2 = timeval2double(t2);

  fprintf(f, "%s: %0.6f %0.6f\n", fname, dt1 - dt0, dt2 - dt1);
}

int main(int argc, const char *argv[])
{
  const char *loadname = argv[1];
  struct timeval time0, timel, timev;
  ncfile *ncf;

  progname = argv[0];
  if (loadname == NULL) {
    extern void ncvalidate_unittests();

    printf("no arguments given; running unit tests\n");
    ncvalidate_unittests();
    return 0;
  }

  gettimeofday(&time0, NULL);
  ncf = nc_loadfile(loadname);
  if (ncf == NULL)
    fatal("nc_loadfile(%s): %s\n", loadname, strerror(errno));

  gettimeofday(&timel, NULL);
  AnalyzeCodeSegments(ncf, loadname);
  gettimeofday(&timev, NULL);

  PrintTimes(stderr, loadname, &time0, &timel, &timev);

  nc_freefile(ncf);

  return 0;
}

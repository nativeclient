/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ncdecode_intable.h - table driven decoder for Native Client.
 *
 * This program generates C source for the ncdecode.c decoder tables.
 * It writes two C .h file to standard out, one for decoding and one
 * for disassembly, that contain six decoder tables.
 *
 */
#include "native_client/include/portability.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#define NEEDSNACLINSTTYPESTRING
#include "ncdecode.h"

typedef uint8_t bool;   /* zero or non-zero */

#define TODO

/* For handling prefixes we consume the prefix byte and then
 * record the state in this record.
 */
typedef struct OpMetaInfo {
  const char *disfmt;         /* as a short string, for printing */
  NaClInstType insttype;
  bool mrmbyte;         /* 0 or 1 */
  uint8_t immtype;      /* IMM_UNKNOWN .. IMM_DEFAULT_4 */
  uint8_t opinmrm;      /* 0 .. 8 */
} OpMetaInfo;

/* The decoder input table is an array of instruction definitions, */
/* defined using OpMetaInfo as defined in ncdecode.h               */
OpMetaInfo *g_Op1ByteTable[NCDTABLESIZE];
/* two byte opcode tables */
OpMetaInfo *g_Op0FXXMetaTable[NCDTABLESIZE];
OpMetaInfo *g_OpF20FXXTable[NCDTABLESIZE];
OpMetaInfo *g_OpF30FXXTable[NCDTABLESIZE];
OpMetaInfo *g_Op660FXXTable[NCDTABLESIZE];
/* three byte opcode tables */
OpMetaInfo *g_Op0F0FTable[NCDTABLESIZE];
OpMetaInfo *g_Op0F38Table[NCDTABLESIZE];
OpMetaInfo *g_Op660F38Table[NCDTABLESIZE];
OpMetaInfo *g_OpF20F38Table[NCDTABLESIZE];
OpMetaInfo *g_Op0F3ATable[NCDTABLESIZE];
OpMetaInfo *g_Op660F3ATable[NCDTABLESIZE];
/* tables for opcodes in ModRM */
OpMetaInfo *g_ModRMOpTable[kNaClMRMGroupsRange][kModRMOpcodeGroupSize];
/* x87 opcode tables */
OpMetaInfo *g_Op87D8[NCDTABLESIZE];
OpMetaInfo *g_Op87D9[NCDTABLESIZE];
OpMetaInfo *g_Op87DA[NCDTABLESIZE];
OpMetaInfo *g_Op87DB[NCDTABLESIZE];
OpMetaInfo *g_Op87DC[NCDTABLESIZE];
OpMetaInfo *g_Op87DD[NCDTABLESIZE];
OpMetaInfo *g_Op87DE[NCDTABLESIZE];
OpMetaInfo *g_Op87DF[NCDTABLESIZE];
static OpMetaInfo DecodeUndefinedInstInfo = {
  "undefined", NACLi_UNDEFINED, 0, IMM_NONE, 0
};

/* Note: in general all errors in this module will be fatal.
 * To debug: use gdb or your favorite debugger.
 */
static void fatal(const char *s) {
  fprintf(stderr, "%s\n", s);
  fprintf(stderr, "fatal error, cannot recover\n");
  exit(-1);
}

/* operandsize == 0 implies operand size determined by operand size attributes
 * opbytes: length of opcode, one or two bytes
 * mrmbyte: 0 or 1, 1 if mrmbyte is present
 * immbytes: bytes of immediate: 1, 2, 4
 * itype: decoder treatment, an NaClInstType as in ncdecode.h
 * disfmt: format string for disassembly
 */
static OpMetaInfo *NewMetaDefn(const bool mrmbyte, const uint8_t immtype,
                               const NaClInstType itype, const char *disfmt) {
  OpMetaInfo *imd = (OpMetaInfo *)malloc(sizeof(*imd));
  if (imd == NULL) {
    fatal("NewMetaDefn: malloc failed");
  }
  imd->insttype = itype;
  imd->disfmt = disfmt;
  imd->mrmbyte = mrmbyte;
  imd->immtype = immtype;
  imd->opinmrm = 0;
  return imd;
}

static void EncodeOpByte1(const uint8_t byte1, const bool mrmbyte,
                          const uint8_t immtype,
                          const NaClInstType itype, const char *disfmt) {
  g_Op1ByteTable[byte1] = NewMetaDefn(mrmbyte, immtype, itype, disfmt);
}

static void EncodeOp0F(const uint8_t byte2, const bool mrmbyte,
                       const uint8_t immtype,
                       const NaClInstType itype, const char *disfmt) {
  g_Op0FXXMetaTable[byte2] = NewMetaDefn(mrmbyte, immtype, itype, disfmt);
}

static void EncodeOp660F(const uint8_t byte2, const bool mrmbyte,
                         const uint8_t immtype,
                         const NaClInstType itype, const char *disfmt) {
  g_Op660FXXTable[byte2] = NewMetaDefn(mrmbyte, immtype, itype, disfmt);
}

static void EncodeOpF20F(const uint8_t byte2, const bool mrmbyte,
                         const uint8_t immtype,
                         const NaClInstType itype, const char *disfmt) {
  g_OpF20FXXTable[byte2] = NewMetaDefn(mrmbyte, immtype, itype, disfmt);
}

static void EncodeOpF30F(const uint8_t byte2, const bool mrmbyte,
                         const uint8_t immtype,
                         const NaClInstType itype, const char *disfmt) {
  g_OpF30FXXTable[byte2] = NewMetaDefn(mrmbyte, immtype, itype, disfmt);
}

static void EncodeOp0F0F(const uint8_t byte2, const bool mrmbyte,
                         const uint8_t immtype,
                         const NaClInstType itype, const char *disfmt) {
  g_Op0F0FTable[byte2] = NewMetaDefn(mrmbyte, immtype, itype, disfmt);
}

static void EncodeOp0F38(const uint8_t byte2, const bool mrmbyte,
                         const uint8_t immtype,
                         const NaClInstType itype, const char *disfmt) {
  g_Op0F38Table[byte2] = NewMetaDefn(mrmbyte, immtype, itype, disfmt);
}

static void EncodeOp660F38(const uint8_t byte2, const bool mrmbyte,
                           const uint8_t immtype,
                           const NaClInstType itype, const char *disfmt) {
  g_Op660F38Table[byte2] = NewMetaDefn(mrmbyte, immtype, itype, disfmt);
}

static void EncodeOpF20F38(const uint8_t byte2, const bool mrmbyte,
                           const uint8_t immtype,
                           const NaClInstType itype, const char *disfmt) {
  g_OpF20F38Table[byte2] = NewMetaDefn(mrmbyte, immtype, itype, disfmt);
}

static void EncodeOp0F3A(const uint8_t byte2, const bool mrmbyte,
                         const uint8_t immtype,
                         const NaClInstType itype, const char *disfmt) {
  g_Op0F3ATable[byte2] = NewMetaDefn(mrmbyte, immtype, itype, disfmt);
}

static void EncodeOp660F3A(const uint8_t byte2, const bool mrmbyte,
                           const uint8_t immtype,
                           const NaClInstType itype, const char *disfmt) {
  g_Op660F3ATable[byte2] = NewMetaDefn(mrmbyte, immtype, itype, disfmt);
}

static void EncodeModRMOp(const uint8_t group, const uint8_t nnn,
                          const NaClInstType itype, const char *disfmt) {
  OpMetaInfo *idefn = NewMetaDefn(1, IMM_NONE, itype, disfmt);
  g_ModRMOpTable[group][nnn] = idefn;
}

static void SetOpByte1OpInMRM(const uint8_t byte1, const uint8_t group) {
  assert(g_Op1ByteTable[byte1] != &DecodeUndefinedInstInfo);
  g_Op1ByteTable[byte1]->opinmrm = group;
}

/* TODO - this function generates an error since it is not used */
#if 0
static void SetModRMOpImmType(const uint8_t group, const uint8_t nnn,
                              const uint8_t it) {
  g_ModRMOpTable[group][nnn]->immtype = it;
}
#endif
static void SetOp0FOpInMRM(const uint8_t byte2, const uint8_t group) {
  assert(g_Op0FXXMetaTable[byte2] != &DecodeUndefinedInstInfo);
  g_Op0FXXMetaTable[byte2]->opinmrm = group;
}

static void SetOp66OpInMRM(const uint8_t byte2, const uint8_t group) {
  assert(g_Op660FXXTable[byte2] != &DecodeUndefinedInstInfo);
  g_Op660FXXTable[byte2]->opinmrm = group;
}

/* TODO - this function generates an error since it is not used */
#if 0
static void SetOpF2OpInMRM(const uint8_t byte2, const uint8_t group) {
  assert(g_OpF20FXXTable[byte2] != &DecodeUndefinedInstInfo);
  g_OpF20FXXTable[byte2]->opinmrm = group;
}

static void SetOpF3OpInMRM(const uint8_t byte2, const uint8_t group) {
  assert(g_OpF30FXXTable[byte2] != &DecodeUndefinedInstInfo);
  g_OpF30FXXTable[byte2]->opinmrm = group;
}
#endif

extern int asprintf(char **s, const char *fmt, ...);
/* note that this function accepts only one string in s */
static char *aprintf(const char *fmt, const char *s) {
  char *rstring = NULL;
  /* there will be one spare byte, because %s is replaced, but it's ok */
  int length = strlen(fmt) + strlen(s);
  rstring = malloc(length);
  if (rstring != NULL) {
    sprintf(rstring,fmt,s);
  } else {
    fprintf(stderr, "malloc failed in aprintf (%s, %s) \n", fmt, s);
  }
  return rstring;
}

static void ALUOperandVariants00_05(const uint8_t base,
                                    const NaClInstType itype,
                                    const char *ocstr) {
  EncodeOpByte1(base,   1, IMM_NONE, itype, aprintf("%s  $Eb, $Gb", ocstr));
  EncodeOpByte1(base+1, 1, IMM_NONE, itype, aprintf("%s  $Ev, $Gv", ocstr));
  EncodeOpByte1(base+2, 1, IMM_NONE, itype, aprintf("%s  $Gb, $Eb", ocstr));
  EncodeOpByte1(base+3, 1, IMM_NONE, itype, aprintf("%s  $Gv, $Ev", ocstr));
  EncodeOpByte1(base+4, 0, IMM_FIXED1, itype, aprintf("%s  %%al, $Ib", ocstr));
  EncodeOpByte1(base+5, 0, IMM_DATAV, itype, aprintf("%s  %%eax, $Iz", ocstr));
}

static void OneRegVariants00_07(const uint8_t base,
                                const NaClInstType itype,
                                const char *ocstr) {
  EncodeOpByte1(base,   0, IMM_NONE, itype, aprintf("%s  %%eax", ocstr));
  EncodeOpByte1(base+1, 0, IMM_NONE, itype, aprintf("%s  %%ecx", ocstr));
  EncodeOpByte1(base+2, 0, IMM_NONE, itype, aprintf("%s  %%edx", ocstr));
  EncodeOpByte1(base+3, 0, IMM_NONE, itype, aprintf("%s  %%ebx", ocstr));
  EncodeOpByte1(base+4, 0, IMM_NONE, itype, aprintf("%s  %%esp", ocstr));
  EncodeOpByte1(base+5, 0, IMM_NONE, itype, aprintf("%s  %%ebp", ocstr));
  EncodeOpByte1(base+6, 0, IMM_NONE, itype, aprintf("%s  %%esi", ocstr));
  EncodeOpByte1(base+7, 0, IMM_NONE, itype, aprintf("%s  %%edi", ocstr));
}

/* TODO - this function generates an error since it is not used */
#if 0
static char *asmprint(char **asmstr, const char *opcode,
                      const char *reg, const char *dest) {
  if (asprintf(asmstr, "%s %s, %s", opcode, reg, dest) < 0) {
      fatal("asprintf failed");
  }
  return *asmstr;
}
#endif

static void InitializeGlobalTables() {
  int i, j;
  /* pre-initialize g_Op1ByteTable */
  for (i = 0; i < NCDTABLESIZE; i++) {
    g_Op1ByteTable[i] = &DecodeUndefinedInstInfo;
    g_Op0FXXMetaTable[i] = &DecodeUndefinedInstInfo;
    g_Op660FXXTable[i] = &DecodeUndefinedInstInfo;
    g_OpF20FXXTable[i] = &DecodeUndefinedInstInfo;
    g_OpF30FXXTable[i] = &DecodeUndefinedInstInfo;
    g_Op0F0FTable[i] = &DecodeUndefinedInstInfo;
    g_Op0F38Table[i] = &DecodeUndefinedInstInfo;
    g_Op660F38Table[i] = &DecodeUndefinedInstInfo;
    g_OpF20F38Table[i] = &DecodeUndefinedInstInfo;
    g_Op0F3ATable[i] = &DecodeUndefinedInstInfo;
    g_Op660F3ATable[i] = &DecodeUndefinedInstInfo;
    g_Op87D8[i] = &DecodeUndefinedInstInfo;
    g_Op87D9[i] = &DecodeUndefinedInstInfo;
    g_Op87DA[i] = &DecodeUndefinedInstInfo;
    g_Op87DB[i] = &DecodeUndefinedInstInfo;
    g_Op87DC[i] = &DecodeUndefinedInstInfo;
    g_Op87DD[i] = &DecodeUndefinedInstInfo;
    g_Op87DE[i] = &DecodeUndefinedInstInfo;
    g_Op87DF[i] = &DecodeUndefinedInstInfo;
  }
  for (i = 0; i < kNaClMRMGroupsRange; i++) {
    for (j = 0; j < 8; j++) {
      g_ModRMOpTable[i][j] = &DecodeUndefinedInstInfo;
    }
  }
}


static void Encode87Op(OpMetaInfo *g87tab[],
                       const char *set1[8],
                       const char *set2[64]) {
  int j, reg;
  uint8_t i;

  for (i = 0; i < 0xc0; i++) {
    reg = modrm_reg(i);
    if (set1[reg] == NULL) {
      g87tab[i] = NewMetaDefn(1, IMM_NONE, NACLi_INVALID, "invalid");
    } else {
      g87tab[i] = NewMetaDefn(1, IMM_NONE, NACLi_X87, set1[reg]);
    }
  }
  for (j = 0xc0; j < 0x100; j++) {
    if (set2[j - 0xc0] == NULL) {
      g87tab[j] = NewMetaDefn(1, IMM_NONE, NACLi_INVALID, "invalid");
    } else {
      g87tab[j] = NewMetaDefn(1, IMM_NONE, NACLi_X87, set2[j - 0xc0]);
    }
  }
}

static void BuildSSE4Tables() {
  TODO;
  EncodeOp660F(0x38, 1, IMM_NONE, NACLi_3BYTE, "SSE4");
  EncodeOp660F(0x3A, 1, IMM_FIXED1, NACLi_3BYTE, "SSE4");
  EncodeOpF20F(0x38, 0, IMM_NONE, NACLi_3BYTE, "SSE4");

  EncodeOp660F38(0x00, 1, IMM_NONE, NACLi_SSSE3, "pshufb $V, $W");
  EncodeOp660F38(0x01, 1, IMM_NONE, NACLi_SSSE3, "phaddw $V, $W");
  EncodeOp660F38(0x02, 1, IMM_NONE, NACLi_SSSE3, "phaddd $V, $W");
  EncodeOp660F38(0x03, 1, IMM_NONE, NACLi_SSSE3, "phaddsw $V, $W");
  EncodeOp660F38(0x04, 1, IMM_NONE, NACLi_SSSE3, "pmaddubsw $V, $W");
  EncodeOp660F38(0x05, 1, IMM_NONE, NACLi_SSSE3, "phsubw $V, $W");
  EncodeOp660F38(0x06, 1, IMM_NONE, NACLi_SSSE3, "phsubd $V, $W");
  EncodeOp660F38(0x07, 1, IMM_NONE, NACLi_SSSE3, "phsubsw $V, $W");

  EncodeOp660F38(0x10, 1, IMM_NONE, NACLi_SSE41, "pblendvb $V, $W");
  EncodeOp660F38(0x14, 1, IMM_NONE, NACLi_SSE41, "blendvps $V, $W");
  EncodeOp660F38(0x15, 1, IMM_NONE, NACLi_SSE41, "blendvpd $V, $W");
  EncodeOp660F38(0x17, 1, IMM_NONE, NACLi_SSE41, "ptest $V, $W");

  EncodeOp660F38(0x20, 1, IMM_NONE, NACLi_SSE41, "pmovsxbw $V, $U/M");
  EncodeOp660F38(0x21, 1, IMM_NONE, NACLi_SSE41, "pmovsxbd $V, $U/M");
  EncodeOp660F38(0x22, 1, IMM_NONE, NACLi_SSE41, "pmovsxbq $V, $U/M");
  EncodeOp660F38(0x23, 1, IMM_NONE, NACLi_SSE41, "pmovsxwd $V, $U/M");
  EncodeOp660F38(0x24, 1, IMM_NONE, NACLi_SSE41, "pmovsxwq $V, $U/M");
  EncodeOp660F38(0x25, 1, IMM_NONE, NACLi_SSE41, "pmovsxdq $V, $U/M");

  EncodeOp660F38(0x30, 1, IMM_NONE, NACLi_SSE41, "pmovzxbw $V, $U/M");
  EncodeOp660F38(0x31, 1, IMM_NONE, NACLi_SSE41, "pmovzxbd $V, $U/M");
  EncodeOp660F38(0x32, 1, IMM_NONE, NACLi_SSE41, "pmovzxbq $V, $U/M");
  EncodeOp660F38(0x33, 1, IMM_NONE, NACLi_SSE41, "pmovzxwd $V, $U/M");
  EncodeOp660F38(0x34, 1, IMM_NONE, NACLi_SSE41, "pmovzxwq $V, $U/M");
  EncodeOp660F38(0x35, 1, IMM_NONE, NACLi_SSE41, "pmovzxdq $V, $U/M");
  EncodeOp660F38(0x37, 1, IMM_NONE, NACLi_SSE42, "pcmpgtq $V, $U/M");

  EncodeOp660F38(0x40, 1, IMM_NONE, NACLi_SSE41, "pmulld $V, $W");
  EncodeOp660F38(0x41, 1, IMM_NONE, NACLi_SSE41, "phminposuw $V, $W");

  EncodeOp660F38(0x80, 1, IMM_NONE, NACLi_INVALID, "NVEPT $G, $M");
  EncodeOp660F38(0x81, 1, IMM_NONE, NACLi_INVALID, "NVVPID $G, $M");

  EncodeOp0F38(0xf0, 1, IMM_NONE, NACLi_MOVBE, "MOVBE $G, $M");
  EncodeOp0F38(0xf1, 1, IMM_NONE, NACLi_MOVBE, "MOVBE $M, $G");
  EncodeOpF20F38(0xf0, 1, IMM_NONE, NACLi_SSE42, "CRC32 $Gd, $Eb");
  EncodeOpF20F38(0xf1, 1, IMM_NONE, NACLi_SSE42, "CRC32 $Gd, $Ev");

  EncodeOp660F38(0x08, 1, IMM_NONE, NACLi_SSSE3, "psignb $V, $W");
  EncodeOp660F38(0x09, 1, IMM_NONE, NACLi_SSSE3, "psignw $V, $W");
  EncodeOp660F38(0x0a, 1, IMM_NONE, NACLi_SSSE3, "psignd $V, $W");
  EncodeOp660F38(0x0b, 1, IMM_NONE, NACLi_SSSE3, "pmulhrsw $V, $W");

  EncodeOp660F38(0x1c, 1, IMM_NONE, NACLi_SSSE3, "pabsb $V, $W");
  EncodeOp660F38(0x1d, 1, IMM_NONE, NACLi_SSSE3, "pabsw $V, $W");
  EncodeOp660F38(0x1e, 1, IMM_NONE, NACLi_SSSE3, "pabsd $V, $W");

  EncodeOp660F38(0x28, 1, IMM_NONE, NACLi_SSE41, "pmuldq $V, $W");
  EncodeOp660F38(0x29, 1, IMM_NONE, NACLi_SSE41, "pcmpeqq $V, $W");
  EncodeOp660F38(0x2a, 1, IMM_NONE, NACLi_SSE41, "movntdqa $V, $W");
  EncodeOp660F38(0x2b, 1, IMM_NONE, NACLi_SSE41, "packusdw $V, $W");

  EncodeOp660F38(0x38, 1, IMM_NONE, NACLi_SSE41, "pminsb $V, $W");
  EncodeOp660F38(0x39, 1, IMM_NONE, NACLi_SSE41, "pminsd $V, $W");
  EncodeOp660F38(0x3a, 1, IMM_NONE, NACLi_SSE41, "pminuw $V, $W");
  EncodeOp660F38(0x3b, 1, IMM_NONE, NACLi_SSE41, "pminud $V, $W");
  EncodeOp660F38(0x3c, 1, IMM_NONE, NACLi_SSE41, "pmaxsb $V, $W");
  EncodeOp660F38(0x3d, 1, IMM_NONE, NACLi_SSE41, "pmaxsd $V, $W");
  EncodeOp660F38(0x3e, 1, IMM_NONE, NACLi_SSE41, "pmaxuw $V, $W");
  EncodeOp660F38(0x3f, 1, IMM_NONE, NACLi_SSE41, "pmaxud $V, $W");

  EncodeOp660F3A(0x14, 1, IMM_FIXED1, NACLi_SSE41, "pextrb $R/M, $V, $Ib");
  EncodeOp660F3A(0x15, 1, IMM_FIXED1, NACLi_SSE41, "pextrw $R/M, $V, $Ib");
  EncodeOp660F3A(0x16, 1, IMM_FIXED1, NACLi_SSE41, "pextrd/q $E, $V, $Ib");
  EncodeOp660F3A(0x17, 1, IMM_FIXED1, NACLi_SSE41, "extractps $E, $V, $Ib");

  EncodeOp660F3A(0x20, 1, IMM_FIXED1, NACLi_SSE41, "pinsrb $V, $R/M, $Ib");
  EncodeOp660F3A(0x21, 1, IMM_FIXED1, NACLi_SSE41, "insertps $V, $U/M, $Ib");
  EncodeOp660F3A(0x22, 1, IMM_FIXED1, NACLi_SSE41, "pinsrd/q $V, $E, $Ib");

  EncodeOp660F3A(0x40, 1, IMM_FIXED1, NACLi_SSE41, "dpps $V, $W, $Ib");
  EncodeOp660F3A(0x41, 1, IMM_FIXED1, NACLi_SSE41, "dppd $V, $W, $Ib");
  EncodeOp660F3A(0x42, 1, IMM_FIXED1, NACLi_SSE41, "mpsadbw $V, $W, $Ib");

  EncodeOp660F3A(0x60, 1, IMM_FIXED1, NACLi_SSE42, "pcmpestrm $V, $W, $Ib");
  EncodeOp660F3A(0x61, 1, IMM_FIXED1, NACLi_SSE42, "pcmpestri $V, $W, $Ib");
  EncodeOp660F3A(0x62, 1, IMM_FIXED1, NACLi_SSE42, "pcmpistrm $V, $W, $Ib");
  EncodeOp660F3A(0x63, 1, IMM_FIXED1, NACLi_SSE42, "pcmpistri $V, $W, $Ib");

  EncodeOp660F3A(0x08, 1, IMM_FIXED1, NACLi_SSE41, "roundps $V, $W, $Ib");
  EncodeOp660F3A(0x09, 1, IMM_FIXED1, NACLi_SSE41, "roundpd $V, $W, $Ib");
  EncodeOp660F3A(0x0a, 1, IMM_FIXED1, NACLi_SSE41, "roundss $V, $W, $Ib");
  EncodeOp660F3A(0x0b, 1, IMM_FIXED1, NACLi_SSE41, "roundsd $V, $W, $Ib");
  EncodeOp660F3A(0x0c, 1, IMM_FIXED1, NACLi_SSE41, "blendps $V, $W, $Ib");
  EncodeOp660F3A(0x0d, 1, IMM_FIXED1, NACLi_SSE41, "blendpd $V, $W, $Ib");
  EncodeOp660F3A(0x0e, 1, IMM_FIXED1, NACLi_SSE41, "pblendw $V, $W, $Ib");
  EncodeOp660F3A(0x0f, 1, IMM_FIXED1, NACLi_SSSE3, "palignr $V, $W, $Ib");
}

static void Buildx87Tables() {
  int i;
  /* since these are so repetative I'm using a little bit of a hack */
  /* to make this more concise.                                     */
  static const char *k87D8Table1[8] =
    {"fadd", "fmul", "fcom", "fcomp", "fsub", "fsubr", "fdiv", "fdivr"};
  static const char *k87D8Table2[64] = {
    "fadd", "fadd", "fadd", "fadd", "fadd", "fadd", "fadd", "fadd",
    "fmul", "fmul", "fmul", "fmul", "fmul", "fmul", "fmul", "fmul",
    "fcom", "fcom", "fcom", "fcom", "fcom", "fcom", "fcom", "fcom",
    "fcomp", "fcomp", "fcomp", "fcomp", "fcomp", "fcomp", "fcomp", "fcomp",
    "fsub", "fsub", "fsub", "fsub", "fsub", "fsub", "fsub", "fsub",
    "fsubr", "fsubr", "fsubr", "fsubr", "fsubr", "fsubr", "fsubr", "fsubr",
    "fdiv", "fdiv", "fdiv", "fdiv", "fdiv", "fdiv", "fdiv", "fdiv",
    "fdivr", "fdivr", "fdivr", "fdivr", "fdivr", "fdivr", "fdivr", "fdivr"};

  static const char *k87D9Table1[8] =
    {"fld", NULL, "fst", "fstp", "fldenv", "fldcw", "fnstenv", "fnstcw"};
  static const char *k87D9Table2[64] = {
    "fld", "fld", "fld", "fld", "fld", "fld", "fld", "fld",
    "fxch", "fxch", "fxch", "fxch", "fxch", "fxch", "fxch", "fxch",
    "fnop", NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    "fchs", "fabs", NULL, NULL, "ftst", "fxam", NULL, NULL,
    "fld1", "fldl2t", "fldl2e", "fldpi", "fldlg2", "fldln2", "fldz", NULL,
    "f2xm1", "fyl2x", "fptan", "fpatan",
    "fxtract", "fprem1", "fdecstp", "fincstp",
    "fprem", "fyl2xp1", "fsqrt", "fsincos",
    "frndint", "fscale", "fsin", "fcos" };

  static const char *k87DATable1[8] =
    {"fiadd", "fimul", "ficom", "ficomp", "fisub", "fisubr", "fidiv", "fidivr"};
  static const char *k87DATable2[64] = {
    "fcmovb", "fcmovb", "fcmovb", "fcmovb",
    "fcmovb", "fcmovb", "fcmovb", "fcmovb",
    "fcmove", "fcmove", "fcmove", "fcmove",
    "fcmove", "fcmove", "fcmove", "fcmove",
    "fcmovbe", "fcmovbe", "fcmovbe", "fcmovbe",
    "fcmovbe", "fcmovbe", "fcmovbe", "fcmovbe",
    "fcmovu", "fcmovu", "fcmovu", "fcmovu",
    "fcmovu", "fcmovu", "fcmovu", "fcmovu",
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, "fucompp", NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };

  static const char *k87DBTable1[8] =
    {"fild", "fisttp", "fist", "fistp", NULL, "fld", NULL, "fstp"};
  static const char *k87DBTable2[64] = {
    "fcmovnb", "fcmovnb", "fcmovnb", "fcmovnb",
    "fcmovnb", "fcmovnb", "fcmovnb", "fcmovnb",
    "fcmovne", "fcmovne", "fcmovne", "fcmovne",
    "fcmovne", "fcmovne", "fcmovne", "fcmovne",
    "fcmovnbe", "fcmovnbe", "fcmovnbe", "fcmovnbe",
    "fcmovnbe", "fcmovnbe", "fcmovnbe", "fcmovnbe",
    "fcmovnu", "fcmovnu", "fcmovnu", "fcmovnu",
    "fcmovnu", "fcmovnu", "fcmovnu", "fcmovnu",
    NULL, NULL, "fnclex", "fninit", NULL, NULL, NULL, NULL,
    "fucomi", "fucomi", "fucomi", "fucomi",
    "fucomi", "fucomi", "fucomi", "fucomi",
    "fcomi", "fcomi", "fcomi", "fcomi",
    "fcomi", "fcomi", "fcomi", "fcomi",
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

  static const char *k87DCTable1[8] =
    {"fadd", "fmul", "fcom", "fcomp", "fsub", "fsubr", "fdiv", "fdivr"};
  static const char *k87DCTable2[64] = {
    "fadd", "fadd", "fadd", "fadd", "fadd", "fadd", "fadd", "fadd",
    "fmul", "fmul", "fmul", "fmul", "fmul", "fmul", "fmul", "fmul",
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    "fsubr", "fsubr", "fsubr", "fsubr", "fsubr", "fsubr", "fsubr", "fsubr",
    "fsub", "fsub", "fsub", "fsub", "fsub", "fsub", "fsub", "fsub",
    "fdivr", "fdivr", "fdivr", "fdivr", "fdivr", "fdivr", "fdivr", "fdivr",
    "fdiv", "fdiv", "fdiv", "fdiv", "fdiv", "fdiv", "fdiv", "fdiv" };

  static const char *k87DDTable1[8] =
    {"fld", "fisttp", "fst", "fstp", "frstor", NULL, "fnsave", "fnstsw"};
  static const char *k87DDTable2[64] = {
    "ffree", "ffree", "ffree", "ffree", "ffree", "ffree", "ffree", "ffree",
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    "fst", "fst", "fst", "fst", "fst", "fst", "fst", "fst",
    "fstp", "fstp", "fstp", "fstp", "fstp", "fstp", "fstp", "fstp",
    "fucom", "fucom", "fucom", "fucom", "fucom", "fucom", "fucom", "fucom",
    "fucomp", "fucomp", "fucomp", "fucomp",
    "fucomp", "fucomp", "fucomp", "fucomp",
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };

  static const char *k87DETable1[8] =
    {"fiadd", "fimul", "ficom", "ficomp", "fisub", "fisubr", "fidiv", "fidivr"};
  static const char *k87DETable2[64] = {
    "faddp", "faddp", "faddp", "faddp", "faddp", "faddp", "faddp", "faddp",
    "fmulp", "fmulp", "fmulp", "fmulp", "fmulp", "fmulp", "fmulp", "fmulp",
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, "fcompp", NULL, NULL, NULL, NULL, NULL, NULL,
    "fsubrp", "fsubrp", "fsubrp", "fsubrp",
    "fsubrp", "fsubrp", "fsubrp", "fsubrp",
    "fsubp", "fsubp", "fsubp", "fsubp",
    "fsubp", "fsubp", "fsubp", "fsubp",
    "fdivrp", "fdivrp", "fdivrp", "fdivrp",
    "fdivrp", "fdivrp", "fdivrp", "fdivrp",
    "fdivp", "fdivp", "fdivp", "fdivp",
    "fdivp", "fdivp", "fdivp", "fdivp"};

  static const char *k87DFTable1[8] =
    {"fild", "fisttp", "fist", "fistp", "fbld", "fild", "fbstp", "fistp"};
  static const char *k87DFTable2[64] = {
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    "fnstsw", NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    "fucomip", "fucomip", "fucomip", "fucomip",
    "fucomip", "fucomip", "fucomip", "fucomip",
    "fcomip", "fcomip", "fcomip", "fcomip",
    "fcomip", "fcomip", "fcomip", "fcomip",
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };

  Encode87Op(g_Op87D8, k87D8Table1, k87D8Table2);
  Encode87Op(g_Op87D9, k87D9Table1, k87D9Table2);
  Encode87Op(g_Op87DA, k87DATable1, k87DATable2);
  Encode87Op(g_Op87DB, k87DBTable1, k87DBTable2);
  Encode87Op(g_Op87DC, k87DCTable1, k87DCTable2);
  Encode87Op(g_Op87DD, k87DDTable1, k87DDTable2);
  Encode87Op(g_Op87DE, k87DETable1, k87DETable2);
  Encode87Op(g_Op87DF, k87DFTable1, k87DFTable2);
  /* fix instruction type for x87 conditional moves */
  for (i = 0; i < 8; i++) {
    g_Op87DA[0xc0 + i]->insttype = NACLi_FCMOV;
    g_Op87DA[0xc8 + i]->insttype = NACLi_FCMOV;
    g_Op87DA[0xd0 + i]->insttype = NACLi_FCMOV;
    g_Op87DA[0xd8 + i]->insttype = NACLi_FCMOV;
    g_Op87DB[0xc0 + i]->insttype = NACLi_FCMOV;
    g_Op87DB[0xc8 + i]->insttype = NACLi_FCMOV;
    g_Op87DB[0xd0 + i]->insttype = NACLi_FCMOV;
    g_Op87DB[0xd8 + i]->insttype = NACLi_FCMOV;
  }
}

static void BuildMetaTables() {
  InitializeGlobalTables();
  /* now add the real contents */
  /* Eight opcode groups with a regular pattern... */
  ALUOperandVariants00_05(0x00, NACLi_386L, "add");
  ALUOperandVariants00_05(0x08, NACLi_386L, "or");
  ALUOperandVariants00_05(0x10, NACLi_386L, "adc");
  ALUOperandVariants00_05(0x18, NACLi_386L, "sbb");
  ALUOperandVariants00_05(0x20, NACLi_386L, "and");
  ALUOperandVariants00_05(0x28, NACLi_386L, "sub");
  ALUOperandVariants00_05(0x30, NACLi_386L, "xor");
  ALUOperandVariants00_05(0x38, NACLi_386, "cmp");
  /* Fill in gaps between 00 and 0x40 */
  EncodeOpByte1(0x06, 0, IMM_NONE, NACLi_ILLEGAL, "push %es");
  EncodeOpByte1(0x16, 0, IMM_NONE, NACLi_ILLEGAL, "push %ss");
  EncodeOpByte1(0x26, 0, IMM_NONE, NACLi_ILLEGAL, "[seg %es]");
  EncodeOpByte1(0x36, 0, IMM_NONE, NACLi_ILLEGAL, "[seg %ss]");
  EncodeOpByte1(0x07, 0, IMM_NONE, NACLi_ILLEGAL, "pop %es");
  EncodeOpByte1(0x17, 0, IMM_NONE, NACLi_ILLEGAL, "pop %ss");
  EncodeOpByte1(0x27, 0, IMM_NONE, NACLi_ILLEGAL, "daa");
  EncodeOpByte1(0x37, 0, IMM_NONE, NACLi_ILLEGAL, "aaa");  /* deprecated */
  EncodeOpByte1(0x0e, 0, IMM_NONE, NACLi_ILLEGAL, "push %cs");
  EncodeOpByte1(0x1e, 0, IMM_NONE, NACLi_ILLEGAL, "push %ds");
  EncodeOpByte1(0x2e, 0, IMM_NONE, NACLi_ILLEGAL, "[seg %cs]");
  EncodeOpByte1(0x3e, 0, IMM_NONE, NACLi_ILLEGAL, "[seg %ds]");
  /* 0x0f is an escape to two-byte instructions, below... */
  EncodeOpByte1(0x0f, 0, IMM_NONE, NACLi_UNDEFINED, "[two-byte opcode]");
  EncodeOpByte1(0x1f, 0, IMM_NONE, NACLi_ILLEGAL, "pop %ds");
  EncodeOpByte1(0x2f, 0, IMM_NONE, NACLi_ILLEGAL, "das");
  EncodeOpByte1(0x3f, 0, IMM_NONE, NACLi_ILLEGAL, "aas");  /* deprecated */
  /* another easy pattern, 0x40-0x5f */
  /* inc and dec are deprecated in x86-64; NaCl discourages their use. */
  OneRegVariants00_07(0x40, NACLi_386L, "inc");
  OneRegVariants00_07(0x48, NACLi_386L, "dec");
  OneRegVariants00_07(0x50, NACLi_386, "push");
  OneRegVariants00_07(0x58, NACLi_386, "pop");
  /* 0x60-0x6f */
  EncodeOpByte1(0x60, 0, IMM_NONE, NACLi_ILLEGAL, "pusha");
  EncodeOpByte1(0x61, 0, IMM_NONE, NACLi_ILLEGAL, "popa");  /* also POPAD */
  EncodeOpByte1(0x62, 1, IMM_NONE, NACLi_ILLEGAL, "bound $Gv, $Ma"); /* deprecated */
  EncodeOpByte1(0x63, 1, IMM_NONE, NACLi_SYSTEM, "arpl $Ew, $Gw");  /* system */
  EncodeOpByte1(0x64, 0, IMM_NONE, NACLi_ILLEGAL, "[seg fs]");
  EncodeOpByte1(0x65, 0, IMM_NONE, NACLi_ILLEGAL, "[seg gs]");
  EncodeOpByte1(0x66, 0, IMM_NONE, NACLi_ILLEGAL, "[data16]");
  EncodeOpByte1(0x67, 0, IMM_NONE, NACLi_ILLEGAL, "[addr size]");
  EncodeOpByte1(0x68, 0, IMM_DATAV, NACLi_386, "push $Iz");
  EncodeOpByte1(0x69, 1, IMM_DATAV, NACLi_386, "imul $Gv, $Ev, $Iz");
  EncodeOpByte1(0x6a, 0, IMM_FIXED1, NACLi_386, "push $Ib");
  EncodeOpByte1(0x6b, 1, IMM_FIXED1, NACLi_386, "imul $Gv, $Ev, $Ib");
  EncodeOpByte1(0x6c, 0, IMM_NONE, NACLi_ILLEGAL, "insb $Y, $D");
  EncodeOpByte1(0x6d, 0, IMM_NONE, NACLi_ILLEGAL, "insw/d $Y, $D");
  EncodeOpByte1(0x6e, 0, IMM_NONE, NACLi_ILLEGAL, "outsb $D, $X");
  EncodeOpByte1(0x6f, 0, IMM_NONE, NACLi_ILLEGAL, "outsw/d $D, $X");
  /* 0x70-0x77: jumps */
  EncodeOpByte1(0x70, 0, IMM_FIXED1, NACLi_JMP8, "jo $Jb");
  EncodeOpByte1(0x71, 0, IMM_FIXED1, NACLi_JMP8, "jno $Jb");
  EncodeOpByte1(0x72, 0, IMM_FIXED1, NACLi_JMP8, "jb $Jb");
  EncodeOpByte1(0x73, 0, IMM_FIXED1, NACLi_JMP8, "jnb $Jb");
  EncodeOpByte1(0x74, 0, IMM_FIXED1, NACLi_JMP8, "jz $Jb");
  EncodeOpByte1(0x75, 0, IMM_FIXED1, NACLi_JMP8, "jnz $Jb");
  EncodeOpByte1(0x76, 0, IMM_FIXED1, NACLi_JMP8, "jbe $Jb");
  EncodeOpByte1(0x77, 0, IMM_FIXED1, NACLi_JMP8, "jnbe $Jb");
  EncodeOpByte1(0x78, 0, IMM_FIXED1, NACLi_JMP8, "js $Jb");
  EncodeOpByte1(0x79, 0, IMM_FIXED1, NACLi_JMP8, "jns $Jb");
  EncodeOpByte1(0x7a, 0, IMM_FIXED1, NACLi_JMP8, "jp $Jb");
  EncodeOpByte1(0x7b, 0, IMM_FIXED1, NACLi_JMP8, "jnp $Jb");
  EncodeOpByte1(0x7c, 0, IMM_FIXED1, NACLi_JMP8, "jl $Jb");
  EncodeOpByte1(0x7d, 0, IMM_FIXED1, NACLi_JMP8, "jge $Jb");
  EncodeOpByte1(0x7e, 0, IMM_FIXED1, NACLi_JMP8, "jle $Jb");
  EncodeOpByte1(0x7f, 0, IMM_FIXED1, NACLi_JMP8, "jg $Jb");
  /* 0x80-0x8f: Gpr1, test, xchg, mov, lea, mov, pop */
  EncodeOpByte1(0x80, 1, IMM_FIXED1, NACLi_OPINMRM, "$group1 $Eb, $Ib");
  SetOpByte1OpInMRM(0x80, GROUP1);
  EncodeOpByte1(0x81, 1, IMM_DATAV, NACLi_OPINMRM, "$group1 $Ev, $Iz");
  SetOpByte1OpInMRM(0x81, GROUP1);
  /* The AMD manual shows 0x82 as a synonym for 0x80, however these are */
  /* all illegal in 64-bit mode so we omit them here too.               */
  EncodeOpByte1(0x82, 1, IMM_FIXED1, NACLi_ILLEGAL, "undef");
  /* table disagrees with objdump on 0x83? */
  EncodeOpByte1(0x83, 1, IMM_FIXED1, NACLi_OPINMRM, "$group1 $Ev, $Ib");
  SetOpByte1OpInMRM(0x83, GROUP1);
  EncodeOpByte1(0x84, 1, IMM_NONE, NACLi_386, "test $E, $G");
  EncodeOpByte1(0x85, 1, IMM_NONE, NACLi_386, "test $E, $G");
  EncodeOpByte1(0x86, 1, IMM_NONE, NACLi_386L, "xchg $E, $G");
  EncodeOpByte1(0x87, 1, IMM_NONE, NACLi_386L, "xchg $E, $G");
  EncodeOpByte1(0x88, 1, IMM_NONE, NACLi_386, "mov $Eb, $Gb");
  EncodeOpByte1(0x89, 1, IMM_NONE, NACLi_386, "mov $Ev, $Gv");
  EncodeOpByte1(0x8a, 1, IMM_NONE, NACLi_386, "mov $Gb, $Eb");
  EncodeOpByte1(0x8b, 1, IMM_NONE, NACLi_386, "mov $Gv, $Ev");
  EncodeOpByte1(0x8c, 1, IMM_NONE, NACLi_ILLEGAL, "mov $E, $S");
  EncodeOpByte1(0x8d, 1, IMM_NONE, NACLi_386, "lea $G, $M");
  EncodeOpByte1(0x8e, 1, IMM_NONE, NACLi_ILLEGAL, "mov $S, $E");
  EncodeOpByte1(0x8f, 1, IMM_NONE, NACLi_OPINMRM, "$group1a $Ev");
  SetOpByte1OpInMRM(0x8f, GROUP1A);
  /* 0x90-0x9f */
  EncodeOpByte1(0x90, 0, IMM_NONE, NACLi_386R, "nop");
  EncodeOpByte1(0x91, 0, IMM_NONE, NACLi_386L, "xchg %eax, %ecx");
  EncodeOpByte1(0x92, 0, IMM_NONE, NACLi_386L, "xchg %eax, %edx");
  EncodeOpByte1(0x93, 0, IMM_NONE, NACLi_386L, "xchg %eax, %ebx");
  EncodeOpByte1(0x94, 0, IMM_NONE, NACLi_386L, "xchg %eax, %esp");
  EncodeOpByte1(0x95, 0, IMM_NONE, NACLi_386L, "xchg %eax, %ebp");
  EncodeOpByte1(0x96, 0, IMM_NONE, NACLi_386L, "xchg %eax, %esi");
  EncodeOpByte1(0x97, 0, IMM_NONE, NACLi_386L, "xchg %eax, %edi");
  EncodeOpByte1(0x98, 0, IMM_NONE, NACLi_386, "cbw");  /* cwde cdqe */
  EncodeOpByte1(0x99, 0, IMM_NONE, NACLi_386, "cwd");  /* cdq cqo */
  EncodeOpByte1(0x9a, 0, IMM_FARPTR, NACLi_ILLEGAL, "lcall $A");
  EncodeOpByte1(0x9b, 0, IMM_NONE, NACLi_X87, "wait");
  EncodeOpByte1(0x9c, 0, IMM_NONE, NACLi_ILLEGAL, "pushf $F");
  EncodeOpByte1(0x9d, 0, IMM_NONE, NACLi_ILLEGAL, "popf $F");
  EncodeOpByte1(0x9e, 0, IMM_NONE, NACLi_386, "sahf");
  EncodeOpByte1(0x9f, 0, IMM_NONE, NACLi_386, "lahf");
  /* 0xa0-0xaf */
  EncodeOpByte1(0xa0, 0, IMM_ADDRV, NACLi_386, "mov %al, $O");
  EncodeOpByte1(0xa1, 0, IMM_ADDRV, NACLi_386, "mov %eax, $O");
  EncodeOpByte1(0xa2, 0, IMM_ADDRV, NACLi_386, "mov $O, %al");
  EncodeOpByte1(0xa3, 0, IMM_ADDRV, NACLi_386, "mov $O, %eax");
  EncodeOpByte1(0xa4, 0, IMM_NONE, NACLi_386R, "movsb $X, $Y");
  EncodeOpByte1(0xa5, 0, IMM_NONE, NACLi_386R, "movsw  $X, $Y");
  EncodeOpByte1(0xa6, 0, IMM_NONE, NACLi_386RE, "cmpsb $X, $Y");
  EncodeOpByte1(0xa7, 0, IMM_NONE, NACLi_386RE, "cmpsw $X, $Y");
  EncodeOpByte1(0xa8, 0, IMM_FIXED1, NACLi_386, "test %al, $I");
  EncodeOpByte1(0xa9, 0, IMM_DATAV, NACLi_386, "test %eax, $I");
  EncodeOpByte1(0xaa, 0, IMM_NONE, NACLi_386R, "stosb $Y, %al");
  EncodeOpByte1(0xab, 0, IMM_NONE, NACLi_386R, "stosw $Y, $eax");
  /* ISE reviewers suggested omiting lods and scas */
  EncodeOpByte1(0xac, 0, IMM_NONE, NACLi_ILLEGAL, "lodsb %al, $X");
  EncodeOpByte1(0xad, 0, IMM_NONE, NACLi_ILLEGAL, "lodsw %eax, $X");
  EncodeOpByte1(0xae, 0, IMM_NONE, NACLi_386RE, "scasb %al, $X");
  EncodeOpByte1(0xaf, 0, IMM_NONE, NACLi_386RE, "scasw %eax, $X");
  /* 0xb0-0xbf */
  EncodeOpByte1(0xb0, 0, IMM_FIXED1, NACLi_386, "mov %al, $Ib");
  EncodeOpByte1(0xb1, 0, IMM_FIXED1, NACLi_386, "mov %cl, $Ib");
  EncodeOpByte1(0xb2, 0, IMM_FIXED1, NACLi_386, "mov %dl, $Ib");
  EncodeOpByte1(0xb3, 0, IMM_FIXED1, NACLi_386, "mov %bl, $Ib");
  EncodeOpByte1(0xb4, 0, IMM_FIXED1, NACLi_386, "mov %ah, $Ib");
  EncodeOpByte1(0xb5, 0, IMM_FIXED1, NACLi_386, "mov %ch, $Ib");
  EncodeOpByte1(0xb6, 0, IMM_FIXED1, NACLi_386, "mov %dh, $Ib");
  EncodeOpByte1(0xb7, 0, IMM_FIXED1, NACLi_386, "mov %bh, $Ib");
  EncodeOpByte1(0xb8, 0, IMM_DATAV, NACLi_386, "mov %eax, $Iv");
  EncodeOpByte1(0xb9, 0, IMM_DATAV, NACLi_386, "mov %ecx, $Iv");
  EncodeOpByte1(0xba, 0, IMM_DATAV, NACLi_386, "mov %edx, $Iv");
  EncodeOpByte1(0xbb, 0, IMM_DATAV, NACLi_386, "mov %ebx, $Iv");
  EncodeOpByte1(0xbc, 0, IMM_DATAV, NACLi_386, "mov %esp, $Iv");
  EncodeOpByte1(0xbd, 0, IMM_DATAV, NACLi_386, "mov %ebp, $Iv");
  EncodeOpByte1(0xbe, 0, IMM_DATAV, NACLi_386, "mov %esi, $Iv");
  EncodeOpByte1(0xbf, 0, IMM_DATAV, NACLi_386, "mov %edi, $Iv");
  /* 0xc0-0xcf */
  EncodeOpByte1(0xc0, 1, IMM_FIXED1, NACLi_OPINMRM, "$group2 $Eb, $Ib");
  SetOpByte1OpInMRM(0xc0, GROUP2);
  EncodeOpByte1(0xc1, 1, IMM_FIXED1, NACLi_OPINMRM, "$group2 $Ev, $Ib");
  SetOpByte1OpInMRM(0xc1, GROUP2);
  EncodeOpByte1(0xc2, 0, IMM_FIXED2, NACLi_RETURN, "ret $Iw");
  EncodeOpByte1(0xc3, 0, IMM_NONE, NACLi_RETURN, "ret");
  EncodeOpByte1(0xc4, 1, IMM_NONE, NACLi_ILLEGAL, "les $G, $M");
  EncodeOpByte1(0xc5, 1, IMM_NONE, NACLi_ILLEGAL, "lds $G, $M");
  EncodeOpByte1(0xc6, 1, IMM_FIXED1, NACLi_OPINMRM, "$group11 $Eb, $Ib");
  SetOpByte1OpInMRM(0xc6, GROUP11);
  EncodeOpByte1(0xc7, 1, IMM_DATAV, NACLi_OPINMRM, "$group11 $Ev, $Iz");
  SetOpByte1OpInMRM(0xc7, GROUP11);
  EncodeOpByte1(0xc8, 0, IMM_FIXED3, NACLi_ILLEGAL, "enter $I, $I");
  EncodeOpByte1(0xc9, 0, IMM_NONE, NACLi_386, "leave");
  EncodeOpByte1(0xca, 0, IMM_FIXED2, NACLi_RETURN, "ret (far)");
  EncodeOpByte1(0xcb, 0, IMM_NONE, NACLi_RETURN, "ret (far)");
  EncodeOpByte1(0xcc, 0, IMM_NONE, NACLi_ILLEGAL, "int3");
  EncodeOpByte1(0xcd, 0, IMM_FIXED1, NACLi_ILLEGAL, "int $Iv");
  EncodeOpByte1(0xce, 0, IMM_NONE, NACLi_ILLEGAL, "into");
  EncodeOpByte1(0xcf, 0, IMM_NONE, NACLi_SYSTEM, "iret");
  /* 0xd0-0xdf */
  EncodeOpByte1(0xd0, 1, IMM_NONE, NACLi_OPINMRM, "$group2 $Eb, 1");
  SetOpByte1OpInMRM(0xd0, GROUP2);
  EncodeOpByte1(0xd1, 1, IMM_NONE, NACLi_OPINMRM, "$group2 $Ev, 1");
  SetOpByte1OpInMRM(0xd1, GROUP2);
  EncodeOpByte1(0xd2, 1, IMM_NONE, NACLi_OPINMRM, "$group2 $Eb, %cl");
  SetOpByte1OpInMRM(0xd2, GROUP2);
  EncodeOpByte1(0xd3, 1, IMM_NONE, NACLi_OPINMRM, "$group2 $Ev, %cl");
  SetOpByte1OpInMRM(0xd3, GROUP2);
  /* ISE reviewers suggested omision of AAM, AAD */
  EncodeOpByte1(0xd4, 0, IMM_FIXED1, NACLi_ILLEGAL, "aam");  /* deprecated */
  EncodeOpByte1(0xd5, 0, IMM_FIXED1, NACLi_ILLEGAL, "aad");  /* deprecated */
  EncodeOpByte1(0xd6, 0, IMM_NONE, NACLi_ILLEGAL, "salc");
  /* ISE reviewers suggested this omision */
  EncodeOpByte1(0xd7, 0, IMM_NONE, NACLi_ILLEGAL, "xlat");
  EncodeOpByte1(0xd8, 1, IMM_NONE, NACLi_X87, "x87");
  EncodeOpByte1(0xd9, 1, IMM_NONE, NACLi_X87, "x87");
  EncodeOpByte1(0xda, 1, IMM_NONE, NACLi_X87, "x87");
  EncodeOpByte1(0xdb, 1, IMM_NONE, NACLi_X87, "x87");
  EncodeOpByte1(0xdc, 1, IMM_NONE, NACLi_X87, "x87");
  EncodeOpByte1(0xdd, 1, IMM_NONE, NACLi_X87, "x87");
  EncodeOpByte1(0xde, 1, IMM_NONE, NACLi_X87, "x87");
  EncodeOpByte1(0xdf, 1, IMM_NONE, NACLi_X87, "x87");
  /* 0xe0-0xef */
  /* ISE reviewers suggested making loopne, loope, loop, jcxz illegal */
  /* There are faster alternatives on modern x86 implementations. */
  EncodeOpByte1(0xe0, 0, IMM_FIXED1, NACLi_ILLEGAL, "loopne $Jb");
  EncodeOpByte1(0xe1, 0, IMM_FIXED1, NACLi_ILLEGAL, "loope $Jb");
  EncodeOpByte1(0xe2, 0, IMM_FIXED1, NACLi_ILLEGAL, "loop $Jb");
  EncodeOpByte1(0xe3, 0, IMM_FIXED1, NACLi_ILLEGAL, "jcxz $Jb");
  /* I/O instructions */
  EncodeOpByte1(0xe4, 0, IMM_FIXED1, NACLi_ILLEGAL, "in %al, $I");
  EncodeOpByte1(0xe5, 0, IMM_FIXED1, NACLi_ILLEGAL, "in %eax, $I");
  EncodeOpByte1(0xe6, 0, IMM_FIXED1, NACLi_ILLEGAL, "out %al, $I");
  EncodeOpByte1(0xe7, 0, IMM_FIXED1, NACLi_ILLEGAL, "out %eax, $I");
  EncodeOpByte1(0xe8, 0, IMM_DATAV, NACLi_JMPZ, "call $Jz");
  EncodeOpByte1(0xe9, 0, IMM_DATAV, NACLi_JMPZ, "jmp $Jz");
  EncodeOpByte1(0xea, 0, IMM_FARPTR, NACLi_ILLEGAL, "ljmp $A");
  EncodeOpByte1(0xeb, 0, IMM_FIXED1, NACLi_JMP8, "jmp $Jb");
  EncodeOpByte1(0xec, 0, IMM_NONE, NACLi_ILLEGAL, "in %al, %dx");
  EncodeOpByte1(0xed, 0, IMM_NONE, NACLi_ILLEGAL, "in %eax, %dx");
  EncodeOpByte1(0xee, 0, IMM_NONE, NACLi_ILLEGAL, "out %dx, %al");
  EncodeOpByte1(0xef, 0, IMM_NONE, NACLi_ILLEGAL, "out %dx, %eax");
  /* 0xf0-0xff */
  EncodeOpByte1(0xf0, 0, IMM_NONE, NACLi_ILLEGAL, "[lock]");
  EncodeOpByte1(0xf1, 0, IMM_NONE, NACLi_ILLEGAL, "int1");
  EncodeOpByte1(0xf2, 0, IMM_NONE, NACLi_ILLEGAL, "[repne]");
  EncodeOpByte1(0xf3, 0, IMM_NONE, NACLi_ILLEGAL, "[rep]");
  /* NaCl uses the hlt instruction for bytes that should never be */
  /* executed. hlt causes immediate termination of the module. */
  EncodeOpByte1(0xf4, 0, IMM_NONE, NACLi_386, "hlt");
  EncodeOpByte1(0xf5, 0, IMM_NONE, NACLi_386, "cmc");
  /* Note: /0 and /1 also have an immediate */
  EncodeOpByte1(0xf6, 1, IMM_GROUP3_F6, NACLi_OPINMRM, "$group3 $Eb");
  SetOpByte1OpInMRM(0xf6, GROUP3);
  EncodeOpByte1(0xf7, 1, IMM_GROUP3_F7, NACLi_OPINMRM, "$group3 $Ev");
  SetOpByte1OpInMRM(0xf7, GROUP3);
  EncodeOpByte1(0xf8, 0, IMM_NONE, NACLi_386, "clc");
  EncodeOpByte1(0xf9, 0, IMM_NONE, NACLi_386, "stc");
  EncodeOpByte1(0xfa, 0, IMM_NONE, NACLi_SYSTEM, "cli");
  EncodeOpByte1(0xfb, 0, IMM_NONE, NACLi_SYSTEM, "sti");
  /* cld and std are generated by gcc, used for mem move operations */
  EncodeOpByte1(0xfc, 0, IMM_NONE, NACLi_386, "cld");
  EncodeOpByte1(0xfd, 0, IMM_NONE, NACLi_386, "std");
  EncodeOpByte1(0xfe, 1, IMM_NONE, NACLi_OPINMRM, "$group4 $Eb");
  SetOpByte1OpInMRM(0xfe, GROUP4);
  /* Note: /3 and /5 are $Mp rather than $Ev */
  EncodeOpByte1(0xff, 1, IMM_NONE, NACLi_OPINMRM, "$group5 $Ev");
  SetOpByte1OpInMRM(0xff, GROUP5);

  /* Opcodes encoded in the modrm field */
  /* Anything not done explicitly is marked illegal */
  /* group1 */
  EncodeModRMOp(GROUP1, 0, NACLi_386L, "add");
  EncodeModRMOp(GROUP1, 1, NACLi_386L, "or");
  EncodeModRMOp(GROUP1, 2, NACLi_386L, "adc");
  EncodeModRMOp(GROUP1, 3, NACLi_386L, "sbb");
  EncodeModRMOp(GROUP1, 4, NACLi_386L, "and");
  EncodeModRMOp(GROUP1, 5, NACLi_386L, "sub");
  EncodeModRMOp(GROUP1, 6, NACLi_386L, "xor");
  EncodeModRMOp(GROUP1, 7, NACLi_386, "cmp");
  /* group1a */
  EncodeModRMOp(GROUP1A, 0, NACLi_386, "pop $Ev");
  /* all other group1a opcodes are illegal */
  /* group2 */
  EncodeModRMOp(GROUP2, 0, NACLi_386, "rol");
  EncodeModRMOp(GROUP2, 1, NACLi_386, "ror");
  EncodeModRMOp(GROUP2, 2, NACLi_386, "rcl");
  EncodeModRMOp(GROUP2, 3, NACLi_386, "rcr");
  EncodeModRMOp(GROUP2, 4, NACLi_386, "shl");  /* sal */
  EncodeModRMOp(GROUP2, 5, NACLi_386, "shr");  /* sar */
  /* note 2, 6 is illegal according to Intel, shl according to AMD  */
  EncodeModRMOp(GROUP2, 7, NACLi_386, "sar");
  /* group3 */
  EncodeModRMOp(GROUP3, 0, NACLi_386, "test $I");
  /* this is such a weird case ... just put a special case in ncdecode.c */
  /* note 3, 1 is handled by a special case in the decoder */
  /* SetModRMOpImmType(3, 0, IMM_FIXED1); */
  EncodeModRMOp(GROUP3, 2, NACLi_386L, "not");
  EncodeModRMOp(GROUP3, 3, NACLi_386L, "neg");
  EncodeModRMOp(GROUP3, 4, NACLi_386, "mul %eax");
  EncodeModRMOp(GROUP3, 5, NACLi_386, "imul %eax");
  EncodeModRMOp(GROUP3, 6, NACLi_386, "div %eax");
  EncodeModRMOp(GROUP3, 7, NACLi_386, "idiv %eax");
  /* group4 */
  EncodeModRMOp(GROUP4, 0, NACLi_386L, "inc");
  EncodeModRMOp(GROUP4, 1, NACLi_386L, "dec");
  /* group5 */
  EncodeModRMOp(GROUP5, 0, NACLi_386L, "inc");
  EncodeModRMOp(GROUP5, 1, NACLi_386L, "dec");
  EncodeModRMOp(GROUP5, 2, NACLi_INDIRECT, "call *");  /* call indirect */
  EncodeModRMOp(GROUP5, 3, NACLi_ILLEGAL, "lcall *");  /* far call */
  EncodeModRMOp(GROUP5, 4, NACLi_INDIRECT, "jmp *");   /* jump indirect */
  EncodeModRMOp(GROUP5, 5, NACLi_ILLEGAL, "ljmp *");   /* far jmp */
  EncodeModRMOp(GROUP5, 6, NACLi_386, "push");
  /* group6 */
  EncodeModRMOp(GROUP6, 0, NACLi_SYSTEM, "sldt");
  EncodeModRMOp(GROUP6, 1, NACLi_SYSTEM, "str");
  EncodeModRMOp(GROUP6, 2, NACLi_SYSTEM, "lldt");
  EncodeModRMOp(GROUP6, 3, NACLi_SYSTEM, "ltr");
  EncodeModRMOp(GROUP6, 4, NACLi_SYSTEM, "verr");
  EncodeModRMOp(GROUP6, 5, NACLi_SYSTEM, "verw");
  /* group7 */
  EncodeModRMOp(GROUP7, 0, NACLi_SYSTEM, "sgdt");
  EncodeModRMOp(GROUP7, 1, NACLi_SYSTEM, "sidt"); /* monitor mwait */
  EncodeModRMOp(GROUP7, 2, NACLi_SYSTEM, "lgdt");
  EncodeModRMOp(GROUP7, 3, NACLi_SYSTEM, "lidt");
  EncodeModRMOp(GROUP7, 4, NACLi_SYSTEM, "smsw");
  EncodeModRMOp(GROUP7, 6, NACLi_SYSTEM, "lmsw");
  EncodeModRMOp(GROUP7, 7, NACLi_SYSTEM, "invlpg"); /* swapgs rdtscp */
  /* group8 */
  /* ISE reviewers suggested omitting bt* */
  EncodeModRMOp(GROUP8, 4, NACLi_ILLEGAL, "bt");   /* deprecated */
  EncodeModRMOp(GROUP8, 5, NACLi_ILLEGAL, "bts");  /* deprecated */
  EncodeModRMOp(GROUP8, 6, NACLi_ILLEGAL, "btr");  /* deprecated */
  EncodeModRMOp(GROUP8, 7, NACLi_ILLEGAL, "btc");  /* deprecated */
  /* group9 */
  /* If the effective operand size is 16 or 32 bits, cmpxchg8b is used. */
  /* If the size is 64 bits, cmpxchg16b is used, (64-bit mode only)     */
  /* These instructions do support the LOCK prefix */
  EncodeModRMOp(GROUP9, 1, NACLi_CMPXCHG8B, "cmpxchg8b");
  /* group10 - all illegal */
  /* group11 */
  EncodeModRMOp(GROUP11, 0, NACLi_386, "mov");
  /* group12 */
  EncodeModRMOp(GROUP12, 2, NACLi_MMXSSE2, "psrlw");
  EncodeModRMOp(GROUP12, 4, NACLi_MMXSSE2, "psraw");
  EncodeModRMOp(GROUP12, 6, NACLi_MMXSSE2, "psllw");
  /* group13 */
  EncodeModRMOp(GROUP13, 2, NACLi_MMXSSE2, "psrld");
  EncodeModRMOp(GROUP13, 4, NACLi_MMXSSE2, "psrad");
  EncodeModRMOp(GROUP13, 6, NACLi_MMXSSE2, "pslld");
  /* group14 */
  EncodeModRMOp(GROUP14, 2, NACLi_MMXSSE2, "psrlq");
  EncodeModRMOp(GROUP14, 3, NACLi_SSE2x, "psrldq");
  EncodeModRMOp(GROUP14, 6, NACLi_MMXSSE2, "psllq");
  EncodeModRMOp(GROUP14, 7, NACLi_SSE2x, "pslldq");
  /* group15 */
  EncodeModRMOp(GROUP15, 0, NACLi_ILLEGAL, "fxsave");
  EncodeModRMOp(GROUP15, 1, NACLi_ILLEGAL, "fxrstor");
  EncodeModRMOp(GROUP15, 2, NACLi_ILLEGAL, "ldmxcsr");  /* SSE */
  EncodeModRMOp(GROUP15, 3, NACLi_ILLEGAL, "stmxcsr");
  EncodeModRMOp(GROUP15, 4, NACLi_ILLEGAL, "invalid");
  TODO;
  EncodeModRMOp(GROUP15, 5, NACLi_ILLEGAL, "lfence");
  EncodeModRMOp(GROUP15, 6, NACLi_SSE2, "mfence");
  EncodeModRMOp(GROUP15, 7, NACLi_ILLEGAL, "sfence/clflush");
  /* group16 - SSE prefetch instructions */
  EncodeModRMOp(GROUP16, 0, NACLi_SSE, "prefetch NTA");
  EncodeModRMOp(GROUP16, 1, NACLi_SSE, "prefetch T0");
  EncodeModRMOp(GROUP16, 2, NACLi_SSE, "prefetch T1");
  EncodeModRMOp(GROUP16, 3, NACLi_SSE, "prefetch T1");
  EncodeModRMOp(GROUP16, 4, NACLi_ILLEGAL, "NOP (prefetch)");
  EncodeModRMOp(GROUP16, 5, NACLi_ILLEGAL, "NOP (prefetch)");
  EncodeModRMOp(GROUP16, 6, NACLi_ILLEGAL, "NOP (prefetch)");
  EncodeModRMOp(GROUP16, 7, NACLi_ILLEGAL, "NOP (prefetch)");
  /* groupp: prefetch - requires longmode or 3DNow! */
  /* It may be the case that these can also be enabled by CPUID_ECX_PRE; */
  /* This enabling is not supported by the validator at this time. */
  EncodeModRMOp(GROUPP, 0, NACLi_3DNOW, "prefetch exclusive");
  EncodeModRMOp(GROUPP, 1, NACLi_3DNOW, "prefetch modified");
  EncodeModRMOp(GROUPP, 2, NACLi_ILLEGAL, "[prefetch reserved]");
  EncodeModRMOp(GROUPP, 3, NACLi_ILLEGAL, "prefetch modified");
  EncodeModRMOp(GROUPP, 4, NACLi_ILLEGAL, "[prefetch reserved]");
  EncodeModRMOp(GROUPP, 5, NACLi_ILLEGAL, "[prefetch reserved]");
  EncodeModRMOp(GROUPP, 6, NACLi_ILLEGAL, "[prefetch reserved]");
  EncodeModRMOp(GROUPP, 7, NACLi_ILLEGAL, "[prefetch reserved]");

  /* encode opbyte 2; first byte is 0x0f */
  /* holes are undefined instructions    */
  /* This code used to allow the data16 (0x16) prefix to be used with */
  /* any two-byte opcode, until it caused a bug. Now all allowed      */
  /* prefixes need to be added explicitly.                            */
  /* See http://code.google.com/p/nativeclient/issues/detail?id=50    */
  /* Note: /0 and /1 have $Mw/Rv instead of $Ew */
  EncodeOp0F(0x00, 1, IMM_NONE, NACLi_OPINMRM, "$group6 $Ew");
  SetOp0FOpInMRM(0x0, GROUP6);
  /* Group7 is all privileged/system instructions */
  EncodeOp0F(0x01, 1, IMM_NONE, NACLi_OPINMRM, "$group7");
  SetOp0FOpInMRM(0x01, GROUP7);
  EncodeOp0F(0x02, 1, IMM_NONE, NACLi_SYSTEM, "lar $G, $E");
  EncodeOp0F(0x03, 1, IMM_NONE, NACLi_ILLEGAL, "lsl $Gv, $Ew");
  EncodeOp0F(0x05, 0, IMM_NONE, NACLi_SYSCALL, "syscall");
  EncodeOp0F(0x06, 0, IMM_NONE, NACLi_SYSTEM, "clts");
  EncodeOp0F(0x07, 0, IMM_NONE, NACLi_ILLEGAL, "sysret");
  EncodeOp0F(0x08, 0, IMM_NONE, NACLi_SYSTEM, "invd");
  EncodeOp0F(0x09, 0, IMM_NONE, NACLi_SYSTEM, "wbinvd");
  EncodeOp0F(0x0b, 0, IMM_NONE, NACLi_ILLEGAL, "ud2");
  EncodeOp0F(0x0d, 1, IMM_NONE, NACLi_OPINMRM, "$groupP (prefetch)");
  SetOp0FOpInMRM(0x0d, GROUPP);
  EncodeOp0F(0x0e, 0, IMM_NONE, NACLi_3DNOW, "femms");
  /* 3DNow instruction encodings use a MODRM byte and a 1-byte  */
  /* immediate which defines the opcode.                        */
  EncodeOp0F(0x0f, 1, IMM_FIXED1, NACLi_3DNOW, "3DNow");
  /* 0x10-17 appear below with the other newer opcodes ... */
  EncodeOp0F(0x18, 1, IMM_NONE, NACLi_OPINMRM, "$group16");
  SetOp0FOpInMRM(0x18, GROUP16);
  /* this nop takes an MRM byte */
  EncodeOp0F(0x1f, 1, IMM_NONE, NACLi_386, "nop");
  EncodeOp0F(0x20, 1, IMM_NONE, NACLi_SYSTEM, "mov $C, $R");
  EncodeOp0F(0x21, 1, IMM_NONE, NACLi_SYSTEM, "mov $D, $R");
  EncodeOp0F(0x22, 1, IMM_NONE, NACLi_SYSTEM, "mov $R, $C");
  EncodeOp0F(0x23, 1, IMM_NONE, NACLi_SYSTEM, "mov $R, $D");
  /* These two seem to be a mistake */
  /*  EncodeOp0F(0x24, 0, IMM_NONE, NACLi_SYSTEM, "mov $T, $R"); */
  /*  EncodeOp0F(0x26, 0, IMM_NONE, NACLi_SYSTEM, "mov $R, $T"); */
  /* 0x30-0x38 */
  EncodeOp0F(0x30, 0, IMM_NONE, NACLi_RDMSR, "wrmsr");
  EncodeOp0F(0x31, 0, IMM_NONE, NACLi_RDTSC, "rdtsc");
  EncodeOp0F(0x32, 0, IMM_NONE, NACLi_RDMSR, "rdmsr");
  EncodeOp0F(0x33, 0, IMM_NONE, NACLi_SYSTEM, "rdpmc");
  EncodeOp0F(0x34, 0, IMM_NONE, NACLi_SYSENTER, "sysenter");
  EncodeOp0F(0x35, 0, IMM_NONE, NACLi_SYSENTER, "sysexit");
  EncodeOp0F(0x38, 1, IMM_NONE, NACLi_3BYTE, "SSSE3, SSE4");
  EncodeOp0F(0x3a, 1, IMM_FIXED1, NACLi_3BYTE, "SSSE3, SSE4");
  /* 0x40-0x48 */
  EncodeOp0F(0x40, 1, IMM_NONE, NACLi_CMOV, "cmovo $Gv, $Ev");
  EncodeOp0F(0x41, 1, IMM_NONE, NACLi_CMOV, "cmovno $Gv, $Ev");
  EncodeOp0F(0x42, 1, IMM_NONE, NACLi_CMOV, "cmovb $Gv, $Ev");
  EncodeOp0F(0x43, 1, IMM_NONE, NACLi_CMOV, "cmovnb $Gv, $Ev");
  EncodeOp0F(0x44, 1, IMM_NONE, NACLi_CMOV, "cmovz $Gv, $Ev");
  EncodeOp0F(0x45, 1, IMM_NONE, NACLi_CMOV, "cmovnz $Gv, $Ev");
  EncodeOp0F(0x46, 1, IMM_NONE, NACLi_CMOV, "cmovbe $Gv, $Ev");
  EncodeOp0F(0x47, 1, IMM_NONE, NACLi_CMOV, "cmovnbe $Gv, $Ev");
  EncodeOp0F(0x48, 1, IMM_NONE, NACLi_CMOV, "cmovs $Gv, $Ev");
  EncodeOp0F(0x49, 1, IMM_NONE, NACLi_CMOV, "cmovns $Gv, $Ev");
  EncodeOp0F(0x4a, 1, IMM_NONE, NACLi_CMOV, "cmovp $Gv, $Ev");
  EncodeOp0F(0x4b, 1, IMM_NONE, NACLi_CMOV, "cmovnp $Gv, $Ev");
  EncodeOp0F(0x4c, 1, IMM_NONE, NACLi_CMOV, "cmovl $Gv, $Ev");
  EncodeOp0F(0x4d, 1, IMM_NONE, NACLi_CMOV, "cmovnl $Gv, $Ev");
  EncodeOp0F(0x4e, 1, IMM_NONE, NACLi_CMOV, "cmovle $Gv, $Ev");
  EncodeOp0F(0x4f, 1, IMM_NONE, NACLi_CMOV, "cmovnle $Gv, $Ev");
  /* repeat for 0x66 prefix */
  EncodeOp660F(0x40, 1, IMM_NONE, NACLi_CMOV, "cmovo $Gv, $Ev");
  EncodeOp660F(0x41, 1, IMM_NONE, NACLi_CMOV, "cmovno $Gv, $Ev");
  EncodeOp660F(0x42, 1, IMM_NONE, NACLi_CMOV, "cmovb $Gv, $Ev");
  EncodeOp660F(0x43, 1, IMM_NONE, NACLi_CMOV, "cmovnb $Gv, $Ev");
  EncodeOp660F(0x44, 1, IMM_NONE, NACLi_CMOV, "cmovz $Gv, $Ev");
  EncodeOp660F(0x45, 1, IMM_NONE, NACLi_CMOV, "cmovnz $Gv, $Ev");
  EncodeOp660F(0x46, 1, IMM_NONE, NACLi_CMOV, "cmovbe $Gv, $Ev");
  EncodeOp660F(0x47, 1, IMM_NONE, NACLi_CMOV, "cmovnbe $Gv, $Ev");
  EncodeOp660F(0x48, 1, IMM_NONE, NACLi_CMOV, "cmovs $Gv, $Ev");
  EncodeOp660F(0x49, 1, IMM_NONE, NACLi_CMOV, "cmovns $Gv, $Ev");
  EncodeOp660F(0x4a, 1, IMM_NONE, NACLi_CMOV, "cmovp $Gv, $Ev");
  EncodeOp660F(0x4b, 1, IMM_NONE, NACLi_CMOV, "cmovnp $Gv, $Ev");
  EncodeOp660F(0x4c, 1, IMM_NONE, NACLi_CMOV, "cmovl $Gv, $Ev");
  EncodeOp660F(0x4d, 1, IMM_NONE, NACLi_CMOV, "cmovnl $Gv, $Ev");
  EncodeOp660F(0x4e, 1, IMM_NONE, NACLi_CMOV, "cmovle $Gv, $Ev");
  EncodeOp660F(0x4f, 1, IMM_NONE, NACLi_CMOV, "cmovnle $Gv, $Ev");

  /* 0x80-0x8f */
  EncodeOp0F(0x80, 0, IMM_DATAV, NACLi_JMPZ, "jo $Jz");
  EncodeOp0F(0x81, 0, IMM_DATAV, NACLi_JMPZ, "jno $Jz");
  EncodeOp0F(0x82, 0, IMM_DATAV, NACLi_JMPZ, "jb $Jz");
  EncodeOp0F(0x83, 0, IMM_DATAV, NACLi_JMPZ, "jnb $Jz");
  EncodeOp0F(0x84, 0, IMM_DATAV, NACLi_JMPZ, "jz $Jz");
  EncodeOp0F(0x85, 0, IMM_DATAV, NACLi_JMPZ, "jnz $Jz");
  EncodeOp0F(0x86, 0, IMM_DATAV, NACLi_JMPZ, "jbe $Jz");
  EncodeOp0F(0x87, 0, IMM_DATAV, NACLi_JMPZ, "jnbe $Jz");
  EncodeOp0F(0x88, 0, IMM_DATAV, NACLi_JMPZ, "js $Jz");
  EncodeOp0F(0x89, 0, IMM_DATAV, NACLi_JMPZ, "jns $Jz");
  EncodeOp0F(0x8a, 0, IMM_DATAV, NACLi_JMPZ, "jp $Jz");
  EncodeOp0F(0x8b, 0, IMM_DATAV, NACLi_JMPZ, "jnp $Jz");
  EncodeOp0F(0x8c, 0, IMM_DATAV, NACLi_JMPZ, "jl $Jz");
  EncodeOp0F(0x8d, 0, IMM_DATAV, NACLi_JMPZ, "jge $Jz");
  EncodeOp0F(0x8e, 0, IMM_DATAV, NACLi_JMPZ, "jle $Jz");
  EncodeOp0F(0x8f, 0, IMM_DATAV, NACLi_JMPZ, "jg $Jz");
  /* 0x90-0x9f */
  EncodeOp0F(0x90, 1, IMM_NONE, NACLi_386, "seto $Eb");
  EncodeOp0F(0x91, 1, IMM_NONE, NACLi_386, "setno $Eb");
  EncodeOp0F(0x92, 1, IMM_NONE, NACLi_386, "setb $Eb");
  EncodeOp0F(0x93, 1, IMM_NONE, NACLi_386, "setnb $Eb");
  EncodeOp0F(0x94, 1, IMM_NONE, NACLi_386, "setz $Eb");
  EncodeOp0F(0x95, 1, IMM_NONE, NACLi_386, "setnz $Eb");
  EncodeOp0F(0x96, 1, IMM_NONE, NACLi_386, "setbe $Eb");
  EncodeOp0F(0x97, 1, IMM_NONE, NACLi_386, "setnbe $Eb");
  EncodeOp0F(0x98, 1, IMM_NONE, NACLi_386, "sets $Eb");
  EncodeOp0F(0x99, 1, IMM_NONE, NACLi_386, "setns $Eb");
  EncodeOp0F(0x9a, 1, IMM_NONE, NACLi_386, "setp $Eb");
  EncodeOp0F(0x9b, 1, IMM_NONE, NACLi_386, "setnp $Eb");
  EncodeOp0F(0x9c, 1, IMM_NONE, NACLi_386, "setl $Eb");
  EncodeOp0F(0x9d, 1, IMM_NONE, NACLi_386, "setge $Eb");
  EncodeOp0F(0x9e, 1, IMM_NONE, NACLi_386, "setle $Eb");
  EncodeOp0F(0x9f, 1, IMM_NONE, NACLi_386, "setg $Eb");
  /* 0xa0-0xaf */
  EncodeOp0F(0xa0, 0, IMM_NONE, NACLi_ILLEGAL, "push %fs");
  EncodeOp0F(0xa1, 0, IMM_NONE, NACLi_ILLEGAL, "pop %fs");
  EncodeOp0F(0xa2, 0, IMM_NONE, NACLi_386, "cpuid");
  EncodeOp0F(0xa3, 1, IMM_NONE, NACLi_ILLEGAL, "bt $Ev, $Gv");
  /* ISE reviewers suggested omitting shld */
  EncodeOp0F(0xa4, 1, IMM_FIXED1, NACLi_386, "shld $Ev, $Gv, $Ib");
  EncodeOp0F(0xa5, 1, IMM_NONE, NACLi_386, "shld $Ev, $Gv, %cl");
  EncodeOp0F(0xa6, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp0F(0xa7, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp0F(0xa8, 0, IMM_NONE, NACLi_ILLEGAL, "push %gs");
  EncodeOp0F(0xa9, 0, IMM_NONE, NACLi_ILLEGAL, "pop %gs");
  EncodeOp0F(0xaa, 0, IMM_NONE, NACLi_SYSTEM, "rsm");
  EncodeOp0F(0xab, 1, IMM_NONE, NACLi_ILLEGAL, "bts $Ev, $Gv");
  /* ISE reviewers suggested omitting shrd */
  EncodeOp0F(0xac, 1, IMM_FIXED1, NACLi_386, "shrd $Ev, $Gv, $Ib");
  EncodeOp0F(0xad, 1, IMM_NONE, NACLi_386, "shrd $Ev, $Gv, %cl");
  /* fxsave fxrstor ldmxcsr stmxcsr clflush */
  /* lfence mfence sfence */
  /* Note: size of memory operand varies with MRM. */
  EncodeOp0F(0xae, 1, IMM_NONE, NACLi_OPINMRM, "$group15 $M");
  SetOp0FOpInMRM(0xae, GROUP15);
  EncodeOp0F(0xaf, 1, IMM_NONE, NACLi_386, "imul $Gv, $Ev");
  EncodeOp660F(0xaf, 1, IMM_NONE, NACLi_386, "imul $Gv, $Ev");
  /* 0xb0-0xbf */
  /* Note NaCl doesn't allow 16-bit cmpxchg */
  EncodeOp0F(0xb0, 1, IMM_NONE, NACLi_386L, "cmpxchg $E, $G");
  EncodeOp0F(0xb1, 1, IMM_NONE, NACLi_386L, "cmpxchg $E, $G");
  EncodeOp0F(0xb2, 1, IMM_NONE, NACLi_ILLEGAL, "lss $Mp");
  EncodeOp0F(0xb3, 1, IMM_NONE, NACLi_ILLEGAL, "btr $Ev, $Gv");
  EncodeOp0F(0xb4, 1, IMM_NONE, NACLi_ILLEGAL, "lfs $Mp");
  EncodeOp0F(0xb5, 1, IMM_NONE, NACLi_ILLEGAL, "lgs $Mp");
  EncodeOp0F(0xb6, 1, IMM_NONE, NACLi_386, "movzx $Gv, $Eb");
  EncodeOp660F(0xb6, 1, IMM_NONE, NACLi_386, "movzx $Gv, $Eb");
  EncodeOp0F(0xb7, 1, IMM_NONE, NACLi_386, "movzx $Gv, $Ew");
  EncodeOp660F(0xb7, 1, IMM_NONE, NACLi_386, "movzx $Gv, $Ew");

  EncodeOp0F(0xc8, 0, IMM_NONE, NACLi_386, "bswap %eax");
  EncodeOp0F(0xc9, 0, IMM_NONE, NACLi_386, "bswap %ecx");
  EncodeOp0F(0xca, 0, IMM_NONE, NACLi_386, "bswap %edx");
  EncodeOp0F(0xcb, 0, IMM_NONE, NACLi_386, "bswap %ebx");
  EncodeOp0F(0xcc, 0, IMM_NONE, NACLi_386, "bswap %esp");
  EncodeOp0F(0xcd, 0, IMM_NONE, NACLi_386, "bswap %ebp");
  EncodeOp0F(0xce, 0, IMM_NONE, NACLi_386, "bswap %esi");
  EncodeOp0F(0xcf, 0, IMM_NONE, NACLi_386, "bswap %edi");

  /* Instructions from ?? 0F 10 to ?? 0F 1F */
  EncodeOp0F(0x10, 1, IMM_NONE, NACLi_SSE, "movups $Vps, $Wps");
  EncodeOpF30F(0x10, 1, IMM_NONE, NACLi_SSE, "movss $Vss, $Wss");
  EncodeOp660F(0x10, 1, IMM_NONE, NACLi_SSE2, "movupd $Vpd, $Wpd");
  EncodeOpF20F(0x10, 1, IMM_NONE, NACLi_SSE2, "movsd $Vsd, $Wsd");

  EncodeOp0F(0x11, 1, IMM_NONE, NACLi_SSE, "movups $Wps, $Vps");
  EncodeOpF30F(0x11, 1, IMM_NONE, NACLi_SSE, "movss $Wss, $Vss");
  EncodeOp660F(0x11, 1, IMM_NONE, NACLi_SSE2, "movupd $Wpd, $Vpd");
  EncodeOpF20F(0x11, 1, IMM_NONE, NACLi_SSE2, "movsd $Wsd, $Vsd");

  EncodeOp0F(0x12, 1, IMM_NONE, NACLi_SSE, "movlps $Vps, $Mq");   /* or movhlps */
  EncodeOpF30F(0x12, 1, IMM_NONE, NACLi_SSE3, "movsldup $Vps, $Wps");
  EncodeOp660F(0x12, 1, IMM_NONE, NACLi_SSE2, "movlpd $Vps, $Mq");
  EncodeOpF20F(0x12, 1, IMM_NONE, NACLi_SSE3, "movddup $Vpd, $Wsd");

  EncodeOp0F(0x13, 1, IMM_NONE, NACLi_SSE, "movlps $Mq, $Vps");
  EncodeOpF30F(0x13, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x13, 1, IMM_NONE, NACLi_SSE2, "movlpd $Mq, $Vsd");
  EncodeOpF20F(0x13, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x14, 1, IMM_NONE, NACLi_SSE, "unpcklps $Vps, $Wq");
  EncodeOpF30F(0x14, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x14, 1, IMM_NONE, NACLi_SSE2, "unpcklpd $Vpd, $Wq");
  EncodeOpF20F(0x14, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x15, 1, IMM_NONE, NACLi_SSE, "unpckhps $Vps, $Wq");
  EncodeOpF30F(0x15, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x15, 1, IMM_NONE, NACLi_SSE2, "unpckhpd $Vpd, $Wq");
  EncodeOpF20F(0x15, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x16, 1, IMM_NONE, NACLi_SSE, "movhps $Vps, $Mq");    /* or movlhps */
  EncodeOpF30F(0x16, 1, IMM_NONE, NACLi_SSE3, "movshdup $Vps, $Wps");
  EncodeOp660F(0x16, 1, IMM_NONE, NACLi_SSE2, "movhpd $Vsd, $Mq");
  EncodeOpF20F(0x16, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x17, 1, IMM_NONE, NACLi_SSE, "movhps $Mq, $Vps");
  EncodeOpF30F(0x17, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x17, 1, IMM_NONE, NACLi_SSE2, "movhpd $Mq, $Vpd");
  EncodeOpF20F(0x17, 0, IMM_NONE, NACLi_INVALID, "invalid");

  /* Instructions from ?? 0F 28 to ?? 0F 2F */
  EncodeOp0F(0x28, 1, IMM_NONE, NACLi_SSE, "movaps $Vps, $Wps");
  EncodeOpF30F(0x28, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x28, 1, IMM_NONE, NACLi_SSE2, "movapd $Vpd, $Wpd");
  EncodeOpF20F(0x28, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x29, 1, IMM_NONE, NACLi_SSE, "movaps $Wps, $Vps");
  EncodeOpF30F(0x29, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x29, 1, IMM_NONE, NACLi_SSE2, "movapd $Wpd, $Vpd");
  EncodeOpF20F(0x29, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x2a, 1, IMM_NONE, NACLi_SSE, "cvtpi2ps $Vps, $Qq");
  EncodeOpF30F(0x2a, 1, IMM_NONE, NACLi_SSE, "cvtsi2ss $Vss, $Ed");
  EncodeOp660F(0x2a, 1, IMM_NONE, NACLi_SSE2, "cvtpi2pd $Vpd $Qq");
  EncodeOpF20F(0x2a, 1, IMM_NONE, NACLi_SSE2, "cvtsi2sd $Vsd, $Ed");

  EncodeOp0F(0x2b, 1, IMM_NONE, NACLi_SSE, "movntps $Mdq, $Vps");
  EncodeOpF30F(0x2b, 1, IMM_NONE, NACLi_SSE4A, "movntss $Md, $Vss");
  EncodeOp660F(0x2b, 1, IMM_NONE, NACLi_SSE2, "movntpd $Mdq, $Vpd");
  EncodeOpF20F(0x2b, 1, IMM_NONE, NACLi_SSE4A, "movntsd $Mq, $Vsd");

  EncodeOp0F(0x2c, 1, IMM_NONE, NACLi_SSE, "cvttps2pi $Pq, $Wps");
  EncodeOpF30F(0x2c, 1, IMM_NONE, NACLi_SSE, "cvttss2si $Gd, $Wss");
  EncodeOp660F(0x2c, 1, IMM_NONE, NACLi_SSE2, "cvttpd2pi $Pq, $Wpd");
  EncodeOpF20F(0x2c, 1, IMM_NONE, NACLi_SSE2, "cvttsd2si $Gd, $Wsd");

  EncodeOp0F(0x2d, 1, IMM_NONE, NACLi_SSE, "cvtps2pi $Pq, $Wps");
  EncodeOpF30F(0x2d, 1, IMM_NONE, NACLi_SSE, "cvtss2si $Gd, $Wss");
  EncodeOp660F(0x2d, 1, IMM_NONE, NACLi_SSE2, "cvtpd2pi $Pq, $Wpd");
  EncodeOpF20F(0x2d, 1, IMM_NONE, NACLi_SSE2, "cvtsd2si $Gd, $Wsd");

  EncodeOp0F(0x2e, 1, IMM_NONE, NACLi_SSE, "ucomiss $Vss, $Wss");
  EncodeOpF30F(0x2e, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x2e, 1, IMM_NONE, NACLi_SSE2, "ucomisd $Vps, $Wps");
  EncodeOpF20F(0x2e, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x2f, 1, IMM_NONE, NACLi_SSE, "comiss $Vps, $Wps");
  EncodeOpF30F(0x2f, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x2f, 1, IMM_NONE, NACLi_SSE2, "comisd $Vpd, $Wsd");
  EncodeOpF20F(0x2f, 0, IMM_NONE, NACLi_INVALID, "invalid");

  /* Instructions from ?? 0F 38 and ?? 0F 3A are SSE4, below  */
  /* Others from ?? 0F 39 to ?? 0F 3F are invalid             */

  /* Instructions from ?? 0F 50 to ?? 0F 7F */
  EncodeOp0F(0x50, 1, IMM_NONE, NACLi_SSE, "movmskps $Gd, $VRps");
  EncodeOpF30F(0x50, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x50, 1, IMM_NONE, NACLi_SSE2, "movmskpd $Gd, $VRpd");
  EncodeOpF20F(0x50, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x51, 1, IMM_NONE, NACLi_SSE, "sqrtps $Vps, $Wps");
  EncodeOpF30F(0x51, 1, IMM_NONE, NACLi_SSE, "sqrtss $Vss, $Wss");
  EncodeOp660F(0x51, 1, IMM_NONE, NACLi_SSE2, "sqrtpd $Vpd, $Wpd");
  EncodeOpF20F(0x51, 1, IMM_NONE, NACLi_SSE2, "sqrtsd $Vsd, $Wsd");

  EncodeOp0F(0x52, 1, IMM_NONE, NACLi_SSE, "rsqrtps $Vps, $Wps");
  EncodeOpF30F(0x52, 1, IMM_NONE, NACLi_SSE, "rsqrtss $Vss, $Wss");
  EncodeOp660F(0x52, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF20F(0x52, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x53, 1, IMM_NONE, NACLi_SSE, "rcpps $Vps, $Wps");
  EncodeOpF30F(0x53, 1, IMM_NONE, NACLi_SSE, "rcpss $Vss, $Wss");
  EncodeOp660F(0x53, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF20F(0x53, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x54, 1, IMM_NONE, NACLi_SSE, "andps $Vps, $Wps");
  EncodeOpF30F(0x54, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x54, 1, IMM_NONE, NACLi_SSE2, "andpd $Vpd, $Wpd");
  EncodeOpF20F(0x54, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x55, 1, IMM_NONE, NACLi_SSE, "andnps $Vps, $Wps");
  EncodeOpF30F(0x55, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x55, 1, IMM_NONE, NACLi_SSE2, "andnpd $Vpd, $Wpd");
  EncodeOpF20F(0x55, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x56, 1, IMM_NONE, NACLi_SSE, "orps $Vps, $Wps");
  EncodeOpF30F(0x56, 1, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x56, 1, IMM_NONE, NACLi_SSE2, "orpd $Vpd, $Wpd");
  EncodeOpF20F(0x56, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x57, 1, IMM_NONE, NACLi_SSE, "xorps $Vps, $Wps");
  EncodeOpF30F(0x57, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x57, 1, IMM_NONE, NACLi_SSE2, "xorpd $Vpd, $Wpd");
  EncodeOpF20F(0x57, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x58, 1, IMM_NONE, NACLi_SSE, "addps $Vps, $Wps");
  EncodeOpF30F(0x58, 1, IMM_NONE, NACLi_SSE, "addss $Vss, $Wss");
  EncodeOp660F(0x58, 1, IMM_NONE, NACLi_SSE2, "addpd $Vpd, $Wpd");
  EncodeOpF20F(0x58, 1, IMM_NONE, NACLi_SSE2, "addsd $Vsd, $Wsd");

  EncodeOp0F(0x59, 1, IMM_NONE, NACLi_SSE, "mulps $Vps, $Wps");
  EncodeOpF30F(0x59, 1, IMM_NONE, NACLi_SSE, "mulss $Vss, $Wss");
  EncodeOp660F(0x59, 1, IMM_NONE, NACLi_SSE2, "mulpd $Vpd, $Wpd");
  EncodeOpF20F(0x59, 1, IMM_NONE, NACLi_SSE2, "mulsd $Vsd, $Wsd");

  EncodeOp0F(0x5a, 1, IMM_NONE, NACLi_SSE2, "cvtps2pd $Vpd, $Wps");
  EncodeOpF30F(0x5a, 1, IMM_NONE, NACLi_SSE2, "cvtss2sd $Vsd, $Wss");
  EncodeOp660F(0x5a, 1, IMM_NONE, NACLi_SSE2, "cvtpd2ps $Vps, $Wpd");
  EncodeOpF20F(0x5a, 1, IMM_NONE, NACLi_SSE2, "cvtsd2ss $Vss, $Wsd");

  EncodeOp0F(0x5b, 1, IMM_NONE, NACLi_SSE2, "cvtdq2ps $Vps, $Wdq");
  EncodeOpF30F(0x5b, 1, IMM_NONE, NACLi_SSE2, "cvttps2dq $Vdq, $Wps");
  EncodeOp660F(0x5b, 1, IMM_NONE, NACLi_SSE2, "cvtps2dq $Vdq, $Wps");
  EncodeOpF20F(0x5b, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x5c, 1, IMM_NONE, NACLi_SSE, "subps $Vps, $Wps");
  EncodeOpF30F(0x5c, 1, IMM_NONE, NACLi_SSE, "subss $Vss, $Wss");
  EncodeOp660F(0x5c, 1, IMM_NONE, NACLi_SSE2, "subpd $Vpd, $Wpd");
  EncodeOpF20F(0x5c, 1, IMM_NONE, NACLi_SSE2, "subsd $Vsd, $Wsd");

  EncodeOp0F(0x5d, 1, IMM_NONE, NACLi_SSE, "minps $Vps, $Wps");
  EncodeOpF30F(0x5d, 1, IMM_NONE, NACLi_SSE, "minss $Vss, $Wss");
  EncodeOp660F(0x5d, 1, IMM_NONE, NACLi_SSE2, "minpd $Vpd, $Wpd");
  EncodeOpF20F(0x5d, 1, IMM_NONE, NACLi_SSE2, "minsd $Vsd, $Wsd");

  EncodeOp0F(0x5e, 1, IMM_NONE, NACLi_SSE, "divps $Vps, $Wps");
  EncodeOpF30F(0x5e, 1, IMM_NONE, NACLi_SSE, "divss $Vss, $Wss");
  EncodeOp660F(0x5e, 1, IMM_NONE, NACLi_SSE2, "divpd $Vpd, $Wpd");
  EncodeOpF20F(0x5e, 1, IMM_NONE, NACLi_SSE2, "divsd $Vsd, $Wsd");

  EncodeOp0F(0x5f, 1, IMM_NONE, NACLi_SSE, "maxps $Vps, $Wps");
  EncodeOpF30F(0x5f, 1, IMM_NONE, NACLi_SSE, "maxss $Vss, $Wss");
  EncodeOp660F(0x5f, 1, IMM_NONE, NACLi_SSE2, "maxpd $Vpd, $Wpd");
  EncodeOpF20F(0x5f, 1, IMM_NONE, NACLi_SSE2, "maxsd $Vsd, $Wsd");

  EncodeOp0F(0x60, 1, IMM_NONE, NACLi_MMX, "punpcklbw $Pq, $Qd");
  EncodeOpF30F(0x60, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x60, 1, IMM_NONE, NACLi_SSE2, "punpcklbw $Vdq, $Wq");
  EncodeOpF20F(0x60, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x61, 1, IMM_NONE, NACLi_MMX, "punpcklwd $Pq, $Qd");
  EncodeOpF30F(0x61, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x61, 1, IMM_NONE, NACLi_SSE2, "punpcklwd $Vdq, $Wq");
  EncodeOpF20F(0x61, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x62, 1, IMM_NONE, NACLi_MMX, "punpckldq $Pq, $Qd");
  EncodeOpF30F(0x62, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x62, 1, IMM_NONE, NACLi_SSE2, "punpckldq $Vdq, $Wq");
  EncodeOpF20F(0x62, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x63, 1, IMM_NONE, NACLi_MMX, "packsswb $Pq, $Qq");
  EncodeOpF30F(0x63, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x63, 1, IMM_NONE, NACLi_SSE2, "packsswb $Vdq, $Wdq");
  EncodeOpF20F(0x63, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x64, 1, IMM_NONE, NACLi_MMX, "pcmpgtb $Pq, $Qq");
  EncodeOpF30F(0x64, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x64, 1, IMM_NONE, NACLi_SSE2, "pcmpgtb $Vdq, $Wdq");
  EncodeOpF20F(0x64, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x65, 1, IMM_NONE, NACLi_MMX, "pcmpgtw $Pq, $Qq");
  EncodeOpF30F(0x65, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x65, 1, IMM_NONE, NACLi_SSE2, "pcmpgtw $Vdq, $Wdq");
  EncodeOpF20F(0x65, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x66, 1, IMM_NONE, NACLi_MMX, "pcmpgtd $Pq, $Qq");
  EncodeOpF30F(0x66, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x66, 1, IMM_NONE, NACLi_SSE2, "pcmpgtd $Vdq, $Wdq");
  EncodeOpF20F(0x66, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x67, 1, IMM_NONE, NACLi_MMX, "packuswb $Pq, $Qq");
  EncodeOpF30F(0x67, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x67, 1, IMM_NONE, NACLi_SSE2, "packuswb $Vdq, $Wdq");
  EncodeOpF20F(0x67, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x68, 1, IMM_NONE, NACLi_MMX, "punpckhbw $Pq, $Qd");
  EncodeOpF30F(0x68, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x68, 1, IMM_NONE, NACLi_SSE2, "punpckhbw $Vdq, $Wq");
  EncodeOpF20F(0x68, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x69, 1, IMM_NONE, NACLi_MMX, "punpckhwd $Pq, $Qd");
  EncodeOpF30F(0x69, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x69, 1, IMM_NONE, NACLi_SSE2, "punpckhwd $Vdq, $Wq");
  EncodeOpF20F(0x69, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x6a, 1, IMM_NONE, NACLi_MMX, "punpckhdq $Pq, $Qd");
  EncodeOpF30F(0x6a, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x6a, 1, IMM_NONE, NACLi_SSE2, "punpckhdq $Vdq, $Wq");
  EncodeOpF20F(0x6a, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x6b, 1, IMM_NONE, NACLi_MMX, "packssdw $Pq, $Qq");
  EncodeOpF30F(0x6b, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x6b, 1, IMM_NONE, NACLi_SSE2, "packssdw $Vdq, $Wdq");
  EncodeOpF20F(0x6b, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x6c, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0x6c, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x6c, 1, IMM_NONE, NACLi_SSE2, "punpcklqdq $Vdq, $Wq");
  EncodeOpF20F(0x6c, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x6d, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0x6d, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x6d, 1, IMM_NONE, NACLi_SSE2, "punpckhqdq $Vdq, $Wq");
  EncodeOpF20F(0x6d, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x6e, 1, IMM_NONE, NACLi_MMX, "movd $Pq, $Ed");
  EncodeOpF30F(0x6e, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x6e, 1, IMM_NONE, NACLi_SSE2, "movd $Vdq, $Edq");
  EncodeOpF20F(0x6e, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x6f, 1, IMM_NONE, NACLi_MMX, "movq $Pq, $Qq");
  EncodeOpF30F(0x6f, 1, IMM_NONE, NACLi_SSE2, "movdqu $Vdq, $Wdq");
  EncodeOp660F(0x6f, 1, IMM_NONE, NACLi_SSE2, "movdqa $Vdq, $Wdq");
  EncodeOpF20F(0x6f, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x70, 1, IMM_FIXED1, NACLi_SSE, "pshufw $Pq, $Qq, $Ib");
  EncodeOpF30F(0x70, 1, IMM_FIXED1, NACLi_SSE2, "pshufhw $Vq, $Wq, $Ib");
  EncodeOp660F(0x70, 1, IMM_FIXED1, NACLi_SSE2, "pshufd $Vdq, $Wdq, $Ib");
  EncodeOpF20F(0x70, 1, IMM_FIXED1, NACLi_SSE2, "pshuflw $Vq, $Wq, $Ib");

  EncodeOp0F(0x71, 1, IMM_FIXED1, NACLi_OPINMRM, "$group12 $PRq, $Ib");
  EncodeOpF30F(0x71, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x71, 1, IMM_FIXED1, NACLi_OPINMRM, "$group12 $VRdq, $Ib");
  EncodeOpF20F(0x71, 0, IMM_NONE, NACLi_INVALID, "invalid");
  SetOp0FOpInMRM(0x71, GROUP12);
  SetOp66OpInMRM(0x71, GROUP12);

  EncodeOp0F(0x72, 1, IMM_FIXED1, NACLi_OPINMRM, "$group13 $PRq, $Ib");
  EncodeOpF30F(0x72, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x72, 1, IMM_FIXED1, NACLi_OPINMRM, "$group13 $VRdq, $Ib");
  EncodeOpF20F(0x72, 0, IMM_NONE, NACLi_INVALID, "invalid");
  SetOp0FOpInMRM(0x72, GROUP13);
  SetOp66OpInMRM(0x72, GROUP13);

  EncodeOp0F(0x73, 1, IMM_FIXED1, NACLi_OPINMRM, "$group14 $PRq, $Ib");
  EncodeOpF30F(0x73, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x73, 1, IMM_FIXED1, NACLi_OPINMRM, "$group14 $VRdq, $Ib");
  EncodeOpF20F(0x73, 0, IMM_NONE, NACLi_INVALID, "invalid");
  SetOp0FOpInMRM(0x73, GROUP14);
  SetOp66OpInMRM(0x73, GROUP14);

  EncodeOp0F(0x74, 1, IMM_NONE, NACLi_MMX, "pcmpeqb $Pq, $Qq");
  EncodeOpF30F(0x74, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x74, 1, IMM_NONE, NACLi_SSE2, "pcmpeqb $Vdq, $Wdq");
  EncodeOpF20F(0x74, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x75, 1, IMM_NONE, NACLi_MMX, "pcmpeqw $Pq, $Qq");
  EncodeOpF30F(0x75, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x75, 1, IMM_NONE, NACLi_SSE2, "pcmpeqw $Vdq, $Wdq");
  EncodeOpF20F(0x75, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x76, 1, IMM_NONE, NACLi_MMX, "pcmpeqd $Pq, $Qq");
  EncodeOpF30F(0x76, 1, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x76, 1, IMM_NONE, NACLi_SSE2, "pcmpeqd $Vdq, $Wdq");
  EncodeOpF20F(0x76, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x77, 0, IMM_NONE, NACLi_MMX, "emms");
  EncodeOpF30F(0x77, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x77, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF20F(0x77, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x78, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0x78, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x78, 1, IMM_FIXED1, NACLi_OPINMRM, "$group17 $Vdq, $Ib, $Ib");
  SetOp66OpInMRM(0x78, GROUP17);
  EncodeOpF20F(0x78, 1, IMM_FIXED2, NACLi_SSE4A, "insertq $Vdq, $VRq, $Ib, $Ib");

  EncodeOp0F(0x79, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0x79, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x79, 1, IMM_NONE, NACLi_SSE4A, "extrq $Vdq, $VRq");
  EncodeOpF20F(0x79, 1, IMM_NONE, NACLi_SSE4A, "insertq $Vdq, $VRdq");

  EncodeOp0F(0x7a, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0x7a, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x7a, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF20F(0x7a, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x7b, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0x7b, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x7b, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF20F(0x7b, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x7c, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0x7c, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x7c, 1, IMM_NONE, NACLi_SSE3, "haddpd $Vpd, $Wpd");
  EncodeOpF20F(0x7c, 1, IMM_NONE, NACLi_SSE3, "haddps $Vps, $Wps");

  EncodeOp0F(0x7d, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0x7d, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0x7d, 1, IMM_NONE, NACLi_SSE3, "hsubpd $Vpd, $Wpd");
  EncodeOpF20F(0x7d, 1, IMM_NONE, NACLi_SSE3, "hsubps $Vps, $Wps");

  EncodeOp0F(0x7e, 1, IMM_NONE, NACLi_MMX, "movd $Ed, $Pd");
  EncodeOpF30F(0x7e, 1, IMM_NONE, NACLi_SSE2, "movq $Vq, $Wq");
  EncodeOp660F(0x7e, 1, IMM_NONE, NACLi_SSE2, "movd $Ed, $Vd");
  EncodeOpF20F(0x7e, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0x7f, 1, IMM_NONE, NACLi_MMX, "movq $Qq, $Pq");
  EncodeOpF30F(0x7f, 1, IMM_NONE, NACLi_SSE2, "movdqu $Wdq, $Vdq");
  EncodeOp660F(0x7f, 1, IMM_NONE, NACLi_SSE2, "movdqa $Wdq, $Vdq");
  EncodeOpF20F(0x7f, 0, IMM_NONE, NACLi_INVALID, "invalid");

  /* Instructions from ?? 0F b8 to ?? 0F bf */
  EncodeOp0F(0xb8, 0, IMM_NONE, NACLi_INVALID, "reserved");
  EncodeOpF30F(0xb8, 1, IMM_NONE, NACLi_POPCNT, "popcnt");
  EncodeOpF20F(0xb8, 0, IMM_NONE, NACLi_INVALID, "reserved");

  EncodeOp0F(0xb9, 1, IMM_NONE, NACLi_OPINMRM, "$group10");
  EncodeOpF30F(0xb9, 0, IMM_NONE, NACLi_INVALID, "reserved");
  EncodeOpF20F(0xb9, 0, IMM_NONE, NACLi_INVALID, "reserved");

  EncodeOp0F(0xba, 1, IMM_FIXED1, NACLi_OPINMRM, "$group8 $Ev, $Ib");
  EncodeOpF30F(0xba, 0, IMM_FIXED1, NACLi_INVALID, "reserved");
  EncodeOpF20F(0xba, 0, IMM_NONE, NACLi_INVALID, "reserved");
  SetOp0FOpInMRM(0xba, GROUP8);

  EncodeOp0F(0xbb, 1, IMM_NONE, NACLi_ILLEGAL, "btc $Ev, $Gv");
  EncodeOpF30F(0xbb, 0, IMM_NONE, NACLi_INVALID, "reserved");
  EncodeOpF20F(0xbb, 0, IMM_NONE, NACLi_INVALID, "reserved");

  EncodeOp0F(0xbc, 1, IMM_NONE, NACLi_386, "bsf $Gv, $Ev");
  EncodeOpF30F(0xbc, 0, IMM_NONE, NACLi_INVALID, "reserved");
  EncodeOpF20F(0xbc, 0, IMM_NONE, NACLi_INVALID, "reserved");

  EncodeOp0F(0xbd, 1, IMM_NONE, NACLi_386, "bsr $Gv, $Ev");
  /* lzcnt is treated as a bsr on machines that don't have lzcnt */
  EncodeOpF30F(0xbd, 1, IMM_NONE, NACLi_LZCNT, "lzcnt $Gv, $Ev");
  EncodeOpF20F(0xbd, 0, IMM_NONE, NACLi_INVALID, "reserved");

  EncodeOp0F(0xbe, 1, IMM_NONE, NACLi_386, "movsx $Gv, $Eb");
  EncodeOp660F(0xbe, 1, IMM_NONE, NACLi_386, "movsx $Gv, $Eb");
  EncodeOpF30F(0xbe, 1, IMM_NONE, NACLi_INVALID, "reserved");
  EncodeOpF20F(0xbe, 1, IMM_NONE, NACLi_INVALID, "reserved");

  EncodeOp0F(0xbf, 1, IMM_NONE, NACLi_386, "movsx $Gv, $Ew");
  EncodeOp660F(0xbf, 1, IMM_NONE, NACLi_386, "movsx $Gv, $Ew");
  EncodeOpF30F(0xbf, 0, IMM_NONE, NACLi_INVALID, "reserved");
  EncodeOpF20F(0xbf, 0, IMM_NONE, NACLi_INVALID, "reserved");

  /* Instructions from ?? 0F C0 to ?? 0F FF */
  /* xadd and cmpxchg appear to be the only instructions designed   */
  /* for use with multiple prefix bytes. We may need to create      */
  /* a special case for this if somebody actually needs it.         */
  EncodeOp0F(0xc0, 1, IMM_NONE, NACLi_386L, "xadd $E, $G");
  EncodeOp0F(0xc1, 1, IMM_NONE, NACLi_386L, "xadd $E, $G");

  EncodeOp0F(0xc2, 1, IMM_FIXED1, NACLi_SSE, "cmpps $V, $W, $I");
  EncodeOpF30F(0xc2, 1, IMM_FIXED1, NACLi_SSE, "cmpss $V, $W, $I");
  EncodeOp660F(0xc2, 1, IMM_FIXED1, NACLi_SSE2, "cmppd $V, $W, $I");
  EncodeOpF20F(0xc2, 1, IMM_FIXED1, NACLi_SSE2, "cmpsd $V, $W, $I");

  EncodeOp0F(0xc3, 1, IMM_NONE, NACLi_SSE2, "movnti $Md, $Gd");
  EncodeOpF30F(0xc3, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xc3, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF20F(0xc3, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xc4, 1, IMM_FIXED1, NACLi_SSE, "pinsrw $Pq, $Ew, $Ib");
  EncodeOpF30F(0xc4, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xc4, 1, IMM_FIXED1, NACLi_SSE2, "pinsrw $Vdq, $Ew, $Ib");
  EncodeOpF20F(0xc4, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xc5, 1, IMM_FIXED1, NACLi_SSE, "pextrw $Gd, $PRq, $Ib");
  EncodeOpF30F(0xc5, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xc5, 1, IMM_FIXED1, NACLi_SSE2, "pextrw $Gd, $VRdq, $Ib");
  EncodeOpF20F(0xc5, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xc6, 1, IMM_FIXED1, NACLi_SSE, "shufps $Vps, $Wps, $Ib");
  EncodeOpF30F(0xc6, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xc6, 1, IMM_FIXED1, NACLi_SSE2, "shufpd $Vpd, $Wpd, $Ib");
  EncodeOpF20F(0xc6, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xc7, 1, IMM_NONE, NACLi_OPINMRM, "$group9 $Mq");
  SetOp0FOpInMRM(0xc7, GROUP9);

  EncodeOp0F(0xd0, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0xd0, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xd0, 1, IMM_NONE, NACLi_SSE3, "addsubpd $Vpd, $Wpd");
  EncodeOpF20F(0xd0, 1, IMM_NONE, NACLi_SSE3, "addsubps $Vps, $Wps");

  EncodeOp0F(0xd1, 1, IMM_NONE, NACLi_MMX, "psrlw $Pq, $Qq");
  EncodeOpF30F(0xd1, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xd1, 1, IMM_NONE, NACLi_SSE2, "psrlw $Vdq, $Wdq");
  EncodeOpF20F(0xd1, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xd2, 1, IMM_NONE, NACLi_MMX, "psrld $Pq, $Qq");
  EncodeOpF30F(0xd2, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xd2, 1, IMM_NONE, NACLi_SSE2, "psrld $Vdq, $Wdq");
  EncodeOpF20F(0xd2, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xd3, 1, IMM_NONE, NACLi_MMX, "psrlq $Pq, $Qq");
  EncodeOpF30F(0xd3, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xd3, 1, IMM_NONE, NACLi_SSE2, "psrlq $Vdq, $Wdq");
  EncodeOpF20F(0xd3, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xd4, 1, IMM_NONE, NACLi_SSE2, "paddq $Pq, $Qq");
  EncodeOpF30F(0xd4, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xd4, 1, IMM_NONE, NACLi_SSE2, "paddq $Vdq, $Wdq");
  EncodeOpF20F(0xd4, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xd5, 1, IMM_NONE, NACLi_MMX, "pmullw $Pq, $Qq");
  EncodeOpF30F(0xd5, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xd5, 1, IMM_NONE, NACLi_SSE2, "pmullw $Vdq, $Wdq");
  EncodeOpF20F(0xd5, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xd6, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0xd6, 1, IMM_NONE, NACLi_SSE2, "movq2dq $Vdq, $PRq");
  EncodeOp660F(0xd6, 1, IMM_NONE, NACLi_SSE2, "movq $Wq, $Vq");
  EncodeOpF20F(0xd6, 1, IMM_NONE, NACLi_SSE2, "movdq2q $Pq, $VRq");

  EncodeOp0F(0xd7, 1, IMM_NONE, NACLi_SSE, "pmovmskb $Gd, $PRq");
  EncodeOpF30F(0xd7, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xd7, 1, IMM_NONE, NACLi_SSE2, "pmovmskb $Gd, $VRdq");
  EncodeOpF20F(0xd7, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xd8, 1, IMM_NONE, NACLi_MMX, "psubusb $Pq, $Qq");
  EncodeOpF30F(0xd8, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xd8, 1, IMM_NONE, NACLi_SSE2, "psubusb $Vdq, $Wdq");
  EncodeOpF20F(0xd8, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xd9, 1, IMM_NONE, NACLi_MMX, "psubusw $Pq, $Qq");
  EncodeOpF30F(0xd9, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xd9, 1, IMM_NONE, NACLi_SSE2, "psubusw $Vdq, $Wdq");
  EncodeOpF20F(0xd9, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xda, 1, IMM_NONE, NACLi_SSE, "pminub $Pq, $Qq");
  EncodeOpF30F(0xda, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xda, 1, IMM_NONE, NACLi_SSE2, "pminub $Vdq, $Wdq");
  EncodeOpF20F(0xda, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xdb, 1, IMM_NONE, NACLi_MMX, "pand $Pq, $Qq");
  EncodeOpF30F(0xdb, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xdb, 1, IMM_NONE, NACLi_SSE2, "pand $Vdq, $Wdq");
  EncodeOpF20F(0xdb, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xdc, 1, IMM_NONE, NACLi_MMX, "paddusb $Pq, $Qq");
  EncodeOpF30F(0xdc, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xdc, 1, IMM_NONE, NACLi_SSE2, "paddusb $Vdq, $Wdq");
  EncodeOpF20F(0xdc, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xdd, 1, IMM_NONE, NACLi_MMX, "paddusw $Pq, $Qq");
  EncodeOpF30F(0xdd, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xdd, 1, IMM_NONE, NACLi_SSE2, "paddusw $Vdq, $Wdq");
  EncodeOpF20F(0xdd, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xde, 1, IMM_NONE, NACLi_SSE, "pmaxub $Pq, $Qq");
  EncodeOpF30F(0xde, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xde, 1, IMM_NONE, NACLi_SSE2, "pmaxub $Vdq, $Wdq");
  EncodeOpF20F(0xde, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xdf, 1, IMM_NONE, NACLi_MMX, "pandn $Pq, $Qq");
  EncodeOpF30F(0xdf, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xdf, 1, IMM_NONE, NACLi_SSE2, "pandn $Vdq, $Wdq");
  EncodeOpF20F(0xdf, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xe0, 1, IMM_NONE, NACLi_SSE, "pavgb $Pq, $Qq");
  EncodeOpF30F(0xe0, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xe0, 1, IMM_NONE, NACLi_SSE2, "pavgb $Vdq, $Wdq");
  EncodeOpF20F(0xe0, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xe1, 1, IMM_NONE, NACLi_MMX, "psraw $Pq, $Qq");
  EncodeOpF30F(0xe1, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xe1, 1, IMM_NONE, NACLi_SSE2, "psraw $Vdq, $Wdq");
  EncodeOpF20F(0xe1, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xe2, 1, IMM_NONE, NACLi_MMX, "psrad $Pq, $Qq");
  EncodeOpF30F(0xe2, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xe2, 1, IMM_NONE, NACLi_SSE2, "psrad $Vdq, $Wdq");
  EncodeOpF20F(0xe2, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xe3, 1, IMM_NONE, NACLi_SSE, "pavgw $Pq, $Qq");
  EncodeOpF30F(0xe3, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xe3, 1, IMM_NONE, NACLi_SSE2, "pavgw $Vdq, $Wdq");
  EncodeOpF20F(0xe3, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xe4, 1, IMM_NONE, NACLi_SSE, "pmulhuw $Pq, $Qq");
  EncodeOpF30F(0xe4, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xe4, 1, IMM_NONE, NACLi_SSE2, "pmulhuw $Vdq, $Wdq");
  EncodeOpF20F(0xe4, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xe5, 1, IMM_NONE, NACLi_MMX, "pmulhw $Pq, $Qq");
  EncodeOpF30F(0xe5, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xe5, 1, IMM_NONE, NACLi_SSE2, "pmulhw $Vdq, $Wdq");
  EncodeOpF20F(0xe5, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xe6, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0xe6, 1, IMM_NONE, NACLi_SSE2, "cvtdq2pd $Vpd, $Wq");
  EncodeOp660F(0xe6, 1, IMM_NONE, NACLi_SSE2, "cvttpd2dq $Vq, $Wpd");
  EncodeOpF20F(0xe6, 1, IMM_NONE, NACLi_SSE2, "cvtpd2dq $Vq, $Wpd");

  EncodeOp0F(0xe7, 1, IMM_NONE, NACLi_SSE, "movntq $Mq, $Pq");
  EncodeOpF30F(0xe7, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xe7, 1, IMM_NONE, NACLi_SSE2, "movntdq $Mdq, $Vdq");
  EncodeOpF20F(0xe7, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xe8, 1, IMM_NONE, NACLi_MMX, "psubsb $Pq, $Qq");
  EncodeOpF30F(0xe8, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xe8, 1, IMM_NONE, NACLi_SSE2, "psubsb $Vdq, $Wdq");
  EncodeOpF20F(0xe8, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xe9, 1, IMM_NONE, NACLi_MMX, "psubsw $Pq, $Qq");
  EncodeOpF30F(0xe9, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xe9, 1, IMM_NONE, NACLi_SSE2, "psubsw $Vdq, $Wdq");
  EncodeOpF20F(0xe9, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xea, 1, IMM_NONE, NACLi_SSE, "pminsw $Pq, $Qq");
  EncodeOpF30F(0xea, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xea, 1, IMM_NONE, NACLi_SSE2, "pminsw $Vdq, $Wdq");
  EncodeOpF20F(0xea, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xeb, 1, IMM_NONE, NACLi_MMX, "por $Pq, $Qq");
  EncodeOpF30F(0xeb, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xeb, 1, IMM_NONE, NACLi_SSE2, "por $Vdq, $Wdq");
  EncodeOpF20F(0xeb, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xec, 1, IMM_NONE, NACLi_MMX, "paddsb $Pq, $Qq");
  EncodeOpF30F(0xec, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xec, 1, IMM_NONE, NACLi_SSE2, "paddsb $Vdq, $Wdq");
  EncodeOpF20F(0xec, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xed, 1, IMM_NONE, NACLi_MMX, "paddsw $Pq, $Qq");
  EncodeOpF30F(0xed, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xed, 1, IMM_NONE, NACLi_SSE2, "paddsw $Vdq, $Wdq");
  EncodeOpF20F(0xed, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xee, 1, IMM_NONE, NACLi_SSE, "pmaxsw $Pq, $Qq");
  EncodeOpF30F(0xee, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xee, 1, IMM_NONE, NACLi_SSE2, "pmaxsw $Vdq, $Wdq");
  EncodeOpF20F(0xee, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xef, 1, IMM_NONE, NACLi_MMX, "pxor $Pq, $Qq");
  EncodeOpF30F(0xef, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xef, 1, IMM_NONE, NACLi_SSE2, "pxor $Vdq, $Wdq");
  EncodeOpF20F(0xef, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xf0, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0xf0, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xf0, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF20F(0xf0, 1, IMM_NONE, NACLi_SSE3, "lddqu $Vpd, $Mdq");

  EncodeOp0F(0xf1, 1, IMM_NONE, NACLi_MMX, "psllw $Pq, $Qq");
  EncodeOpF30F(0xf1, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xf1, 1, IMM_NONE, NACLi_SSE2, "psllw $Vdq, $Wdq");
  EncodeOpF20F(0xf1, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xf2, 1, IMM_NONE, NACLi_MMX, "pslld $Pq, $Qq");
  EncodeOpF30F(0xf2, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xf2, 1, IMM_NONE, NACLi_SSE2, "pslld $Vdq, $Wdq");
  EncodeOpF20F(0xf2, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xf3, 1, IMM_NONE, NACLi_MMX, "psllq $Pq, $Qq");
  EncodeOpF30F(0xf3, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xf3, 1, IMM_NONE, NACLi_SSE2, "psllq $Vdq, $Wdq");
  EncodeOpF20F(0xf3, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xf4, 1, IMM_NONE, NACLi_SSE2, "pmuludq $Pq, $Qq");
  EncodeOpF30F(0xf4, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xf4, 1, IMM_NONE, NACLi_SSE2, "pmuludq $Vdq, $Wdq");
  EncodeOpF20F(0xf4, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xf5, 1, IMM_NONE, NACLi_MMX, "pmaddwd $Pq, $Qq");
  EncodeOpF30F(0xf5, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xf5, 1, IMM_NONE, NACLi_SSE2, "pmaddwd $Vdq, $Wdq");
  EncodeOpF20F(0xf5, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xf6, 1, IMM_NONE, NACLi_SSE, "psadbw $Pq, $Qq");
  EncodeOpF30F(0xf6, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xf6, 1, IMM_NONE, NACLi_SSE2, "psadbw $Vdq, $Wdq");
  EncodeOpF20F(0xf6, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xf7, 1, IMM_NONE, NACLi_SSE, "maskmovq $Pq, $PRq");
  EncodeOpF30F(0xf7, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xf7, 1, IMM_NONE, NACLi_SSE2, "maskmovdqu $Vdq, $VRdq");
  EncodeOpF20F(0xf7, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xf8, 1, IMM_NONE, NACLi_MMX, "psubb $Pq, $Qq");
  EncodeOpF30F(0xf8, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xf8, 1, IMM_NONE, NACLi_SSE2, "psubb $Vdq, $Wdq");
  EncodeOpF20F(0xf8, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xf9, 1, IMM_NONE, NACLi_MMX, "psubw $Pq, $Qq");
  EncodeOpF30F(0xf9, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xf9, 1, IMM_NONE, NACLi_SSE2, "psubw $Vdq, $Wdq");
  EncodeOpF20F(0xf9, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xfa, 1, IMM_NONE, NACLi_MMX, "psubd $Pq, $Qq");
  EncodeOpF30F(0xfa, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xfa, 1, IMM_NONE, NACLi_SSE2, "psubd $Vdq, $Wdq");
  EncodeOpF20F(0xfa, 0, IMM_NONE, NACLi_INVALID, "invalid");

  /* Here is an SSE2 instruction that doesn't require 0x66 */
  EncodeOp0F(0xfb, 1, IMM_NONE, NACLi_SSE2, "psubq $Pq, $Qq");
  EncodeOpF30F(0xfb, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xfb, 1, IMM_NONE, NACLi_SSE2, "psubq $Vdq, $Wdq");
  EncodeOpF20F(0xfb, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xfc, 1, IMM_NONE, NACLi_MMX, "paddb $Pq, $Qq");
  EncodeOpF30F(0xfc, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xfc, 1, IMM_NONE, NACLi_SSE2, "paddb $Vdq, $Wdq");
  EncodeOpF20F(0xfc, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xfd, 1, IMM_NONE, NACLi_MMX, "paddw $Pq, $Qq");
  EncodeOpF30F(0xfd, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xfd, 1, IMM_NONE, NACLi_SSE2, "paddw $Vdq, $Wdq");
  EncodeOpF20F(0xfd, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xfe, 1, IMM_NONE, NACLi_MMX, "paddd $Pq, $Qq");
  EncodeOpF30F(0xfe, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xfe, 1, IMM_NONE, NACLi_SSE2, "paddd $Vdq, $Wdq");
  EncodeOpF20F(0xfe, 0, IMM_NONE, NACLi_INVALID, "invalid");

  EncodeOp0F(0xff, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF30F(0xff, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOp660F(0xff, 0, IMM_NONE, NACLi_INVALID, "invalid");
  EncodeOpF20F(0xff, 0, IMM_NONE, NACLi_INVALID, "invalid");

  /* three byte opcodes */
  /* 3DNow! */
  EncodeOp0F0F(0x90, 1, IMM_NONE, NACLi_3DNOW, "pfcmpge $P, $Q");
  EncodeOp0F0F(0x94, 1, IMM_NONE, NACLi_3DNOW, "pfmin $P, $Q");
  EncodeOp0F0F(0x96, 1, IMM_NONE, NACLi_3DNOW, "pfrcp $P, $Q");
  EncodeOp0F0F(0x97, 1, IMM_NONE, NACLi_3DNOW, "pfrsqrt $P, $Q");
  EncodeOp0F0F(0xa0, 1, IMM_NONE, NACLi_3DNOW, "pfcmpgt $P, $Q");
  EncodeOp0F0F(0xa4, 1, IMM_NONE, NACLi_3DNOW, "pfmax $P, $Q");
  EncodeOp0F0F(0xa6, 1, IMM_NONE, NACLi_3DNOW, "pfrcpit1 $P, $Q");
  EncodeOp0F0F(0xa7, 1, IMM_NONE, NACLi_3DNOW, "pfrsqit1 $P, $Q");
  EncodeOp0F0F(0xb0, 1, IMM_NONE, NACLi_3DNOW, "pfcmpeq $P, $Q");
  EncodeOp0F0F(0xb4, 1, IMM_NONE, NACLi_3DNOW, "pfmul $P, $Q");
  EncodeOp0F0F(0xb6, 1, IMM_NONE, NACLi_3DNOW, "pfrcpit2 $P, $Q");
  EncodeOp0F0F(0xb7, 1, IMM_NONE, NACLi_3DNOW, "pmulhrw $P, $Q");
  EncodeOp0F0F(0x0c, 1, IMM_NONE, NACLi_E3DNOW, "pi2fw $P, $Q");
  EncodeOp0F0F(0x0d, 1, IMM_NONE, NACLi_3DNOW, "pi2fd $P, $Q");
  EncodeOp0F0F(0x1c, 1, IMM_NONE, NACLi_E3DNOW, "pf2iw $P, $Q");
  EncodeOp0F0F(0x1d, 1, IMM_NONE, NACLi_3DNOW, "pf2id $P, $Q");
  EncodeOp0F0F(0x8a, 1, IMM_NONE, NACLi_E3DNOW, "pfnacc $P, $Q");
  EncodeOp0F0F(0x8e, 1, IMM_NONE, NACLi_E3DNOW, "pfpnacc $P, $Q");
  EncodeOp0F0F(0x9a, 1, IMM_NONE, NACLi_3DNOW, "pfsub $P, $Q");
  EncodeOp0F0F(0x9e, 1, IMM_NONE, NACLi_3DNOW, "pfadd $P, $Q");
  EncodeOp0F0F(0xaa, 1, IMM_NONE, NACLi_3DNOW, "pfsubr $P, $Q");
  EncodeOp0F0F(0xae, 1, IMM_NONE, NACLi_3DNOW, "pfacc $P, $Q");
  EncodeOp0F0F(0xbb, 1, IMM_NONE, NACLi_E3DNOW, "pswapd $P, $Q");
  EncodeOp0F0F(0xbf, 1, IMM_NONE, NACLi_3DNOW, "pavgusb $P, $Q");
  /* SSSE3 */
  EncodeOp0F38(0x00, 1, IMM_NONE, NACLi_SSSE3, "pshufb $P, $Q");
  EncodeOp0F38(0x01, 1, IMM_NONE, NACLi_SSSE3, "phaddw $P, $Q");
  EncodeOp0F38(0x02, 1, IMM_NONE, NACLi_SSSE3, "phaddd $P, $Q");
  EncodeOp0F38(0x03, 1, IMM_NONE, NACLi_SSSE3, "phaddsw $P, $Q");
  EncodeOp0F38(0x04, 1, IMM_NONE, NACLi_SSSE3, "pmaddubsw $P, $Q");
  EncodeOp0F38(0x05, 1, IMM_NONE, NACLi_SSSE3, "phsubw $P, $Q");
  EncodeOp0F38(0x06, 1, IMM_NONE, NACLi_SSSE3, "phsubd $P, $Q");
  EncodeOp0F38(0x07, 1, IMM_NONE, NACLi_SSSE3, "phsubsw $P, $Q");
  EncodeOp0F38(0x08, 1, IMM_NONE, NACLi_SSSE3, "psignb $P, $Q");
  EncodeOp0F38(0x09, 1, IMM_NONE, NACLi_SSSE3, "psignw $P, $Q");
  EncodeOp0F38(0x0a, 1, IMM_NONE, NACLi_SSSE3, "psignd $P, $Q");
  EncodeOp0F38(0x0b, 1, IMM_NONE, NACLi_SSSE3, "pmulhrsw $P, $Q");
  EncodeOp0F38(0x1c, 1, IMM_NONE, NACLi_SSSE3, "pabsb $P, $Q");
  EncodeOp0F38(0x1d, 1, IMM_NONE, NACLi_SSSE3, "pabsw $P, $Q");
  EncodeOp0F38(0x1e, 1, IMM_NONE, NACLi_SSSE3, "pabsd $P, $Q");
  EncodeOp0F3A(0x0f, 1, IMM_FIXED1, NACLi_SSSE3, "palignr $P, $Q, $Ib");

  BuildSSE4Tables();
  Buildx87Tables();
}

static void PrintHeader(FILE *f, const char *argv0, const char *fname) {
  time_t timeofday;

  if (time(&timeofday) < 0) {
    fprintf(stderr, "time() failed\n");
    exit(-1);
  }
  fprintf(f, "/* %s\n", fname);
  fprintf(f, " * THIS FILE IS AUTO-GENERATED. DO NOT EDIT.\n");
  fprintf(f, " * This file was auto-generated by %s on %s",
          argv0, ctime(&timeofday));
  fprintf(f, "\n");
  fprintf(f, " * You must include ncdecode.h before this file.\n");
  fprintf(f, " */\n");
}

static void PrintDecodeTable(FILE *f) {
  /* now print the table */
  int opc;
  int group, nnn;
  OpMetaInfo *opinfo = NULL;
  fprintf(f, "static const struct OpInfo kDecode1ByteOp[NCDTABLESIZE] = {\n");

  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op1ByteTable[opc]->insttype),
            g_Op1ByteTable[opc]->mrmbyte,
            g_Op1ByteTable[opc]->immtype,
            g_Op1ByteTable[opc]->opinmrm,
            g_Op1ByteTable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const struct OpInfo kDecodeModRMOp[kNaClMRMGroupsRange]");
  fprintf(f, "[kModRMOpcodeGroupSize] = {\n");
  for (group = 0; group < kNaClMRMGroupsRange; group++) {
    fprintf(f, "  {\n");
    for (nnn = 0; nnn < kModRMOpcodeGroupSize; nnn++) {
      fprintf(f, "    /* %d, %d */  { %s, %d, %d, %d },\t/* %s */\n",
              group, nnn,
              NaClInstTypeString(g_ModRMOpTable[group][nnn]->insttype),
              g_ModRMOpTable[group][nnn]->mrmbyte,
              g_ModRMOpTable[group][nnn]->immtype,
              g_ModRMOpTable[group][nnn]->opinmrm,
              g_ModRMOpTable[group][nnn]->disfmt);
    }
    fprintf(f, "  },\n");
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const struct OpInfo kDecode0FXXOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op0FXXMetaTable[opc]->insttype),
            g_Op0FXXMetaTable[opc]->mrmbyte,
            g_Op0FXXMetaTable[opc]->immtype,
            g_Op0FXXMetaTable[opc]->opinmrm,
            g_Op0FXXMetaTable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const struct OpInfo kDecode660FXXOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    opinfo = g_Op660FXXTable[opc];
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(opinfo->insttype),
            opinfo->mrmbyte,
            opinfo->immtype,
            opinfo->opinmrm,
            opinfo->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const struct OpInfo kDecodeF20FXXOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
  fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_OpF20FXXTable[opc]->insttype),
            g_OpF20FXXTable[opc]->mrmbyte,
            g_OpF20FXXTable[opc]->immtype,
            g_OpF20FXXTable[opc]->opinmrm,
            g_OpF20FXXTable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const struct OpInfo kDecodeF30FXXOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_OpF30FXXTable[opc]->insttype),
            g_OpF30FXXTable[opc]->mrmbyte,
            g_OpF30FXXTable[opc]->immtype,
            g_OpF30FXXTable[opc]->opinmrm,
            g_OpF30FXXTable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  /* three byte opcode tables */
  fprintf(f, "static const struct OpInfo kDecode0F0FOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op0F0FTable[opc]->insttype),
            g_Op0F0FTable[opc]->mrmbyte,
            g_Op0F0FTable[opc]->immtype,
            g_Op0F0FTable[opc]->opinmrm,
            g_Op0F0FTable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const struct OpInfo kDecode0F38Op[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op0F38Table[opc]->insttype),
            g_Op0F38Table[opc]->mrmbyte,
            g_Op0F38Table[opc]->immtype,
            g_Op0F38Table[opc]->opinmrm,
            g_Op0F38Table[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const struct OpInfo kDecode0F3AOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op0F3ATable[opc]->insttype),
            g_Op0F3ATable[opc]->mrmbyte,
            g_Op0F3ATable[opc]->immtype,
            g_Op0F3ATable[opc]->opinmrm,
            g_Op0F3ATable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const struct OpInfo kDecode660F38Op[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op660F38Table[opc]->insttype),
            g_Op660F38Table[opc]->mrmbyte,
            g_Op660F38Table[opc]->immtype,
            g_Op660F38Table[opc]->opinmrm,
            g_Op660F38Table[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const struct OpInfo kDecodeF20F38Op[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_OpF20F38Table[opc]->insttype),
            g_OpF20F38Table[opc]->mrmbyte,
            g_OpF20F38Table[opc]->immtype,
            g_OpF20F38Table[opc]->opinmrm,
            g_OpF20F38Table[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const struct OpInfo kDecode660F3AOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op660F3ATable[opc]->insttype),
            g_Op660F3ATable[opc]->mrmbyte,
            g_Op660F3ATable[opc]->immtype,
            g_Op660F3ATable[opc]->opinmrm,
            g_Op660F3ATable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "\n/* x87 opcode tables*/\n");
  fprintf(f, "static const struct OpInfo kDecode87D8[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op87D8[opc]->insttype),
            g_Op87D8[opc]->mrmbyte,
            g_Op87D8[opc]->immtype,
            g_Op87D8[opc]->opinmrm,
            g_Op87D8[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const struct OpInfo kDecode87D9[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op87D9[opc]->insttype),
            g_Op87D9[opc]->mrmbyte,
            g_Op87D9[opc]->immtype,
            g_Op87D9[opc]->opinmrm,
            g_Op87D9[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const struct OpInfo kDecode87DA[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op87DA[opc]->insttype),
            g_Op87DA[opc]->mrmbyte,
            g_Op87DA[opc]->immtype,
            g_Op87DA[opc]->opinmrm,
            g_Op87DA[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const struct OpInfo kDecode87DB[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op87DB[opc]->insttype),
            g_Op87DB[opc]->mrmbyte,
            g_Op87DB[opc]->immtype,
            g_Op87DB[opc]->opinmrm,
            g_Op87DB[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const struct OpInfo kDecode87DC[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op87DC[opc]->insttype),
            g_Op87DC[opc]->mrmbyte,
            g_Op87DC[opc]->immtype,
            g_Op87DC[opc]->opinmrm,
            g_Op87DC[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const struct OpInfo kDecode87DD[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op87DD[opc]->insttype),
            g_Op87DD[opc]->mrmbyte,
            g_Op87DD[opc]->immtype,
            g_Op87DD[opc]->opinmrm,
            g_Op87DD[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const struct OpInfo kDecode87DE[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op87DE[opc]->insttype),
            g_Op87DE[opc]->mrmbyte,
            g_Op87DE[opc]->immtype,
            g_Op87DE[opc]->opinmrm,
            g_Op87DE[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const struct OpInfo kDecode87DF[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  { %s, %d, %d, %d },\t/* %s */\n", opc,
            NaClInstTypeString(g_Op87DF[opc]->insttype),
            g_Op87DF[opc]->mrmbyte,
            g_Op87DF[opc]->immtype,
            g_Op87DF[opc]->opinmrm,
            g_Op87DF[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
}

static void PrintDisasmTable(FILE *f) {
  int opc;
  int group, nnn;
  fprintf(f, "static const char *kDisasm1ByteOp[NCDTABLESIZE] = {\n");
  /* now print the table */

  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op1ByteTable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const char *kDisasmModRMOp[]");
  fprintf(f, "[kNaClMRMGroupsRange] = {\n");
  for (group = 0; group < kNaClMRMGroupsRange; group++) {
    fprintf(f, "   {");
    for (nnn = 0; nnn < kModRMOpcodeGroupSize; nnn++) {
      fprintf(f, "  /* %d %d */  \"%s\",\n", group, nnn,
              g_ModRMOpTable[group][nnn]->disfmt);
    }
    fprintf(f, "   },\n");
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const char *kDisasm0FXXOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op0FXXMetaTable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const char *kDisasm660FXXOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op660FXXTable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const char *kDisasmF20FXXOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_OpF20FXXTable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "static const char *kDisasmF30FXXOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_OpF30FXXTable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  /* three byte opcode tables including 3DNow */
  fprintf(f, "static const char *kDisasm0F0FOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op0F0FTable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const char *kDisasm0F38Op[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op0F38Table[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const char *kDisasm660F38Op[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op660F38Table[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const char *kDisasmF20F38Op[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_OpF20F38Table[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const char *kDisasm0F3AOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op0F3ATable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const char *kDisasm660F3AOp[NCDTABLESIZE] = {\n");
  /* now print the table */
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op660F3ATable[opc]->disfmt);
  }
  fprintf(f, "};\n\n");

  fprintf(f, "\n/* x87 opcode tables*/\n");
  fprintf(f, "static const char *kDisasm87D8[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op87D8[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const char *kDisasm87D9[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op87D9[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const char *kDisasm87DA[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op87DA[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const char *kDisasm87DB[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op87DB[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const char *kDisasm87DC[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op87DC[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const char *kDisasm87DD[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op87DD[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const char *kDisasm87DE[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op87DE[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
  fprintf(f, "static const char *kDisasm87DF[NCDTABLESIZE] = {\n");
  for (opc = 0; opc < NCDTABLESIZE; opc++) {
    fprintf(f, "  /* %02x */  \"%s\",\n", opc, g_Op87DF[opc]->disfmt);
  }
  fprintf(f, "};\n\n");
}


FILE *mustopen(const char *fname, const char *how) {
  FILE *f = fopen(fname, how);
  if (f == NULL) {
    fprintf(stderr, "could not fopen(%s, %s)\n", fname, how);
    fprintf(stderr, "exiting now\n");
    exit(-1);
  }
  return f;
}

#define DECODETAB "ncdecodetab.h"
#define DISASMTAB "ncdisasmtab.h"
int main(const int argc, const char *argv[]) {
  FILE *f;
  UNREFERENCED_PARAMETER(argc);
  BuildMetaTables();
  f = mustopen(DECODETAB, "w");
  PrintHeader(f, argv[0], DECODETAB);
  PrintDecodeTable(f);
  fclose(f);

  f = mustopen(DISASMTAB, "w");
  PrintHeader(f, argv[0], DISASMTAB);
  PrintDisasmTable(f);
  fclose(f);
  return 0;
}

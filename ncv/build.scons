# -*- python -*-
# Copyright 2008, Google Inc.
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#     * Neither the name of Google Inc. nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import os
import sys
Import('env')

# Needed, so we can include generated headers with paths from googleclient.
# TODO: figure out a better way to do this.
env.Append(CPPPATH=['.'])


# TODO: clean this up more
sdl_env = env.Clone()
sdl_env.Tool('sdl')
sdl_dll = []
if env.Bit('windows'):
  sdl_dll += sdl_env.Replicate('$STAGING_DIR', '$SDL_DIR/lib/SDL.dll')
  sdl_dll += sdl_env.Replicate('.', '$SDL_DIR/lib/SDL.dll')
elif env.Bit('mac'):
  sdl_dll += sdl_env.Replicate('$TARGET_ROOT/Frameworks',
                               '$SDL_DIR/SDL.framework')
  sdl_dll += sdl_env.Replicate('$OBJ_ROOT/Frameworks', '$SDL_DIR/SDL.framework')
elif env.Bit('linux'):
  sdl_dll += sdl_env.Replicate('$STAGING_DIR', '$SDL_DIR/lib/libSDL-1.2.so.0')
  sdl_dll += sdl_env.Replicate('.', '$SDL_DIR/lib/libSDL-1.2.so.0')

# Make a copy of debug CRT for now.
# TODO: there should be a better way to generalize this requirement.
crt = []
if env.AllBits('windows', 'debug'):
  for i in ['.', '$STAGING_DIR']:
    crt += env.Replicate(i, '$VC80_DIR/vc/redist/Debug_NonRedist/'
                         'x86/Microsoft.VC80.DebugCRT')
    crt += env.Replicate(i, '$VC80_DIR/vc/redist/x86/Microsoft.VC80.CRT')


# Isolate the environment for ncdecode_table to prevent a cycle.
env_decode_table = env.Clone()
env_decode_table['COVERAGE_LINKCOM_EXTRAS'] = None
ncdecode_table = env_decode_table.ComponentProgram('ncdecode_table',
                                                   'ncdecode_table.c')
env.Requires(ncdecode_table, crt)
env.Requires(ncdecode_table, sdl_dll)

# Link everyone else to ncvalidate.
env.Append(LIBS = ['ncvalidate', 'ncvtest'])

env.ComponentLibrary('ncvalidate', ['nacl_cpuid.c',
                                    'ncdecode.c',
                                    'ncvalidate.c'])

env.ComponentLibrary('ncvtest', ['ncdecode_tests.c',
                                 'ncfileutil.c'])

ncv_generated_headers = ['native_client/ncv/ncdecodetab.h',
                         'native_client/ncv/ncdisasmtab.h']

for f in ncv_generated_headers:
  src_f = os.path.join(env['SOURCE_ROOT'],'googleclient',f)
  if os.path.exists(src_f):
    print >>sys.stderr, 'A copy of generated file %s exists in the source' % f
    print >>sys.stderr, 'at %s' % src_f
    print >>sys.stderr, 'and will shadow the generated version.  PLEASE FIX.'
    sys.exit(1)

# TODO: formalize code generation and the consumption of code better
env.Command(
    ncv_generated_headers,
    ncdecode_table,
    'cd ${TARGET.dir} && ${SOURCE.abspath}')

env.Append(LIBS = ['platform_qual_lib'])
nacl_cpuid = env.ComponentProgram('nacl_cpuid', 'nacl_cpuid_test.c')
env.Requires(nacl_cpuid, crt)
env.Requires(nacl_cpuid, sdl_dll)

env.ComponentProgram('ncdis', 'ncdis.c')
ncval = env.ComponentProgram('ncval', ['ncval.c', 'ncval_tests.c'])
env.Requires(ncval, crt)
env.Requires(ncval, sdl_dll)

env.ComponentProgram('ncrewrite', 'ncrewrite.c')


# TODO: get OSX and Windows working below here
if env['TARGET_PLATFORM'] in ['WINDOWS', 'MAC']:
  Return()


node = env.Command('nacl_cpuid_test.out',
                   nacl_cpuid,
                   '${SOURCES[0].abspath} > ${TARGET}')

env.ComponentTestOutput('cpuid_test', node)

env.AddNodeToLargeTestsSuite(node)
AlwaysBuild(node)


#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "native_client/include/elf.h"
#include "native_client/ncv/ncdecode.h"


static int write_nops = 0;

static void handle_instruction(const struct NCDecoderState *mstate)
{
  /* Rewrites
       nop; nop; nop
       jmp *%reg
     to
       and $0xffffffe0, %reg
       jmp *%reg
  */
  if(mstate->opinfo->insttype == NACLi_INDIRECT) {
    unsigned char and_instr[3] = {
      0x83, /* "and" opcode */
      0xe0 | modrm_rm(mstate->inst.maddr[1]), /* operand */
      0xe0, /* alignment mask, constant ~31 */
    };

    if((PreviousInst(mstate, -1) != NULL &&
	memcmp(mstate->inst.maddr - 3, and_instr, 3) == 0) ||
       (PreviousInst(mstate, -3) != NULL &&
	mstate->inst.maddr[-3] == 0x90 &&
	mstate->inst.maddr[-2] == 0x90 &&
	mstate->inst.maddr[-1] == 0x90)) {
      if(write_nops)
	memset(mstate->inst.maddr - 3, 0x90, 3);
      else
	memcpy(mstate->inst.maddr - 3, and_instr, 3);
    }
    else {
      fprintf(stderr, "%08x: cannot rewrite\n", mstate->inst.vaddr);
    }
  }
}

static void fixup_file(const char *filename)
{
  int fd = open(filename, O_RDWR);
  if(fd < 0) {
    perror("open");
    exit(1);
  }
  struct stat st;
  if(fstat(fd, &st) < 0) {
    perror("fstat");
    exit(1);
  }
  unsigned char *data = mmap(NULL, st.st_size, PROT_READ | PROT_WRITE,
			     MAP_SHARED, fd, 0);
  if(data == MAP_FAILED) {
    perror("mmap");
    exit(1);
  }
  if(close(fd) < 0) {
    perror("close");
    exit(1);
  }
  Elf32_Ehdr *header = (Elf32_Ehdr *) data;
  assert(memcmp(header->e_ident, ELFMAG, strlen(ELFMAG)) == 0);
  int i;
  for(i = 0; i < header->e_shnum; i++) {
    Elf32_Shdr *section = (Elf32_Shdr *) (data + header->e_shoff +
					  header->e_shentsize * i);
    if((section->sh_flags & SHF_EXECINSTR) != 0) {
      NCDecodeSegment(data + section->sh_offset, section->sh_addr,
		      section->sh_size, NULL);
    }
  }
  if(munmap(data, st.st_size) < 0) {
    perror("munmap");
    exit(1);
  }
}

int main(int argc, const char *argv[])
{
  int i;
  NCDecodeRegisterCallbacks(handle_instruction, NULL, NULL, NULL);
  for(i = 1; i < argc; i++) {
    const char *arg = argv[i];
    if(strcmp(arg, "--nop") == 0)
      write_nops = 1;
    else
      fixup_file(arg);
  }
  return 0;
}

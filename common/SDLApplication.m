// @@REWRITE(insert c-copyright)
// @@REWRITE(delete-start)
// Copyright 2008, Google Inc.  All rights reserved.
//
// @@REWRITE(delete-end)
// MacOSX main() for SDL applications.
//

#import <AppKit/AppKit.h>
#import "SDL.h"
#import <Carbon/Carbon.h>


#undef main

int main(int argc, char **argv) {
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  [NSApplication sharedApplication];

  if (!getenv("NACL_LAUNCHED_FROM_BROWSER")) {
    ProcessSerialNumber psn;
    GetCurrentProcess(&psn);
    TransformProcessType(&psn,  kProcessTransformToForegroundApplication);
    [NSApp activateIgnoringOtherApps:YES];
  }

  SDL_main(argc, argv);
  [pool release];
  return 0;
}

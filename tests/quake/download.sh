#!/bin/bash
# Copyright 2008, Google Inc.
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#     * Neither the name of Google Inc. nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


set -o nounset
set -o errexit

readonly QUAKE_SRC_URL=http://www.libsdl.org/projects/quake/src/sdlquake-1.0.9.tar.gz
readonly QUAKE_DATA_URL=http://www.libsdl.org/projects/quake/data/quakesw-1.0.6.tar.gz

# clean out source except for nacl_file.c
for i in *.c; do
  if [ "nacl_file.c" != ${i} ]; then
    rm ${i}
  fi
done
# remove all headers, if any
rm -f ./*.h
# download tarballs
if which wget ; then
  wget ${QUAKE_SRC_URL} -O quake_src.tgz
  wget ${QUAKE_DATA_URL} -O quake_data.tgz
elif which curl ; then
  curl --url ${QUAKE_SRC_URL} -o quake_src.tgz
  curl --url ${QUAKE_DATA_URL} -o quake_data.tgz
else
  echo "Please install curl or wget and rerun this script"
  exit -1
fi
# extract files from tarballs
tar -x --strip-components=1 -f quake_src.tgz
tar -x -f quake_data.tgz
# apply Native Client patch
patch -p1 <nacl_quake.patch
# clean up - remove the tarballs
rm -rf quake_src.tgz
rm -rf quake_data.tgz

/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "native_client/tests/cloudfs/plugin.h"

#include <stdio.h>
#include <string.h>

#include <nacl/nacl_util.h>

NPIdentifier ScriptablePluginObject::id_getfile;
NPIdentifier ScriptablePluginObject::id_document;
NPIdentifier ScriptablePluginObject::id_body;
NPIdentifier ScriptablePluginObject::id_create_element;
NPIdentifier ScriptablePluginObject::id_create_text_node;
NPIdentifier ScriptablePluginObject::id_append_child;
NPObject* ScriptablePluginObject::window_object;

std::map<NPIdentifier, ScriptablePluginObject::Method>*
    ScriptablePluginObject::method_table;

std::map<NPIdentifier, ScriptablePluginObject::Property>*
    ScriptablePluginObject::property_table;

bool ScriptablePluginObject::InitializeIdentifiers(NPObject* object,
                                                   const char* canvas) {
  window_object = object;

  id_getfile = NPN_GetStringIdentifier("getfile");
  id_document = NPN_GetStringIdentifier("document");
  id_body = NPN_GetStringIdentifier("body");
  id_create_element = NPN_GetStringIdentifier("createElement");
  id_create_text_node = NPN_GetStringIdentifier("createTextNode");
  id_append_child = NPN_GetStringIdentifier("appendChild");

  method_table =
    new(std::nothrow) std::map<NPIdentifier, Method>;
  if (method_table == NULL) {
    return false;
  }

  method_table->insert(
    std::pair<NPIdentifier, Method>(id_getfile,
                                    &ScriptablePluginObject::GetFile));

  property_table =
    new(std::nothrow) std::map<NPIdentifier, Property>;
  if (property_table == NULL) {
    return false;
  }

  return true;
}

bool ScriptablePluginObject::HasMethod(NPIdentifier name) {
  NPUTF8* utf8 = NPN_UTF8FromIdentifier(name);
  NPN_MemFree(utf8);

  std::map<NPIdentifier, Method>::iterator i;
  i = method_table->find(name);
  return i != method_table->end();
}

bool ScriptablePluginObject::HasProperty(NPIdentifier name) {
  NPUTF8* utf8 = NPN_UTF8FromIdentifier(name);
  NPN_MemFree(utf8);

  std::map<NPIdentifier, Property>::iterator i;
  i = property_table->find(name);
  return i != property_table->end();
}

bool ScriptablePluginObject::GetProperty(NPIdentifier name,
                                         NPVariant *result) {
  NPUTF8* utf8 = NPN_UTF8FromIdentifier(name);
  NPN_MemFree(utf8);

  VOID_TO_NPVARIANT(*result);

  std::map<NPIdentifier, Property>::iterator i;
  i = property_table->find(name);
  if (i != property_table->end()) {
    return (this->*(i->second))(result);
  }
  return false;
}

bool ScriptablePluginObject::Invoke(NPIdentifier name,
                                    const NPVariant* args, uint32_t arg_count,
                                    NPVariant* result) {
  std::map<NPIdentifier, Method>::iterator i;
  i = method_table->find(name);
  if (i != method_table->end()) {
    return (this->*(i->second))(args, arg_count, result);
  }
  return false;
}

void ScriptablePluginObject::Notify(const char* url, void* notify_data,
                                    nacl::Handle handle) {
  NPP npp = static_cast<NPP>(notify_data);
  if (handle == nacl::kInvalidHandle) {
    return;
  }

  NPVariant document;
  VOID_TO_NPVARIANT(document);
  if (!NPN_GetProperty(npp, window_object, id_document, &document) ||
      !NPVARIANT_IS_OBJECT(document)) {
    NPN_ReleaseVariantValue(&document);
    nacl::Close(handle);
    return;
  }

  char buffer[1024];
  int count = read(handle, buffer, sizeof buffer);
  while (count > 0) {
    buffer[count-1] = 0;
    DoWrite(buffer, count, npp, document);
    count = read(handle, buffer, sizeof buffer);
  }
  NPN_ReleaseVariantValue(&document);
  close(handle);
}

void ScriptablePluginObject::DoWrite(const char *buffer, int count,
                                     NPP npp, NPVariant doc) {
  NPVariant string;
  STRINGZ_TO_NPVARIANT("pre", string);

  // TODO: Is it better append to to a single pre tag?
  NPVariant pre;
  VOID_TO_NPVARIANT(pre);
  if (NPN_Invoke(npp, NPVARIANT_TO_OBJECT(doc), id_create_element,
                 &string, 1, &pre) &&
      NPVARIANT_IS_OBJECT(pre)) {
    STRINGN_TO_NPVARIANT(buffer, count, string);

    NPVariant text;
    VOID_TO_NPVARIANT(text);
    if (NPN_Invoke(npp, NPVARIANT_TO_OBJECT(doc), id_create_text_node,
                   &string, 1, &text) &&
        NPVARIANT_IS_OBJECT(text)) {
      NPVariant result;
      VOID_TO_NPVARIANT(result);
      NPN_Invoke(npp, NPVARIANT_TO_OBJECT(pre), id_append_child, &text, 1,
                 &result);
      NPN_ReleaseVariantValue(&result);
    }
    NPN_ReleaseVariantValue(&text);

    NPVariant body;
    VOID_TO_NPVARIANT(body);
    if (NPN_GetProperty(npp, NPVARIANT_TO_OBJECT(doc), id_body, &body) &&
        NPVARIANT_IS_OBJECT(body)) {
      NPVariant result;
      VOID_TO_NPVARIANT(result);
      NPN_Invoke(npp, NPVARIANT_TO_OBJECT(body), id_append_child, &pre, 1,
                 &result);
      NPN_ReleaseVariantValue(&result);
    }
    NPN_ReleaseVariantValue(&body);
  }
  NPN_ReleaseVariantValue(&pre);
}

bool ScriptablePluginObject::GetFile(const NPVariant *args,
                                     uint32_t count,
                                     NPVariant* result) {
  const NPString npfname = NPVARIANT_TO_STRING(args[0]);
  char *fname = (char *)NPN_MemAlloc(npfname.utf8length + 1);
  strncpy(fname, npfname.utf8characters, npfname.utf8length);
  fname[npfname.utf8length] = 0;

  NaClNPN_OpenURL(npp_, fname, npp_, Notify);
  NPN_MemFree(fname);
  return true;
}

Plugin::Plugin(NPP npp, const char* canvas)
    : npp_(npp),
      scriptable_object_(NULL),
      window_object_(NULL) {

  if (NPERR_NO_ERROR != NPN_GetValue(npp_, NPNVWindowNPObject, &window_object_)) {
    return;
  }

  if (!ScriptablePluginObject::InitializeIdentifiers(window_object_, canvas)) {
    return;
  }
}

Plugin::~Plugin() {
  if (scriptable_object_) {
    NPN_ReleaseObject(scriptable_object_);
  }
  if (window_object_) {
    NPN_ReleaseObject(window_object_);
  }
}

NPObject* Plugin::GetScriptableObject() {
  if (scriptable_object_ == NULL) {
    scriptable_object_ =
      NPN_CreateObject(npp_, &ScriptablePluginObject::np_class);
  }

  if (scriptable_object_) {
    NPN_RetainObject(scriptable_object_);
  }

  return scriptable_object_;
}

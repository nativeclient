
#include <Python.h>

/* TODO: get this from installed headers */
#include "../../../glibc/sysdeps/nacl/nacl_syscalls.h"

/* TODO: get these from libnacl rather than hardcoding them */
static int (*imc_sendmsg)(int fd, const struct NaClImcMsgHdr *msg, int flags) = 
  NACL_SYSCALL_ADDR(NACL_sys_imc_sendmsg);
static int (*imc_recvmsg)(int fd, struct NaClImcMsgHdr *msg, int flags) = 
  NACL_SYSCALL_ADDR(NACL_sys_imc_recvmsg);


static PyObject *py_imc_sendmsg(PyObject *self, PyObject *args)
{
  int fd;
  char *data;
  int data_size;
  struct NaClImcMsgIoVec iov;
  struct NaClImcMsgHdr msg;

  if(!PyArg_ParseTuple(args, "is#", &fd, &data, &data_size))
    return NULL;
  iov.base = data;
  iov.length = data_size;
  msg.iov = &iov;
  msg.iov_length = 1;
  msg.descv = NULL;
  msg.desc_length = 0;
  imc_sendmsg(fd, &msg, 0);
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *py_imc_recvmsg(PyObject *self, PyObject *args)
{
  int fd;
  char buffer[1024];
  struct NaClImcMsgIoVec iov;
  struct NaClImcMsgHdr msg;
  int result;

  if(!PyArg_ParseTuple(args, "i", &fd))
    return NULL;
  iov.base = buffer;
  iov.length = sizeof(buffer);
  msg.iov = &iov;
  msg.iov_length = 1;
  msg.descv = NULL;
  msg.desc_length = 0;
  result = imc_recvmsg(fd, &msg, 0);
  if(result >= 0)
    return PyString_FromStringAndSize(buffer, result);
  else {
    Py_INCREF(Py_None);
    return Py_None;
  }
}

static PyMethodDef module_methods[] = {
  { "imc_sendmsg", py_imc_sendmsg, METH_VARARGS,
    "NaCl imc_sendmsg() syscall: sends a message" },
  { "imc_recvmsg", py_imc_recvmsg, METH_VARARGS,
    "NaCl imc_recvmsg() syscall: receives a message" },
  { NULL, NULL, 0, NULL }  /* Sentinel */
};

void initnacl(void)
{
  Py_InitModule3("nacl", module_methods, "NaCl syscall wrappers");
}

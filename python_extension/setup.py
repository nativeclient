
from distutils.core import setup, Extension

setup(name="nacl",
      ext_modules=[Extension("nacl", sources=["python-nacl.c"])])

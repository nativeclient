// Copyright 2009 Google Inc. All Rights Reserved.
// Author: neha@google.com (Neha Narula)
//
// Base class used to define policy objects for specific syscalls.

#ifndef NATIVE_CLIENT_SANDBOX_LINUX_NACL_SYSCALL_CHECKER_H_
#define NATIVE_CLIENT_SANDBOX_LINUX_NACL_SYSCALL_CHECKER_H_

#include <sys/user.h>
#include "native_client/sandbox/linux/nacl_syscall_filter.h"

class SyscallChecker {
 public:
  SyscallChecker() {
    allowed_ = false;
  }
  virtual ~SyscallChecker();

  virtual bool RunRule(struct user_regs_struct* regs,
                       SandboxState* state) {
    return true;
  }

  const bool allowed() {
    return allowed_;
  }

  const bool once() {
    return once_;
  }

  void set_allowed(bool allowed) {
    allowed_ = allowed;
  }

  void set_once(bool once) {
    once_ = once;
  }

 private:
  bool allowed_;
  bool once_;
};

#endif  // NATIVE_CLIENT_SANDBOX_LINUX_NACL_SYSCALL_CHECKER_H_

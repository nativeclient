
#include <stdio.h>
#include <string.h>

#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>

#include <npapi.h>
#include <npupp.h>
#include <npruntime.h>

#include "native_client/intermodule_comm/nacl_imc_c.h"
#include "native_client/service_runtime/include/sys/fcntl.h"
#include "native_client/service_runtime/nacl_desc_base.h"
#include "native_client/service_runtime/nacl_desc_imc.h"
#include "native_client/service_runtime/nacl_desc_io.h"
#include "native_client/service_runtime/nrd_xfer_lib/nrd_all_modules.h"
#include "native_client/service_runtime/nrd_xfer_lib/nrd_xfer.h"
#include "native_client/service_runtime/nrd_xfer_lib/nrd_xfer_effector.h"


static NPNetscapeFuncs g_funcs;
static NPIdentifier ident_launch;
static NPIdentifier ident_get_file;
static NPIdentifier ident_send;
static NPIdentifier ident_length;

#define NPN_CreateObject (g_funcs.createobject)
#define NPN_RetainObject (g_funcs.retainobject)
#define NPN_ReleaseObject (g_funcs.releaseobject)
#define NPN_ReleaseVariantValue (g_funcs.releasevariantvalue)
#define NPN_InvokeDefault (g_funcs.invokeDefault)
#define NPN_GetProperty (g_funcs.getproperty)
#define NPN_UTF8FromIdentifier (g_funcs.utf8fromidentifier)
#define NPN_GetStringIdentifier (g_funcs.getstringidentifier)
#define NPN_GetIntIdentifier (g_funcs.getintidentifier)
#define NPN_GetURLNotify (g_funcs.geturlnotify)
#define NPN_PluginThreadAsyncCall (g_funcs.pluginthreadasynccall)


static void PrintIdentifier(NPIdentifier ident)
{
  char *str = NPN_UTF8FromIdentifier(ident);
  printf("  ident = \"%s\"\n", str);
  free(str);
}

static bool
IsType(NPObject *npobj, NPClass *npclass)
{
  /* The underscore in "_class" suggests this member is not part of
     the public API, but it could just be to prevent a clash with the
     C++ keyword. */
  return npobj->_class == npclass;
}


/* Mozilla does not provide default behaviour for these NoOp_* methods
   if NULL is provided instead in the NPClass. */

static bool
NoOp_HasMethod(NPObject* obj, NPIdentifier methodName)
{
  return false;
}

static bool
NoOp_HasProperty(NPObject *obj, NPIdentifier propertyName)
{
  return false;
}

static bool
NoOp_GetProperty(NPObject *obj, NPIdentifier propertyName, NPVariant *result)
{
  return false;
}


struct FileObj {
  struct NPObject header;
  struct NaClDesc *desc;
  /* We keep a filename to pass to sel_ldr, but there's a chance the
     file could be deleted from the cache.
     TODO: pass FDs to sel_ldr instead. */
  char *filename;
};

static NPObject *
FileObj_Allocate(NPP npp, NPClass *npclass)
{
  struct FileObj *obj = malloc(sizeof(struct FileObj));
  return &obj->header;
}

static void
FileObj_Deallocate(NPObject *npobj)
{
  struct FileObj *obj = (struct FileObj *) npobj;
  NaClDescUnref(obj->desc);
  free(obj->filename);
  free(obj);
}

static NPClass FileObj_Vtable = {
  .structVersion = NP_CLASS_STRUCT_VERSION,
  .allocate = FileObj_Allocate,
  .deallocate = FileObj_Deallocate,
  .invalidate = NULL,
  .hasMethod = NoOp_HasMethod,
  .invoke = NULL,
  .invokeDefault = NULL,
  .hasProperty = NoOp_HasProperty,
  .getProperty = NoOp_GetProperty,
  .setProperty = NULL,
  .removeProperty = NULL,
};


struct SendObj {
  struct NPObject header;
  NPP plugin;
  struct NaClDesc *desc;
};

static NPObject *
SendObj_Allocate(NPP npp, NPClass *npclass)
{
  struct SendObj *obj = malloc(sizeof(struct SendObj));
  obj->plugin = npp;
  return &obj->header;
}

static void
SendObj_Deallocate(NPObject *npobj)
{
  struct SendObj *obj = (struct SendObj *) npobj;
  NaClDescUnref(obj->desc);
  free(obj);
}

static bool
SendObj_HasMethod(NPObject* npobj, NPIdentifier methodName)
{
  return methodName == ident_send;
}

static bool
ConvertDescArray(NPP plugin, NPObject *array, struct NaClImcTypedMsgHdr *header)
{
  NPVariant length_np;
  if(!NPN_GetProperty(plugin, array, ident_length, &length_np) ||
     !NPVARIANT_IS_INT32(length_np)) {
    NPN_ReleaseVariantValue(&length_np);
    return false;
  }
  int length = NPVARIANT_TO_INT32(length_np);
  NPN_ReleaseVariantValue(&length_np);

  /* Firstly, there will be a limit to the number of capabilities we
     can send in a message.  Secondly, the array is not necessarily a
     primitive Javascript array; its .length property could be much
     larger than the amount of space Javascript is normally allowed to
     allocate. */
  if(length > 32)
    return false;

  struct NaClDesc **descs = malloc(sizeof(struct NaClDesc *) * length);
  if(descs == NULL)
    return false;
  int i;
  for(i = 0; i < length; i++) {
    NPIdentifier index_id = NPN_GetIntIdentifier(i);
    NPVariant element;
    if(!NPN_GetProperty(plugin, array, index_id, &element) ||
       !NPVARIANT_IS_OBJECT(element))
      return false; /* TODO: clear up */
    NPObject *file_npobj = NPVARIANT_TO_OBJECT(element);
    if(!IsType(file_npobj, &FileObj_Vtable))
      return false; /* TODO: clear up */
    struct FileObj *file_obj = (struct FileObj *) file_npobj;
    NaClDescRef(file_obj->desc);
    descs[i] = file_obj->desc;
    NPN_ReleaseVariantValue(&element);
  }
  header->ndescv = descs;
  header->ndesc_length = length;
  return true;
}

static bool
SendObj_Invoke(NPObject *npobj, NPIdentifier methodName,
	       const NPVariant *args, uint32_t argCount, NPVariant *result)
{
  struct SendObj *obj = (struct SendObj *) npobj;
  if(methodName == ident_send) {
    if(argCount == 2 &&
       NPVARIANT_IS_STRING(args[0]) &&
       NPVARIANT_IS_OBJECT(args[1])) {
      NPString message_data = NPVARIANT_TO_STRING(args[0]);
      NPObject *caps_array = NPVARIANT_TO_OBJECT(args[1]);
      struct NaClNrdXferEffector effector;
      if(!NaClNrdXferEffectorCtor(&effector, obj->desc))
	return false;
      struct NaClImcMsgIoVec iovec;
      struct NaClImcTypedMsgHdr header;
      if(!ConvertDescArray(obj->plugin, caps_array, &header))
	return false;
      /* TODO: allow sending any data, not just UTF-8 */
      iovec.base = (char *) message_data.utf8characters;
      iovec.length = message_data.utf8length;
      header.iov = &iovec;
      header.iov_length = 1;
      header.flags = 0;
      NaClImcSendTypedMessage(obj->desc, (struct NaClDescEffector *) &effector,
			      &header, 0);
      int i;
      for(i = 0; i < header.ndesc_length; i++)
	NaClDescUnref(header.ndescv[i]);
      free(header.ndescv);
      effector.base.vtbl->Dtor(&effector.base);
    }
    VOID_TO_NPVARIANT(*result);
    return true;
  }
  return false;
}

static NPClass SendObj_Vtable = {
  .structVersion = NP_CLASS_STRUCT_VERSION,
  .allocate = SendObj_Allocate,
  .deallocate = SendObj_Deallocate,
  .invalidate = NULL,
  .hasMethod = SendObj_HasMethod,
  .invoke = SendObj_Invoke,
  .invokeDefault = NULL,
  .hasProperty = NoOp_HasProperty,
  .getProperty = NoOp_GetProperty,
  .setProperty = NULL,
  .removeProperty = NULL,
};


struct LauncherObj {
  struct NPObject header;
  NPP plugin;
};

static NPObject *
LauncherObj_Allocate(NPP npp, NPClass *npclass)
{
  struct LauncherObj *obj = malloc(sizeof(struct LauncherObj));
  obj->plugin = npp;
  return &obj->header;
}

static bool
LauncherObj_HasMethod(NPObject* obj, NPIdentifier methodName)
{
  return (methodName == ident_launch ||
	  methodName == ident_get_file);
}

struct ReceiveCallbackArgs {
  NPP plugin;
  NPObject *callback;
  char buffer[1024];
  int size;
};

static void
DoReceiveCallback(void *handle)
{
  struct ReceiveCallbackArgs *call = handle;
  NPVariant args[1];
  NPVariant result;
  /* TODO: don't assume this data is UTF-8 */
  STRINGN_TO_NPVARIANT(call->buffer, call->size, args[0]);
  NPN_InvokeDefault(call->plugin, call->callback, args, 1, &result);
  NPN_ReleaseVariantValue(&result);
  free(call);
  /* We don't do NPN_ReleaseObject() on the callback here because we
     couldn't do NPN_RetainObject() earlier. */
}

struct ReceiveThreadArgs {
  NPP plugin;
  NPObject *callback;
  struct NaClDesc *desc;
};

static void *
ReceiveThread(void *handle)
{
  /* We create one thread for every socket we listen on.  It would be
     more efficient to have one thread using poll().  NPAPI doesn't
     provide a way for us to share the browser's event loop. */
  struct ReceiveThreadArgs *args = handle;
  struct NaClNrdXferEffector effector;
  if(!NaClNrdXferEffectorCtor(&effector, args->desc))
    return NULL;
  while(true) {
    struct ReceiveCallbackArgs *call =
      malloc(sizeof(struct ReceiveCallbackArgs));
    if(call == NULL)
      break;
    struct NaClImcMsgIoVec iovec;
    struct NaClImcTypedMsgHdr header;
    iovec.base = call->buffer;
    iovec.length = sizeof(call->buffer);
    header.iov = &iovec;
    header.iov_length = 1;
    header.ndescv = NULL;
    header.ndesc_length = 0;
    header.flags = 0;
    int got_bytes =
      NaClImcRecvTypedMessage(args->desc, (struct NaClDescEffector *) &effector,
			      &header, 0);
    if(got_bytes < 0) {
      free(call);
      break;
    }
    /* We can't call NPN_RetainObject() here because it is not safe to
       call it from this thread.  We assume that
       NPN_PluginThreadAsyncCall() schedules functions to be called in
       order, so that the function is not freed until later. */
    call->plugin = args->plugin;
    call->callback = args->callback;
    call->size = got_bytes;
    NPN_PluginThreadAsyncCall(args->plugin, DoReceiveCallback, call);
  }
  effector.base.vtbl->Dtor(&effector.base);
  NaClDescUnref(args->desc); /* Supposed to be thread safe */
  NPN_PluginThreadAsyncCall(args->plugin, (void (*)(void *)) NPN_ReleaseObject,
			    args->callback);
  free(args);
  return NULL;
}

static bool
ConvertArgvArray(NPP plugin, NPObject *array,
		 char ***result_argv, int *result_size)
{
  NPVariant length_np;
  if(!NPN_GetProperty(plugin, array, ident_length, &length_np) ||
     !NPVARIANT_IS_INT32(length_np)) {
    NPN_ReleaseVariantValue(&length_np);
    return false;
  }
  int length = NPVARIANT_TO_INT32(length_np);
  NPN_ReleaseVariantValue(&length_np);

  /* Arbitrary limit */
  if(length > 100)
    return false;

  char **argv = malloc(sizeof(char *) * length);
  if(argv == NULL)
    return false;
  int i;
  for(i = 0; i < length; i++) {
    NPIdentifier index_id = NPN_GetIntIdentifier(i);
    NPVariant element;
    if(!NPN_GetProperty(plugin, array, index_id, &element) ||
       !NPVARIANT_IS_STRING(element))
      return false; /* TODO: clear up */
    NPString string = NPVARIANT_TO_STRING(element);
    char *copy = malloc(string.utf8length + 1);
    if(copy == NULL)
      return false; /* TODO: clear up */
    /* TODO: allow sending any data, not just UTF-8 */
    memcpy(copy, string.utf8characters, string.utf8length);
    copy[string.utf8length] = 0;
    argv[i] = copy;
    NPN_ReleaseVariantValue(&element);
  }
  *result_argv = argv;
  *result_size = length;
  return true;
}

static struct NaClDesc *
LaunchProcess(NPP plugin, const char *executable, char **argv, int argv_size,
	      NPObject *receive_callback)
{
  int socks[2];
  if(NaClSocketPair(socks) < 0) {
    perror("socketpair");
    return NULL;
  }
  int pid = fork();
  if(pid < 0) {
    NaClClose(socks[0]);
    NaClClose(socks[1]);
    perror("fork");
    return NULL; /* TODO: report error */
  }
  if(pid == 0) {
    NaClClose(socks[1]);
    char fd_buffer[40];
    snprintf(fd_buffer, sizeof(fd_buffer), "%i:%i", 3, socks[0]);

    int args_len = 9 + argv_size;
    const char *args[args_len];
    int i = 0;
    args[i++] = "sel_ldr";
    args[i++] = "-i";
    args[i++] = fd_buffer;
    args[i++] = "-E";
    args[i++] = "NACL_FD=3";
    args[i++] = "-f";
    args[i++] = executable;
    args[i++] = "--";
    int j;
    for(j = 0; j < argv_size; j++)
      args[i++] = argv[j];
    args[i++] = NULL;
    assert(i == args_len);

    execvp("sel_ldr", (char **) args);
    perror("exec");
    _exit(1);
  }
  NaClClose(socks[0]);
  if(fcntl(socks[1], F_SETFD, FD_CLOEXEC) < 0) {
    perror("fcntl");
    return NULL;
  }

  struct NaClDesc *desc = malloc(sizeof(struct NaClDescImcDesc));
  if(!NaClDescImcDescCtor((struct NaClDescImcDesc *) desc, socks[1]))
    return NULL;

  struct ReceiveThreadArgs *args = malloc(sizeof(struct ReceiveThreadArgs));
  if(args == NULL)
    return NULL;
  NaClDescRef(desc);
  NPN_RetainObject(receive_callback);
  args->plugin = plugin;
  args->desc = desc;
  args->callback = receive_callback;
  pthread_t thread_id;
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
  if(pthread_create(&thread_id, &attr, ReceiveThread, args) != 0)
    return NULL;

  return desc;
}

static bool
LauncherObj_Invoke(NPObject *npobj, NPIdentifier methodName,
		   const NPVariant *args, uint32_t argCount, NPVariant *result)
{
  struct LauncherObj *obj = (struct LauncherObj *) npobj;
  if(methodName == ident_launch) {
    if(argCount == 3 &&
       NPVARIANT_IS_OBJECT(args[0]) &&
       NPVARIANT_IS_OBJECT(args[1]) &&
       NPVARIANT_IS_OBJECT(args[2])) {
      NPObject *file_npobj = NPVARIANT_TO_OBJECT(args[0]);
      NPObject *argv_obj = NPVARIANT_TO_OBJECT(args[1]);
      NPObject *receive_callback = NPVARIANT_TO_OBJECT(args[2]);
      char **argv;
      int argv_size;
      if(IsType(file_npobj, &FileObj_Vtable) &&
	 ConvertArgvArray(obj->plugin, argv_obj, &argv, &argv_size)) {
	struct FileObj *file_obj = (struct FileObj *) file_npobj;
	struct NaClDesc *desc = LaunchProcess(obj->plugin, file_obj->filename,
					      argv, argv_size,
					      receive_callback);
	int i;
	for(i = 0; i < argv_size; i++)
	  free(argv[i]);
	free(argv);

	if(desc != NULL) {
	  NPObject *send_npobj = NPN_CreateObject(obj->plugin, &SendObj_Vtable);
	  struct SendObj *send_obj = (struct SendObj *) send_npobj;
	  send_obj->desc = desc;
	  OBJECT_TO_NPVARIANT(send_npobj, *result);
	  return true;
	}
      }
    }
    VOID_TO_NPVARIANT(*result);
    return true;
  }
  if(methodName == ident_get_file) {
    if(argCount == 2 &&
       NPVARIANT_IS_STRING(args[0]) &&
       NPVARIANT_IS_OBJECT(args[1])) {
      /* TODO: do same-origin check (assuming they are useful). */
      NPString url = NPVARIANT_TO_STRING(args[0]);
      NPObject *callback = NPVARIANT_TO_OBJECT(args[1]);
      NPN_RetainObject(callback);
      /* TODO: I don't think utf8characters is guaranteed to be
	 NULL-terminated. */
      NPError err = NPN_GetURLNotify(obj->plugin, url.utf8characters,
				     NULL, callback);
    }
    /* TODO: handle errors */
    VOID_TO_NPVARIANT(*result);
    return true;
  }
  return false;
}

static NPClass LauncherObj_Vtable = {
  .structVersion = NP_CLASS_STRUCT_VERSION,
  .allocate = LauncherObj_Allocate,
  .deallocate = NULL,
  .invalidate = NULL,
  .hasMethod = LauncherObj_HasMethod,
  .invoke = LauncherObj_Invoke,
  .invokeDefault = NULL,
  .hasProperty = NoOp_HasProperty,
  .getProperty = NoOp_GetProperty,
  .setProperty = NULL,
  .removeProperty = NULL,
};


NPError
NPP_New(NPMIMEType pluginType, NPP instance, uint16 mode,
	int16 argc, char **argn, char **argv, NPSavedData *saved)
{
  return NPERR_NO_ERROR;
}

NPError
NPP_Destroy(NPP instance, NPSavedData **save)
{
  /* TODO: kill sel_ldr subprocesses, and do waitpid() to avoid a zombie */
  return NPERR_NO_ERROR;
}

NPError
NPP_GetValue(NPP instance, NPPVariable variable, void *result)
{
  switch(variable) {
  case NPPVpluginScriptableNPObject:
    {
      NPObject *obj = NPN_CreateObject(instance, &LauncherObj_Vtable);
      *(NPObject **) result = obj;
      break;
    }
  default:
    return NPERR_GENERIC_ERROR;
  }
  return NPERR_NO_ERROR;
}

NPError
NPP_NewStream(NPP instance, NPMIMEType type, NPStream *stream,
	      NPBool seekable, uint16 *stype)
{
  *stype = NP_ASFILEONLY;
  return NPERR_NO_ERROR;
}

void
NPP_StreamAsFile(NPP instance, NPStream *stream, const char *filename)
{
  /* TODO: this function calls LOG_FATAL on error; not good */
  struct NaClDesc *desc = (struct NaClDesc *)
    NaClDescIoDescOpen((char *) filename, NACL_ABI_O_RDONLY, 0);
  if(desc == NULL)
    return;

  NPObject *callback = stream->notifyData;
  NPObject *file_npobj = NPN_CreateObject(instance, &FileObj_Vtable);
  struct FileObj *file_obj = (struct FileObj *) file_npobj;
  file_obj->desc = desc;
  file_obj->filename = strdup(filename);
  if(file_obj->filename == NULL)
    return;
  NPVariant result;
  NPVariant args[1];
  OBJECT_TO_NPVARIANT(file_npobj, args[0]);
  bool success = NPN_InvokeDefault(instance, callback, args, 1, &result);
  NPN_ReleaseVariantValue(&result);
  NPN_ReleaseObject(callback);
  NPN_ReleaseObject(file_npobj);
}

void
NPP_URLNotify(NPP instance, const char *url, NPReason reason, void *notifyData)
{
  /* TODO: handle file not found errors */
}


NPError
NP_Initialize(NPNetscapeFuncs *funcs, NPPluginFuncs *callbacks)
{
  g_funcs = *funcs;
  callbacks->newp = NPP_New;
  callbacks->destroy = NPP_Destroy;
  callbacks->getvalue = NPP_GetValue;
  callbacks->newstream = NPP_NewStream;
  callbacks->asfile = NPP_StreamAsFile;
  callbacks->urlnotify = NPP_URLNotify;

  ident_launch = NPN_GetStringIdentifier("launch");
  ident_get_file = NPN_GetStringIdentifier("get_file");
  ident_send = NPN_GetStringIdentifier("send");
  ident_length = NPN_GetStringIdentifier("length");

  NaClNrdAllModulesInit();

  return NPERR_NO_ERROR;
}

char *
NP_GetMIMEDescription(void)
{
  return "application/x-nacl-imc::Native Client IMC plugin";
}

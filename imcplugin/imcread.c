
#include <stdio.h>
#include <string.h>

#include <sys/nacl_imc_api.h>
#include <sys/nacl_syscalls.h>


void print_string(const char *msg, int size)
{
  printf("\"");
  int i;
  for(i = 0; i < size; i++) {
    char c = msg[i];
    if (32 <= c && c < 127) {
      printf("%c", c);
    }
    else {
      printf("\\x%02x", c);
    }
  }
  printf("\"\n");
}

void send_message(int desc, char *data)
{
  struct NaClImcMsgIoVec iov;
  struct NaClImcMsgHdr msg;
  iov.base = data;
  iov.length = strlen(data);
  msg.iov = &iov;
  msg.iov_length = 1;
  msg.descv = NULL;
  msg.desc_length = 0;

  int result = imc_sendmsg(desc, &msg, 0);
  if(result < 0) {
    perror("sendmsg");
  }
}

void read_file(int desc)
{
  while(1) {
    char buf[2000];
    int got = read(desc, buf, sizeof(buf));
    if(got <= 0)
      break;
    printf("NaCl read from FD %i: ", desc);
    print_string(buf, got);
  }
}

void read_message(int desc)
{
  char buf[2000];
  int fds[8];
  struct NaClImcMsgIoVec iov;
  struct NaClImcMsgHdr msg;
  iov.base = buf;
  iov.length = sizeof(buf);
  msg.iov = &iov;
  msg.iov_length = 1;
  msg.descv = fds;
  msg.desc_length = 8;

  int result = imc_recvmsg(desc, &msg, 0);
  if(result < 0) {
    perror("recvmsg");
  }
  else {
    printf("NaCl got message: ");
    print_string(buf, result);
    printf("NaCl received %i FDs\n", msg.desc_length);
    int i;
    for(i = 0; i < msg.desc_length; i++) {
      read_file(msg.descv[i]);
      close(msg.descv[i]);
    }
  }
}

int main(int argc, char* argv[]) {
  printf("NaCl module started\n");
  printf("argc = %i\n", argc);
  int i;
  for(i = 0; i < argc; i++)
    printf("argv[i] = \"%s\"\n", argv[i]);
  send_message(3, "Hello from NaCl to Javascript!");
  send_message(3, "Hello from NaCl to Javascript (message 2)");
  read_message(3);
  read_message(3);
  return 0;
}

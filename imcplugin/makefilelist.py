
import glob
import os


def find_files(path, rel_path=""):
    is_dir = os.path.isdir(path)
    yield path, rel_path, is_dir
    if is_dir:
        for leafname in sorted(os.listdir(path)):
            for result in find_files(os.path.join(path, leafname),
                                     os.path.join(rel_path, leafname)):
                yield result


def main():
    all = []
    for install_dir in glob.glob("../../../install-trees/*"):
        all.extend(find_files(install_dir, "/"))
    all.extend(find_files("../install-stubout", "/"))
    all.extend(find_files(".", ""))
    print "dirs = {}"
    for path, relpath, isdir in all:
        if isdir:
            print "dirs[%r] = %r;" % (relpath, path)
    print "files = {}"
    for path, relpath, isdir in all:
        if not isdir:
            print "files[%r] = %r;" % (relpath, path)


if __name__ == "__main__":
    main()


import os
import shutil
import signal
import subprocess
import tempfile
import unittest


HEREDIR = os.path.dirname(__file__)


def write_file(filename, data):
    fh = open(filename, "w")
    try:
        fh.write(data)
    finally:
        fh.close()


class TempDirTestCase(unittest.TestCase):

    def setUp(self):
        self._on_teardown = []

    def make_temp_dir(self):
        temp_dir = tempfile.mkdtemp(prefix="tmp-%s-" % self.__class__.__name__)
        def tear_down():
            shutil.rmtree(temp_dir)
        self._on_teardown.append(tear_down)
        return temp_dir

    def tearDown(self):
        for func in reversed(self._on_teardown):
            func()


class ImcPluginTests(TempDirTestCase):

    def _run_firefox(self, html_file, expect):
        home_dir = self.make_temp_dir()
        os.makedirs(os.path.join(home_dir, ".mozilla/plugins"))
        os.makedirs(os.path.join(home_dir, ".mozilla/firefox/default"))
        # This profiles.ini file tells Mozilla to use the "default"
        # profile with our user.js file, rather than making up a new
        # profile name.
        write_file(os.path.join(home_dir, ".mozilla/firefox/profiles.ini"),
                   """
[General]
StartWithLastProfile=0

[Profile0]
Name=default
IsRelative=1
Path=default
""")
        write_file(os.path.join(home_dir, ".mozilla/firefox/default/user.js"),
                   'user_pref("browser.dom.window.dump.enabled", true);\n')
        shutil.copy(os.path.join(HEREDIR, "plugin.so"),
                    os.path.join(home_dir, ".mozilla/plugins/plugin.so"))
        env = os.environ.copy()
        # Reusing current X display.  TODO: launch a scratch Xvfb.
        if "XAUTHORITY" not in env:
            env["XAUTHORITY"] = os.path.expanduser("~/.Xauthority")
        env["HOME"] = home_dir

        proc = subprocess.Popen(["firefox", html_file],
                                env=env, stdout=subprocess.PIPE)
        try:
            while len(expect) > 0:
                # TODO: add a timeout.  Unfortunately we can't really
                # tell if the test has failed.
                line = proc.stdout.readline()
                if line == "":
                    raise AssertionError("Output ended too soon")
                # Ignore noise from Mozilla.
                if line.startswith("*** e = [Exception"):
                    continue
                self.assertEquals(line.rstrip("\n"), expect.pop(0))
        finally:
            os.kill(proc.pid, signal.SIGKILL)
        proc.wait()

    def test_getting_file(self):
        html_dir = self.make_temp_dir()
        write_file(os.path.join(html_dir, "test-file"), "blah blah")
        write_file(os.path.join(html_dir, "test.html"), r"""
Testing plugin...
<object id="plugin" type="application/x-nacl-imc">Plugin not working</object>
<script type="text/javascript">
function onload() {
    try {
        var plugin = document.getElementById("plugin");
        plugin.get_file("test-file", function(file) {
            dump("got file\n");
            dump("END\n");
        });
    }
    catch(e) {
        dump(e + "\n");
    }
}
window.onload = onload;
</script>
""")
        self._run_firefox(os.path.join(html_dir, "test.html"),
                          ["got file"])

    def test_launching_nacl_process(self):
        html_dir = self.make_temp_dir()
        shutil.copy(os.path.join(HEREDIR, "tests/printargs"),
                    os.path.join(html_dir, "printargs"))
        write_file(os.path.join(html_dir, "test.html"), r"""
Testing plugin...
<object id="plugin" type="application/x-nacl-imc">Plugin not working</object>
<script type="text/javascript">
function callback() {}
function onload() {
    try {
        var plugin = document.getElementById("plugin");
        plugin.get_file("printargs", function(file) {
            plugin.launch(file, ["arg 1 foo", "arg 2 baz"], callback);
        });
    }
    catch(e) {
        dump(e + "\n");
    }
}
window.onload = onload;
</script>
""")
        expect = ["NaCl module started",
                  "argc = 3",
                  'argv[0] = "NaClMain"',
                  'argv[1] = "arg 1 foo"',
                  'argv[2] = "arg 2 baz"',
                  "NACL_FD = 3",
                  "END"]
        self._run_firefox(os.path.join(html_dir, "test.html"), expect)

    def test_sending_to_nacl(self):
        html_dir = self.make_temp_dir()
        shutil.copy(os.path.join(HEREDIR, "tests/imc-read"),
                    os.path.join(html_dir, "imc-read"))
        write_file(os.path.join(html_dir, "test.html"), r"""
Testing plugin...
<object id="plugin" type="application/x-nacl-imc">Plugin not working</object>
<script type="text/javascript">
function callback() {}
function onload() {
    try {
        var plugin = document.getElementById("plugin");
        plugin.get_file("imc-read", function(file) {
            var proc = plugin.launch(file, [], callback);
            proc.send("Hello from Javascript to NaCl\n", []);
            proc.send("Second message\n", []);
        });
    }
    catch(e) {
        dump(e + "\n");
    }
}
window.onload = onload;
</script>
""")
        expect = ["NaCl got message: Hello from Javascript to NaCl",
                  "NaCl received 0 FDs",
                  "NaCl got message: Second message",
                  "NaCl received 0 FDs"]
        self._run_firefox(os.path.join(html_dir, "test.html"), expect)

    def test_receiving_from_nacl(self):
        html_dir = self.make_temp_dir()
        shutil.copy(os.path.join(HEREDIR, "tests/imc-send"),
                    os.path.join(html_dir, "imc-send"))
        write_file(os.path.join(html_dir, "test.html"), r"""
Testing plugin...
<object id="plugin" type="application/x-nacl-imc">Plugin not working</object>
<script type="text/javascript">
function onload() {
    try {
        var plugin = document.getElementById("plugin");
        function callback(msg) {
            dump("Javascript got message: " + msg + "\n");
        }
        plugin.get_file("imc-send", function(file) {
            var proc = plugin.launch(file, [], callback);
        });
    }
    catch(e) {
        dump(e + "\n");
    }
}
window.onload = onload;
</script>
""")
        expect = ["Javascript got message: Hello from NaCl to Javascript!",
                  "Javascript got message: Message 2"]
        self._run_firefox(os.path.join(html_dir, "test.html"), expect)


if __name__ == "__main__":
    unittest.main()

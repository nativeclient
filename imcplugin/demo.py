
import nacl
import sys


# Borrowed from traceback module but trimmed down.
# TODO: Support loading standard library.
def format_exception_only(etype, value):
    if (isinstance(etype, BaseException) or
        etype is None or type(etype) is str):
        return [_format_final_exc_line(etype, value)]
    stype = etype.__name__
    return [_format_final_exc_line(stype, value)]

def _format_final_exc_line(etype, value):
    valuestr = _some_str(value)
    if value is None or not valuestr:
        line = "%s\n" % etype
    else:
        line = "%s: %s\n" % (etype, valuestr)
    return line

def _some_str(value):
    try:
        return str(value)
    except:
        return '<unprintable %s object>' % type(value).__name__


class Stdout(object):

    def write(self, data):
        if data.strip() != "":
            print_str(data)


nacl_fd = 3

def print_str(data):
    nacl.imc_sendmsg(nacl_fd, "Prnt%s" % str(data))


def repl():
    print_str("Welcome to Python %s" % sys.version)
    sys.stdout = Stdout()
    env = {}
    while True:
        msg = nacl.imc_recvmsg(nacl_fd)
        if msg.startswith("Eval"):
            data = msg[4:]
            try:
                code = compile(data + "\n", "<repl>", "single")
                exec code in env
            except Exception:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                print_str("".join(format_exception_only(exc_type, exc_value)))


if __name__ == "__main__":
    try:
        repl()
    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print_str("Exited: %s" %
                  "".join(format_exception_only(exc_type, exc_value)))

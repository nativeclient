
#include <stdio.h>

#include <sys/nacl_imc_api.h>
#include <sys/nacl_syscalls.h>


int read_message(int desc)
{
  char buf[2000];
  int fds[8];
  struct NaClImcMsgIoVec iov;
  struct NaClImcMsgHdr msg;
  iov.base = buf;
  iov.length = sizeof(buf);
  msg.iov = &iov;
  msg.iov_length = 1;
  msg.descv = fds;
  msg.desc_length = 8;

  int result = imc_recvmsg(desc, &msg, 0);
  if(result <= 0) {
    printf("failed, result=%i\n", result);
    return 0;
  }
  else {
    int i;
    printf("NaCl got message: ");
    for(i = 0; i < result; i++)
      printf("%c", buf[i]);
    printf("NaCl received %i FDs\n", msg.desc_length);
    return 1;
  }
}

int main(int argc, char* argv[])
{
  while(read_message(3))
    ;
  return 0;
}


#include <stdio.h>
#include <stdlib.h>


int main(int argc, char* argv[])
{
  int i;
  printf("NaCl module started\n");
  printf("argc = %i\n", argc);
  for(i = 0; i < argc; i++)
    printf("argv[%i] = \"%s\"\n", i, argv[i]);
  printf("NACL_FD = %s\n", getenv("NACL_FD"));
  printf("END\n");
  return 0;
}


#include <stdio.h>
#include <string.h>

#include <sys/nacl_imc_api.h>
#include <sys/nacl_syscalls.h>


void send_message(int desc, char *data)
{
  struct NaClImcMsgIoVec iov;
  struct NaClImcMsgHdr msg;
  iov.base = data;
  iov.length = strlen(data);
  msg.iov = &iov;
  msg.iov_length = 1;
  msg.descv = NULL;
  msg.desc_length = 0;

  int result = imc_sendmsg(desc, &msg, 0);
  if(result < 0)
    perror("sendmsg");
}

int main(int argc, char* argv[])
{
  send_message(3, "Hello from NaCl to Javascript!");
  send_message(3, "Message 2");
  return 0;
}

#!/bin/bash

set -e

libdir=scons-out/dbg-linux/lib

./tools_bin/linux/sdk/nacl-sdk/bin/nacl-gcc \
    -static -Wall imcplugin/imcread.c -o imcplugin/imcread

./tools_bin/linux/sdk/nacl-sdk/bin/nacl-gcc \
    -static -Wall imcplugin/tests/printargs.c -o imcplugin/tests/printargs
./tools_bin/linux/sdk/nacl-sdk/bin/nacl-gcc \
    -static -Wall imcplugin/tests/imc-read.c -o imcplugin/tests/imc-read
./tools_bin/linux/sdk/nacl-sdk/bin/nacl-gcc \
    -static -Wall imcplugin/tests/imc-send.c -o imcplugin/tests/imc-send

# Needs xulrunner-1.9-dev rather than libxul-dev on hardy
gcc $(pkg-config mozilla-plugin --cflags --libs) \
    -DNACL_LINUX -I.. \
    -Wall -Wl,-z,defs -shared -fPIC imcplugin/imcplugin.c \
    -o imcplugin/plugin.so \
    $libdir/libgoogle_nacl_imc_c.a \
    $libdir/libnrd_xfer.a \
    $libdir/libnaclthread.a \
    $libdir/libgio.a \
    -lrt -lstdc++ -lcrypto

(cd imcplugin && python makefilelist.py >filelist.js)

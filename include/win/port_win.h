/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef NATIVE_CLIENT_INCLUDE_WIN_PORT_WIN_H_
#define NATIVE_CLIENT_INCLUDE_WIN_PORT_WIN_H_

#pragma warning( disable : 4255 4668 4820 4826 4619)
/* TODO: disabled warnings about Windows-specific secure functions
 * such as sprintf_s */
#pragma warning( disable : 4996)
#pragma warning( disable : 4127)  /* conditional expression is constant */
#include <windows.h>
#include "native_client/include/elf.h"

// types missing on Windows
typedef long              off_t;
//typedef __int64 intptr_t;
typedef unsigned int      size_t;
typedef int               ssize_t;
typedef unsigned int      uint32_t;
typedef int               int32_t;
typedef unsigned char     uint8_t;
typedef unsigned int      u_int32_t;
typedef unsigned short    __uint16_t;
typedef unsigned short    uint16_t;
typedef short             int16_t;
typedef int               mode_t;
typedef long              _off_t;
typedef long int          __loff_t;
typedef signed char       int8_t;
typedef unsigned long     DWORD;
typedef long              clock_t;
typedef __int64           int64_t;
typedef unsigned __int64  uint64_t;

#if !defined(__cplusplus) || defined(__STDC_LIMIT_MACROS)
/* only what we need so far */
# define UINT8_MAX       (255)
#endif

//arguments processing
int ffs(int x);
int getopt(int argc, char *argv[], char *optstring);
extern char	*optarg;		// global argument pointer
extern int	optind; 	// global argv index


/*************************** stdio.h  ********************/
#ifndef NULL
#ifdef __cplusplus
#define NULL    0
#else
#define NULL    ((void *)0)
#endif
#endif

/* Seek method constants */
#define SEEK_CUR    1
#define SEEK_END    2
#define SEEK_SET    0

/************************************************************/

/* missing from Windows <errno.h> */
#define EDQUOT 122 /* Quota exceeded */


struct timezone {
  int tz_minuteswest;
  int tz_dsttime;
};

/* missing from win stdio.h and fcntl.h */

/* from bits/fcntl.h */
#define O_ACCMODE 0003

/* from linux/limits.h, via sys/param.h */
#define PATH_MAX 4096

#endif  /* NATIVE_CLIENT_INCLUDE_WIN_PORT_WIN_H_ */

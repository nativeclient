/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "native_client/npapi_plugin/npapi_bridge/npmodule.h"

#include <Carbon/Carbon.h>
#include <CoreFoundation/CoreFoundation.h>

namespace nacl {

int16_t NPModule::HandleEvent(void* param) {
  EventRecord* event = static_cast<EventRecord*>(param);
  if (!event) {
    return false;
  }
  switch (event->what) {
    case updateEvt:
      Redraw();
      return true;
      break;
  }
  return true;
}

void NPModule::Redraw() {
  if (!window_ || !window_->window || !bitmap_data_ ||
      window_->clipRect.right <= window_->clipRect.left) {
    return;
  }

  RgnHandle saved_clip = NewRgn();
  if (!saved_clip) {
    return;
  }
  GrafPtr saved_port;
  Rect revealed_rect;
  short saved_port_top;
  short saved_port_left;

  NP_Port* npport = static_cast<NP_Port*>(window_->window);
  CGrafPtr our_port = npport->port;
  GetPort(&saved_port);
  SetPort(reinterpret_cast<GrafPtr>(our_port));

  Rect port_rect;
  GetPortBounds(our_port, &port_rect);
  saved_port_top = port_rect.top;
  saved_port_left = port_rect.left;
  GetClip(saved_clip);

  revealed_rect.top = window_->clipRect.top + npport->porty;
  revealed_rect.left = window_->clipRect.left + npport->portx;
  revealed_rect.bottom = window_->clipRect.bottom + npport->porty;
  revealed_rect.right = window_->clipRect.right + npport->portx;
  SetOrigin(npport->portx, npport->porty);
  ClipRect(&revealed_rect);

  Rect bounds;
  SetRect(&bounds, 0, 0, window_->width - 1, window_->height - 1);
  GWorldPtr gworld;
  OSErr result = NewGWorld(&gworld, 32, &bounds, 0, 0, 0);
  if (result == noErr) {
    PixMapHandle pixmap;
    pixmap = GetGWorldPixMap(gworld);
    if (LockPixels(pixmap)) {
      // TODO : Find a way to avoid bitmap copy and conversion
      //              (ARGB to RGBA) operations.
      uint32_t* dst = reinterpret_cast<uint32_t*>(GetPixBaseAddr(pixmap));
      uint32_t* src = static_cast<uint32_t*>(bitmap_data_);
      for (int n = 0; n < window_->width * window_->height; ++n) {
        // Convert from ARGB to RGBA.
        uint32_t color = *src++;
        color = (0x000000ff & (color >> 24) |
                (0x0000ff00 & (color >> 8)) |
                (0x00ff0000 & (color << 8)) |
                (0xff000000 & (color << 24)));
        *dst++ = color;
      }
      UnlockPixels(pixmap);
    }
    LockPortBits(gworld);
    CopyBits(GetPortBitMapForCopyBits(gworld),
             GetPortBitMapForCopyBits(our_port),
             &bounds,
             &bounds,
             srcCopy,
             NULL);
    UnlockPortBits(gworld);
    DisposeGWorld(gworld);
  }

  SetOrigin(saved_port_left, saved_port_top);
  SetClip(saved_clip);
  SetPort(saved_port);
  DisposeRgn(saved_clip);
}

}  // namespace nacl

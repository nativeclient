/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


// NaCl-NPAPI Interface

#ifndef NATIVE_CLIENT_NPAPI_PLUGIN_NPAPI_BRIDGE_NPMODULE_H_
#define NATIVE_CLIENT_NPAPI_PLUGIN_NPAPI_BRIDGE_NPMODULE_H_

#if !NACL_WINDOWS
#include <pthread.h>
#endif
#include <string.h>
#include <stdlib.h>

#if NACL_LINUX && defined(MOZ_X11)
#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#endif  // NACL_LINUX && defined(MOZ_X11)

#include <string>

#include "native_client/intermodule_comm/nacl_imc.h"
#include "native_client/nonnacl_util/sel_ldr_launcher.h"
#include "native_client/npapi_plugin/npinstance.h"
#include "native_client/tools/npapi_runtime/nacl_npapi.h"
#include "native_client/tools/npapi_runtime/npbridge.h"
#include "native_client/tools/npapi_runtime/nprpc.h"

namespace nacl {

// Represents the plugin end of the connection to the NaCl module. The opposite
// end is the NPNavigator.
class NPModule : public NPBridge, public NPInstance {
  // The child process that executes the NaCl module program.
  SelLdrLauncher* subprocess_;
  // The child process handle.
  Handle child_;

  // The proxy NPObject that represents the plugin's scriptable object.
  NPObjectProxy* proxy_;

  // The URL origin of the HTML page that started this module.
  std::string origin_;

  // The name of the file currently being loaded by NPN_GetURLNotify().
  char* filename_;
  // The URL currently being loaded by NPN_GetURLNotify().
  char* url_;
  // The file handle of the file currently being loaded by NPN_GetURLNotify().
  Handle stream_;

  // The number of arguments to pass to the child process.
  int argc_;
  // The array of arguments to pass to the child process.
  char const* argv_[kMaxArg];

  // The thread that monitors the NPAPI runtime behavior.
#if NACL_WINDOWS
  HANDLE monitor_thread_;
#else
  pthread_t monitor_thread_;
  bool monitor_thread_started_;
  // TODO: investigate a cleaner solution than this.
#endif

  // The NPWindow of this plugin instance.
  NPWindow* window_;
  // The shared memory object handle that keeps the window content rendered by
  // the child process.
  HtpHandle bitmap_shm_;
  // The start address of bitmap_shm_ mapped in the browser's address space.
  void* bitmap_data_;
  // The size of bitmap_shm_.
  size_t bitmap_size_;
  bool origin_valid_;

#if NACL_WINDOWS
  // The original window procedure for the NPWindow.
  WNDPROC original_window_procedure_;
  // The window procedure that intercepts messages sent to the NPWindow.
  static LRESULT CALLBACK WindowProcedure(HWND, UINT, WPARAM, LPARAM);
#endif
#if NACL_LINUX && defined(MOZ_X11)
  // The event handler that processes events sent to the NPWindow.
  static void EventHandler(Widget widget, NPModule* module,
                           XEvent* xevent, Boolean* b);
#endif

  // Redraws the NPWindow with bitmap_data_.
  void Redraw();

 public:
  // Creates a new instance of NPModule. All parameters should be unmodified
  // variables from NPP_New().
  explicit NPModule(NPP npp, int argc, char* argn[], char* argv[]);
  ~NPModule();

  const char* GetApplicationName() const {
    if (!subprocess_) {
      return NULL;
    }
    return subprocess_->GetApplicationName();
  }

  Handle child() const {
    return child_;
  }

  const char* filename() const {
    return filename_;
  }
  void set_filename(const char* filename) {
    if (filename_ != NULL) {
      free(filename_);
      filename_ = NULL;
    }
    if (filename) {
#if NACL_WINDOWS
      filename_ = _strdup(filename);
#else
      filename_ = strdup(filename);
#endif  // NACL_WINDOWS
    }
  }

  const char* url() const {
    return url_;
  }
  void set_url(const char* url) {
    if (url_ != NULL) {
      free(url_);
      url_ = NULL;
    }
    if (url) {
#if NACL_WINDOWS
      url_ = _strdup(url);
#else
      url_ = strdup(url);
#endif  // NACL_WINDOWS
    }
  }

#if !NACL_WINDOWS
  bool monitor_thread_started() {
    return monitor_thread_started_;
  }
  void set_monitor_thread_started(bool mon_thread_started) {
    monitor_thread_started_ = mon_thread_started;
  }
#endif  // !NACL_WINDOWS

  // Spawns a child process that executes the binary specified by
  // application_name. Start returns the child process handle.
  Handle Start(const char* application_name);

  // Dispatches the received request message.
  virtual int Dispatch(RpcHeader* request, int length);

  // Processes NPN_GetValue() request from the child process.
  int GetValue(RpcHeader* request, int length, NPNVariable variable);
  // Processes NPN_Status() request from the child process.
  int SetStatus(RpcHeader* request, int length);
  // Processes NPN_InvalidateRect() request from the child process.
  int InvalidateRect(RpcHeader* request, int length);
  // Processes NPN_ForceRedraw() request from the child process.
  int ForceRedraw(RpcHeader* request, int length);
  // Processes NaClNPN_CreateArray() request from the child process.
  int CreateArray(RpcHeader* request, int length);
  // Processes NaClNPN_OpenURL() request from the child process.
  int OpenURL(RpcHeader* request, int length);

  // Invokes NPP_New() in the child process.
  NPError New();

  //
  // NPInstance methods
  //

  // Processes NPP_Destroy() invocation from the browser.
  NPError Destroy(NPSavedData** save);
  // Processes NPP_SetWindow() invocation from the browser.
  NPError SetWindow(NPWindow* window);
  // Processes NPP_GetValue() invocation from the browser.
  NPError GetValue(NPPVariable variable, void *value);
  // Processes NPP_HandleEvent() invocation from the browser.
  int16_t HandleEvent(void* event);
  // Processes NPP_GetScriptableInstance() invocation from the browser.
  NPObject* GetScriptableInstance();
  // Processes NPP_NewStream() invocation from the browser.
  NPError NewStream(NPMIMEType type,
                    NPStream* stream, NPBool seekable,
                    uint16_t* stype);
  // Processes NPP_StreamAsFile() invocation from the browser.
  void StreamAsFile(NPStream* stream, const char* filename);
  // Processes NPP_DestroyStream() invocation from the browser.
  NPError DestroyStream(NPStream *stream, NPError reason);
  // Processes NPP_URLNotify() invocation from the browser.
  void URLNotify(const char* url, NPReason reason, void* notify_data);

  // The timeout value in seconds to pop up a message box to terminate the
  // non-responding child process.
  static const int kTimeout = 5;
  // sel_ldr instances initially communicate with the browser over channel 5.
  // After the requisite connects/accepts have been done, this channel is
  // no longer used.
  static const int kStartupChannel = 5;
};

}  // namespace nacl

#endif  // NATIVE_CLIENT_NPAPI_PLUGIN_NPAPI_BRIDGE_NPMODULE_H_

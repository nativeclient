/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <process.h>  // _beginthread, _endthread
#include <windows.h>
#include <windowsx.h>

#include "native_client/npapi_plugin/npapi_bridge/npmodule.h"

namespace nacl {

namespace {

unsigned __stdcall MonitorActivities(void* param) {
  NPModule* module = static_cast<NPModule*>(param);
  time_t timeout = NPModule::kTimeout;
  while (module->child() != kInvalidHandle) {
    Sleep(1000);
    if (timeout <= module->IsWaitingFor()) {
        char message[MAX_PATH + 256];
        _snprintf_s(message, sizeof message, sizeof message,
                    "Module: %s\n"
                    "PID: %u\n"
                    "\n"
                    "This Native Client module has become unresponsive.\n"
                    "Would you like to end the module process?",
                    module->GetApplicationName(),
                    module->peer_pid());
      int msgbox_id = MessageBoxA(
          NULL,
          message,
          "Unresponsive Native Client Module",  // Caption.
          MB_ICONWARNING | MB_YESNO);
      switch (msgbox_id) {
        case IDYES:
          TerminateProcess(module->child(), 1);
          break;
        case IDNO:
          timeout = module->IsWaitingFor() + NPModule::kTimeout;
          break;
      }
    } else {
      timeout = NPModule::kTimeout;
    }
  }
  _endthreadex(0);
  return 0;
}

}

Handle NPModule::Start(const char* application_name) {
  subprocess_ = new SelLdrLauncher();
  if (!subprocess_->Start(application_name,
                          kStartupChannel,
                          0,
                          NULL,
                          argc_,
                          argv_)) {
    delete subprocess_;
    subprocess_ = NULL;
    return kInvalidHandle;
  }
  child_ = subprocess_->child();
  set_peer_pid(GetProcessId(child_));
  set_channel(subprocess_->channel());
  monitor_thread_ = reinterpret_cast<HANDLE>(
      _beginthreadex(NULL, 0, MonitorActivities, this, 0, NULL));
  return child_;
}

LRESULT CALLBACK NPModule::WindowProcedure(HWND hwnd, UINT msg,
                                           WPARAM wparam, LPARAM lparam) {
  switch (msg) {
    case WM_PAINT: {
      PAINTSTRUCT ps;
      HDC hdc = BeginPaint(hwnd, &ps);
      NPModule* module = reinterpret_cast<NPModule*>(
          GetWindowLong(hwnd, GWL_USERDATA));
      if (module && module->bitmap_data_) {
        uint32_t* pixel_bits = static_cast<uint32_t*>(module->bitmap_data_);
        BITMAPINFO bmp_info;
        bmp_info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
        bmp_info.bmiHeader.biWidth = module->window_->width;
        bmp_info.bmiHeader.biHeight =
            -static_cast<LONG>(module->window_->height);  // top-down
        bmp_info.bmiHeader.biPlanes = 1;
        bmp_info.bmiHeader.biBitCount = 32;
        bmp_info.bmiHeader.biCompression = BI_RGB;
        bmp_info.bmiHeader.biSizeImage = 0;
        bmp_info.bmiHeader.biXPelsPerMeter = 0;
        bmp_info.bmiHeader.biYPelsPerMeter = 0;
        bmp_info.bmiHeader.biClrUsed = 0;
        bmp_info.bmiHeader.biClrImportant = 0;
        SetDIBitsToDevice(hdc, 0, 0,
                          module->window_->width, module->window_->height,
                          0, 0,
                          0, module->window_->height,
                          pixel_bits, &bmp_info,
                          DIB_RGB_COLORS);
      }
      EndPaint(hwnd, &ps);
      break;
    }
    default:
      break;
  }
  return DefWindowProc(hwnd, msg, wparam, lparam);
}

void NPModule::Redraw() {
  HWND hwnd = static_cast<HWND>(window_->window);
  UpdateWindow(hwnd);
}

}  // namespace nacl

/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <unistd.h>
#include <limits.h>
#include <sys/wait.h>

#include "native_client/npapi_plugin/npapi_bridge/npmodule.h"

namespace nacl {

namespace {

void* MonitorActivities(void* param) {
  NPModule* module = static_cast<NPModule*>(param);
  time_t timeout = NPModule::kTimeout;
  while (module->child() != kInvalidHandle) {
    sleep(1);
    if (timeout <= module->IsWaitingFor()) {
      const char* const kSelMonBasename = "sel_mon";
      const char* plugin_dirname = SelLdrLauncher::GetPluginDirname();
      char sel_mon_path[PATH_MAX + 1];
      if (NULL == plugin_dirname) {
        // TODO: better reporting that we couldn't find sel_mon.
        printf("GetPluginDirname returned NULL -- load failed\n");
        kill(module->child(), SIGHUP);
      }
      snprintf(sel_mon_path, sizeof(sel_mon_path),
               "%s/%s",
               plugin_dirname,
               kSelMonBasename);
      pid_t pid = fork();
      if (pid == 0) {
        // Pops up a message box using sel_mon.
        char message[PATH_MAX + 256];
        snprintf(message, sizeof message,
                 "Module: %s\n"
                 "PID: %u\n"
                 "\n"
                 "This Native Client module has become unresponsive.\n"
                 "Would you like to end the module process?",
                 module->GetApplicationName(),
                 module->child());
        execl(sel_mon_path, sel_mon_path,
              message,  // Message.
              "Unresponsive Native Client Module",  // Caption.
              NULL);
        kill(module->child(), SIGHUP);
        _exit(EXIT_FAILURE);
      } else if (pid != -1) {
        int status;
        waitpid(pid, &status, 0);
        if (WEXITSTATUS(status) != EXIT_SUCCESS) {
          timeout = module->IsWaitingFor() + NPModule::kTimeout;
          continue;
        }
      }
      kill(module->child(), SIGHUP);
    } else {
      timeout = NPModule::kTimeout;
    }
  }
  return 0;
}

}

Handle NPModule::Start(const char* application_name) {
  subprocess_ = new SelLdrLauncher();
  if (!subprocess_->Start(application_name,
                          kStartupChannel,
                          0,
                          NULL,
                          argc_,
                          argv_)) {
    delete subprocess_;
    subprocess_ = NULL;
    return kInvalidHandle;
  }
  child_ = subprocess_->child();
  set_peer_pid(child_);
  set_channel(subprocess_->channel());
  if (0 == pthread_create(&monitor_thread_, NULL, MonitorActivities, this)) {
    set_monitor_thread_started(true);
  }
  return child_;
}

#if NACL_LINUX

void NPModule::Redraw() {
  Display* display =
      static_cast<NPSetWindowCallbackStruct*>(window_->ws_info)->display;
  Drawable window = reinterpret_cast<Drawable>(window_->window);
  GC gc = XCreateGC(display, window, 0, NULL);
  XImage image;
  memset(&image, 0, sizeof image);
  image.format = ZPixmap;
  image.data = reinterpret_cast<char*>(bitmap_data_);
  image.width = window_->width;
  image.height = window_->height;
  image.xoffset = 0;
  image.byte_order = LSBFirst;
  image.bitmap_bit_order = MSBFirst;
  image.bits_per_pixel = 32;
  image.bytes_per_line = 4 * window_->width;
  image.bitmap_unit = 32;
  image.bitmap_pad = 32;
  image.depth = 24;
  XPutImage(display, window, gc, &image, 0, 0, 0, 0,
            window_->width, window_->height);
  XFreeGC(display, gc);
  XFlush(display);
}

#endif

}  // namespace nacl

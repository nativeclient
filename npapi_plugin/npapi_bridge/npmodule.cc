/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#if NACL_WINDOWS
#include <windows.h>
#include <windowsx.h>
#endif  // NACL_WINDOWS

#if NACL_LINUX && defined(MOZ_X11)
#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#endif  // NACL_LINUX && defined(MOZ_X11)

#include "native_client/npapi_plugin/origin.h"
#include "native_client/npapi_plugin/npapi_bridge/npmodule.h"

#include "native_client/service_runtime/include/sys/audio_video.h"
#include "native_client/service_runtime/nacl_config.h"
#include "native_client/service_runtime/sel_util.h"
#include "native_client/tools/npapi_runtime/nacl_util.h"

namespace {

// This implementation of same-origin policy does not take document.domain
// element into account.
std::string GetModuleOrigin(NPP instance) {
  std::string origin;
  NPVariant loc_value;
  NPVariant href_value;
  NPIdentifier href_id = NPN_GetStringIdentifier("href");
  NPIdentifier location_id = NPN_GetStringIdentifier("location");

  VOID_TO_NPVARIANT(loc_value);
  VOID_TO_NPVARIANT(href_value);
  do {
    NPObject* win_obj;
    if (NPN_GetValue(instance, NPNVWindowNPObject, &win_obj) !=
            NPERR_NO_ERROR) {
      break;
    }
    if (!NPN_GetProperty(instance, win_obj, location_id, &loc_value) ||
        !NPVARIANT_IS_OBJECT(loc_value)) {
      break;
    }
    NPObject* loc_obj = NPVARIANT_TO_OBJECT(loc_value);
    if (!NPN_GetProperty(instance, loc_obj, href_id, &href_value) ||
        !NPVARIANT_IS_STRING(href_value)) {
      break;
    }
    std::string href(NPVARIANT_TO_STRING(href_value).utf8characters,
                     NPVARIANT_TO_STRING(href_value).utf8length);
    origin = nacl::UrlToOrigin(href);
  } while (0);
  NPN_ReleaseVariantValue(&loc_value);
  NPN_ReleaseVariantValue(&href_value);
  return origin;
}

}  // namespace

namespace nacl {

NPModule::NPModule(NPP npp, int argc, char* argn[], char* argv[])
    : NPBridge(npp),
      subprocess_(NULL),
      child_(kInvalidHandle),
      proxy_(NULL),
      filename_(NULL),
      url_(NULL),
      stream_(kInvalidHandle),
#if NACL_WINDOWS
      monitor_thread_(kInvalidHtpHandle),
#else
      monitor_thread_(0),
      monitor_thread_started_(false),
#endif
      window_(NULL),
      bitmap_shm_(kInvalidHtpHandle),
      bitmap_data_(NULL),
      bitmap_size_(0) {
  const char* agent = NPN_UserAgent(npp);
  set_is_webkit(strstr(agent, "AppleWebKit") ? true : false);

  origin_ = GetModuleOrigin(npp);

  // Check that origin is in the list of permitted origins.
  origin_valid_ = OriginIsInWhitelist(origin_);

  argc_ = 0;
  for (int i = 0; i < argc; ++i) {
    // Researve last three arguments for the handle number for the connection
    // to the plungin, for the size of NPVariant in the plugin, and for NULL.
    if (kMaxArg - 4 <= argc_) {
      break;
    }
    // Note argv[i] can be a NULL pointer.
    argv_[argc_++] = strdup(argn[i] ? argn[i] : "");
    argv_[argc_++] = strdup(argv[i] ? argv[i] : "");
  }
  // The last two arguments are the handle value for the connection to the
  // plugin, and for the size of NPVariant in the plugin.
  //
  // SelLdrLauncher will replace "$CHAN" with the IMC descriptor.
  argv_[argc_++] = strdup("$CHAN");
  char variant_size[4];
#if NACL_WINDOWS
  _snprintf(variant_size, sizeof variant_size, "%u", sizeof(NPVariant));
#else
  snprintf(variant_size, sizeof variant_size, "%zu", sizeof(NPVariant));
#endif
  argv_[argc_++] = strdup(variant_size);
  argv_[argc_] = NULL;

#if NACL_WINDOWS
  original_window_procedure_ = NULL;
#endif

  NPCapability capability = { 0, 0 };
  proxy_ = new(std::nothrow) NPObjectProxy(this, capability);
}

NPModule::~NPModule() {
  if (window_ && window_->window) {
#if NACL_WINDOWS
    SubclassWindow(reinterpret_cast<HWND>(window_->window),
                   original_window_procedure_);
#endif
  }
  if (subprocess_) {
    delete subprocess_;
  }
  child_ = kInvalidHandle;
  if (filename_ != NULL) {
    free(filename_);
  }
  if (url_ != NULL) {
    free(url_);
  }
  for (int i = 0; i < argc_; ++i) {
    free(const_cast<char *>(argv_[i]));
  }
  // Note proxy objects are deleted by Navigator.
  if (proxy_) {
    proxy_->Detach();
    NPN_ReleaseObject(proxy_);
  }
#if NACL_WINDOWS
  if (monitor_thread_ != kInvalidHtpHandle) {
    WaitForSingleObject(monitor_thread_, INFINITE);
    CloseHandle(monitor_thread_);
  }
#else
  if (monitor_thread_started()) {
    // TODO: perhaps if we used pthread_detach we wouldn't have to join.
    pthread_join(monitor_thread_, NULL);
  }
#endif
}

int NPModule::Dispatch(RpcHeader* request, int len) {
  int result;
  switch (request->type) {
    case RPC_GET_WINDOW_OBJECT:
      result = GetValue(request, len, NPNVWindowNPObject);
      break;
    case RPC_GET_PLUGIN_ELEMENT_OBJECT:
      result = GetValue(request, len, NPNVPluginElementNPObject);
      break;
    case RPC_SET_STATUS:
      result = SetStatus(request, len);
      break;
    case RPC_INVALIDATE_RECT:
      result = InvalidateRect(request, len);
      break;
    case RPC_FORCE_REDRAW:
      result = ForceRedraw(request, len);
      break;
    case RPC_CREATE_ARRAY:
      result = CreateArray(request, len);
      break;
    case RPC_OPEN_URL:
      result = OpenURL(request, len);
      break;
    default:
      return NPBridge::Dispatch(request, len);
      break;
  }
  return result;
}

int NPModule::GetValue(RpcHeader* request, int len, NPNVariable variable) {
  IOVec vecv[2];
  IOVec* vecp = vecv;
  vecp->base = request;
  vecp->length = sizeof(RpcHeader);
  ++vecp;

  NPObject* object;
  request->error_code = NPN_GetValue(npp(), variable, &object);

  RpcStack stack(this);
  if (request->error_code == NPERR_NO_ERROR) {
    stack.Push(object);
  }
  vecp = stack.SetIOVec(vecp);
  return Respond(request, vecv, vecp - vecv);
}

int NPModule::SetStatus(RpcHeader* request, int len) {
  RpcArg arg(this, request, len);
  arg.Step(sizeof(RpcHeader));

  IOVec vecv[2];
  IOVec* vecp = vecv;
  vecp->base = request;
  vecp->length = sizeof(RpcHeader);
  ++vecp;
  const char* status = arg.GetString();
  if (status) {
    NPN_Status(npp(), status);
  }
  return Respond(request, vecv, vecp - vecv);
}

int NPModule::InvalidateRect(RpcHeader* request, int len) {
  RpcArg arg(this, request, len);
  arg.Step(sizeof(RpcHeader));
  const NPRect* nprect = arg.GetRect();

  IOVec vecv[1];
  IOVec* vecp = vecv;
  vecp->base = request;
  vecp->length = sizeof(RpcHeader);
  ++vecp;
  if (window_ && window_->window && nprect) {
#if NACL_WINDOWS
    HWND hwnd = static_cast<HWND>(window_->window);
    RECT rect;
    rect.left = nprect->left;
    rect.top = nprect->top;
    rect.right = nprect->right;
    rect.bottom = nprect->bottom;
    ::InvalidateRect(hwnd, &rect, FALSE);
#endif
#if NACL_OSX
    NPN_InvalidateRect(npp(), const_cast<NPRect*>(nprect));
#endif
  }
  return Respond(request, vecv, vecp - vecv);
}

int NPModule::ForceRedraw(RpcHeader* request, int len) {
  IOVec vecv[1];
  IOVec* vecp = vecv;
  vecp->base = request;
  vecp->length = sizeof(RpcHeader);
  ++vecp;
  if (window_ && window_->window) {
#if !NACL_OSX
    Redraw();
#else
    NPN_ForceRedraw(npp());
#endif
  }
  return Respond(request, vecv, vecp - vecv);
}

int NPModule::CreateArray(RpcHeader* request, int len) {
  IOVec vecv[2];
  IOVec* vecp = vecv;
  vecp->base = request;
  vecp->length = sizeof(RpcHeader);
  ++vecp;

  NPObject* window;
  NPN_GetValue(npp(), NPNVWindowNPObject, &window);

  NPString script;
  script.utf8characters = "new Array();";
  script.utf8length = strlen(script.utf8characters);
  NPVariant result;
  RpcStack stack(this);
  if (NPN_Evaluate(npp(), window, &script, &result) &&
      NPVARIANT_IS_OBJECT(result)) {
    stack.Push(NPVARIANT_TO_OBJECT(result));
    vecp = stack.SetIOVec(vecp);
  }

  NPN_ReleaseObject(window);

  return Respond(request, vecv, vecp - vecv);
}

int NPModule::OpenURL(RpcHeader* request, int len) {
  RpcArg arg(this, request, len);
  arg.Step(sizeof(RpcHeader));

  IOVec vecv[1];
  IOVec* vecp = vecv;
  vecp->base = request;
  vecp->length = sizeof(RpcHeader);
  ++vecp;

  const char* url = arg.GetString();
  if (url) {
    request->error_code = NPN_GetURLNotify(npp(), url, NULL, NULL);
  } else {
    request->error_code = NPERR_GENERIC_ERROR;
  }
  if (request->error_code == NPERR_NO_ERROR) {
    // NPP_NewStream, NPP_DestroyStream, and NPP_URLNotify will be invoked
    // later.
    set_url(url);
  }
  return Respond(request, vecv, vecp - vecv);
}

NPError NPModule::New() {
  RpcHeader request;
  request.type = RPC_NEW;
  IOVec vecv[2];
  IOVec* vecp = vecv;
  vecp->base = &request;
  vecp->length = sizeof request;
  ++vecp;

  NPSize window_size;
  if (bitmap_data_) {
    window_size.width = window_->width;
    window_size.height = window_->height;
  } else {
    window_size.width = 0;
    window_size.height = 0;
  }
  vecp->base = &window_size;
  vecp->length = sizeof window_size;
  ++vecp;

  clear_handle_count();
  if (bitmap_shm_ != kInvalidHtpHandle) {
    add_handle(bitmap_shm_);
  }

  int length;
  RpcHeader* reply = Request(&request, vecv, vecp - vecv, &length);
  if (reply == NULL) {
    return NPERR_GENERIC_ERROR;
  }
  RpcArg result(this, reply, length);
  result.Step(sizeof(RpcHeader));
  NPError nperr = static_cast<NPError>(reply->error_code);
  NPCapability* capability = result.GetCapability();
  if (capability && proxy_) {
    proxy_->set_capability(*capability);
    AddProxy(proxy_);
  }
  return nperr;
}

//
// NPInstance methods
//

NPError NPModule::Destroy(NPSavedData** save) {
  NPError nperr;
  RpcHeader request;
  request.type = RPC_DESTROY;
  IOVec vecv[1];
  IOVec* vecp = vecv;
  vecp->base = &request;
  vecp->length = sizeof request;
  ++vecp;
  int length;
  RpcHeader* reply = Request(&request, vecv, vecp - vecv, &length);
  if (reply == NULL) {
    nperr = NPERR_INVALID_INSTANCE_ERROR;
  } else {
    nperr = static_cast<NPError>(reply->error_code);
  }
  delete this;
  return nperr;
}

#if NACL_LINUX && defined(MOZ_X11)

void NPModule::EventHandler(Widget widget,
                            NPModule* module,
                            XEvent* xevent,
                            Boolean* b) {
  Window window = reinterpret_cast<Window>(module->window_->window);
  NPSetWindowCallbackStruct* wcbs =
      reinterpret_cast<NPSetWindowCallbackStruct*>(module->window_->ws_info);
  Display* display = wcbs->display;
  switch (xevent->type) {
    case Expose:
      // Exposure events come in multiples, one per rectangle uncovered.
      // We just look at one and redraw the whole region.
      while (XCheckTypedWindowEvent(display, window, Expose, xevent));
      module->Redraw();
      break;
    default:
      // Other types of events should be handled here.
      break;
  }
}

#endif  // NACL_LINUX && defined(MOZ_X11)

NPError NPModule::SetWindow(NPWindow* window) {
  if (!window_ && window->window && 0 < window->width && 0 < window->height) {
    // don't allow invalid window sizes
    if ((window->width < kNaClVideoMinWindowSize) ||
        (window->width > kNaClVideoMaxWindowSize) ||
        (window->height < kNaClVideoMinWindowSize) ||
        (window->height > kNaClVideoMaxWindowSize)) {
      return NPERR_GENERIC_ERROR;
    }
#if NACL_WINDOWS
    HWND hwnd = static_cast<HWND>(window->window);
    original_window_procedure_ =
        SubclassWindow(hwnd, reinterpret_cast<WNDPROC>(WindowProcedure));
    SetWindowLong(hwnd, GWL_USERDATA, reinterpret_cast<LONG>(this));
#endif  // NACL_WINDOWS
#if NACL_LINUX && defined(MOZ_X11)
    Window xwin = reinterpret_cast<Window>(window->window);
    NPSetWindowCallbackStruct* npsw =
        reinterpret_cast<NPSetWindowCallbackStruct*>(window->ws_info);
    Widget widget = XtWindowToWidget(npsw->display, xwin);
    if (widget) {
      // Only enable Expose events.
      long event_mask = ExposureMask;
      XSelectInput(npsw->display, xwin, event_mask);
      XtAddEventHandler(widget, event_mask, False,
                        reinterpret_cast<XtEventHandler>(EventHandler),
                        this);
    }
#endif  // NACL_LINUX && defined(MOZ_X11)
    // We are using 32-bit ARGB format (4 bytes per pixel).
    bitmap_size_ = 4 * window->width * window->height;
    bitmap_size_ = NaClRoundAllocPage(bitmap_size_);
    // TODO : Check failure.
    bitmap_shm_ = CreateShmDesc(CreateMemoryObject(bitmap_size_),
                                 bitmap_size_);
    if (bitmap_shm_ == kInvalidHtpHandle) {
      return NPERR_GENERIC_ERROR;
    }
    bitmap_data_ = Map(NULL, bitmap_size_,
                       kProtRead | kProtWrite, kMapShared,
                       bitmap_shm_, 0);
    if (bitmap_data_ == kMapFailed) {
      Close(bitmap_shm_);
      bitmap_shm_ = kInvalidHtpHandle;
      bitmap_data_ = NULL;
      return NPERR_GENERIC_ERROR;
    }
    window_ = window;
  }
  return NPERR_NO_ERROR;
}

NPError NPModule::GetValue(NPPVariable variable, void *value) {
  switch (variable) {
  case NPPVpluginNameString:
    *static_cast<const char**>(value) = "NativeClient NPAPI bridge plug-in";
    break;
  case NPPVpluginDescriptionString:
    *static_cast<const char**>(value) =
        "A plug-in for NPAPI based NativeClient modules.";
    break;
  case NPPVpluginScriptableNPObject:
    *reinterpret_cast<NPObject**>(value) = GetScriptableInstance();
    if (!*reinterpret_cast<NPObject**>(value)) {
      return NPERR_GENERIC_ERROR;
    }
    break;
  default:
    return NPERR_INVALID_PARAM;
  }
  return NPERR_NO_ERROR;
}

#if !NACL_OSX

int16_t NPModule::HandleEvent(void* event) {
  return 0;
}

#endif  // NACL_OSX

// Note GetScriptableInstance() is invoked before the NaCl module is started,
// but we still need to return a valide NPObject pointer at least on Windows.
NPObject* NPModule::GetScriptableInstance() {
  if (proxy_) {
    NPN_RetainObject(proxy_);
  }
  return proxy_;
}

NPError NPModule::NewStream(NPMIMEType type, NPStream* stream, NPBool seekable,
                            uint16_t* stype) {
  *stype = NP_ASFILEONLY;
  return NPERR_NO_ERROR;
}

void NPModule::StreamAsFile(NPStream* stream, const char* filename) {
  if (url()) {
    if (filename && UrlToOrigin(stream->url) == origin_) {
      stream_ = OpenFile(filename);
      if (stream_ != kInvalidHandle) {
        set_filename(filename);
      } else {
        set_filename(NULL);
      }
    } else {
      set_filename(NULL);
    }
  } else if (filename && subprocess_ == NULL) {
    // This filename is for the file specified by the src attribute.
    if (origin_valid_) {
      // check ABI version compatibility
      NPError np = nacl::CheckExecutableVersion(npp(), filename);
      if (NPERR_NO_ERROR == np) {
        Start(filename);
        // Use NaCl ABI (X86 ELF).
        set_peer_npvariant_size(12);
        New();
      } else {
        fprintf(stderr, "Load failed: possible ABI version mismatch\n");
      }
    } else {
      fprintf(stderr, "Load failed: NaCl module did not come from a whitelisted"
                      " source.\nSee npapi_plugin/origin.cc for the list.");
      NPObject* window;
      NPN_GetValue(npp(), NPNVWindowNPObject, &window);
      NPString script;
      script.utf8characters = "alert('Load failed: NaCl module did not"
          " come from a whitelisted source.\\n"
          "See npapi_plugin/origin.cc for the list.');";
      script.utf8length = strlen(script.utf8characters);
      NPVariant result;
      NPN_Evaluate(npp(), window, &script, &result);
    }
  }
}

NPError NPModule::DestroyStream(NPStream *stream, NPError reason) {
  return NPERR_NO_ERROR;
}

void NPModule::URLNotify(const char* url, NPReason reason, void* notify_data) {
  if (this->url() == NULL) {
    return;
  }

  if (filename_ == NULL) {
    reason = NPRES_NETWORK_ERR;
  }

  HtpHandle handle = kInvalidHtpHandle;
  if (reason == NPRES_DONE) {
    handle = CreateIoDesc(stream_);
  }

  RpcHeader request;
  request.type = RPC_NOTIFY_URL;
  request.error_code = reason;
  IOVec vecv[2];
  IOVec* vecp = vecv;
  vecp->base = &request;
  vecp->length = sizeof request;
  ++vecp;
  RpcStack stack(this);
  vecp = stack.SetIOVec(vecp);

  clear_handle_count();
  if (handle != kInvalidHtpHandle) {
    add_handle(handle);
  }

  int length;
  Request(&request, vecv, vecp - vecv, &length);

  Close(handle);

  set_url(NULL);
}

}  // namespace nacl

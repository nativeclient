/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <new>
#include <signal.h>
#include <string.h>
#include "native_client/npapi_plugin/srpc/npapi_native.h"
#include "native_client/npapi_plugin/srpc/plugin.h"
#include "native_client/npapi_plugin/srpc/shared_memory.h"
#include "native_client/npapi_plugin/srpc/utility.h"
#include "native_client/service_runtime/include/bits/mman.h"
#include "native_client/service_runtime/include/sys/stat.h"
#include "native_client/service_runtime/internal_errno.h"
#include "native_client/service_runtime/sel_util.h"

namespace nacl_srpc {

int SharedMemory::number_alive = 0;

// SharedMemory implements only the connect method.
bool SharedMemory::HasMethod(NPObject *obj, NPIdentifier name) {
  dprintf(("SharedMemory::HasMethod(%p, %s)\n", obj, IdentToString(name)));

  return (Plugin::kReadIdent == name) || (Plugin::kWriteIdent == name);
}

bool SharedMemory::Invoke(NPObject *obj,
                          NPIdentifier name,
                          const NPVariant *args,
                          uint32_t arg_count,
                          NPVariant *result) {
  SharedMemory* shared_memory = reinterpret_cast<SharedMemory*>(obj);

  dprintf(("SharedMemory::Invoke(%p, %s, %d)\n",
           obj, IdentToString(name), arg_count));

  VOID_TO_NPVARIANT(*result);
  if (Plugin::kReadIdent == name) {
    // Type signature:
    // string read(integer, integer)
    // All failures set an exception and return undefined.
    if (2 != arg_count) {
      NPN_SetException(obj, "Incorrect argument count");
      return false;
    }
    // read() takes an offset and a length, returning a string.
    uint32_t offset;
    if (!NPVariantToScalar(&args[0], &offset)) {
      NPN_SetException(obj, "Bad type for first argument");
      return false;
    }
    uint32_t len;
    if (!NPVariantToScalar(&args[1], &len)) {
      NPN_SetException(obj, "Bad type for second argument");
      return false;
    }
    // Ensure we will access valid addresses.
    if (NULL == shared_memory->map_addr_) {
      NPN_SetException(obj, "Shared memory not mapped");
      return false;
    }
    if (offset + len < offset) {
      NPN_SetException(obj, "Offset + length overflows");
      return false;
    }
    if (offset + len > shared_memory->size_) {
      NPN_SetException(obj, "Offset + length overlaps end of shared memory");
      return false;
    }
    // UTF-8 encoding may result in a 2x size increase at the most.
    // TODO: should we do a pre-scan to get the real size?
    uint32_t utf8_buffer_len = 2 * len;
    if ((utf8_buffer_len < len) ||
        (utf8_buffer_len + 1 < utf8_buffer_len)) {
      // Unsigned overflow.
      NPN_SetException(obj, "len too large to hold requested UTF8 chars");
      return false;
    }

    char* ret_string =
        reinterpret_cast<char*>(NPN_MemAlloc(utf8_buffer_len + 1));
    if (NULL == ret_string) {
      NPN_SetException(obj, "out of memory");
      return false;
    }
    unsigned char* shm_addr =
        reinterpret_cast<unsigned char*>(shared_memory->map_addr_) + offset;
    unsigned char* out = reinterpret_cast<unsigned char*>(ret_string);
    // NPAPI wants length to be the number of bytes, not UTF-8 characters.
    for (unsigned int i = 0; i < len; ++i) {
      unsigned char c = *shm_addr;
      if (c < 128) {
        // Code results in a one byte encoding
        *out = c;
        ++out;
      } else {
        // Code results in a two byte encoding
        out[0] = 0xc0 | (c >> 6);
        out[1] = 0x80 | (c & 0x3f);
        out += 2;
      }
      ++shm_addr;
    }
    // Terminate the result string with 0.
    *out = 0;
    // Return the variant value.
    ScalarToNPVariant(ret_string, result);
    return true;
  } else if (Plugin::kWriteIdent == name) {
    // Type signature:
    // void write(integer, integer, string)
    // All failures set an exception and return undefined.
    if (3 != arg_count) {
      NPN_SetException(obj, "Incorrect argument count");
      return false;
    }
    // write() takes an offset, a length, and a string.
    uint32_t offset;
    if (!NPVariantToScalar(&args[0], &offset)) {
      NPN_SetException(obj, "Bad type for first argument");
      return false;
    }
    uint32_t len;
    if (!NPVariantToScalar(&args[1], &len)) {
      NPN_SetException(obj, "Bad type for second argument");
      return false;
    }
    // Ensure we will access valid addresses.
    if (NULL == shared_memory->map_addr_) {
      NPN_SetException(obj, "Shared memory not mapped");
      return false;
    }
    if (offset + len < offset) {
      NPN_SetException(obj, "Offset + length overflows");
      return false;
    }
    if (offset + len > shared_memory->size_) {
      NPN_SetException(obj, "Offset + length overlaps end of shared memory");
      return false;
    }
    // The input is a JavaScript string, which must consist of UFT-8
    // characters with character codes between 0 and 255 inclusive.
    char* string;
    if (!NPVariantToScalar(&args[2], &string)) {
      NPN_SetException(obj, "Bad type for third argument");
      return false;
    }
    const unsigned char* str = reinterpret_cast<unsigned const char*>(string);
    const size_t utf_bytes = strlen(string);
    unsigned char* shm_addr =
        reinterpret_cast<unsigned char*>(shared_memory->map_addr_) + offset;

    for (unsigned int i = 0; i < len; ++i) {
      unsigned char c1 = str[0];
      unsigned char c2 = 0;

      // Check that we are still pointing into the JavaScript string we were
      // passed.
      if (i >= utf_bytes) {
        return false;
      }
      // Process the byte in the string as UTF-8 characters.
      if (c1 & 0x80) {
        c2 = str[1];
        // Assert two byte encoding.
        // The first character must contain 110xxxxxb and the
        // second must contain 10xxxxxxb.
        if (((c1 & 0xc3) != c1) || ((c2 & 0xbf) != c2)) {
          NPN_SetException(obj, "Bad utf8 character value");
          return false;
        }
        *shm_addr = (c1 << 6) | (c2 & 0x3f);
        str += 2;
      } else {
        // One-byte encoding.
        *shm_addr = c1;
        ++str;
      }
      ++shm_addr;
    }
    return true;
  }
  NPN_SetException(obj, "Unknown method");
  return false;
}

// SharedMemory have no properties.
bool SharedMemory::HasProperty(NPObject *obj, NPIdentifier name) {
  dprintf(("SharedMemory::HasProperty(%p, %s)\n", obj, IdentToString(name)));

  return false;
}

bool SharedMemory::GetProperty(NPObject *obj,
                               NPIdentifier name,
                               NPVariant *variant) {
  dprintf(("SharedMemory::GetProperty(%p, %s)\n", obj, IdentToString(name)));

  VOID_TO_NPVARIANT(*variant);
  return false;
}

bool SharedMemory::SetProperty(NPObject *obj,
                               NPIdentifier name,
                               const NPVariant *variant) {
  dprintf(("SharedMemory::SetProperty(%p, %s, %p)\n",
           obj, IdentToString(name), variant));

  return false;
}

SharedMemory* SharedMemory::New(Plugin* plugin, struct NaClDesc* desc) {
  static NPClass sharedMemoryClass = {
    NP_CLASS_STRUCT_VERSION,
    Allocate,
    Deallocate,
    Invalidate,
    HasMethod,
    Invoke,
    0,  // There is no InvokeDefault for SharedMemory.
    HasProperty,
    GetProperty,
    SetProperty,
  };
  struct nacl_abi_stat st;

  dprintf(("SharedMemory::New(%p, %p)\n", plugin, desc));

  SharedMemory* shared_memory =
    reinterpret_cast<SharedMemory*>(NPN_CreateObject(plugin->npp(),
                                                      &sharedMemoryClass));
  if (NULL == shared_memory) {
    return NULL;
  }

  shared_memory->plugin_ = plugin;
  shared_memory->desc_ = desc;
  // Set size from stat call.
  int rval = desc->vtbl->Fstat(desc, plugin->effp_, &st);
  if (0 == rval) {
    void* map_addr = NULL;
    size_t size = st.nacl_abi_st_size;
    // When probing by VirtualAlloc/mmap, use the same granularity
    // as the Map virtual function (64KB).
    size_t rounded_size = NaClRoundAllocPage(size);
    // Set the object size.
    shared_memory->size_ = rounded_size;
    dprintf(("SharedMemory::New: size 0x%08x\n", (unsigned) rounded_size));
    // Find an address range to map the object into.
    const int kMaxTries = 10;
    int tries = 0;
    do {
      ++tries;
#if NACL_WINDOWS
      map_addr = VirtualAlloc(NULL, rounded_size, MEM_RESERVE, PAGE_READWRITE);
      if (NULL == map_addr ||!VirtualFree(map_addr, 0, MEM_RELEASE)) {
        continue;
      }
#else
      map_addr = mmap(NULL,
                      rounded_size,
                      PROT_READ | PROT_WRITE,
                      MAP_SHARED | MAP_ANONYMOUS,
                      0,
                      0);
      dprintf(("SharedMemory::New: trying addr %p %d\n", map_addr, errno));
      if (MAP_FAILED == map_addr || munmap(map_addr, rounded_size)) {
        map_addr = NULL;
        continue;
      }
#endif
      dprintf(("SharedMemory::New: trying addr %p\n", map_addr));
      rval = desc->vtbl->Map(desc,
                             plugin->effp_,
                             map_addr,
                             rounded_size,
                             NACL_ABI_PROT_READ | NACL_ABI_PROT_WRITE,
                             NACL_ABI_MAP_SHARED,
                             0);
      dprintf(("SharedMemory::New: result 0x%08x\n", rval));
      if (!NaClIsNegErrno(rval)) {
        map_addr = (void*) rval;
        break;
      }
    } while (NULL == map_addr && tries < kMaxTries);
    dprintf(("SharedMemory::New: addr %p\n", map_addr));
    shared_memory->map_addr_ = map_addr;
  } else {
    dprintf(("SharedMemory::New: Fstat failed\n"));
    shared_memory->map_addr_ = NULL;
  }

  return shared_memory;
}

SharedMemory* SharedMemory::New(Plugin* plugin, off_t length) {
  size_t size = static_cast<size_t>(length);
  // The Map virtual function rounds up to the nearest AllocPage.
  size_t rounded_size = NaClRoundAllocPage(size);
  NaClHandle handle = nacl::CreateMemoryObject(rounded_size);
  if (nacl::kInvalidHandle == handle) {
    return NULL;
  }
  struct NaClDescImcShm *imc_desc =
      reinterpret_cast<struct NaClDescImcShm*>(malloc(sizeof(*imc_desc)));
  if (NULL == imc_desc) {
    nacl::Close(handle);
    return NULL;
  }
  struct NaClDesc* desc = reinterpret_cast<struct NaClDesc*>(imc_desc);

  dprintf(("SharedMemory::New(%p, 0x%08x)\n", plugin, (unsigned) length));
  if (!NaClDescImcShmCtor(imc_desc, handle, length)) {
    free(imc_desc);
    nacl::Close(handle);
    return NULL;
  }
  // Allocate the object through the canonical factory and return.
  return New(plugin, desc);
}

NPObject *SharedMemory::Allocate(NPP npp, NPClass *theClass) {
  dprintf(("SharedMemory::Allocate(%d)\n", ++number_alive));

  return new(std::nothrow) SharedMemory(npp);
}

void SharedMemory::Deallocate(NPObject *obj) {
  SharedMemory* shared_memory = reinterpret_cast<SharedMemory*>(obj);

  dprintf(("SharedMemory::Deallocate(%p, %d)\n", obj, --number_alive));

  // Free the memory that was mapped to the descriptor.
  if (shared_memory->desc_ && shared_memory->plugin_) {
    shared_memory->desc_->vtbl->Unmap(shared_memory->desc_,
                                      shared_memory->plugin_->effp_,
                                      shared_memory->map_addr_,
                                      shared_memory->size_);
  }
  // TODO: is there a missing NaClDescUnref here?
  delete reinterpret_cast<SharedMemory*>(obj);
}

void SharedMemory::Invalidate(NPObject *obj) {
  SharedMemory* shared_memory = reinterpret_cast<SharedMemory*>(obj);

  dprintf(("SharedMemory::Invalidate(%p)\n", shared_memory));

  // Invalidates are called by Firefox in abitrary order.  Hence, the plugin
  // could have been freed/trashed before we get invalidated.  Therefore, we
  // cannot use the effp_ member of the plugin object.
  // TODO: fix the resulting address space leak.
  // Free the memory that was mapped to the descriptor.
  // shared_memory->desc_->vtbl->Unmap(shared_memory->desc_,
  //                                   shared_memory->plugin_->effp_,
  //                                   shared_memory->map_addr_,
  //                                   shared_memory->size_);
  // After invalidation, the browser does not respect reference counting,
  // so we shut down here what we can and prevent attempts to shut down
  // other linked structures in Deallocate.
  shared_memory->plugin_ = NULL;
  shared_memory->desc_ = NULL;
}

SharedMemory::SharedMemory(NPP npp) : UnknownHandle(npp) {
  dprintf(("SharedMemory::SharedMemory(%p)\n", this));
}

SharedMemory::~SharedMemory() {
  dprintf(("SharedMemory::~SharedMemory(%p)\n", this));
}

}  // namespace nacl_srpc

/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <assert.h>
#include <new>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>

#include "native_client/nonnacl_util/sel_ldr_launcher.h"
#include "native_client/tools/npapi_runtime/nacl_npapi.h"

#include "native_client/npapi_plugin/origin.h"
#include "native_client/npapi_plugin/srpc/closure.h"
#include "native_client/npapi_plugin/srpc/npapi_native.h"
#include "native_client/npapi_plugin/srpc/plugin.h"
#include "native_client/npapi_plugin/srpc/srpc.h"
#include "native_client/npapi_plugin/srpc/connected_socket.h"
#include "native_client/npapi_plugin/srpc/service_runtime_interface.h"
#include "native_client/npapi_plugin/srpc/shared_memory.h"
#include "native_client/npapi_plugin/srpc/video.h"
#include "native_client/npapi_plugin/srpc/utility.h"

#include "native_client/service_runtime/include/sys/fcntl.h"
#include "native_client/service_runtime/nacl_desc_io.h"
#include "native_client/service_runtime/nacl_host_desc.h"
#include "native_client/service_runtime/nacl_log.h"

namespace nacl_srpc {

bool Plugin::identifiers_initialized = false;
NPIdentifier Plugin::kConnectIdent;
NPIdentifier Plugin::kHeightIdent;
NPIdentifier Plugin::kHrefIdent;
NPIdentifier Plugin::kLengthIdent;
NPIdentifier Plugin::kLocationIdent;
NPIdentifier Plugin::kMapIdent;
NPIdentifier Plugin::kModuleReadyIdent;
NPIdentifier Plugin::kNullNpapiMethodIdent;
NPIdentifier Plugin::kOnfailIdent;
NPIdentifier Plugin::kOnloadIdent;
NPIdentifier Plugin::kReadIdent;
NPIdentifier Plugin::kShmFactoryIdent;
NPIdentifier Plugin::kSignaturesIdent;
NPIdentifier Plugin::kSrcIdent;
NPIdentifier Plugin::kToStringIdent;
NPIdentifier Plugin::kUrlAsNaClDescIdent;
NPIdentifier Plugin::kValueOfIdent;
NPIdentifier Plugin::kVideoUpdateModeIdent;
NPIdentifier Plugin::kWidthIdent;
NPIdentifier Plugin::kWriteIdent;


class LoadNaClAppNotify : public Closure {
 public:
  LoadNaClAppNotify(Plugin *plugin);
  virtual ~LoadNaClAppNotify();
  virtual void Run(NPStream* stream, const char* fname);
 private:
  Plugin* plugin_;
};

LoadNaClAppNotify::LoadNaClAppNotify(Plugin* plugin)
    : plugin_(plugin) {
  dprintf(("LoadNaClAppNotify ctor\n"));
}

LoadNaClAppNotify::~LoadNaClAppNotify() {
  dprintf(("LoadNaClAppNotify dtor\n"));
}

void LoadNaClAppNotify::Run(NPStream* stream, const char* fname) {
  dprintf(("LoadNaClAppNotify Run %p, %p\n", stream, fname));
  if (NULL != fname) {
    plugin_->set_nacl_module_origin(nacl::UrlToOrigin(stream->url));
    plugin_->set_local_url(fname);
    plugin_->Load();
  }
}

char *MemAllocStrdup(const char *str) {
  int lenz = strlen(str) + 1;
  char *dup = static_cast<char *>(NPN_MemAlloc(lenz));
  if (NULL != dup) {
    strncpy(dup, str, lenz);
  }
  // else abort();
  return dup;
}

class UrlAsNaClDescNotify : public Closure {
 public:
  UrlAsNaClDescNotify(Plugin *plugin, std::string url, NPObject *callback_obj);
  virtual ~UrlAsNaClDescNotify();
  virtual void Run(NPStream *stream, const char *fname);
 private:
  Plugin* plugin_;
  std::string url_;
  NPObject* np_callback_;
};

UrlAsNaClDescNotify::UrlAsNaClDescNotify(Plugin* plugin,
                                         std::string url,
                                         NPObject *callback_obj) :
    plugin_(plugin),
    url_(url),
    np_callback_(callback_obj) {
  dprintf(("UrlAsNaClDescNotify ctor\n"));
  NPN_RetainObject(np_callback_);
}

UrlAsNaClDescNotify::~UrlAsNaClDescNotify() {
  dprintf(("UrlAsNaClDescNotify dtor\n"));
  NPN_ReleaseObject(np_callback_);
  np_callback_ = NULL;
}

void UrlAsNaClDescNotify::Run(NPStream *stream, const char *fname) {
  // open file as NaClHostDesc, create NaClDesc object, make available
  // via np_callback_
  NPVariant retval;
  NPVariant status;
  NPObject *nacl_desc = NULL;
  NPIdentifier callback_selector = Plugin::kOnfailIdent;

  dprintf(("UrlAsNaClDescNotify::Run(%p, %s)\n", stream, fname));

  VOID_TO_NPVARIANT(retval);
  VOID_TO_NPVARIANT(status);

  // execute body once; construct to use break statement to exit body early
  do {

    if (NULL == fname) {
      dprintf(("fetch failed\n"));
      ScalarToNPVariant("URL fetch failed", &status);
      break;
    }

    dprintf(("fetched FQ URL %s\n", stream->url));
    std::string url_origin = nacl::UrlToOrigin(stream->url);
    if (url_origin != plugin_->origin()) {
      dprintf(("same origin policy forbids access: "
               " page from origin %s attempted to"
               " fetch page with origin %s\n",
               plugin_->origin().c_str(),
               url_origin.c_str()));

      ScalarToNPVariant("Same origin violation", &status);
      break;
    }

    NaClHostDesc *nhd = static_cast<NaClHostDesc *>(malloc(sizeof *nhd));
    if (NULL == nhd) {
      dprintf(("no memory for nhd\n"));
      // TODO failure callback

      ScalarToNPVariant("No memory for NaClHostDesc object", &status);
      break;
    }
    int oserr = NaClHostDescOpen(nhd, const_cast<char *>(fname),
                                 NACL_ABI_O_RDONLY, 0);
    if (0 != oserr) {
      dprintf(("NaClHostDescOpen failed, NaCl error %d\n", oserr));
      free(nhd);

      ScalarToNPVariant("NaClHostDescOpen failed", &status);
      break;
    }
    NaClDescIoDesc *ndiod = NaClDescIoDescMake(nhd);  // takes ownership of nhd
    if (NULL == ndiod) {
      dprintf(("no memory for ndiod\n"));
      NaClHostDescClose(nhd);
      free(nhd);

      ScalarToNPVariant("No memory for NaClDescIoDesc object", &status);
      break;
    }
    dprintf(("created ndiod %p\n", ndiod));
    nacl_desc = UnknownHandle::New(plugin_,
                                   reinterpret_cast<NaClDesc *>(ndiod));
    callback_selector = Plugin::kOnloadIdent;

    ScalarToNPVariant(static_cast<NPObject*>(nacl_desc), &status);
    // NPVariant takes ownership of NPObject nacl_desc
  } while (0);

  dprintf(("calling np_callback_ %p, nacl_desc %p, status %p\n",
           np_callback_, nacl_desc, &status));
  NPN_Invoke(plugin_->npp(), np_callback_,
             callback_selector, &status, 1, &retval);

  dprintf(("releasing status %p\n", &status));
  NPN_ReleaseVariantValue(&status);
  NPN_ReleaseVariantValue(&retval);
}

bool Plugin::HasMethod(NPObject* obj, NPIdentifier name) {
  dprintf(("Plugin::HasMethod(%p, %s)\n", obj, IdentToString(name)));

  Plugin* plugin = reinterpret_cast<Plugin*>(obj);

  // Methods may be defined on the plugin as intrinsics.
  // Test for them first.
  if ((kShmFactoryIdent == name) || (kUrlAsNaClDescIdent == name))
    return true;
  // Intrinsic properties are not methods.
  if ((kHeightIdent == name) ||
      (kModuleReadyIdent == name) ||
      (kSrcIdent == name) ||
      (kVideoUpdateModeIdent == name) ||
      (kWidthIdent == name))
    return false;
  // If the method was not found as an intrinsic, try the loaded module, if any.
  if (NULL != plugin->socket_) {
    return plugin->socket_->HasMethod(plugin->socket_, name);
  }
  // Otherwise, the identifier specifies an invalid method.
  return false;
}

bool Plugin::Invoke(NPObject* obj,
                    NPIdentifier name,
                    const NPVariant *args,
                    uint32_t arg_count,
                    NPVariant *result) {
  Plugin* plugin = reinterpret_cast<Plugin*>(obj);

  dprintf(("Plugin::Invoke(%p, %s, %d)\n",
           obj, IdentToString(name), arg_count));

  VOID_TO_NPVARIANT(*result);
  // Methods may be defined on the plugin as intrinsics.
  // Test for them first.
  if (kShmFactoryIdent == name) {
    // Type signature:
    // SharedMemory* __shmFactory(integer)
    if (1 != arg_count) {
      NPN_SetException(obj, "Bad argument count to __shmFactory");
      return false;
    }
    uint32_t size;
    if (!NPVariantToScalar(&args[0], &size)) {
      NPN_SetException(obj, "incorrect type for argument");
      return false;
    }
    SharedMemory* shared_memory =
        SharedMemory::New(plugin, static_cast<size_t>(size));
    if (NULL == shared_memory) {
      NPN_SetException(obj, "out of memory");
      return false;
    }
    ScalarToNPVariant(static_cast<NPObject*>(shared_memory), result);
    return true;
  } else if (kUrlAsNaClDescIdent == name) {
    // Type signature:
    // __UrlAsNaClDesc(string, Function)
    if (2 != arg_count) {
      NPN_SetException(obj, "Bad argument count to __urlAsNaClDesc");
      return false;
    }
    char* url;
    if (!NPVariantToScalar(&args[0], &url)) {
      NPN_SetException(obj, "Bad first argument to __urlAsNaClDesc");
      return false;
    }
    NPObject* callback_obj;
    if (!NPVariantToScalar(&args[1], &callback_obj)) {
      NPN_SetException(obj, "Bad second argument to __urlAsNaClDesc");
      return false;
    }
    dprintf(("loading %s as file\n", url));
    UrlAsNaClDescNotify* callback =
        new(std::nothrow) UrlAsNaClDescNotify(plugin, url, callback_obj);
    if (NULL == callback) {
      delete[] url;
      NPN_SetException(obj, "Out of memory in __urlAsNaClDesc");
      return false;
    }
    NPError err = NPN_GetURLNotify(plugin->npp_, url, NULL, callback);
    delete[] url;
    if (NPERR_NO_ERROR != err) {
      dprintf(("failed to load URL %s to local file. Error %d\n", url, err));
      // callback is always deleted in URLNotify
      NPN_SetException(obj, "specified url could not be loaded");
      return false;
    }
    return true;
  }
  // If the method was not found as an intrinsic, try the loaded module, if any.
  if (NULL != plugin->socket_) {
    // Socket/srpc client is responsible for setting exceptions.
    bool rv = plugin->socket_->Invoke(plugin->socket_,
                                      name,
                                      args,
                                      arg_count,
                                      result);
    if (!rv) {
      NPN_SetException(plugin, "Method invocation failed");
    }
    return rv;
  }
  // Otherwise, the identifier specifies an invalid method.
  NPN_SetException(obj, "Unrecognized method specified");
  return false;
}

bool Plugin::InvokeDefault(NPObject* obj,
                           const NPVariant* args,
                           uint32_t arg_count,
                           NPVariant* result) {
  dprintf(("Plugin::InvokeDefault(%p, %d)\n", obj, arg_count));

  ScalarToNPVariant(1, result);
  return true;
}

bool Plugin::HasProperty(NPObject* obj, NPIdentifier name) {
  Plugin* plugin = reinterpret_cast<Plugin*>(obj);

  dprintf(("Plugin::HasProperty(%p, %s)\n", obj, IdentToString(name)));

  // There are some intrinsic properties to the plugin.  Check for them first.
  if ((kHeightIdent == name) ||
      (kModuleReadyIdent == name) ||
      (kSrcIdent == name) ||
      (kVideoUpdateModeIdent == name) ||
      (kWidthIdent == name)) {
    return true;
  }
  // If property was not found as an intrinsic, try the loaded module, if any.
  if (plugin->socket_) {
    return plugin->socket_->HasProperty(plugin->socket_, name);
  }
  // Otherwise, the identifier specifies an invalid property.
  return false;
}

bool Plugin::GetProperty(NPObject* obj,
                         NPIdentifier name,
                         NPVariant* variant) {
  Plugin* plugin = reinterpret_cast<Plugin*>(obj);

  dprintf(("Plugin::GetProperty(%p, %s)\n", obj, IdentToString(name)));

  VOID_TO_NPVARIANT(*variant);
  // There are some intrinsic properties to the plugin.  Check for them first.
  if (kHeightIdent == name) {
    ScalarToNPVariant(plugin->height_, variant);
    return true;
  } else if (kModuleReadyIdent == name) {
    ScalarToNPVariant((plugin->socket_ ? 1 : 0), variant);
    return true;
  } else if (kSrcIdent == name) {
    ScalarToNPVariant(plugin->local_url_, variant);
    return true;
  } else if (kVideoUpdateModeIdent == name) {
    ScalarToNPVariant(plugin->video_update_mode_, variant);
    return true;
  } else if (kWidthIdent == name) {
    ScalarToNPVariant(plugin->width_, variant);
    return true;
  }
  // If property was not found as an intrinsic, try the loaded module, if any.
  if (plugin->socket_) {
    return plugin->socket_->GetProperty(plugin->socket_, name, variant);
  }
  // Otherwise, the identifier specifies an invalid property.
  return false;
}

bool Plugin::SetProperty(NPObject* obj,
                         NPIdentifier name,
                         const NPVariant* variant) {
  Plugin* plugin = reinterpret_cast<Plugin*>(obj);

  dprintf(("Plugin::SetProperty(%p, %s, %p)\n",
           obj, IdentToString(name), variant));
  dprintf(("kSrcIdent = %s\n", IdentToString(kSrcIdent)));

  // There are some intrinsic properties to the plugin.  Check for them first.
  if (kHeightIdent == name) {
    int32_t height;
    if (!NPVariantToScalar(variant, &height)) {
      NPN_SetException(obj, "height must be an integer");
      return false;
    }
    plugin->height_ = height;
    return true;
  } else if (kModuleReadyIdent == name) {
    // Module ready is a read-only property.
    NPN_SetException(obj, "__moduleReady is a read-only property");
    return false;
  } else if (kSrcIdent == name) {
    char* url;
    if (!NPVariantToScalar(variant, &url)) {
      NPN_SetException(obj, "src must be a string");
      return false;
    }
    if (NULL != plugin->service_runtime_interface_) {
      dprintf(("Plugin::SetProperty: unloading previous\n"));
      // Plugin owns socket_, so when we change to a new socket we need to
      // give up ownership of the old one.
      NPN_ReleaseObject(plugin->socket_);
      plugin->socket_ = NULL;
      plugin->service_runtime_interface_ = NULL;
    }
    // Load the new module if the origin of the page is valid.
    dprintf(("Plugin::SetProperty src = '%s'\n", url));
    LoadNaClAppNotify* callback =
        new(std::nothrow) LoadNaClAppNotify(plugin);
    if (NULL == callback) {
      NPN_SetException(obj, "setting src failed to create callback");
      return false;
    }
    NPError err = NPN_GetURLNotify(plugin->npp_, url, NULL, callback);
    if (NPERR_NO_ERROR != err) {
      dprintf(("Failed to load URL to local file. Error %d\n", err));
      // callback is always deleted in URLNotify
      NPN_SetException(obj, "setting src failed to load url");
      return false;
    }
    return true;
  } else if (kVideoUpdateModeIdent == name) {
    int32_t video_update_mode;
    if (!NPVariantToScalar(variant, &video_update_mode)) {
      NPN_SetException(obj, "videoUpdateMode must be an integer");
      return false;
    }
    plugin->video_update_mode_ = video_update_mode;
    return true;
  } else if (kWidthIdent == name) {
    int32_t width;
    if (!NPVariantToScalar(variant, &width)) {
      NPN_SetException(obj, "width must be an integer");
      return false;
    }
    plugin->width_ = width;
    return true;
  }
  // If property was not found as an intrinsic, try the loaded module, if any.
  if (plugin->socket_) {
    return plugin->socket_->SetProperty(plugin->socket_, name, variant);
  }
  // Otherwise, the identifier specifies an invalid property.
  NPN_SetException(obj, "Attempted to set unrecognized property");
  return false;
}

Plugin* Plugin::New(NPP instance, nacl::SRPC_Plugin* srpc_plugin) {
  static NPClass pluginClass = {
    NP_CLASS_STRUCT_VERSION,
    Allocate,
    Deallocate,
    Invalidate,
    HasMethod,
    Invoke,
    InvokeDefault,
    HasProperty,
    GetProperty,
    SetProperty,
  };
  NPVariant loc_value;
  NPVariant href_value;

  dprintf(("Plugin::New()\n"));

  Plugin* plugin =
      reinterpret_cast<Plugin*>(NPN_CreateObject(instance, &pluginClass));
  if (NULL == plugin) {
    return NULL;
  }

  if (!plugin->Start()) {
    NPN_ReleaseObject(plugin);
    return NULL;
  }
  plugin->srpc_plugin_ = srpc_plugin;

  VOID_TO_NPVARIANT(loc_value);
  VOID_TO_NPVARIANT(href_value);

  do {
    NPObject *win_obj;
    if (NPERR_NO_ERROR
        != NPN_GetValue(instance, NPNVWindowNPObject, &win_obj)) {
      dprintf(("Plugin::New: No window object\n"));
      // no window; no URL as NaCl descriptors will be allowed
      break;
    }

    if (!NPN_GetProperty(instance, win_obj, kLocationIdent, &loc_value)) {
      dprintf(("Plugin::New no location property value\n"));
      break;
    }
    NPObject *loc_obj;
    if (!NPVariantToScalar(&loc_value, &loc_obj)) {
      dprintf(("Plugin::New location property of wrong type\n"));
      break;
    }

    if (!NPN_GetProperty(instance, loc_obj, kHrefIdent, &href_value)) {
      dprintf(("Plugin::New no href property value\n"));
      break;
    }
    char* str;
    if (!NPVariantToScalar(&href_value, &str)) {
      dprintf(("Plugin::New href property of wrong type\n"));
      break;
    }
    std::string href(str);
    dprintf(("Plugin::New: href %s\n", href.c_str()));

    plugin->origin_ = nacl::UrlToOrigin(href);
    dprintf(("Plugin::New: origin %s\n", plugin->origin_.c_str()));
    // Check that origin is in the list of permitted origins.
    plugin->origin_valid_ = nacl::OriginIsInWhitelist(plugin->origin_);
    // this implementation of same-origin policy does not take
    // document.domain element into account.
    //
  } while (0);

  NPN_ReleaseVariantValue(&loc_value);
  NPN_ReleaseVariantValue(&href_value);
  return plugin;
}

NPObject *Plugin::Allocate(NPP npp, NPClass *theClass) {
  dprintf(("Plugin::Allocate()\n"));

  return new(std::nothrow) Plugin(npp);
}

Plugin::Plugin(NPP npp) :
  npp_(npp),
  socket_(NULL),
  service_runtime_interface_(NULL),
  local_url_(NULL),
  height_(0),
  video_update_mode_(nacl::kVideoUpdatePluginPaint),
  width_(0),
  effp_(NULL) {

  dprintf(("Plugin::Plugin(%p)\n", this));

  if (!identifiers_initialized) {
    kConnectIdent         = NPN_GetStringIdentifier("connect");
    kHeightIdent          = NPN_GetStringIdentifier("height");
    kHrefIdent            = NPN_GetStringIdentifier("href");
    kLengthIdent          = NPN_GetStringIdentifier("length");
    kLocationIdent        = NPN_GetStringIdentifier("location");
    kMapIdent             = NPN_GetStringIdentifier("map");
    kModuleReadyIdent     = NPN_GetStringIdentifier("__moduleReady");
    kNullNpapiMethodIdent = NPN_GetStringIdentifier("__nullNpapiMethod");
    kOnfailIdent          = NPN_GetStringIdentifier("onfail");
    kOnloadIdent          = NPN_GetStringIdentifier("onload");
    kReadIdent            = NPN_GetStringIdentifier("read");
    kShmFactoryIdent      = NPN_GetStringIdentifier("__shmFactory");
    kSignaturesIdent      = NPN_GetStringIdentifier("__signatures");
    kSrcIdent             = NPN_GetStringIdentifier("src");
    kToStringIdent        = NPN_GetStringIdentifier("toString");
    kUrlAsNaClDescIdent   = NPN_GetStringIdentifier("__urlAsNaClDesc");
    kValueOfIdent         = NPN_GetStringIdentifier("valueOf");
    kVideoUpdateModeIdent = NPN_GetStringIdentifier("videoUpdateMode");
    kWidthIdent           = NPN_GetStringIdentifier("width");
    kWriteIdent           = NPN_GetStringIdentifier("write");

    identifiers_initialized = true;
  }
}

bool Plugin::Start() {
  struct NaClDesc* pair[2];

  if (0 != NaClCommonDescMakeBoundSock(pair)) {
    dprintf(("Plugin::Plugin: make bound sock failed.\n"));
    return false;
  }
  if (!NaClNrdXferEffectorCtor(&eff_, pair[0])) {
    dprintf(("Plugin::Plugin: EffectorCtor failed.\n"));
    return false;
  }
  effp_ = (struct NaClDescEffector*) &eff_;
  return true;
}

Plugin::~Plugin() {
  dprintf(("Plugin::~Plugin(%p)\n", this));

  if (NULL != local_url_)
    NPN_MemFree(local_url_);
}

void Plugin::Deallocate(NPObject* obj) {
  Plugin* plugin = reinterpret_cast<Plugin*>(obj);

  dprintf(("Plugin::Deallocate(%p)\n", plugin));

  // hard shutdown
  if (NULL != plugin->service_runtime_interface_) {
    plugin->service_runtime_interface_->Shutdown();
  }
  // Free the connected socket for this plugin, if any.
  if (NULL != plugin->socket_) {
    dprintf(("Plugin::Deallocate: unloading\n"));
    // Deallocating a plugin releases ownership of the socket.
    NPN_ReleaseObject(plugin->socket_);
  }
  // Clear the pointers to the connected socket and service runtime interface.
  plugin->socket_ = NULL;
  plugin->service_runtime_interface_ = NULL;
  // Delete this plugin instance.
  delete plugin;
}

void Plugin::Invalidate(NPObject* obj) {
  Plugin* plugin = reinterpret_cast<Plugin*>(obj);

  dprintf(("Plugin::Invalidate(%p)\n", obj));

  // perhaps change to do soft shutdown here?
  if (NULL != plugin->service_runtime_interface_) {
    plugin->service_runtime_interface_->Shutdown();
    // TODO: this needs to free the interface and set it to NULL.
  }
  // After invalidation, the browser does not respect reference counting,
  // so we shut down here what we can and prevent attempts to shut down
  // other linked structures in Deallocate.
}

void Plugin::set_local_url(const char* name) {
  dprintf(("Plugin::set_local_url(%s)\n", name));
  local_url_ = MemAllocStrdup(name);
}

// Create a new service node from a downloaded service.
bool Plugin::Load() {
  dprintf(("Plugin::Load(%s)\n", local_url_));
  // If the origin is not in the whitelist, refuse to load.
  if (!origin_valid_) {
    printf("Load failed: NaCl module did not come from a whitelisted"
           " source.\nSee npapi_plugin/origin.cc for the list.");
    NPObject* window;
    NPN_GetValue(npp(), NPNVWindowNPObject, &window);
    NPString script;
    script.utf8characters = "alert('Load failed: NaCl module did not"
        " come from a whitelisted source.\\n"
        "See npapi_plugin/origin.cc"
        " for the list.');";
    script.utf8length = strlen(script.utf8characters);
    NPVariant result;
    NPN_Evaluate(npp(), window, &script, &result);
    return false;
  }
  // Catch any bad accesses, etc., while loading.
  nacl_srpc::ScopedCatchSignals sigcatcher(
      (nacl_srpc::ScopedCatchSignals::SigHandlerType) SignalHandler);
  if (PLUGIN_SETJMP(loader_env, 1)) {
    return false;
  }

  dprintf(("Load: NaCl module from '%s'\n", local_url_));

  // check ABI version compatibility
  NPError np = nacl::CheckExecutableVersion(npp(), local_url_);
  if (NPERR_NO_ERROR != np) {
    dprintf(("Load: FAILED due to possible ABI version mismatch\n"));
    return false;
  }
  // Load a file via a forked sel_ldr process.
  service_runtime_interface_ = new(std::nothrow) ServiceRuntimeInterface(this);
  if (NULL == service_runtime_interface_) {
    dprintf((" ServiceRuntimeInterface Ctor failed\n"));
    return false;
  }
  if (!service_runtime_interface_->Start(local_url_)) {
    dprintf(("  Load: FAILED to start service runtime"));
    return false;
  }
  dprintf(("  Load: started sel_ldr\n"));
  socket_ = service_runtime_interface_->default_socket();
  dprintf(("  Load: established socket %p\n", socket_));
  // Plugin takes ownership of socket_ from service_runtime_interface_,
  // so we do not need to call NPN_RetainObject.
  return true;
}

void Plugin::SignalHandler(int value) {
  dprintf(("Plugin::SignalHandler()\n"));
  PLUGIN_LONGJMP(loader_env, value);
}


PLUGIN_JMPBUF Plugin::loader_env;


}  // namespace nacl_srpc

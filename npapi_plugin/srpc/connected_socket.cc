/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <new>
#include <signal.h>
#include <string.h>
#include "native_client/npapi_plugin/srpc/connected_socket.h"
#include "native_client/npapi_plugin/srpc/npapi_native.h"
#include "native_client/npapi_plugin/srpc/plugin.h"
#include "native_client/npapi_plugin/srpc/service_runtime_interface.h"
#include "native_client/npapi_plugin/srpc/shared_memory.h"
#include "native_client/npapi_plugin/srpc/srpc_client.h"
#include "native_client/npapi_plugin/srpc/video.h"
#include "native_client/npapi_plugin/srpc/utility.h"
#include "native_client/service_runtime/nacl_desc_imc.h"
#include "native_client/service_runtime/nacl_sync.h"
#include "native_client/service_runtime/nacl_threads.h"

namespace nacl_srpc {


// Define all the static variables.
int ConnectedSocket::number_alive = 0;
PLUGIN_JMPBUF ConnectedSocket::socket_env;

// ConnectedSocket implements a method for each method exported from
// the NaCl service runtime.
bool ConnectedSocket::HasMethod(NPObject *obj, NPIdentifier name) {
  ConnectedSocket* socket = reinterpret_cast<ConnectedSocket*>(obj);

  dprintf(("ConnectedSocket::HasMethod(%p, %s)\n", obj, IdentToString(name)));

  return socket->srpc_client_->HasMethod(name);
}

bool ConnectedSocket::Invoke(NPObject *obj,
                             NPIdentifier name,
                             const NPVariant *args,
                             uint32_t arg_count,
                             NPVariant *result) {
  ConnectedSocket* socket = reinterpret_cast<ConnectedSocket*>(obj);

  dprintf(("ConnectedSocket::Invoke(%p, %s, %d)\n",
           obj, IdentToString(name), arg_count));

  VOID_TO_NPVARIANT(*result);
  ScopedCatchSignals sigcatcher(
      (ScopedCatchSignals::SigHandlerType) SignalHandler);
  if (PLUGIN_SETJMP(socket_env, 1)) {
    NPN_SetException(socket->plugin_, "method invocation raised signal");
    return false;
  }

  // Invoke the specified method of the service.
  // SrpcClient is responsible for setting exceptions.
  return socket->srpc_client_->Invoke(name, args, arg_count, result);
}

void ConnectedSocket::SignalHandler(int value) {
  dprintf(("ConnectedSocket::SignalHandler()\n"));

  PLUGIN_LONGJMP(socket_env, value);
}

// ConnectedSocket exports the signature property and the
// names of the methods exported from the service.
bool ConnectedSocket::HasProperty(NPObject *obj, NPIdentifier name) {
  dprintf(("ConnectedSocket::HasProperty(%p, %s)\n", obj, IdentToString(name)));

  if (Plugin::kSignaturesIdent == name) {
    return true;
  }
  // Safari apparently wants us to return false here.  This means programmers
  // will have to use a JavaScript function object to wrap NativeClient
  // methods they intend to invoke indirectly.
  return false;
}

bool ConnectedSocket::GetProperty(NPObject *obj,
                                  NPIdentifier name,
                                  NPVariant *variant) {
  dprintf(("ConnectedSocket::GetProperty(%p, %s)\n", obj, IdentToString(name)));

  VOID_TO_NPVARIANT(*variant);
  if (Plugin::kSignaturesIdent == name) {
    ConnectedSocket* socket = reinterpret_cast<ConnectedSocket*>(obj);
    ScalarToNPVariant(static_cast<NPObject*>(socket->signatures_), variant);
    NPN_RetainObject(socket->signatures_);
  } else {
    // Firefox wants this to return true and a null.
  }
  return true;
}

bool ConnectedSocket::SetProperty(NPObject *obj,
                                  NPIdentifier name,
                                  const NPVariant *variant) {
  dprintf(("ConnectedSocket::SetProperty(%p, %s, %p)\n",
           obj, IdentToString(name), variant));

  return false;
}

ConnectedSocket* ConnectedSocket::New(Plugin* plugin,
                                      struct NaClDesc* desc,
                                      bool is_srpc_client,
                                      ServiceRuntimeInterface* serv_rtm_info,
                                      bool is_command_channel) {
  static NPClass socketClass = {
    NP_CLASS_STRUCT_VERSION,
    Allocate,
    Deallocate,
    Invalidate,
    HasMethod,
    Invoke,
    0,
    HasProperty,
    GetProperty,
    SetProperty,
  };
  nacl::VideoScopedGlobalLock video_lock;

  dprintf(("ConnectedSocket::New(%p, %p, %d, %p, %d)\n", plugin, desc,
           is_srpc_client, serv_rtm_info, is_command_channel));

  ConnectedSocket* socket =
    reinterpret_cast<ConnectedSocket*>(NPN_CreateObject(plugin->npp(),
                                                        &socketClass));
  if (NULL == socket) {
    return NULL;
  }

  socket->plugin_ = plugin;
  socket->is_command_channel_ = is_command_channel;
  NaClDescRef(desc);  // BUG(sehr): probably not right
  socket->desc_ = desc;
  socket->service_runtime_info_ = serv_rtm_info;
  if (is_srpc_client) {
    // Get SRPC client interface going over socket.
    socket->srpc_client_ = new(std::nothrow) SrpcClient();
    if (NULL == socket->srpc_client_) {
      // Return an error.
      NPN_ReleaseObject(socket);
      dprintf(("ConnectedSocket::New -- new failed.\n"));
      return NULL;
    }
    if (!socket->srpc_client_->Init(plugin, socket)) {
      delete socket->srpc_client_;
      socket->srpc_client_ = NULL;
      NPN_ReleaseObject(socket);
      dprintf(("ConnectedSocket::New -- SrpcClient::Init failed.\n"));
      return NULL;
    }
    // Prefetch the signatures for use by clients.
    socket->signatures_ = socket->srpc_client_->GetSignatureObject();
    // only enable display on socket with service_runtime_info
    if (NULL != socket->service_runtime_info_) {
      if (NULL != socket->plugin_) {
        plugin->srpc_plugin()->video()->Enable();
      }
    }
  } else {
    socket->srpc_client_ = NULL;
    socket->signatures_ = NULL;
  }

  return socket;
}

NPObject *ConnectedSocket::Allocate(NPP npp, NPClass *theClass) {
  dprintf(("ConnectedSocket::Allocate(%d)\n", ++number_alive));

  return new(std::nothrow) ConnectedSocket(npp);
}

void ConnectedSocket::Deallocate(NPObject *obj) {
  ConnectedSocket* socket = reinterpret_cast<ConnectedSocket*>(obj);

  dprintf(("ConnectedSocket::Deallocate(%p, %d)\n", obj, --number_alive));

  // Release the contained descriptor.
  if (NULL != socket->desc_) {
    dprintf((" NaClDescUnref(%p)\n", socket->desc_));
    NaClDescUnref(socket->desc_);
    socket->desc_ = NULL;
  }
  // Release the other NPAPI objects.
  if (socket->signatures_) {
    NPN_ReleaseObject(socket->signatures_);
    socket->signatures_ = NULL;
  }
  if (socket->plugin_) {
    socket->plugin_ = NULL;
  }

  // And free the memory for this object.
  delete socket;
  dprintf(("ConnectedSocket::Deallocate done\n"));
}

void ConnectedSocket::Invalidate(NPObject *obj) {
  ConnectedSocket* socket = reinterpret_cast<ConnectedSocket*>(obj);

  dprintf(("ConnectedSocket::Invalidate(%p)\n", socket));

  // After invalidation, the browser does not respect reference counting,
  // so we shut down here what we can and prevent attempts to shut down
  // other linked structures in Deallocate.

  // Unreference the socket object.
  NaClDescUnref(socket->desc_);
  // Block access to the other contained structures.
  socket->desc_ = NULL;
  socket->signatures_ = NULL;
  socket->plugin_ = NULL;
}

ConnectedSocket::ConnectedSocket(NPP npp) : UnknownHandle(npp),
  signatures_(NULL),
  service_runtime_info_(NULL),
  srpc_client_(NULL),
  is_command_channel_(false) {
  dprintf(("ConnectedSocket::ConnectedSocket(%p)\n", this));
}

ConnectedSocket::~ConnectedSocket() {
  dprintf(("ConnectedSocket::~ConnectedSocket(%p)\n", this));

  // Free the SRPC connection.
  delete srpc_client_;
  //  Free the rpc method descriptors and terminate the connection to
  //  the service runtime instance.
  dprintf(("ConnectedSocket(%p): deleting SRI %p\n",
           this, service_runtime_info_));
  if (service_runtime_info_) {
    delete service_runtime_info_;
  }
}

}  // namespace nacl_srpc

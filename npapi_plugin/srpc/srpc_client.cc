/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <map>
#include <new>
#if NACL_WINDOWS
#include <process.h>
#endif
#include <string>
#include "native_client/nonnacl_util/sel_ldr_launcher.h"
#include "native_client/npapi_plugin/srpc/connected_socket.h"
#include "native_client/npapi_plugin/srpc/npapi_native.h"
#include "native_client/npapi_plugin/srpc/plugin.h"
#include "native_client/npapi_plugin/srpc/socket_address.h"
#include "native_client/npapi_plugin/srpc/srpc_client.h"
#include "native_client/npapi_plugin/srpc/unknown_handle.h"
#include "native_client/npapi_plugin/srpc/utility.h"
#include "native_client/service_runtime/nacl_desc_base.h"
#include "native_client/service_runtime/nacl_log.h"

namespace nacl_srpc {

int SrpcClient::number_alive = 0;

// Forward declarations of functions marshalling to/from NPAPI.
static bool MarshallInputs(Plugin* plugin,
                           const NPVariant* args,
                           NaClSrpcArg* inputs[],
                           NaClSrpcArg* outputs[]);
static bool MarshallOutputs(Plugin* plugin,
                            const NaClSrpcArg** outs,
                            NPVariant* result);
static uint32_t SignatureLength(NaClSrpcArg* in_index[],
                                NaClSrpcArg* out_index[]);
static uint32_t ArgsLength(const NaClSrpcArg* index[]);

// MethodInfo records the method names and type signatures of an SRPC server.
class MethodInfo {
 public:
  MethodInfo(const char* name,
             uint32_t number,
             const char* ins,
             const char* outs) :
    name_(strdup(name)),
    number_(number),
    ins_(strdup(ins)),
    outs_(strdup(outs)) { }

  ~MethodInfo() {
    free(reinterpret_cast<void*>(name_));
    free(reinterpret_cast<void*>(ins_));
    free(reinterpret_cast<void*>(outs_));
  }

  const char* TypeName(NaClSrpcArgType type);
  RetArray* Signature(Plugin* plugin);

 private:

 public:
  char* name_;
  uint32_t number_;
  char* ins_;
  char* outs_;
};

const char* MethodInfo::TypeName(NaClSrpcArgType type) {
  const char* str;
  char* retstr;

  str = "BAD_TYPE";
  switch (type) {
   case NACL_SRPC_ARG_TYPE_INVALID:
    str = "INVALID";
    break;
   case NACL_SRPC_ARG_TYPE_DOUBLE:
    str = "double";
    break;
   case NACL_SRPC_ARG_TYPE_INT:
    str = "int";
    break;
   case NACL_SRPC_ARG_TYPE_STRING:
    str = "string";
    break;
   case NACL_SRPC_ARG_TYPE_HANDLE:
    str = "handle";
    break;
   case NACL_SRPC_ARG_TYPE_BOOL:
    str = "bool";
    break;
   case NACL_SRPC_ARG_TYPE_CHAR_ARRAY:
    str = "char[]";
    break;
   case NACL_SRPC_ARG_TYPE_INT_ARRAY:
    str = "int[]";
    break;
   case NACL_SRPC_ARG_TYPE_DOUBLE_ARRAY:
    str = "double[]";
    break;
   // The two cases below are added for compatibility, they are used
   // only in the new plugin code.
   case NACL_SRPC_ARG_TYPE_OBJECT:
   case NACL_SRPC_ARG_TYPE_VARIANT_ARRAY:
    str = "BAD TYPE";
    break;
#ifdef _MSC_VER
   default:
    str = "BAD TYPE";
    break;
#endif  // _MSC_VER
  }
  size_t len = strlen(str) + 1;
  retstr = reinterpret_cast<char*>(NPN_MemAlloc(len));
  if (NULL == retstr) {
    return NULL;
  }
  strncpy(retstr, str, len);
  return retstr;
}

RetArray* MethodInfo::Signature(Plugin* plugin) {
  RetArray* toplevel = RetArray::New(plugin, 3);
  if (NULL == toplevel) {
    return NULL;
  }

  dprintf(("Signature: %p->[0] = %p (%s)\n",
           toplevel, name_, name_));

  ScalarToNPVariant(name_, &toplevel->values_[0]);

  uint32_t ins_length = strlen(ins_);
  RetArray* args = RetArray::New(plugin, ins_length);
  if (NULL == args && 0 != ins_length) {
    NPN_ReleaseObject(toplevel);
    return NULL;
  }
  dprintf(("Signature: %p->[1] = %p\n", toplevel, args));
  ScalarToNPVariant(static_cast<NPObject*>(args), &toplevel->values_[1]);
  uint32_t i;
  for (i = 0; i < ins_length; ++i) {
    dprintf(("Signature: %p->[1][%d] = %p (%s)\n",
             &args->values_[i], i,
             TypeName(static_cast<NaClSrpcArgType>(ins_[i])),
             TypeName(static_cast<NaClSrpcArgType>(ins_[i]))));
    ScalarToNPVariant(TypeName(static_cast<NaClSrpcArgType>(ins_[i])),
                      &args->values_[i]);
  }
  uint32_t outs_length = strlen(outs_);
  RetArray* rets = RetArray::New(plugin, outs_length);
  if (NULL == args && 0 != outs_length) {
    // BUG: not freeing type name strings.
    NPN_ReleaseObject(toplevel);
    return NULL;
  }
  dprintf(("Signature: %p->[2] = %p\n", toplevel, rets));
  ScalarToNPVariant(static_cast<NPObject*>(rets), &toplevel->values_[2]);
  for (i = 0; i < outs_length; ++i) {
    dprintf(("Signature: %p->[2][%d] = %p (%s)\n",
             &rets->values_[i], i,
             TypeName(static_cast<NaClSrpcArgType>(outs_[i])),
             TypeName(static_cast<NaClSrpcArgType>(outs_[i]))));
    ScalarToNPVariant(TypeName(static_cast<NaClSrpcArgType>(outs_[i])),
                      &rets->values_[i]);
  }
  return toplevel;
}

// A utility class that builds and deletes parameter vectors used in
// rpcs.
class SrpcParams {
 public:
  SrpcParams() {
    ins_[0] = NULL;
    outs_[0] = NULL;
  }

  ~SrpcParams() {
    FreeVector(ins_);
    FreeVector(outs_);
  }

  bool BuildVectors(const char* in_types, const char* out_types);

 private:
  bool BuildOneVector(NaClSrpcArg* vec[], const char* types);
  void FreeVector(NaClSrpcArg* vec[]);

 public:
  // The ins and outs arrays contain one more element, to hold a NULL pointer
  // to indicate the end of the list.
  NaClSrpcArg* ins_[NACL_SRPC_MAX_ARGS + 1];
  NaClSrpcArg* outs_[NACL_SRPC_MAX_ARGS + 1];
};

bool SrpcParams::BuildVectors(const char* in_types, const char* out_types) {
  if (!BuildOneVector(ins_, in_types)) {
    return false;
  }
  if (!BuildOneVector(outs_, out_types)) {
    FreeVector(ins_);
    return false;
  }
  return true;
}

bool SrpcParams::BuildOneVector(NaClSrpcArg* vec[], const char* types) {
  const uint32_t kLength = strlen(types);
  if (NACL_SRPC_MAX_ARGS < kLength) {
    return false;
  }
  NaClSrpcArg* args = new(std::nothrow) NaClSrpcArg[kLength];
  if (NULL == args) {
    return false;
  }
  memset(static_cast<void*>(args), 0, kLength * sizeof(NaClSrpcArg));
  for (uint32_t i = 0; i < kLength; ++i) {
    vec[i] = &args[i];
    args[i].tag = static_cast<NaClSrpcArgType>(types[i]);
  }
  vec[kLength] = NULL;
  return true;
}

void SrpcParams::FreeVector(NaClSrpcArg* vec[]) {
  if (NULL == vec[0]) {
    return;
  }
  for (NaClSrpcArg** argp = vec; *argp; ++argp) {
    switch ((*argp)->tag) {
     case NACL_SRPC_ARG_TYPE_CHAR_ARRAY:
      free((*argp)->u.caval.carr);
      break;
     case NACL_SRPC_ARG_TYPE_DOUBLE_ARRAY:
      free((*argp)->u.daval.darr);
      break;
     case NACL_SRPC_ARG_TYPE_HANDLE:
      break;
     case NACL_SRPC_ARG_TYPE_INT_ARRAY:
      free((*argp)->u.iaval.iarr);
      break;
     case NACL_SRPC_ARG_TYPE_STRING:
      free((*argp)->u.sval);
      break;
     case NACL_SRPC_ARG_TYPE_BOOL:
     case NACL_SRPC_ARG_TYPE_DOUBLE:
     case NACL_SRPC_ARG_TYPE_INT:
     // The two cases below are added for compatibility, they are used
     // only in the new plugin code.
     case NACL_SRPC_ARG_TYPE_OBJECT:
     case NACL_SRPC_ARG_TYPE_VARIANT_ARRAY:
     default:
       break;
    }
  }
  // Free the vector containing the arguments themselves.
  delete[] vec[0];
}

SrpcClient::SrpcClient() :
  plugin_(NULL),
  is_command_channel_(false) {
  dprintf(("SrpcClient::SrpcClient(%p, %d)\n", this, ++number_alive));
}

bool SrpcClient::Init(Plugin* plugin, ConnectedSocket* socket) {
  dprintf(("SrpcClient::Init(%p, %p, %p, %d)\n",
           this, plugin, socket, socket->is_command_channel()));

  // Open the channel to pass RPC information back and forth
  if (!NaClSrpcClientCtor(&srpc_channel_, socket->desc())) {
     dprintf(("SrpcClient::SrpcClient: Ctor failed\n"));
    return false;
  }
  // Set the relevant state.
  plugin_ = plugin;
  is_command_channel_ = socket->is_command_channel();
  dprintf(("SrpcClient::SrpcClient: Ctor worked\n"));
  // Record the method names for later dispatches.
  GetMethods();
  dprintf(("SrpcClient::SrpcClient: GetMethods worked\n"));
  return true;
}

SrpcClient::~SrpcClient() {
  dprintf(("SrpcClient::~SrpcClient(%p, %d)\n", this, --number_alive));
  if (NULL == plugin_) {
    // Client was never correctly initialized.
    dprintf(("SrpcClient::~SrpcClient: no plugin\n"));
    return;
  }
  dprintf(("SrpcClient::~SrpcClient: testing command channel\n"));
  if (is_command_channel_) {
    // Send the shutdown message to the command channel of the server.
#ifdef SRPC_PLUGIN_DEBUG
    NaClSrpcError rv =
#endif
    NaClSrpcInvokeByName(&srpc_channel_, "__shutdown");
    dprintf(("SrpcClient::~SrpcClient(%p) shutdown returned %d\n", this, rv));
  }
  dprintf(("SrpcClient::~SrpcClient: destroying the channel\n"));
  // And delete the connection.
  NaClSrpcDtor(&srpc_channel_);
  dprintf(("SrpcClient::~SrpcClient: done\n"));
}

void SrpcClient::GetMethods() {
  dprintf(("SrpcClient::GetMethods(%p)\n", this));
  // Get the exported methods.
  const struct NaClSrpcDesc *descrs = srpc_channel_.rpc_descr;
  uint32_t count = srpc_channel_.rpc_count;
  // Intern the methods into a mapping from NPIdentifiers to MethodInfo.
  for (uint32_t i = 0; i < count; ++i) {
    const char* name = descrs[i].rpc_name;
    const NPIdentifier ident = NPN_GetStringIdentifier(name);
    MethodInfo* method_info =
      new(std::nothrow) MethodInfo(name,
                                   i,
                                   descrs[i].in_args,
                                   descrs[i].out_args);
    if (NULL == method_info) {
      return;
    }
    // Install in the map only if successfully read.
    methods_[ident] = method_info;
  }
}

NPObject* SrpcClient::GetSignatureObject() {
  dprintf(("SrpcClient::GetSignatureObject(%p)\n", this));
  // Get the exported methods.
  const struct NaClSrpcDesc *descrs = srpc_channel_.rpc_descr;
  uint32_t count = srpc_channel_.rpc_count;
  RetArray* ret_array = RetArray::New(plugin_, count);
  if (NULL == ret_array && 0 != count) {
    return NULL;
  }
  for (uint32_t i = 0; i < count; ++i) {
    const char* name = descrs[i].rpc_name;
    const NPIdentifier ident = NPN_GetStringIdentifier(name);
    NPObject* sig = methods_[ident]->Signature(plugin_);
    // TODO: I don't think this is needed.
    NPN_RetainObject(sig);
    ScalarToNPVariant(sig, &ret_array->values_[i]);
  }
  return ret_array;
}

bool SrpcClient::HasMethod(NPIdentifier ident) {
  dprintf(("SrpcClient::HasMethod(%p, %s)\n", this, IdentToString(ident)));
  if (Plugin::kNullNpapiMethodIdent == ident) {
    return true;
  }
  return NULL != methods_[ident];
}

bool SrpcClient::Invoke(NPIdentifier ident,
                        const NPVariant* args,
                        int argc,
                        NPVariant* result) {
  // It would be better if we could set the exception on each detailed failure
  // case.  However, there are calls to Invoke from within the plugin itself,
  // and these could leave residual exceptions pending.  This seems to be
  // happening specifically with hard_shutdowns.
  dprintf(("SrpcClient::Invoke(%p, %s, %d)\n",
           this, IdentToString(ident), argc));

  VOID_TO_NPVARIANT(*result);
  // Support to measure the cost of invoking an NPAPI method.
  if (Plugin::kNullNpapiMethodIdent == ident) {
    dprintf(("SrpcClient::Invoke: null method\n"));
    ScalarToNPVariant(0, result);
    return true;
  }

  // Ensure Invoke was called with an identifier that had a binding.
  if (NULL == methods_[ident]) {
    dprintf(("SrpcClient::Invoke: ident not in methods_\n"));
    return false;
  }

  // Catch signals from SRPC/IMC/etc.
  ScopedCatchSignals sigcatcher(
      (ScopedCatchSignals::SigHandlerType) SignalHandler);

  SrpcParams params;
  dprintf(("SrpcClient::Invoke: params '%s' '%s'\n",
           methods_[ident]->ins_, methods_[ident]->outs_));
  if (!params.BuildVectors(methods_[ident]->ins_, methods_[ident]->outs_)) {
    dprintf(("SrpcClient::Invoke: params not built correctly\n"));
    return false;
  }

  // The arguments passed to the method contain, in order:
  // 1) the actual values to be passed to the RPC
  // 2) the length of any array-typed return value
  // Hence the number of parameters passed to the invocation must equal
  // the sum of the inputs and the number of array-typed return values.
  const uint32_t kSignatureLength = SignatureLength(params.ins_, params.outs_);
  if (0 > argc || kSignatureLength != static_cast<uint32_t>(argc)) {
    dprintf(("SrpcClient::Invoke: signature length incorrect %u %d\n",
             kSignatureLength, argc));
    return false;
  }

  if (!MarshallInputs(plugin_,
                      args,
                      params.ins_,
                      params.outs_)) {
    dprintf(("SrpcClient::Invoke: MarshallInputs failed\n"));
    return false;
  }
  dprintf(("SrpcClient::Invoke: sending the rpc\n"));
  // Call the method
  NaClSrpcError err = NaClSrpcInvokeV(&srpc_channel_,
                                      methods_[ident]->number_,
                                      params.ins_,
                                      params.outs_);
  dprintf(("SrpcClient::Invoke: got response %d\n", err));
  if (NACL_SRPC_RESULT_OK != err) {
    dprintf(("SrpcClient::Invoke: returned err %s\n",
             NaClSrpcErrorString(err)));
    return false;
  }
  if (!MarshallOutputs(plugin_,
                       const_cast<const NaClSrpcArg**>(params.outs_),
                       result)) {
    dprintf(("SrpcClient::Invoke: MarshallOutputs failed\n"));
    return false;
  }

  dprintf(("SrpcClient::Invoke: done\n"));

  return true;
}

void SrpcClient::SignalHandler(int value) {
  dprintf(("SrpcClient::SignalHandler()\n"));
  PLUGIN_LONGJMP(srpc_env, value);
}

PLUGIN_JMPBUF SrpcClient::srpc_env;

//
// Utilities to support marshalling to/from NPAPI.
//
static bool MarshallInputs(Plugin* plugin,
                           const NPVariant* args,
                           NaClSrpcArg* inputs[],
                           NaClSrpcArg* outputs[]) {
  uint32_t i;
  uint32_t inputs_length;

  // Iterate over argument array.
  // BUG: all error handling may result in leaked array memory.
  for (i = 0; i < NACL_SRPC_MAX_ARGS; ++i) {
    // A NULL element in the pointer array indicates the end of the arguments.
    if (NULL == inputs[i]) {
      break;
    }
    switch (inputs[i]->tag) {
    case NACL_SRPC_ARG_TYPE_BOOL:
      {
        int32_t bool_val;
        if (!NPVariantToScalar(&args[i], &bool_val)) {
          dprintf(("Bool was not of correct type\n"));
          return false;
        }
        inputs[i]->u.bval = (bool_val != 0);
      }
      break;
    case NACL_SRPC_ARG_TYPE_DOUBLE:
      if (!NPVariantToScalar(&args[i], &inputs[i]->u.dval)) {
        dprintf(("Double was not of correct type\n"));
        return false;
      }
      break;
    case NACL_SRPC_ARG_TYPE_HANDLE:
      {
        NPObject* handle_npobj;
        if (!NPVariantToScalar(&args[i], &handle_npobj)) {
          dprintf(("Handle was not an NPObject (or derived type)\n"));
          return false;
        }
        UnknownHandle* handle = reinterpret_cast<UnknownHandle*>(handle_npobj);
        if (!UnknownHandle::is_valid(handle)) {
          dprintf(("Handle was not an UnknownHandle (or derived type)\n"));
          return false;
        }
        // Make result available to the caller.
        inputs[i]->u.hval = handle->desc();
      }
      break;
    case NACL_SRPC_ARG_TYPE_INT:
      if (!NPVariantToScalar(&args[i], &inputs[i]->u.ival)) {
        dprintf(("Int was not of correct type\n"));
        return false;
      }
      break;
    case NACL_SRPC_ARG_TYPE_STRING:
      if (!NPVariantToScalar(&args[i], &inputs[i]->u.sval)) {
        dprintf(("String was not of correct type\n"));
        return false;
      }
      break;

    case NACL_SRPC_ARG_TYPE_CHAR_ARRAY:
      if (!NPVariantToArray(&args[i],
                            plugin,
                            &inputs[i]->u.caval.count,
                            &inputs[i]->u.caval.carr)) {
        dprintf(("Char array was not of correct type\n"));
        return false;
      }
      break;
    case NACL_SRPC_ARG_TYPE_DOUBLE_ARRAY:
      if (!NPVariantToArray(&args[i],
                            plugin,
                            &inputs[i]->u.daval.count,
                            &inputs[i]->u.daval.darr)) {
        dprintf(("Double array was not of correct type\n"));
        return false;
      }
      break;
    case NACL_SRPC_ARG_TYPE_INT_ARRAY:
      if (!NPVariantToArray(&args[i],
                            plugin,
                            &inputs[i]->u.iaval.count,
                            &inputs[i]->u.iaval.iarr)) {
        dprintf(("Int array was not of correct type\n"));
        return false;
      }
      break;
    // The two cases below are added for compatibility, they are used
    // only in the new plugin code.
    case NACL_SRPC_ARG_TYPE_OBJECT:
    case NACL_SRPC_ARG_TYPE_VARIANT_ARRAY:
    default:
      return false;
    }
  }

  inputs_length = i;

  // Set the length values for the array-typed returns.
  // BUG: all error handling may result in leaked array memory.
  for (i = 0; i < NACL_SRPC_MAX_ARGS; ++i) {
    // A NULL element in the pointer array indicates the end of the arguments.
    if (NULL == outputs[i]) {
      break;
    }
    switch (outputs[i]->tag) {
    case NACL_SRPC_ARG_TYPE_CHAR_ARRAY:
      if (!NPVariantToAllocatedArray(&args[i + inputs_length],
                                     &outputs[i]->u.caval.count,
                                     &outputs[i]->u.caval.carr)) {
        return false;
      }
      break;
    case NACL_SRPC_ARG_TYPE_DOUBLE_ARRAY:
      if (!NPVariantToAllocatedArray(&args[i + inputs_length],
                                     &outputs[i]->u.daval.count,
                                     &outputs[i]->u.daval.darr)) {
        return false;
      }
      break;
    case NACL_SRPC_ARG_TYPE_INT_ARRAY:
      if (!NPVariantToAllocatedArray(&args[i + inputs_length],
                                     &outputs[i]->u.iaval.count,
                                     &outputs[i]->u.iaval.iarr)) {
        return false;
      }
      break;
    case NACL_SRPC_ARG_TYPE_BOOL:
    case NACL_SRPC_ARG_TYPE_DOUBLE:
    case NACL_SRPC_ARG_TYPE_HANDLE:
    case NACL_SRPC_ARG_TYPE_INT:
    case NACL_SRPC_ARG_TYPE_STRING:
      break;
    // The two cases below are added for compatibility, they are used
    // only in the new plugin code.
    case NACL_SRPC_ARG_TYPE_OBJECT:
    case NACL_SRPC_ARG_TYPE_VARIANT_ARRAY:

    default:
      return false;
    }
  }
  return true;
}

static bool MarshallOutputs(Plugin* plugin,
                            const NaClSrpcArg** outs,
                            NPVariant* result) {
  // Find the length of the return vector.
  uint32_t length = ArgsLength(outs);
  RetArray* retarray = NULL;

  VOID_TO_NPVARIANT(*result);
  // If the return vector only has one element, return that as a scalar.
  // Otherwise, return a RetArray with the results.
  NPVariant* retvalue = NULL;
  if ((0 > length) || (NACL_SRPC_MAX_ARGS <= length)) {
    // Something went badly wrong.  Recover as gracefully as possible.
    return false;
  } else if (0 == length) {
    return true;
  } else if (1 == length) {
    retvalue = result;
  } else {
    retarray = RetArray::New(plugin, length);
    if (NULL == retarray) {
      return false;
    }
    retvalue = retarray->values_;
  }

  // BUG: all error handling may result in leaked array memory.
  for (uint32_t i = 0; i < length; ++i, ++retvalue) {
    switch (outs[i]->tag) {
     case NACL_SRPC_ARG_TYPE_BOOL:
      {
        int32_t bool_val = (outs[i]->u.bval ? 1 : 0);
        if (!ScalarToNPVariant(bool_val, retvalue)) {
          return false;
        }
      }
      break;
     case NACL_SRPC_ARG_TYPE_CHAR_ARRAY:
      if (!ArrayToNPVariant(outs[i]->u.caval.count,
                            outs[i]->u.caval.carr,
                            plugin,
                            retvalue)) {
        return false;
      }
      break;
     case NACL_SRPC_ARG_TYPE_DOUBLE:
      if (!ScalarToNPVariant(outs[i]->u.dval, retvalue)) {
        return false;
      }
      break;
     case NACL_SRPC_ARG_TYPE_DOUBLE_ARRAY:
      if (!ArrayToNPVariant(outs[i]->u.daval.count,
                            outs[i]->u.daval.darr,
                            plugin,
                            retvalue)) {
        return false;
      }
      break;
     case NACL_SRPC_ARG_TYPE_HANDLE:
      {
        struct NaClDesc* desc = outs[i]->u.hval;
        NPObject* handle_obj;
        if (NACL_DESC_CONN_CAP == desc->vtbl->typeTag) {
          handle_obj = SocketAddress::New(plugin, desc);
        } else {
          handle_obj = UnknownHandle::New(plugin, desc);
        }
        if (NULL == handle_obj) {
          return false;
        }
        ScalarToNPVariant(handle_obj, retvalue);
      }
      break;
     case NACL_SRPC_ARG_TYPE_INT:
      if (!ScalarToNPVariant(outs[i]->u.ival, retvalue)) {
        return false;
      }
      break;
     case NACL_SRPC_ARG_TYPE_INT_ARRAY:
      if (!ArrayToNPVariant(outs[i]->u.iaval.count,
                            outs[i]->u.iaval.iarr,
                            plugin,
                            retvalue)) {
        return false;
      }
      break;
     case NACL_SRPC_ARG_TYPE_STRING:
      if (!ScalarToNPVariant(outs[i]->u.sval, retvalue)) {
        return false;
      }
      break;
     // The two cases below are added for compatibility, they are used
     // only in the new plugin code.
     case NACL_SRPC_ARG_TYPE_OBJECT:
     case NACL_SRPC_ARG_TYPE_VARIANT_ARRAY:
     default:
      break;
    }
  }
  if (1 < length) {
    // Attach return value array to result only if all return values
    // were marshalled correctly.
    ScalarToNPVariant(static_cast<NPObject*>(retarray), result);
  }
  return true;
}

// Computes the number of parameters that should be passed to a method.  The
// number is the sum of the input argument count and the number of array-typed
// outputs.
static uint32_t SignatureLength(NaClSrpcArg* in_index[],
                                NaClSrpcArg* out_index[]) {
  uint32_t ins;
  uint32_t array_outs = 0;

  for (ins = 0; ins < NACL_SRPC_MAX_ARGS; ++ins) {
    // A NULL element in the pointer array indicates the end of the arguments.
    if (NULL == in_index[ins]) {
      break;
    }
  }
  for (uint32_t i = 0; i < NACL_SRPC_MAX_ARGS; ++i) {
    // A NULL element in the pointer array indicates the end of the arguments.
    if (NULL == out_index[i]) {
      break;
    }
    switch (out_index[i]->tag) {
     case NACL_SRPC_ARG_TYPE_CHAR_ARRAY:
     case NACL_SRPC_ARG_TYPE_DOUBLE_ARRAY:
     case NACL_SRPC_ARG_TYPE_INT_ARRAY:
       ++array_outs;
       break;
     case NACL_SRPC_ARG_TYPE_STRING:
     case NACL_SRPC_ARG_TYPE_BOOL:
     case NACL_SRPC_ARG_TYPE_DOUBLE:
     case NACL_SRPC_ARG_TYPE_INT:
     // The two cases below are added for compatibility, they are used
     // only in the new plugin code.
     case NACL_SRPC_ARG_TYPE_OBJECT:
     case NACL_SRPC_ARG_TYPE_VARIANT_ARRAY:
     default:
      break;
    }
  }
  return ins + array_outs;
}

static uint32_t ArgsLength(const NaClSrpcArg* index[]) {
  uint32_t i;

  for (i = 0; NULL != index[i]; ++i);
  return i;
}

}  // namespace nacl_srpc

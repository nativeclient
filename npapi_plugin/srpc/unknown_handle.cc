/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <new>
#include <set>
#include <signal.h>
#include <string.h>
#include "native_client/npapi_plugin/srpc/npapi_native.h"
#include "native_client/npapi_plugin/srpc/plugin.h"
#include "native_client/npapi_plugin/srpc/shared_memory.h"
#include "native_client/npapi_plugin/srpc/unknown_handle.h"
#include "native_client/npapi_plugin/srpc/utility.h"

namespace nacl_srpc {

int UnknownHandle::number_alive = 0;

// UnknownHandle implements the map method for shared memory
// TODO: should socket_address should be merged into this class?
bool UnknownHandle::HasMethod(NPObject *obj, NPIdentifier name) {
  dprintf(("UnknownHandle::HasMethod(%p, %s)\n", obj, IdentToString(name)));

  return (Plugin::kMapIdent == name);
}

bool UnknownHandle::Invoke(NPObject *obj,
                           NPIdentifier name,
                           const NPVariant *args,
                           uint32_t arg_count,
                           NPVariant *result) {
  UnknownHandle* unknown_handle = reinterpret_cast<UnknownHandle*>(obj);

  dprintf(("UnknownHandle::Invoke(%p, %s, %d)\n",
           obj, IdentToString(name), arg_count));

  VOID_TO_NPVARIANT(*result);
  if (Plugin::kMapIdent == name) {
    if (0 != arg_count) {
      NPN_SetException(obj, "Wrong argument count to map");
      return false;
    }
    SharedMemory* shared_memory =
        SharedMemory::New(unknown_handle->plugin_, unknown_handle->desc_);
    if (NULL == shared_memory) {
      NPN_SetException(obj, "Out of memory in map");
      return false;
    }
    dprintf(("UnknownHandle::Invoke: new returned %p\n", shared_memory));
    ScalarToNPVariant(static_cast<NPObject*>(shared_memory), result);
    return true;
  }

  NPN_SetException(obj, "Unknown method");
  return false;
}

// UnknownHandle have no properties.
bool UnknownHandle::HasProperty(NPObject *obj, NPIdentifier name) {
  dprintf(("UnknownHandle::HasProperty(%p, %s)\n", obj, IdentToString(name)));

  return false;
}

bool UnknownHandle::GetProperty(NPObject *obj,
                                NPIdentifier name,
                                NPVariant *variant) {
  dprintf(("UnknownHandle::GetProperty(%p, %s)\n", obj, IdentToString(name)));

  VOID_TO_NPVARIANT(*variant);
  return (Plugin::kMapIdent == name);
}

bool UnknownHandle::SetProperty(NPObject *obj,
                                NPIdentifier name,
                                const NPVariant *variant) {
  dprintf(("UnknownHandle::SetProperty(%p, %s, %p)\n",
           obj, IdentToString(name), variant));

  return false;
}

// UnknownHandles are created with a descriptor whose vtbl->typeTag is
// not represented by one of UnknownHandle's subclasses.
UnknownHandle* UnknownHandle::New(Plugin* plugin, struct NaClDesc* desc) {
  static NPClass unknownHandleClass = {
    NP_CLASS_STRUCT_VERSION,
    Allocate,
    Deallocate,
    Invalidate,
    HasMethod,
    Invoke,
    0,
    HasProperty,
    GetProperty,
    SetProperty,
  };

  dprintf(("UnknownHandle::New(%p, %p)\n", plugin, desc));

  UnknownHandle* unknown_handle =
    reinterpret_cast<UnknownHandle*>(NPN_CreateObject(plugin->npp(),
                                                      &unknownHandleClass));
  if (NULL == unknown_handle) {
    return NULL;
  }
  if (NULL == desc) {
    NPN_ReleaseObject(unknown_handle);
    return NULL;
  }
  // Remember the plugin instance to which this handle belongs.
  unknown_handle->plugin_ = plugin;
  // Add a reference to the NaClDesc that this UnknownHandle encapsulates.
  NaClDescRef(desc);
  unknown_handle->desc_ = desc;

  return unknown_handle;
}

NPObject *UnknownHandle::Allocate(NPP npp, NPClass *theClass) {
  dprintf(("UnknownHandle::Allocate(%d)\n", ++number_alive));

  return new(std::nothrow) UnknownHandle(npp);
}

void UnknownHandle::Deallocate(NPObject *obj) {
  UnknownHandle* unknown_handle = reinterpret_cast<UnknownHandle*>(obj);

  dprintf(("UnknownHandle::Deallocate(%p, %d)\n", obj, --number_alive));

  // Release the contained descriptor.
  if (unknown_handle->desc_) {
    NaClDescUnref(unknown_handle->desc_);
  }
  // And free the memory.
  delete unknown_handle;
}

void UnknownHandle::Invalidate(NPObject *obj) {
  UnknownHandle* unknown_handle = reinterpret_cast<UnknownHandle*>(obj);

  dprintf(("UnknownHandle::Invalidate(%p)\n", unknown_handle));

  // After invalidation, the browser does not respect reference counting,
  // so we shut down here what we can and prevent attempts to shut down
  // other linked structures in Deallocate.

  NaClDescUnref(unknown_handle->desc_);
  unknown_handle->desc_ = NULL;
  unknown_handle->plugin_ = NULL;
}

std::set<const UnknownHandle*>* UnknownHandle::valid_handles = NULL;

UnknownHandle::UnknownHandle(NPP npp) : npp_(npp), desc_(NULL) {
  dprintf(("UnknownHandle::UnknownHandle(%p)\n", this));
  if (NULL == valid_handles) {
    valid_handles = new(std::nothrow) std::set<const UnknownHandle*>;
  }
  if (NULL == valid_handles) {
    // There's no clean way to handle errors in a constructor.
    // TODO: move to invoking an insert method in factories.
    abort();
  }
  valid_handles->insert(this);
  dprintf(("UnknownHandle::UnknownHandle(%p, count %d)\n", this,
           valid_handles->count(this)));
}

UnknownHandle::~UnknownHandle() {
  dprintf(("UnknownHandle::~UnknownHandle(%p)\n", this));
  valid_handles->erase(this);
  dprintf(("UnknownHandle::UnknownHandle(%p, count %d)\n", this,
           valid_handles->count(this)));
}

}  // namespace nacl_srpc

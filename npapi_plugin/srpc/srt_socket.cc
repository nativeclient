/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "native_client/npapi_plugin/srpc/srt_socket.h"

#include "native_client/npapi_plugin/srpc/npapi_native.h"
#include "native_client/npapi_plugin/srpc/plugin.h"
#include "native_client/npapi_plugin/srpc/service_runtime_interface.h"
#include "native_client/npapi_plugin/srpc/srpc_client.h"
#include "native_client/npapi_plugin/srpc/utility.h"

namespace nacl_srpc {

NPIdentifier SrtSocket::kHardShutdownIdent;
NPIdentifier SrtSocket::kSetOriginIdent;
NPIdentifier SrtSocket::kStartModule;

// NB: InitializeIdentifiers is not thread-safe.
void SrtSocket::InitializeIdentifiers() {
  static bool initialized = false;

  if (!initialized) {  // branch likely
    kHardShutdownIdent = NPN_GetStringIdentifier("hard_shutdown");
    kStartModule = NPN_GetStringIdentifier("start_module");
    kSetOriginIdent = NPN_GetStringIdentifier("set_origin");
    initialized = true;
  }
}

SrtSocket::SrtSocket(ConnectedSocket *s)
    : connected_socket_(s) {
  NPN_RetainObject(connected_socket_);
  InitializeIdentifiers();  // inlineable tail call.
}

SrtSocket::~SrtSocket() {
  NPN_ReleaseObject(connected_socket_);
}

bool SrtSocket::HardShutdown() {
  if (!(connected_socket()->srpc_client()->HasMethod(kHardShutdownIdent))) {
    dprintf(("No hard_shutdown method was found\n"));
    return false;
  }
  NPVariant result;
  bool rpc_result = (connected_socket()->
                     srpc_client()->Invoke(kHardShutdownIdent,
                                           NULL,
                                           0,
                                           &result));
  if (rpc_result) {
    NPN_ReleaseVariantValue(&result);
  }
  return rpc_result;
}

bool SrtSocket::SetOrigin(std::string origin) {
  if (!(connected_socket()->srpc_client()->HasMethod(kSetOriginIdent))) {
    dprintf(("No set_origin method was found\n"));
    return false;
  }
  NPVariant npv_origin;
  NPVariant result;
  ScalarToNPVariant(origin.c_str(), &npv_origin);
  bool rpc_result = (connected_socket()->
                     srpc_client()->Invoke(kSetOriginIdent,
                                           &npv_origin,
                                           1,
                                           &result));
  NPN_ReleaseVariantValue(&npv_origin);
  if (rpc_result) {
    NPN_ReleaseVariantValue(&result);
  }
  return rpc_result;
}

// make the start_module rpc
bool SrtSocket::StartModule(int *load_status) {
  if (!(connected_socket()->srpc_client()->HasMethod(kStartModule))) {
    dprintf(("No start_module method was found\n"));
    return false;
  }
  NPVariant result;
  bool rpc_result = connected_socket()->srpc_client()->Invoke(kStartModule,
                                                              NULL,
                                                              0,
                                                              &result);
  if (rpc_result) {
    int32_t status;
    if (NPVariantToScalar(&result, &status)) {
      dprintf(("StartModule: start_module RPC returned status code %d\n",
               status));
      if (NULL != load_status) {
        *load_status = status;
      }
    }
    NPN_ReleaseVariantValue(&result);
  }
  return rpc_result;
}

}  // namespace nacl_srpc

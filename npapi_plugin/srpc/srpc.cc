/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



#include <new>
#include <stdio.h>
#include <string.h>
#include <string>
#include "native_client/include/portability.h"
#include "native_client/npapi_plugin/origin.h"
#include "native_client/npapi_plugin/srpc/closure.h"
#include "native_client/npapi_plugin/srpc/npapi_native.h"
#include "native_client/npapi_plugin/srpc/plugin.h"
#include "native_client/npapi_plugin/srpc/shared_memory.h"
#include "native_client/npapi_plugin/npinstance.h"
#include "native_client/npapi_plugin/srpc/srpc.h"
#include "native_client/npapi_plugin/srpc/utility.h"
#include "native_client/service_runtime/nacl_config.h"
#include "native_client/service_runtime/sel_util.h"
#include "native_client/npapi_plugin/srpc/video.h"

namespace nacl {

static int32_t stringToInt32(char *src) {
  const int MAX_CHARACTERS = 32;
  char buf[MAX_CHARACTERS];
  char *pEnd;
  strncpy(buf, src, MAX_CHARACTERS - 1);
  buf[MAX_CHARACTERS - 1] = '\0';
  return strtol(buf, &pEnd, 10);
}

SRPC_Plugin::SRPC_Plugin(NPP npp, int argc, char* argn[], char* argv[]) :
    npp_(npp),
    plugin_(NULL) {
  dprintf(("SRPC_Plugin::SRPC_Plugin(%p, %d)\n", this, argc));
  // SRPC_Plugin initially gets exclusive ownership of plugin_.
  plugin_ = nacl_srpc::Plugin::New(npp_, this);
  if (NULL == plugin_) {
    return;
  }
  // Set up the height and width attributes if passed (for Opera)
  for (int i = 0; i < argc; ++i) {
    if (!strncmp(argn[i], "height", 7)) {
      NPVariant variant;
      int32_t x = stringToInt32(argv[i]);
      nacl_srpc::ScalarToNPVariant(x, &variant);
      nacl_srpc::Plugin::SetProperty(plugin_,
                                     nacl_srpc::Plugin::kHeightIdent,
                                     &variant);
    } else if (!strncmp(argn[i], "width", 6)) {
      NPVariant variant;
      int32_t x = stringToInt32(argv[i]);
      nacl_srpc::ScalarToNPVariant(x, &variant);
      nacl_srpc::Plugin::SetProperty(plugin_,
                                     nacl_srpc::Plugin::kWidthIdent,
                                     &variant);
    } else if (!strncmp(argn[i], "update", 7)) {
      NPVariant variant;
      int32_t x = stringToInt32(argv[i]);
      nacl_srpc::ScalarToNPVariant(x, &variant);
      nacl_srpc::Plugin::SetProperty(plugin_,
                                     nacl_srpc::Plugin::kVideoUpdateModeIdent,
                                     &variant);
    }
  }
  video_ = new(std::nothrow) VideoMap(this);
  if (NULL == video_) {
    // TODO: Move this setup to an Init method that can fail.
    NPN_ReleaseObject(plugin_);
    plugin_ = NULL;
  }
}

SRPC_Plugin::~SRPC_Plugin() {
  nacl_srpc::Plugin *plugin = plugin_;
  {
    VideoScopedGlobalLock video_lock;
    dprintf(("SRPC_Plugin::~SRPC_Plugin(%p)\n", this));
    plugin_ = NULL;
    dprintf(("SRPC_Plugin::~SPRC_Plugin deleting video_\n"));
    if (NULL != video_) {
      delete video_;
      video_ = NULL;
    }
  }
  // don't hold scoped global lock while calling NPN_ReleaseObject
  if (NULL != plugin) {
    // Destroying SRPC_Plugin releases ownership of the plugin.
    NPN_ReleaseObject(plugin);
  }
}

NPError SRPC_Plugin::Destroy(NPSavedData **save) {
  dprintf(("SRPC_Plugin::Destroy(%p, %p)\n", this, save));
  delete this;
  return NPERR_NO_ERROR;
}

// SetWindow is called by the browser as part of the NPAPI interface for
// setting up a plugin that has the ability to draw into a window.  It is
// passed a semi-custom window descriptor (some is platform-neutral, some not)
// as documented in the NPAPI documentation.
NPError SRPC_Plugin::SetWindow(NPWindow *window) {
  NPError ret;
  dprintf(("SRPC_Plugin::SetWindow(%p, %p)\n", this, window));
  if (video_) {
    ret = video_->SetWindow(window);
  } else {
    ret = NPERR_GENERIC_ERROR;
  }
  return ret;
}

NPError SRPC_Plugin::GetValue(NPPVariable variable, void *value) {
  const char** stringp = static_cast<const char**>(value);

  dprintf(("SRPC_Plugin::GetValue(%p, %d)\n", this, variable));

  switch (variable) {
   case NPPVpluginNameString:
    *stringp = "NativeClient Simple RPC + multimedia a/v interface";
    return NPERR_NO_ERROR;
   case NPPVpluginDescriptionString:
    *stringp = "NativeClient Simple RPC interaction w/ multimedia.";
    return NPERR_NO_ERROR;
   case NPPVpluginScriptableNPObject:
    *(static_cast<NPObject**>(value)) = GetScriptableInstance();
    return NPERR_NO_ERROR;
   default:
    return NPERR_INVALID_PARAM;
  }
}

int16_t SRPC_Plugin::HandleEvent(void *param) {
  int16_t ret;
  dprintf(("SRPC_Plugin::HandleEvent(%p, %p)\n", this, param));
  if (video_) {
    ret = video_->HandleEvent(param);
  } else {
    ret = 0;
  }
  return ret;
}

NPObject* SRPC_Plugin::GetScriptableInstance() {
  dprintf(("SRPC_Plugin::GetScriptableInstance(%p)\n", this));

  // Anyone requesting access to the scriptable instance is given shared
  // ownership of plugin_.
  return NPN_RetainObject(plugin_);
}

NPError SRPC_Plugin::NewStream(NPMIMEType type,
                               NPStream *stream,
                               NPBool seekable,
                               uint16_t *stype) {
  dprintf(("SRPC_Plugin::NewStream(%p, %s, %p, %d)\n",
           this, type, stream, seekable));

  *stype = NP_ASFILEONLY;
  return NPERR_NO_ERROR;
}

void SRPC_Plugin::StreamAsFile(NPStream *stream,
                               const char *fname) {
  dprintf(("SRPC_Plugin::StreamAsFile(%p, %p, %s)\n", this, stream, fname));
  nacl_srpc::Closure *closure
      = reinterpret_cast<nacl_srpc::Closure *>(stream->notifyData);

  if (NULL != closure) {
    closure->Run(stream, fname);
  } else {
    // default, src=... statically obtained
    dprintf(("default run\n"));
    plugin_->set_nacl_module_origin(nacl::UrlToOrigin(stream->url));
    plugin_->set_local_url(fname);
    plugin_->Load();
  }
}

NPError SRPC_Plugin::DestroyStream(NPStream *stream,
                                   NPReason reason) {
  dprintf(("SRPC_Plugin::DestroyStream(%p, %p, %d)\n", this, stream, reason));

  return NPERR_NO_ERROR;
}

void SRPC_Plugin::URLNotify(const char *url,
                            NPReason reason,
                            void *notifyData) {
  dprintf(("SRPC_Plugin::URLNotify(%p, %s, %d, %p)\n",
           this, url, reason, notifyData));

  nacl_srpc::Closure *closure
      = reinterpret_cast<nacl_srpc::Closure *>(notifyData);

  if (NPRES_DONE == reason) {
    dprintf(("URLNotify: '%s', rsn NPRES_DONE (%d)\n", url, reason));
  } else {
    dprintf(("Unable to open: '%s' rsn %d\n", url, reason));
    if (NULL != closure) {
      closure->Run(NULL, NULL);
    }
  }
  delete closure;  // NB: delete NULL is okay
}

}  // namespace nacl

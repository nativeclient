/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "native_client/npapi_plugin/srpc/multimedia_socket.h"

#include "native_client/npapi_plugin/srpc/plugin.h"
#include "native_client/npapi_plugin/srpc/service_runtime_interface.h"
#include "native_client/npapi_plugin/srpc/shared_memory.h"
#include "native_client/npapi_plugin/srpc/srpc_client.h"
#include "native_client/npapi_plugin/srpc/utility.h"
#include "native_client/npapi_plugin/srpc/video.h"

#include "native_client/service_runtime/nacl_desc_imc.h"
#include "native_client/service_runtime/nacl_sync.h"
#include "native_client/service_runtime/nacl_sync_checked.h"
#include "native_client/service_runtime/nacl_threads.h"

namespace nacl_srpc {

NPIdentifier MultimediaSocket::kNaClMultimediaBridgeIdent;

// NB: InitializeIdentifiers is not thread-safe.
void MultimediaSocket::InitializeIdentifiers() {
  static bool identifiers_initialized = false;

  if (!identifiers_initialized) {
    kNaClMultimediaBridgeIdent =
        NPN_GetStringIdentifier("nacl_multimedia_bridge");
    identifiers_initialized = true;
  }
}

MultimediaSocket::MultimediaSocket(ConnectedSocket *s)
    : connected_socket_(s) {
  NPN_RetainObject(connected_socket_);
  NaClMutexCtor(&mu_);
  NaClCondVarCtor(&cv_);
  upcall_thread_state_ = UPCALL_THREAD_NOT_STARTED;
  InitializeIdentifiers();  // inlineable; tail call.
}

MultimediaSocket::~MultimediaSocket() {
  enum UpcallThreadState ts;

  NaClXMutexLock(&mu_);
  if (UPCALL_THREAD_NOT_STARTED != (ts = upcall_thread_state_)) {
    while (upcall_thread_state_ != UPCALL_THREAD_EXITED) {
      dprintf(("MultimediaSocket::~MultimediaSocket:"
               " waiting for upcall thread to exit\n"));
      NaClXCondVarWait(&cv_, &mu_);
    }
  }
  NaClXMutexUnlock(&mu_);
  NPN_ReleaseObject(connected_socket_);
  if (ts != UPCALL_THREAD_NOT_STARTED) {
    NaClThreadDtor(&upcall_thread_);
  }
  NaClCondVarDtor(&cv_);
  NaClMutexDtor(&mu_);
}

void MultimediaSocket::UpcallThreadExiting() {
  NaClXMutexLock(&mu_);
  upcall_thread_state_ = UPCALL_THREAD_EXITED;
  NaClXCondVarBroadcast(&cv_);
  NaClXMutexUnlock(&mu_);
}

static int handleUpcall(NaClSrpcChannel* channel,
                        NaClSrpcArg** ins,
                        NaClSrpcArg** outs) {
  int ret;
  ret = NACL_SRPC_RESULT_BREAK;
  if (channel) {
    nacl::VideoScopedGlobalLock video_lock;
    nacl::VideoCallbackData *video_cb_data;
    nacl::SRPC_Plugin *srpc_plugin;
    dprintf(("Upcall: channel %p\n", channel));
    dprintf(("Upcall: server_instance_data %p\n",
        channel->server_instance_data));
    video_cb_data = reinterpret_cast<nacl::VideoCallbackData*>
        (channel->server_instance_data);
    srpc_plugin = video_cb_data->srpc_plugin;
    if (NULL != srpc_plugin) {
      nacl::VideoMap *video = srpc_plugin->video();
      if (video) {
        video->RequestRedraw();
      }
      ret = NACL_SRPC_RESULT_OK;
    } else {
      dprintf(("Upcall: plugin was NULL\n"));
    }
  }
  if (NACL_SRPC_RESULT_OK == ret) {
    dprintf(("Upcall: success\n"));
  } else {
    dprintf(("Upcall: failure\n"));
  }
  return ret;
}

static void WINAPI UpcallThread(void *arg) {
  nacl::VideoCallbackData *cbdata;
  NaClSrpcHandlerDesc handlers[] = {
    { "upcall::", handleUpcall },
    { NULL, NULL }
  };

  cbdata = reinterpret_cast<nacl::VideoCallbackData*>(arg);
  dprintf(("MultimediaSocket::UpcallThread(%p)\n", arg));
  dprintf(("MultimediaSocket::cbdata->srpc_plugin %p\n", cbdata->srpc_plugin));
  // Run the SRPC server.
  NaClSrpcServerLoop(cbdata->handle, handlers, cbdata);
  // release the cbdata
  cbdata->msp->UpcallThreadExiting();
  nacl::VideoGlobalLock();
  nacl::VideoMap::ReleaseCallbackData(cbdata);
  nacl::VideoGlobalUnlock();
  dprintf(("MultimediaSocket::UpcallThread: End\n"));
}

// Support for initializing the NativeClient multimedia system.
bool MultimediaSocket::InitializeModuleMultimedia(Plugin *plugin) {
  dprintf(("MultimediaSocket::InitializeModuleMultimedia(%p)\n", this));

  nacl::SRPC_Plugin *srpc_plugin = plugin->srpc_plugin();
  nacl::VideoMap *video = srpc_plugin->video();
  SharedMemory *video_shared_memory = video->VideoSharedMemorySetup();

  // If there is no display shared memory region, don't initialize.
  // TODO: make sure this makes sense with NPAPI call order.
  if (NULL == video_shared_memory) {
    dprintf(("MultimediaSocket::InitializeModuleMultimedia:"
             "No video_shared_memory.\n"));
    // trivial success case.  NB: if NaCl module and HTML disagrees,
    // then the HTML wins.
    return true;
  }
  // Determine whether the NaCl module has reported having a method called
  // "nacl_multimedia_bridge".
  if (!(connected_socket()->
        srpc_client()->HasMethod(kNaClMultimediaBridgeIdent))) {
    dprintf(("No nacl_multimedia_bridge method was found.\n"));
    return false;
  }
  // Create a socket pair.
  NaClHandle nh[2];
  if (0 != NaClSocketPair(nh)) {
    dprintf(("NaClSocketPair failed!\n"));
    return false;
  }
  // Start a thread to handle the upcalls.
  nacl::VideoCallbackData *cbdata;
  cbdata = video->InitCallbackData(nh[0], srpc_plugin, this);
  dprintf((
      "MultimediaSocket::InitializeModuleMultimedia: launching thread\n"));
  NaClXMutexLock(&mu_);
  if (upcall_thread_state_ != UPCALL_THREAD_NOT_STARTED) {
    dprintf(("Internal error: upcall thread already running\n"));
    NaClXMutexUnlock(&mu_);
    return false;
  }
  if (!NaClThreadCtor(&upcall_thread_, UpcallThread, cbdata, 128 << 10)) {
    NaClXMutexUnlock(&mu_);
    // not constructed, so no dtor will be needed
    video->ForceDeleteCallbackData(cbdata);
    return false;
  }
  upcall_thread_state_ = UPCALL_THREAD_RUNNING;
  NaClXMutexUnlock(&mu_);
  // The arguments to nacl_multimedia_bridge are an object for the video
  // shared memory and a connected socket to send the plugin's up calls to.
  NPVariant args[2];
  NPVariant result;
  struct NaClDescImcDesc *sr_desc =
      reinterpret_cast<struct NaClDescImcDesc *>(malloc(sizeof *sr_desc));
  if (NULL == sr_desc) {
    return false;
  }
  if (!NaClDescImcDescCtor(sr_desc, nh[1])) {
    free(sr_desc);
    return false;
  }
  ConnectedSocket* sock =
      ConnectedSocket::New(connected_socket()->plugin_,
                           reinterpret_cast<struct NaClDesc*>(sr_desc),
                           false,
                           NULL,
                           false);
  if (NULL == sock) {
    free(sr_desc);
    return false;
  }
  dprintf(("CS:IMM args %p result %p\n", args, &result));
  OBJECT_TO_NPVARIANT(video_shared_memory, args[0]);
  OBJECT_TO_NPVARIANT(sock, args[1]);
  bool rpc_result = (connected_socket()->
                     srpc_client()->Invoke(kNaClMultimediaBridgeIdent,
                                           args,
                                           2,
                                           &result));
  // BUG(sehr): probably not right
  NaClDescUnref(reinterpret_cast<struct NaClDesc *>(sr_desc));
  NPN_ReleaseVariantValue(&args[1]);
  if (rpc_result) {
    NPN_ReleaseVariantValue(&result);
  }
  return rpc_result;
}

}  // namespace nacl_srpc

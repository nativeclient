/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_VIDEO_H_
#define NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_VIDEO_H_

#if NACL_OSX
#include <Carbon/Carbon.h>
#endif  // NACL_OSX
#if NACL_WINDOWS
#include <windows.h>
#include <windowsx.h>
#endif  // NACL_WINDOWS
#if NACL_LINUX && defined(MOZ_X11)
#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#endif  // NACL_LINUX && defined(MOZ_X11)

#include "native_client/include/nacl_platform.h"
#include "native_client/include/portability.h"

#include "native_client/service_runtime/include/sys/audio_video.h"
#include "native_client/tools/libav/nacl_av_priv.h"
#include "native_client/include/portability.h"
#include "native_client/intermodule_comm/nacl_htp_c.h"
#include "native_client/npapi_plugin/npinstance.h"
#include "native_client/npapi_plugin/srpc/plugin.h"
#include "native_client/npapi_plugin/srpc/shared_memory.h"
#include "native_client/npapi_plugin/srpc/multimedia_socket.h"
#include "native_client/npapi_plugin/srpc/srpc.h"
#include "native_client/npapi_plugin/srpc/utility.h"
#include "native_client/service_runtime/nacl_config.h"
#include "native_client/service_runtime/sel_util.h"

namespace nacl {

enum VideoUpdateMode {
  kVideoUpdatePluginPaint = 0,   // update via browser plugin paint
  kVideoUpdateAsynchronous = 1   // update asynchronously (faster)
};

enum {
  kClear = 0,
  kSet = 1
};

struct VideoCallbackData {
  NaClHandle handle;
  SRPC_Plugin *srpc_plugin;
  int  refcount;
  nacl_srpc::MultimediaSocket *msp;
  VideoCallbackData(NaClHandle h,
                    SRPC_Plugin *p,
                    int r,
                    nacl_srpc::MultimediaSocket *sockp)
      : handle(h),
        srpc_plugin(p),
        refcount(r),
        msp(sockp) {}
};

class VideoMap {
 public:

  void Disable() { video_enabled_ = false; }
  void Enable() { video_enabled_ = true; }
  int EventQueuePut(union NaClMultimediaEvent *event);
  int EventQueueIsFull();
  static void ForceDeleteCallbackData(VideoCallbackData *vcd);
  int GetButton();
  int GetKeyMod();
  void GetMotion(uint16_t *x, uint16_t *y);
  void GetRelativeMotion(uint16_t x, uint16_t y,
      int16_t *rel_x, int16_t *rel_y);
  int32_t GetVideoUpdateMode() { return video_update_mode_; }
  int16_t HandleEvent(void* event);
  int InitializeSharedMemory(NPWindow* window);
  VideoCallbackData* InitCallbackData(NaClHandle h, SRPC_Plugin *p,
                                      nacl_srpc::MultimediaSocket *msp);
  void Invalidate();
  bool IsEnabled() { return video_enabled_; }
  int Paint();
  void Redraw();
  void RedrawAsync(void *platform_specific);
  static VideoCallbackData* ReleaseCallbackData(VideoCallbackData *vcd);
  void RequestRedraw();
  void SetButton(int button, int state);
  void SetMotion(uint16_t x, uint16_t y, int last_valid);
  void SetKeyMod(int nsym, int state);
  NPError SetWindow(NPWindow* window);
  void set_platform_specific(void *sp) { platform_specific_ = sp; }
  void* platform_specific() { return platform_specific_; }
  HtpHandle video_handle() { return video_handle_; }
  nacl_srpc::SharedMemory* video_shared_memory()
      { return video_shared_memory_; }
  nacl_srpc::SharedMemory* VideoSharedMemorySetup();

  VideoMap(SRPC_Plugin *srpc_plugin);
  ~VideoMap();

#if NACL_WINDOWS
  WNDPROC                  original_window_procedure_;
  static LRESULT CALLBACK  WindowProcedure(HWND hwnd,
                                           UINT msg,
                                           WPARAM wparam,
                                           LPARAM lparam);
#endif  // NACL_WINDOWS
#if NACL_LINUX && defined(MOZ_X11)
  static void XEventHandler(Widget widget,
                            VideoMap* video,
                            XEvent* xevent,
                            Boolean* b);
#endif  // NACL_LINUX && defined(MOZ_X11)

 private:
  volatile int             event_state_button_;
  volatile int             event_state_key_mod_;
  volatile uint16_t        event_state_motion_last_x_;
  volatile uint16_t        event_state_motion_last_y_;
  volatile int             event_state_motion_last_valid_;
  void*                    platform_specific_;
  volatile bool            request_redraw_;
  SRPC_Plugin*             srpc_plugin_;
  NaClVideoShare*          untrusted_video_share_;
  HtpHandle                video_handle_;
  size_t                   video_size_;
  bool                     video_enabled_;
  VideoCallbackData*       video_callback_data_;
  nacl_srpc::SharedMemory* video_shared_memory_;
  int                      video_update_mode_;
  NPWindow*                window_;
};

extern void VideoGlobalLock();
extern void VideoGlobalUnlock();

class VideoScopedGlobalLock {
 public:
  VideoScopedGlobalLock() { VideoGlobalLock(); }
  ~VideoScopedGlobalLock() { VideoGlobalUnlock(); }
};

}  // namespace nacl

#endif  // NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_VIDEO_H_

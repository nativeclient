/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_PLUGIN_H_
#define NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_PLUGIN_H_

#include <stdio.h>
#include <string>
#include "native_client/tools/npapi_runtime/nacl_npapi.h"
#include "native_client/npapi_plugin/srpc/utility.h"
#include "native_client/service_runtime/nrd_xfer_lib/nrd_all_modules.h"
#include "native_client/service_runtime/nrd_xfer_lib/nrd_xfer.h"
#include "native_client/service_runtime/nrd_xfer_lib/nrd_xfer_effector.h"

// An incomplete class for method declarations.
namespace nacl {
class SRPC_Plugin;
}

// An incomplete class for the client runtime.
namespace nacl_srpc {

// Incomplete classes for method declarations.
class ConnectedSocket;
class ServiceRuntimeInterface;
class SharedMemory;
class UnknownHandle;

// The main plugin class.
class Plugin : public NPObject {
 public:
  static Plugin* New(NPP instance, nacl::SRPC_Plugin* srpc_plugin);
  NPP npp() { return npp_; }
  const char* local_url() { return local_url_; }
  nacl::SRPC_Plugin* srpc_plugin() { return srpc_plugin_; }
  void set_local_url(const char*);

  // Load from the local URL saved in local_url_.
  bool Load();

  // Get/SetProperty is public
  static bool GetProperty(NPObject* obj, NPIdentifier name,
                          NPVariant* variant);
  static bool SetProperty(NPObject* obj, NPIdentifier name,
                          const NPVariant* variant);

  // Origin of page with the embed tag that created this plugin instance.
  std::string origin() { return origin_; }

  // Initialize some plugin data.
  bool Start();

  // Origin of NaCl module
  std::string nacl_module_origin() { return nacl_module_origin_; }
  void set_nacl_module_origin(std::string origin) {
    nacl_module_origin_ = origin;
  }

  // Constructor and destructor (for derived class)
  explicit Plugin(NPP);
  ~Plugin();

 private:
  // Method invocation.
  static bool HasMethod(NPObject* obj, NPIdentifier name);
  static bool Invoke(NPObject* obj, NPIdentifier name, const NPVariant* args,
                     uint32_t arg_count, NPVariant *result);
  static bool InvokeDefault(NPObject* obj, const NPVariant* args,
                            uint32_t arg_count, NPVariant* result);

  // Property accessor/mutators.
  static bool HasProperty(NPObject* obj, NPIdentifier name);

  // Allocation and deallocation.
  static NPObject* Allocate(NPP npp, NPClass* theClass);
  static void Deallocate(NPObject* obj);
  static void Invalidate(NPObject* obj);

  // Catch bad accesses during loading.
  static void SignalHandler(int value);

 public:
  // Not really constants.  Do not modify.  Use only after at least
  // one Plugin instance has been constructed.
  static NPIdentifier kConnectIdent;
  static NPIdentifier kHeightIdent;
  static NPIdentifier kLengthIdent;
  static NPIdentifier kMapIdent;
  static NPIdentifier kOnfailIdent;
  static NPIdentifier kOnloadIdent;
  static NPIdentifier kModuleReadyIdent;
  static NPIdentifier kNullNpapiMethodIdent;
  static NPIdentifier kReadIdent;
  static NPIdentifier kShmFactoryIdent;
  static NPIdentifier kSignaturesIdent;
  static NPIdentifier kSrcIdent;
  static NPIdentifier kToStringIdent;
  static NPIdentifier kUrlAsNaClDescIdent;
  static NPIdentifier kValueOfIdent;
  static NPIdentifier kVideoUpdateModeIdent;
  static NPIdentifier kWidthIdent;
  static NPIdentifier kWriteIdent;

  static NPIdentifier kLocationIdent;
  static NPIdentifier kHrefIdent;

 private:
  NPP npp_;

  nacl::SRPC_Plugin* srpc_plugin_;
  ConnectedSocket* socket_;
  ServiceRuntimeInterface* service_runtime_interface_;
  char* local_url_;
  int32_t height_;
  int32_t video_update_mode_;
  int32_t width_;

  std::string origin_;
  std::string nacl_module_origin_;
  bool origin_valid_;

 public:
  struct NaClDescEffector* effp_;
  struct NaClNrdXferEffector eff_;

  static PLUGIN_JMPBUF loader_env;

  static bool identifiers_initialized;
};

}  // namespace nacl_srpc

#endif  // NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_PLUGIN_H_

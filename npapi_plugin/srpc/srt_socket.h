/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_SRT_SOCKET_H_
#define NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_SRT_SOCKET_H_

#include <string>

#include "native_client/npapi_plugin/srpc/connected_socket.h"

namespace nacl_srpc {

class SrtSocket {
 public:
  SrtSocket(ConnectedSocket *s);
  ~SrtSocket();

  bool HardShutdown();
  bool SetOrigin(std::string origin);
  bool StartModule(int *load_status);

  ConnectedSocket *connected_socket() const {
    return connected_socket_;
  }

  // Not really constants.  Do not modify.  Use only after at least
  // one SrtSocket instance has been constructed.
  static NPIdentifier kHardShutdownIdent;
  static NPIdentifier kSetOriginIdent;
  static NPIdentifier kStartModule;
 private:
  ConnectedSocket *connected_socket_;

  static void InitializeIdentifiers();
};  // class SrtSocket

}  // namespace nacl_srpc

#endif  // NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_SRT_SOCKET_H_

/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


// NPAPI Simple RPC Interface

#ifndef NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_SRPC_CLIENT_H_
#define NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_SRPC_CLIENT_H_

#include <map>
#include <stdio.h>
#include "native_client/nonnacl_util/sel_ldr_launcher.h"
#include "native_client/npapi_plugin/srpc/connected_socket.h"
#include "native_client/npapi_plugin/srpc/plugin.h"
#include "native_client/npapi_plugin/srpc/ret_array.h"
#include "native_client/npapi_plugin/srpc/utility.h"
#include "native_client/tools/libsrpc/rpc_interface_desc.h"
#include "native_client/intermodule_comm/nacl_imc.h"

namespace nacl_srpc {

//  SrpcClients are implemented over ConnectedSockets.
class ConnectedSocket;

//  MethodInfo is used by ServiceRuntimeInterface to maintain an efficient
//  lookup for method id numbers and type strings.
class MethodInfo;

//  SrpcClient represents an SRPC connection to a client.
class SrpcClient {
 public:
  SrpcClient();
  //  Init is passed a ConnectedSocket.  It performs service
  //  discovery and provides the interface for future rpcs.
  bool Init(Plugin* plugin, ConnectedSocket* socket);

  //  The destructor closes the connection to sel_ldr.
  ~SrpcClient();

  //  Test whether the SRPC service has a given method.
  bool HasMethod(NPIdentifier method);
  //  Invoke an SRPC method.
  bool Invoke(NPIdentifier ident,
              const NPVariant* args,
              int argc,
              NPVariant* result);

  // Build the signature list object, for use in display, etc.
  NPObject* GetSignatureObject();

 private:
  static void SignalHandler(int value);
  void GetMethods();

 private:
  std::map<NPIdentifier, MethodInfo*> methods_;
  NaClSrpcChannel srpc_channel_;
  Plugin* plugin_;
  bool is_command_channel_;

  static int number_alive;
  static PLUGIN_JMPBUF srpc_env;
};

}  // namespace nacl_srpc

#endif  // NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_SRPC_CLIENT_H_

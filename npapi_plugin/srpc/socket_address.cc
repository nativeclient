/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <new>
#include <signal.h>
#include <string.h>
#include "native_client/npapi_plugin/srpc/connected_socket.h"
#include "native_client/npapi_plugin/srpc/npapi_native.h"
#include "native_client/npapi_plugin/srpc/plugin.h"
#include "native_client/npapi_plugin/srpc/socket_address.h"
#include "native_client/npapi_plugin/srpc/utility.h"

namespace nacl_srpc {

int SocketAddress::number_alive = 0;

// SocketAddress implements only the connect method.
bool SocketAddress::HasMethod(NPObject *obj, NPIdentifier name) {
  dprintf(("SocketAddress::HasMethod(%p, %s)\n", obj, IdentToString(name)));

  return (Plugin::kConnectIdent == name);
}

bool SocketAddress::Invoke(NPObject *obj,
                           NPIdentifier name,
                           const NPVariant *args,
                           uint32_t arg_count,
                           NPVariant *result) {
  SocketAddress* socket_address = reinterpret_cast<SocketAddress*>(obj);

  dprintf(("SocketAddress::Invoke(%p, %s, %d)\n",
           obj, IdentToString(name), arg_count));

  VOID_TO_NPVARIANT(*result);
  // connect() invokes the connect method, which returns a ConnectedSocket.
  if (Plugin::kConnectIdent == name) {
    if (0 != arg_count) {
      NPN_SetException(obj, "wrong number of arguments");
      return false;
    }
    ConnectedSocket* con_sock = socket_address->Connect(false, NULL);
    if (con_sock) {
      ScalarToNPVariant(static_cast<NPObject*>(con_sock), result);
      return true;
    }
  }
  NPN_SetException(obj, "connect failed");
  return false;
}

// SocketAddresses have no properties.
bool SocketAddress::HasProperty(NPObject *obj, NPIdentifier name) {
  dprintf(("SocketAddress::HasProperty(%p, %s)\n", obj, IdentToString(name)));

  return false;
}

bool SocketAddress::GetProperty(NPObject *obj,
                                NPIdentifier name,
                                NPVariant *variant) {
  dprintf(("SocketAddress::GetProperty(%p, %s)\n", obj, IdentToString(name)));

  VOID_TO_NPVARIANT(*variant);
  return false;
}

bool SocketAddress::SetProperty(NPObject *obj,
                                NPIdentifier name,
                                const NPVariant *variant) {
  dprintf(("SocketAddress::SetProperty(%p, %s, %p)\n",
           obj, IdentToString(name), variant));

  return false;
}

SocketAddress* SocketAddress::New(Plugin* plugin, struct NaClDesc* desc) {
  static NPClass socketAddressClass = {
    NP_CLASS_STRUCT_VERSION,
    Allocate,
    Deallocate,
    Invalidate,
    HasMethod,
    Invoke,
    0,  // There is no InvokeDefault for SocketAddresses.
    HasProperty,
    GetProperty,
    SetProperty,
  };

  dprintf(("SocketAddress::New(%p, %p, sa: %s)\n", plugin, desc,
           reinterpret_cast<NaClDescConnCap*>(desc)->cap.path));

  SocketAddress* socket_address =
    reinterpret_cast<SocketAddress*>(NPN_CreateObject(plugin->npp(),
                                                      &socketAddressClass));
  if (NULL == socket_address) {
    return NULL;
  }

  socket_address->plugin_ = plugin;
  socket_address->desc_ = desc;

  return socket_address;
}

NPObject *SocketAddress::Allocate(NPP npp, NPClass *theClass) {
  dprintf(("SocketAddress::Allocate(%d)\n", ++number_alive));

  return new(std::nothrow) SocketAddress(npp);
}

void SocketAddress::Deallocate(NPObject *obj) {
  dprintf(("SocketAddress::Deallocate(%p, %d)\n", obj, --number_alive));

  // TODO: is there a missing NaClDescUnref here?
  delete reinterpret_cast<SocketAddress*>(obj);
}

void SocketAddress::Invalidate(NPObject *obj) {
  SocketAddress* socket_address = reinterpret_cast<SocketAddress*>(obj);

  dprintf(("SocketAddress::Invalidate(%p)\n", socket_address));

  // After invalidation, the browser does not respect reference counting,
  // so we shut down here what we can and prevent attempts to shut down
  // other linked structures in Deallocate.

  socket_address->plugin_ = NULL;
  socket_address->desc_ = NULL;
}

SocketAddress::SocketAddress(NPP npp) : UnknownHandle(npp) {
  dprintf(("SocketAddress::SocketAddress(%p)\n", this));
}

SocketAddress::~SocketAddress() {
  dprintf(("SocketAddress::~SocketAddress(%p)\n", this));
}

// Returns a connected socket for the address.
ConnectedSocket* SocketAddress::Connect(bool is_command_channel,
                                        ServiceRuntimeInterface* sri) {
  dprintf(("SocketAddress::Connect(%d, %p)\n", is_command_channel, sri));
  int rv = (desc_->vtbl->ConnectAddr)(desc_, plugin_->effp_);
  dprintf(("SocketAddress::Connect: returned %d\n", rv));
  if (0 == rv) {
    struct NaClDesc* con_desc = NaClNrdXferEffectorTakeDesc(&plugin_->eff_);
    dprintf(("SocketAddress::Connect: take returned %p\n", con_desc));
    ConnectedSocket* connected_socket =
        ConnectedSocket::New(plugin_, con_desc, true, sri, is_command_channel);
    dprintf(("SocketAddress::Connect: CS returned %p\n", connected_socket));
    return connected_socket;
  } else {
    dprintf(("SocketAddress::Invoke: connect failed\n"));
    return NULL;
  }
}

}  // namespace nacl_srpc

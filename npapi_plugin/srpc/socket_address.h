/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


// NPAPI Simple RPC Interface

#ifndef NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_SOCKET_ADDRESS_H_
#define NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_SOCKET_ADDRESS_H_

#include <setjmp.h>
#include <stdio.h>
#include "native_client/tools/npapi_runtime/nacl_npapi.h"
#include "native_client/npapi_plugin/srpc/unknown_handle.h"
#include "native_client/npapi_plugin/srpc/utility.h"

namespace nacl_srpc {

// Class declarations.
class Plugin;
class ConnectedSocket;
class ServiceRuntimeInterface;


// SocketAddress is used to represent socket address descriptors.
class SocketAddress : public UnknownHandle {
 public:
  // Create a new SocketAddress.
  static SocketAddress* New(Plugin* obj, struct NaClDesc* desc);

  // Get the contained descriptor.
  struct NaClDesc* desc() { return desc_; }

  // Connect to a SocketAddress.
  ConnectedSocket* Connect(bool is_command_channel,
                           ServiceRuntimeInterface *sri);

 private:
  // Method invocation.
  static bool HasMethod(NPObject *obj, NPIdentifier name);
  static bool Invoke(NPObject* obj,
                     NPIdentifier name,
                     const NPVariant* args,
                     uint32_t arg_count,
                     NPVariant* result);
  static void SignalHandler(int value);

  // Property accessor/mutators.
  static bool HasProperty(NPObject* obj, NPIdentifier name);
  static bool GetProperty(NPObject* obj, NPIdentifier name,
                          NPVariant* variant);
  static bool SetProperty(NPObject* obj, NPIdentifier name,
                          const NPVariant *variant);

  // Allocation and deallocation.
  static NPObject* Allocate(NPP npp, NPClass* theClass);
  static void Deallocate(NPObject* obj);
  static void Invalidate(NPObject* obj);
  explicit SocketAddress(NPP npp);
  ~SocketAddress();

 private:
  static int number_alive;
};

}  // namespace nacl_srpc

#endif  // NATIVE_CLIENT_NPAPI_PLUGIN_SRPC_SOCKET_ADDRESS_H_

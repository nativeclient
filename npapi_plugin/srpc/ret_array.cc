/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <new>
#include <stdlib.h>
#include <string.h>
#include "native_client/npapi_plugin/srpc/npapi_native.h"
#include "native_client/npapi_plugin/srpc/plugin.h"
#include "native_client/npapi_plugin/srpc/ret_array.h"
#include "native_client/npapi_plugin/srpc/utility.h"

// TODO: this whole module should probably be replaced by
// a call to javascript eval to create an array object.
//
namespace nacl_srpc {

// Utility function to force to canonical integer NPIdentifiers.
static bool IsIntegerIdentifier(NPIdentifier ident);
static NPIdentifier ForceToIntegerIdent(NPIdentifier ident);

// Class variable definitions.
int RetArray::number_alive = 0;

// RetArray defines no methods.
bool RetArray::HasMethod(NPObject *obj, NPIdentifier name) {
  dprintf(("RetArray::HasMethod(%p, %s)\n", obj, IdentToString(name)));

  return false;
}

bool RetArray::Invoke(NPObject *obj,
                      NPIdentifier name,
                      const NPVariant *args,
                      uint32_t arg_count,
                      NPVariant *result) {
  dprintf(("RetArray::Invoke(%p, %s, %d)\n",
          obj, IdentToString(name), arg_count));

  VOID_TO_NPVARIANT(*result);
  return false;
}

// RetArray defines a "length" property and one int property for
// each valid subscript [0..length-1].
bool RetArray::HasProperty(NPObject *obj, NPIdentifier name) {
  RetArray* retarray = reinterpret_cast<RetArray*>(obj);

  dprintf(("RetArray::HasProperty(%p, %s)\n", obj, IdentToString(name)));

  if (Plugin::kLengthIdent == name) {
    return true;
  }
  if (IsIntegerIdentifier(name)) {
    name = ForceToIntegerIdent(name);
    for (int i = 0; i < retarray->property_count_; ++i) {
      if (name == retarray->properties_[i]) {
        return true;
      }
    }
  }
  return false;
}

bool RetArray::GetProperty(NPObject *obj,
                           NPIdentifier name,
                           NPVariant *variant) {
  RetArray* retarray = reinterpret_cast<RetArray*>(obj);

  dprintf(("RetArray::GetProperty(%p, %s)\n", obj, IdentToString(name)));

  VOID_TO_NPVARIANT(*variant);
  if (Plugin::kLengthIdent == name) {
    ScalarToNPVariant(retarray->property_count_, variant);
    return true;
  }
  name = ForceToIntegerIdent(name);
  for (int i = 0; i < retarray->property_count_; ++i) {
    if (name == retarray->properties_[i]) {
      char* ret_string;
      NPObject* ret_property;
      if (NPVariantToScalar(&retarray->values_[i], &ret_string)) {
        ScalarToNPVariant(ret_string, variant);
        // Free the temporary string used to copy the object.
        free(ret_string);
      } else if (NPVariantToScalar(&retarray->values_[i], &ret_property)) {
        NPN_RetainObject(ret_property);
        *variant = retarray->values_[i];
      } else {
        *variant = retarray->values_[i];
      }
      return true;
    }
  }
  return false;
}

bool RetArray::SetProperty(NPObject *obj,
                           NPIdentifier name,
                           const NPVariant *variant) {
  dprintf(("RetArray::SetProperty(%p, %s, %p)\n",
           obj, IdentToString(name), variant));

  return false;
}

RetArray* RetArray::New(Plugin* plugin, int property_count) {
  dprintf(("RetArray::New(%p, %d)\n", plugin, property_count));

  static NPClass retArrayClass = {
    NP_CLASS_STRUCT_VERSION,
    Allocate,
    Deallocate,
    Invalidate,
    HasMethod,
    Invoke,
    0,
    HasProperty,
    GetProperty,
    SetProperty,
  };

  RetArray* retarray =
    reinterpret_cast<RetArray*>(NPN_CreateObject(plugin->npp(),
                                                 &retArrayClass));
  if (NULL == retarray) {
    return NULL;
  }

  retarray->plugin_ = plugin;
  retarray->property_count_ = property_count;
  if (property_count > 0) {
    // Allocate a vector of identifiers for integers [0,property_count).
    retarray->properties_ = new(std::nothrow) NPIdentifier[property_count];
    if (NULL == retarray->properties_) {
      NPN_ReleaseObject(retarray);
      return NULL;
    }
    // Allocate a vector for the values mapped to.
    retarray->values_ = new(std::nothrow) NPVariant[property_count];
    if (NULL == retarray->values_) {
      delete[] retarray->properties_;
      NPN_ReleaseObject(retarray);
      return NULL;
    }
    for (int i = 0; i < property_count; ++i) {
      retarray->properties_[i] = NPN_GetIntIdentifier(i);
      // VOID represents JavaScript's undefined.
      VOID_TO_NPVARIANT(retarray->values_[i]);
    }
  } else {
    retarray->properties_ = NULL;
    retarray->values_ = NULL;
  }

  return retarray;
}

NPObject *RetArray::Allocate(NPP npp, NPClass *theClass) {
  dprintf(("RetArray::Allocate(%d)\n", ++number_alive));

  return new(std::nothrow) RetArray(npp);
}

void RetArray::Deallocate(NPObject *obj) {
  RetArray* retarray = reinterpret_cast<RetArray*>(obj);

  dprintf(("RetArray::Deallocate(%p, %d)\n", obj, --number_alive));

  if (retarray->property_count_ > 0) {
    for (int i = 0; i < retarray->property_count_; ++i) {
      dprintf(("Deallocate(%p): releasing %d -- \n", retarray, i));
      NPN_ReleaseVariantValue(&retarray->values_[i]);
    }
    delete retarray->properties_;
    delete retarray->values_;
  }
  delete reinterpret_cast<RetArray*>(obj);
}

void RetArray::Invalidate(NPObject *obj) {
  RetArray* retarray = reinterpret_cast<RetArray*>(obj);

  dprintf(("RetArray::Invalidate(%p)\n", obj));

  // After invalidation, the browser does not respect reference counting,
  // so we shut down here what we can and prevent attempts to shut down
  // other linked structures in Deallocate.

  if (retarray->property_count_ > 0) {
    delete retarray->properties_;
    retarray->properties_ = 0;
    delete retarray->values_;
    retarray->values_ = 0;
    retarray->property_count_ = 0;
  }
  retarray->plugin_ = NULL;
}

RetArray::RetArray(NPP npp) : npp_(npp) {
  dprintf(("RetArray::RetArray(%p)\n", this));
}

RetArray::~RetArray() {
  dprintf(("RetArray::~RetArray(%p)\n", this));
}

// Force an identifier to a canonical integer NPIdentifier.  This is
// needed because Safari likes to index by using character string names.
static bool IsIntegerIdentifier(NPIdentifier ident) {
  if (NPN_IdentifierIsString(ident)) {
    char* string = NPN_UTF8FromIdentifier(ident);
    for (char* p = string; '\0' != *p; ++p) {
      if (*p < '0' || *p > '9') {
        return false;
      }
    }
    return true;
  } else {
    return true;
  }
}

static NPIdentifier ForceToIntegerIdent(NPIdentifier ident) {
  if (NPN_IdentifierIsString(ident)) {
    const char* str = NPN_UTF8FromIdentifier(ident);
    int index = atoi(str);
    return NPN_GetIntIdentifier(index);
  } else {
    return ident;
  }
}

}  // namespace nacl_srpc

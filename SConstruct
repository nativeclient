# -*- python -*-
# Copyright 2008, Google Inc.
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#     * Neither the name of Google Inc. nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import glob
import os
import stat
import sys
sys.path.append("./common")
import nacl_util


# Underlay gtest
Dir('gtest').addRepository(Dir('../third_party/gtest/files'))


environment_list = []

# ----------------------------------------------------------
# Base environment for both nacl and non-nacl variants.
pre_base_env = Environment(
    tools = ['component_setup'],
    # Makes SOURCE_ROOT independent of site_init.
    SOURCE_ROOT = Dir('#/../..').abspath,
    # Publish dlls as final products (to staging).
    COMPONENT_LIBRARY_PUBLISH = True,
)
pre_base_env.Append(BUILD_GROUPS=['most'])


# ----------------------------------------------------------
# Generic Test Wrapper
# all test suites know so far
TEST_SUITES = {'all_tests': None}

def AddNodeToTestSuite(env, node, suite_name, node_name=None):
  AlwaysBuild(node)
  env.Alias('all_tests', node)
  for s in suite_name:
    if s not in TEST_SUITES:
      TEST_SUITES[s] = None
    env.Alias(s, node)
  if node_name:
    env.ComponentTestOutput(node_name, node)

pre_base_env.AddMethod(AddNodeToTestSuite)

# ----------------------------------------------------------
# Small tests are usually unit tests
def AddNodeToSmallTestsSuite(env, node, node_name=None):
  env.AddNodeToTestSuite(node, ['small_tests'], node_name)

pre_base_env.AddMethod(AddNodeToSmallTestsSuite)

# ----------------------------------------------------------
# Medium tests are usually larger than unit tests, but don't take as much time
# as large tests
def AddNodeToMediumTestsSuite(env, node, node_name=None):
  env.AddNodeToTestSuite(node, ['medium_tests'], node_name)

pre_base_env.AddMethod(AddNodeToMediumTestsSuite)

# ----------------------------------------------------------
# Large tests are usually long tests

def AddNodeToLargeTestsSuite(env, node, node_name=None):
  env.AddNodeToTestSuite(node, ['large_tests'], node_name)

pre_base_env.AddMethod(AddNodeToLargeTestsSuite)

# ----------------------------------------------------------
# Smoke tests run before any check-in
def AddNodeToSmokeTestSuite(env, node, node_name=None):
  env.AddNodeToTestSuite(node, ['smoke_tests'], node_name)

pre_base_env.AddMethod(AddNodeToSmokeTestSuite)

# ----------------------------------------------------------
# Convenient alias
Alias('unit_tests', 'small_tests')

# ----------------------------------------------------------
def Banner(text):
  print '=' * 70
  print text
  print '=' * 70

pre_base_env.AddMethod(Banner)


def FindLikelySelLdr(directory):
  candidates = []
  if (nacl_util.GetSelLdr('opt')):
    candidates.append(nacl_util.GetSelLdr('opt'))
  if (nacl_util.GetSelLdr('dbg')):
    candidates.append(nacl_util.GetSelLdr('dbg'))
  latest = (None, 0)
  for c in candidates:
    mtime = os.stat(c)[stat.ST_MTIME]
    if mtime > latest[1]:
      latest = (c, mtime)
  return latest[0]


def CommandSelLdrTestNacl(env, name, command,
                          guess_sel_ldr=False,
                          log_verbosity=2,
                          **extra):

  # TODO: we do not model a proper dependency to
  # the sel_ldr here
  if guess_sel_ldr:
    sel_ldr = FindLikelySelLdr(env.subst('$DESTINATION_ROOT'))
    # Bail out if sel_ldr can't be found.
    if not sel_ldr:
      Banner('could not find a sel_ldr binary, try running "scons sel_ldr"')
      return []
  else:
    sel_ldr = '$STAGING_DIR/${PROGPREFIX}sel_ldr${PROGSUFFIX}'

  command = [sel_ldr, '-d', '-f'] + command

  # NOTE: log handling is a little magical
  # We do not pass these via flags because those are not usable for sel_ldr
  # when testing via plugin, esp windows.
  if 'log_golden' in extra:
    logout = '${TARGET}.log'
    extra['logout'] = logout
    extra['osenv'] = 'NACLLOG=%s,NACLVERBOSITY=%d' % (logout, log_verbosity)

  return CommandTestAgainstGoldenOuput(env, name, command, **extra)

pre_base_env.AddMethod(CommandSelLdrTestNacl)

# ----------------------------------------------------------
TEST_EXTRA_ARGS = ['stdin', 'logout',
                   'stdout_golden', 'stderr_golden', 'log_golden',
                   'stdout_filter', 'stderr_filter', 'log_filter',
                   'osenv', 'exit_status']

TEST_SCRIPT = '${SCONSTRUCT_DIR}/tools/command_tester.py'

def CommandTestAgainstGoldenOuput(env, name, command, **extra):
  script_flags = ['--name', name]
  deps = [TEST_SCRIPT]

  # extract deps from command and rewrite
  for n, c in enumerate(command):
    if type(c) != str:
      deps.append(c)
      command[n] = '${SOURCES[%d].abspath}' % (len(deps) - 1)

  # extract deps from flags and rewrite
  for e in extra:
    assert e in TEST_EXTRA_ARGS
    if type(extra[e]) != str:
      deps.append(extra[e])
      extra[e] = '${SOURCES[%d].abspath}' % (len(deps) - 1)
    script_flags.append('--' + e)
    script_flags.append(extra[e])

  # NOTE: "SOURCES[X]" references the scons object in deps[x]
  command = ['${PYTHON}',
             '${SOURCES[0].abspath}',
             ' '.join(script_flags),
             ' '.join(command),
             '> ${TARGET}',
             ]

  # TODO: consider redirecting output into tests/results
  return env.Command(name, deps, " ".join(command))

pre_base_env.AddMethod(CommandTestAgainstGoldenOuput)

# ----------------------------------------------------------
if ARGUMENTS.get('pp', 0):
  def CommandPrettyPrinter(cmd, targets, source, env):
    prefix = env.subst('$SOURCE_ROOT') + '/googleclient/'
    target =  targets[0]
    cmd_tokens = cmd.split()
    if "python" in cmd_tokens[0]:
      cmd_name = cmd_tokens[1]
    else:
      cmd_name = cmd_tokens[0].split('(')[0]
    if cmd_name.startswith(prefix):
      cmd_name = cmd_name[len(prefix):]
    env_name = env.subst('${BUILD_TYPE}${BUILD_SUBTYPE}')
    print '[%s] [%s] %s' % (cmd_name, env_name, target.get_path())
  pre_base_env.Append(PRINT_CMD_LINE_FUNC = CommandPrettyPrinter)

# ----------------------------------------------------------
base_env = pre_base_env.Clone()
base_env.Append(
  BUILD_SUBTYPE = '',
  BUILD_SCONSCRIPTS = [
        # NOTE: this dir also has a nacl.scons
        'tests/npapi_bridge/build.scons',
        'ncv/build.scons',
        'platform_qual_test/build.scons',
        'sandbox/build.scons',
        'service_runtime/build.scons',
        'service_runtime/nrd_xfer_lib/build.scons',
        'tools/libsrpc/build.scons',
        'tools/npapi_runtime/build.scons',
        'intermodule_comm/build.scons',
        'npapi_plugin/build.scons',
        'npapi_plugin/install.scons',
        'gtest/build.scons',
        'nonnacl_util/build.scons',
        'tests/python_version/build.scons',
#        'nacl_ie_plugin/build.scons',
    ],
    CPPDEFINES = [
        ['NACL_BLOCK_SHIFT', '5'],
        ['NACL_BLOCK_SIZE', '32'],
    ],
    CPPPATH = ['$SOURCE_ROOT/googleclient'],
    GEN2_FLAGS = '-c -f "Video|Audio|Multimedia"',
    # NOTE: the EXTRA_* mechanism does not fully work yet
    #       due to scons bugs
    EXTRA_CFLAGS = [],
    EXTRA_CCFLAGS = [],
    EXTRA_CXXFLAGS = [],
    EXTRA_LIBS = [],
    CFLAGS = ['${EXTRA_CFLAGS}'],
    CCFLAGS = ['${EXTRA_CCFLAGS}'],
    CXXFLAGS = ['${EXTRA_CXXFLAGS}'],
    LIBS = ['${EXTRA_LIBS}'],
)
base_env.Replace(
    SDL_HERMETIC_LINUX_DIR='$MAIN_DIR/../third_party/sdl/linux/v1_2_13',
    SDL_HERMETIC_MAC_DIR='$MAIN_DIR/../third_party/sdl/osx/v1_2_13',
    SDL_HERMETIC_WINDOWS_DIR='$MAIN_DIR/../third_party/sdl/win/v1_2_13',
)



# Optionally ignore the build process.
DeclareBit('prebuilt', 'Disable all build steps, only support install steps')
base_env.SetBitFromOption('prebuilt', False)
if base_env.Bit('prebuilt'):
  base_env.Replace(BUILD_SCONSCRIPTS = ['npapi_plugin/install.scons'])

# Add the sdk root path (without setting up the tools).
base_env['NACL_SDK_ROOT_ONLY'] = True
base_env.Tool('naclsdk')


base_env.Help("""\
======================================================================
Help for NaCl
======================================================================

Common tasks:
-------------

* cleaning:        scons -c
* building:        scons
* + nacl:          scons -c MODE=most
* + doc and more:  scons -c MODE=all
* just the doc:    scons MODE=doc
* build mandel:    scons MODE=most mandel.nexe
* some unittests:  scons small_tests
* a smoke test:    scons -k pp=1 smoke_tests
* 2nd smoke test:  scons -k pp=1 MODE=nacl smoke_tests
* firefox plugin:  scons MODE=opt-linux npGoogleNaClPlugin
* sel_ldr:         scons MODE=opt-linux sel_ldr
* firefox install: scons firefox_install
* firefox install pre-built copy: scons firefox_install --prebuilt

Options:
--------
pp=1              use command line pretty printing (more concise output)
sdl=<mode>        where <mode>:
                  'none': don't use SDL (default)
                  'local': use locally installed SDL
                  'hermetic': use the hermetic SDL copy
naclsdk_mode=<mode>   where <mode>:
                      'local': use locally installed sdk kit
                      'download': use the download copy (default)
                      'custom:<path>': use kit at <path>

--prebuilt        Do not build things, just do install steps

Automagically generated help:
-----------------------------
""")

# ---------------------------------------------------------
def GenerateOptimizationLevels(env):
  # Generate debug variant.
  debug_env = env.Clone(tools = ['target_debug'])
  debug_env['OPTIMIZATION_LEVEL'] = 'dbg'
  debug_env['BUILD_TYPE'] = debug_env.subst('$BUILD_TYPE')
  debug_env['BUILD_DESCRIPTION'] = debug_env.subst('$BUILD_DESCRIPTION')
  # Add debug to the default group.
  debug_env.Append(BUILD_GROUPS = ['default'])
  # Add to the list of fully described environments.
  environment_list.append(debug_env)

  # Generate opt variant.
  opt_env = env.Clone(tools = ['target_optimized'])
  opt_env['OPTIMIZATION_LEVEL'] = 'opt'
  opt_env['BUILD_TYPE'] = opt_env.subst('$BUILD_TYPE')
  opt_env['BUILD_DESCRIPTION'] = opt_env.subst('$BUILD_DESCRIPTION')
  # Add to the list of fully described environments.
  environment_list.append(opt_env)

  return (debug_env, opt_env)


# ----------------------------------------------------------
windows_env = base_env.Clone(
    BUILD_TYPE = '${OPTIMIZATION_LEVEL}-win',
    BUILD_TYPE_DESCRIPTION = 'Windows ${OPTIMIZATION_LEVEL} build',
    tools = ['target_platform_windows'],
    ASCOM = '$ASPPCOM /E | as -o $TARGET',
    PDB = '${TARGET.base}.pdb',
)
windows_env.Append(
    CPPDEFINES = [
        ['NACL_WINDOWS', '1'],
        ['NACL_OSX', '0'],
        ['NACL_LINUX', '0'],
        ['_WIN32_WINNT', '0x0501'],
        ['__STDC_LIMIT_MACROS', '1'],
    ],
    NACL_PLATFORM = 'win',
    LIBS = ['wsock32', 'advapi32'],
    CCFLAGS = ['/EHsc', '/WX'],
)

(windows_debug_env,
 windows_optimized_env) = GenerateOptimizationLevels(windows_env)

if ARGUMENTS.get('sdl', 'hermetic') != 'none':
  # These will only apply to sdl!=none builds!
  windows_debug_env.Append(CPPDEFINES = ['_DLL', '_MT'])
  windows_optimized_env.Append(CPPDEFINES = ['_DLL', '_MT'])
  # SDL likes DLLs
  if '/MT' in windows_optimized_env['CCFLAGS']:
    windows_optimized_env.FilterOut(CCFLAGS=['/MT']);
    windows_optimized_env.Append(CCFLAGS=['/MD']);
  if '/MTd' in windows_debug_env['CCFLAGS']:
    windows_debug_env.FilterOut(CCFLAGS=['/MTd']);
    windows_debug_env.Append(CCFLAGS=['/MDd']);
    # this doesn't feel right, but fixes dbg-win
    windows_debug_env.Append(LINKFLAGS = ['/NODEFAULTLIB:msvcrt'])
  # make source level debugging a little easier
  if '/Z7' not in windows_debug_env['CCFLAGS']:
    if '/Zi' not in windows_debug_env['CCFLAGS']:
      windows_debug_env.Append(CCFLAGS=['/Z7'])

# ----------------------------------------------------------

unix_like_env = base_env.Clone()
unix_like_env.Append(
  CCFLAGS = [
    # TODO: explore adding stricter checking from
    #       as we do for the linux_env
    # '-malign-double',
    '-Werror',
    '-Wall',

    # start pedantic flags for code extreme cleaning
    # '-ansi',
    #'-pedantic',
    # '-Wno-long-long',
    # end pedantic flags

    # '-Wswitch-enum',
    '-fvisibility=hidden',
    # '-Wsign-compare',
  ],
  CFLAGS = ['-std=gnu99' ],
  LIBS = ['pthread', 'ssl', 'crypto'],
)

# ----------------------------------------------------------

mac_env = unix_like_env.Clone(
    BUILD_TYPE = '${OPTIMIZATION_LEVEL}-mac',
    BUILD_TYPE_DESCRIPTION = 'MacOS ${OPTIMIZATION_LEVEL} build',
    tools = ['target_platform_mac'],
    # TODO: this should really be able to live in unix_like_env
    #                   but can't due to what the target_platform_x module is
    #                   doing.
    LINK = '$CXX',
    PLUGIN_SUFFIX = '.bundle',
)
mac_env.Append(
    CCFLAGS = ['-mmacosx-version-min=10.4'],
    CFLAGS = ['-std=gnu99'],
    # TODO: remove UNIX_LIKE_CFLAGS when scons bug is fixed
    CPPDEFINES = [['NACL_WINDOWS', '0'],
                  ['NACL_OSX', '1'],
                  ['NACL_LINUX', '0'],
                  ['MAC_OS_X_VERSION_MIN_REQUIRED', 'MAC_OS_X_VERSION_10_4'],
                  ['_DARWIN_C_SOURCE', '1'],
                  ['__STDC_LIMIT_MACROS', '1'],
                  ],
    NACL_PLATFORM = 'osx',
)

(mac_debug_env, mac_optimized_env) = GenerateOptimizationLevels(mac_env)

# ----------------------------------------------------------

linux_env = unix_like_env.Clone(
    BUILD_TYPE = '${OPTIMIZATION_LEVEL}-linux',
    BUILD_TYPE_DESCRIPTION = 'Linux ${OPTIMIZATION_LEVEL} build',
    tools = ['target_platform_linux'],
    # TODO: this should really be able to live in unix_like_env
    #                   but can't due to what the target_platform_x module is
    #                   doing.
    LINK = '$CXX',
)

# -m32 and -L/usr/lib32 are needed to do 32-bit builds on 64-bit
# user-space machines; requires ia32-libs-dev to be installed; or,
# failing that, ia32-libs and symbolic links *manually* created for
# /usr/lib32/libssl.so and /usr/lib32/libcrypto.so to the current
# /usr/lib32/lib*.so.version (tested with ia32-libs 2.2ubuntu11; no
# ia32-libs-dev was available for testing).
# Additional symlinks of this sort are needed for gtk,
# see nonnacl_util/build.scons.

linux_env.SetDefault(
    # NOTE: look into http://www.scons.org/wiki/DoxygenBuilder
    DOXYGEN = ARGUMENTS.get('DOXYGEN', '/usr/bin/doxygen'),
)

linux_env.Append(
    ASFLAGS = ['-m32', ],
    # TODO: move these flags up to unix_like_env.
    #                currently mac issues prevent us from doing that
    CCFLAGS = ['-m32',
               '-Wno-long-long',
               # NOTE: not available with older gcc/g++ versions
               # '-fdiagnostics-show-option',
               '-pedantic'],
    CXXFLAGS=['-std=c++98'],
    CFLAGS=['-std=gnu99'],
    CPPDEFINES = [['NACL_WINDOWS', '0'],
                  ['NACL_OSX', '0'],
                  ['NACL_LINUX', '1'],
                  ['_BSD_SOURCE', '1'],
                  ['_POSIX_C_SOURCE', '199506'],
                  ['_XOPEN_SOURCE', '600'],
                  ['_GNU_SOURCE', '1'],
                  ['__STDC_LIMIT_MACROS', '1'],
                  ],
    LIBS = ['rt'],
    LINKFLAGS = ['-m32', '-L/usr/lib32'],
    NACL_PLATFORM = 'linux',

)

(linux_debug_env, linux_optimized_env) = GenerateOptimizationLevels(linux_env)


# ----------------------------------------------------------
# The nacl_env is used to build native_client modules
# using a special tool chain which produces platform
# independent binaries
# ----------------------------------------------------------
nacl_env = pre_base_env.Clone(
    tools = ['naclsdk'],
    BUILD_TYPE = 'nacl',
    BUILD_TYPE_DESCRIPTION = 'NaCl module build',
    # TODO: explain this
    LINK = '$CXX',
    # always optimize binaries
    CCFLAGS = ['-O2',
               '-mfpmath=sse',
               '-msse',
               '-fomit-frame-pointer',
               '-Wall',
               '-fdiagnostics-show-option',
               '-pedantic',
               '-Werror'],
    CPPPATH = ['$SOURCE_ROOT/googleclient'],
    # from third_party/software_construction_toolkit/files/site_scons/site_tools/naclsdk.py
    LIBPATH = ['$NACL_SDK_LIB'],
)

Banner('Building nexe binaries using sdk at [%s]' %
       nacl_env.subst('$NACL_SDK_ROOT'))


nacl_env.Append(
    BUILD_SCONSCRIPTS = [
      ####  ALPHABETICALLY SORTED ####

      'tests/app_lib/nacl.scons',
      'tests/autoloader/nacl.scons',
      'tests/contest_issues/nacl.scons',
      'tests/cloudfs/nacl.scons',
      'tests/earth/nacl.scons',
      'tests/fib/nacl.scons',
      'tests/file/nacl.scons',
      'tests/hello_world/nacl.scons',
      'tests/imc_shm_mmap/nacl.scons',
      'tests/life/nacl.scons',
      'tests/mandel/nacl.scons',
      'tests/mandel_nav/nacl.scons',
      'tests/many/nacl.scons',
      'tests/mmap/nacl.scons',
      'tests/noop/nacl.scons',
      'tests/npapi_bridge/nacl.scons',
      'tests/npapi_hw/nacl.scons',
      'tests/npapi_pi/nacl.scons',
      'tests/nrd_xfer/nacl.scons',
      'tests/null/nacl.scons',
      'tests/nullptr/nacl.scons',
      'tests/srpc/nacl.scons',
      'tests/srpc_hw/nacl.scons',
      'tests/syscalls/nacl.scons',
      'tests/threads/nacl.scons',
      'tests/tone/nacl.scons',
      'tests/voronoi/nacl.scons',

      'tools/tests/nacl.scons',
      ####  ALPHABETICALLY SORTED ####
   ],
)

environment_list.append(nacl_env)
# ----------------------------------------------------------
# We force this into a separate env so that the tests in nacl_env
# have NO access to any libraries build here but need to link them
# from the sdk libdir
# ----------------------------------------------------------
nacl_extra_sdk_env = pre_base_env.Clone(
    tools = ['naclsdk'],
    BUILD_TYPE = 'nacl_extra_sdk',
    BUILD_TYPE_DESCRIPTION = 'NaCl SDK extra library build',
    # TODO: explain this
    LINK = '$CXX',
    CCFLAGS = ['-O3',
               '-Werror',
               '-Wall',
               '-Wswitch-enum',
               '-g',
               '-fno-builtin',
               '-fno-stack-protector',
               '-DNACL_BLOCK_SHIFT=5',
               '-fdiagnostics-show-option',
               '-pedantic',
               ],
    CPPPATH = ['$SOURCE_ROOT/googleclient'],
)


nacl_extra_sdk_env.Append(
    BUILD_SCONSCRIPTS = [
      ####  ALPHABETICALLY SORTED ####
      # TODO: maybe move this to tools/ as well
      'intermodule_comm/nacl.scons',

      'tools/stubs/nacl.scons',
      'tools/libnacl/nacl.scons',
      'tools/libsrpc/nacl.scons',
      'tools/nc_threads/nacl.scons',
      'tools/libav/nacl.scons',
      'tools/libunimpl/nacl.scons',
      'tools/npapi_runtime/nacl.scons',
      ####  ALPHABETICALLY SORTED ####
   ],
)

environment_list.append(nacl_extra_sdk_env)

nacl_extra_sdk_env.FilterOut(BUILD_GROUPS=['most', 'all'])

# ----------------------------------------------------------
# Targets for updating sdk headers and libraries
# NACL_SDK_ROOT is defined by
# third_party/software_construction_toolkit/files/site_scons/site_tools/naclsdk.py

def AddHeaderToSdk(env, nodes, path=None):
  if path is None:
    path = 'nacl/include/nacl/'

  n = env.Replicate('${NACL_SDK_ROOT}/' + path , nodes)
  env.Alias('extra_sdk_update', n)
  return n

nacl_extra_sdk_env.AddMethod(AddHeaderToSdk)

def AddLibraryToSdk(env, nodes, path=None):
  if path is None:
    path = 'nacl/lib'

  n = env.ReplicatePublished('${NACL_SDK_ROOT}/' + path, nodes, 'link')
  env.Alias('extra_sdk_update', n)
  return n

nacl_extra_sdk_env.AddMethod(AddLibraryToSdk)

def AddObjectToSdk(env, nodes, path=None):
  if path is None:
    path = 'nacl/lib'

  n = env.Replicate('${NACL_SDK_ROOT}/' + path, nodes)
  env.Alias('extra_sdk_update', n)
  return n

nacl_extra_sdk_env.AddMethod(AddObjectToSdk)

# NOTE: a helpful target to test the sdk_extra magic
#       combine this with a 'MODE=nacl -c'
# TODO: find a way to cleanup the libs on top of the headers
nacl_extra_sdk_env.Command('extra_sdk_clean', [],
                           ['rm -rf ${NACL_SDK_ROOT}/nacl/include/nacl*'])

# ----------------------------------------------------------
# CODE COVERAGE
# ----------------------------------------------------------
# ----------------------------------------------------------
# VARIOUS HELPERS
# ----------------------------------------------------------

doc_env = pre_base_env.Clone(
   BUILD_TYPE = 'doc',
   BUILD_TYPE_DESCRIPTION = 'Documentation build',
   HOST_PLATFORMS = '*',
)
doc_env.FilterOut(BUILD_GROUPS=['most'])
environment_list.append(doc_env)
doc_env.Append(
   BUILD_SCONSCRIPTS = [
       'documentation',
   ],
)


# ----------------------------------------------------------

# Blank out defaults.
Default(None)

BuildComponents(environment_list)

# Change default to build everything, but not run tests.
Default(['all_programs', 'all_bundles', 'all_test_programs', 'all_libraries'])

# ----------------------------------------------------------


# Generate a solution, defer to the end.
solution_env = base_env.Clone(tools = ['visual_studio_solution'])
solution = solution_env.Solution(
    'nacl', environment_list,
    exclude_pattern = '.*third_party.*',
    extra_build_targets = {
        'Firefox': 'c:/Program Files/Mozilla FireFox/firefox.exe',
    },
)
solution_env.Alias('solution', solution)

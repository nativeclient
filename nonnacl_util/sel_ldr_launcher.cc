/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "native_client/nonnacl_util/sel_ldr_launcher.h"

#include <stdio.h>
#include <sys/types.h>

#include "native_client/include/nacl_elf.h"

namespace nacl {

SelLdrLauncher::SelLdrLauncher() :
  child_(kInvalidHandle),
  channel_(kInvalidHandle),
  argc_(-1),
  argv_(NULL),
  is_sel_ldr_(true) {
}

void SelLdrLauncher::BuildArgv(const char* sel_ldr_pathname,
                               const char* application_name,
                               int imc_fd,
                               Handle imc_channel_handle,
                               int sel_ldr_argc,
                               const char* sel_ldr_argv[],
                               int application_argc,
                               const char* application_argv[]) {
#if NACL_WINDOWS
  _snprintf_s(channel_buf_, sizeof(channel_buf_), sizeof(channel_buf_),
              "%d:%u", imc_fd, imc_channel_handle);
  _snprintf_s(channel_number_, sizeof(channel_number_), sizeof(channel_number_),
              "%d", imc_fd);
#else
  snprintf(channel_buf_,
           sizeof(channel_buf_),
           "%d:%d",
           imc_fd,
           imc_channel_handle);
  snprintf(channel_number_, sizeof(channel_number_), "%d", imc_fd);
#endif
  // Fixed args are:
  // .../sel_ldr -f <application_name> -i <NaCl fd>:<imcchannel#>
  const char* kFixedArgs[] = {
    const_cast<char*>(sel_ldr_pathname),
    "-f",
    const_cast<char*>(application_name),
    "-i",
    channel_buf_
  };
  const int kFixedArgc = sizeof(kFixedArgs) / sizeof(kFixedArgs[0]);

  argv_ = new char const*[kFixedArgc + sel_ldr_argc
                          + (application_argc + 1) + 1];
  // We use pre-increment everywhere, so we need to initialize to -1.
  argc_ = -1;

  // Copy the fixed arguments to argv_.
  for (int i = 0; i < kFixedArgc; ++i) {
    argv_[++argc_] = kFixedArgs[i];
  }
  // Copy the specified additional arguments (e.g., "-d" or "-P") to sel_ldr
  for (int i = 0; i < sel_ldr_argc; ++i) {
    argv_[++argc_] = sel_ldr_argv[i];
  }
  // Copy the specified arguments to the application
  if (application_argc > 0) {
    argv_[++argc_] = "--";
    for (int i = 0; i < application_argc; ++i) {
      if (strncmp("$CHAN", application_argv[i], 6) == 0) {
        argv_[++argc_] = channel_number_;
      } else {
        argv_[++argc_] = application_argv[i];
      }
    }
  }
  // NULL terminate the argument list.
  argv_[++argc_] = NULL;
}

}  // namespace nacl

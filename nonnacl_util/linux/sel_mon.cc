/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <gtk/gtk.h>
#include <stdlib.h>

const char* kDefaultMessage =
    "A Native Client module has become unresponsive.\n"
    "Would you like to end the module process?";

const char* kDefaultTitle =
    "Unresponsive Native Client Module";

// Pops up a message box.
// usage: sel_mon [message] [caption]
int main(int argc, char* argv[]) {
    gtk_set_locale();
    gtk_init(&argc, &argv);

    GtkWidget* dialog = gtk_message_dialog_new(
        NULL,
        GtkDialogFlags(GTK_DIALOG_MODAL |
                       GTK_DIALOG_DESTROY_WITH_PARENT),
        GTK_MESSAGE_ERROR,
        GTK_BUTTONS_YES_NO,
        "%s",
        (2 <= argc) ? argv[1] : kDefaultMessage);
    gtk_window_set_title(GTK_WINDOW(dialog),
                         (3 <= argc) ? argv[2] : kDefaultTitle);
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
    return (result == GTK_RESPONSE_YES) ? EXIT_SUCCESS : EXIT_FAILURE;
}
